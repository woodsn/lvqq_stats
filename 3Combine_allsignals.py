import condor, time, os, commands,sys
#does change work

signals = ("HVTWZ", "HVTWW")
#signals = ("ggHWWNWA","HVTWW", "HVTWZ", "RSGWW", "VBFWWNWA", "VBF-HVTWW","VBF-HVTWZ")
#masses_higgs = ("700","1000")
#masses_nothiggs=("500","11000")
masses_higgs =("500", "600","700","800", "900", "1000", "1200", "1400")
masses_nothiggs =("500", "600","700","800", "900", "1000","1100", "1200", "1300","1400")


directory_boosted='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/ws/boosted/'
directory_resolved='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/ws/'

for s in signals:
	if s == "ggHWWNWA":
		selection = "RES"
		binning = "ggF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_LP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/June2/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBFWWNWA":
		selection = "RESVBF"
		binning = "VBF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_LP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/June2/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "HVTWW" or s == "HVTWZ":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
#			file1 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
#			file2 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_LP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/June2/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "RSGWW":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_LP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+m+'.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/June2/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)

	if s == "VBF-HVTWW":
		selection = "RESVBF"
		binning = "VBF"
		string = "VBF_HVTWW"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + string + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+string+m+'_ws_'+string+m+'.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'/' + string + '_LP/'+binning+'/VVlvqq_mc15_v2016_'+string+m+'_ws_'+string+m+'.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/June2/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBF-HVTWZ":
		selection = "RESVBF"
		binning = "VBF"
		string2 = "VBF_HVTWZ"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + string2 + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+string2+m+'_ws_'+string2+m+'.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'/' + string2 + '_LP/'+binning+'/VVlvqq_mc15_v2016_'+string2+m+'_ws_'+string2+m+'.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/June2/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)




