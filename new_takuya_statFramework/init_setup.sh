#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

# use recent release
rcSetup Base,2.4.14

# for plotting
#rc checkout_pkg atlasgroups/pubcom/root/atlasstyle/tags/atlasstyle-00-03-05 
svn co svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Columbia/rcarbone/ResonanceFinder/tags/ResonanceFinder-00-00-00 ResonanceFinder
svn co svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Columbia/rcarbone/StatTools/tags/StatTools-00-00-00/CombinationTool CombinationTool
svn co svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Columbia/rcarbone/StatTools/tags/StatTools-00-00-00/NPranking NPranking
svn co svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Columbia/rcarbone/StatTools/tags/StatTools-00-00-00/NuisanceCheck NuisanceCheck
svn co svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Columbia/rcarbone/StatTools/tags/StatTools-00-00-00/NuisanceCheckCombination NuisanceCheckCombination
svn co svn+ssh://svn.cern.ch/reps/atlasinst/Institutes/Columbia/rcarbone/StatTools/tags/StatTools-00-00-00/RunLimits RunLimits
# Rootcore version located here, requires more manual tweaking to work though
# check for recent tags https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DbxStat
#rc checkout_pkg atlasphys-exa/Physics/Exotic/Analysis/DibosonResonance/Data2015/Statistics/ResonanceFinder/tags/ResonanceFinder-00-00-18

rc find_packages
rc compile

# Directory to run scripts from
mkdir -p Run
mv getLimit.py Run/getLimit.py
mv runAsymptoticsCLs.C Run/runAsymptoticsCLs.C
mv run_mkdir.py Run/run_mkdir.py
mv run_ws_example.sh Run/run_ws_example.sh
mv run_limit_example.sh Run/run_limit_example.sh
