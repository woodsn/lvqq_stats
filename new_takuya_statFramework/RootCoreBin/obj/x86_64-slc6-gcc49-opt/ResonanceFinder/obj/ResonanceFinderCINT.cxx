#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIexportdIsharedIgaussdIwoodsndIExtraDimensionsdInew_takuya_statFrameworkdIRootCoreBindIobjdIx86_64mIslc6mIgcc49mIoptdIResonanceFinderdIobjdIResonanceFinderCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "ResonanceFinder/BackgroundTemplateAdder.h"
#include "ResonanceFinder/IHistoManipulator.h"
#include "ResonanceFinder/HistoCollection.h"
#include "ResonanceFinder/BinnedWorkspaceBuilder.h"
#include "ResonanceFinder/IWorkspaceBuilder.h"
#include "ResonanceFinder/Channel.h"
#include "ResonanceFinder/Sample.h"
#include "ResonanceFinder/Variation.h"
#include "ResonanceFinder/MultiplicativeFactor.h"
#include "ResonanceFinder/DownVariationAdder.h"
#include "ResonanceFinder/HistoLumiRescaler.h"
#include "ResonanceFinder/IAnalysisRunner.h"
#include "ResonanceFinder/WorkspaceCollector.h"
#include "ResonanceFinder/IWorkspaceCollector.h"
#include "ResonanceFinder/WorkspaceCollection.h"
#include "ResonanceFinder/LimitRunner.h"
#include "ResonanceFinder/LimitTool.h"
#include "ResonanceFinder/ILimitTool.h"
#include "ResonanceFinder/StatisticalResults.h"
#include "ResonanceFinder/HypoTestInvTool.h"
#include "ResonanceFinder/FitTool.h"
#include "ResonanceFinder/StatisticalResultsCollection.h"
#include "ResonanceFinder/SignalInjector.h"
#include "ResonanceFinder/IWorkspaceManipulator.h"
#include "ResonanceFinder/Plotter.h"
#include "ResonanceFinder/IBinnedAnalysisRunner.h"
#include "ResonanceFinder/IHistoCollector.h"
#include "ResonanceFinder/IBinnedTreeAnalysisRunner.h"
#include "ResonanceFinder/ITreeRunner.h"
#include "ResonanceFinder/TreeHistoCollector.h"
#include "ResonanceFinder/ManualHistoCollector.h"
#include "ResonanceFinder/SystematicsSmoother.h"
#include "ResonanceFinder/VVJJAnalysisRunner.h"
#include "ResonanceFinder/VVJJValidationRunner.h"
#include "ResonanceFinder/VVlvqqAnalysisRunner.h"
#include "ResonanceFinder/VVlvqqValidationRunner.h"
#include "ResonanceFinder/VVllqqAnalysisRunner.h"
#include "ResonanceFinder/VVllqqValidationRunner.h"
#include "ResonanceFinder/VHAnalysisRunner.h"
#include "ResonanceFinder/PullPlotter.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *RFcLcLBackgroundTemplateAdder_Dictionary();
   static void RFcLcLBackgroundTemplateAdder_TClassManip(TClass*);
   static void *new_RFcLcLBackgroundTemplateAdder(void *p = 0);
   static void *newArray_RFcLcLBackgroundTemplateAdder(Long_t size, void *p);
   static void delete_RFcLcLBackgroundTemplateAdder(void *p);
   static void deleteArray_RFcLcLBackgroundTemplateAdder(void *p);
   static void destruct_RFcLcLBackgroundTemplateAdder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::BackgroundTemplateAdder*)
   {
      ::RF::BackgroundTemplateAdder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::BackgroundTemplateAdder));
      static ::ROOT::TGenericClassInfo 
         instance("RF::BackgroundTemplateAdder", "ResonanceFinder/BackgroundTemplateAdder.h", 24,
                  typeid(::RF::BackgroundTemplateAdder), DefineBehavior(ptr, ptr),
                  &RFcLcLBackgroundTemplateAdder_Dictionary, isa_proxy, 0,
                  sizeof(::RF::BackgroundTemplateAdder) );
      instance.SetNew(&new_RFcLcLBackgroundTemplateAdder);
      instance.SetNewArray(&newArray_RFcLcLBackgroundTemplateAdder);
      instance.SetDelete(&delete_RFcLcLBackgroundTemplateAdder);
      instance.SetDeleteArray(&deleteArray_RFcLcLBackgroundTemplateAdder);
      instance.SetDestructor(&destruct_RFcLcLBackgroundTemplateAdder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::BackgroundTemplateAdder*)
   {
      return GenerateInitInstanceLocal((::RF::BackgroundTemplateAdder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::BackgroundTemplateAdder*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLBackgroundTemplateAdder_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::BackgroundTemplateAdder*)0x0)->GetClass();
      RFcLcLBackgroundTemplateAdder_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLBackgroundTemplateAdder_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLBinnedWorkspaceBuilder_Dictionary();
   static void RFcLcLBinnedWorkspaceBuilder_TClassManip(TClass*);
   static void delete_RFcLcLBinnedWorkspaceBuilder(void *p);
   static void deleteArray_RFcLcLBinnedWorkspaceBuilder(void *p);
   static void destruct_RFcLcLBinnedWorkspaceBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::BinnedWorkspaceBuilder*)
   {
      ::RF::BinnedWorkspaceBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::BinnedWorkspaceBuilder));
      static ::ROOT::TGenericClassInfo 
         instance("RF::BinnedWorkspaceBuilder", "ResonanceFinder/BinnedWorkspaceBuilder.h", 17,
                  typeid(::RF::BinnedWorkspaceBuilder), DefineBehavior(ptr, ptr),
                  &RFcLcLBinnedWorkspaceBuilder_Dictionary, isa_proxy, 0,
                  sizeof(::RF::BinnedWorkspaceBuilder) );
      instance.SetDelete(&delete_RFcLcLBinnedWorkspaceBuilder);
      instance.SetDeleteArray(&deleteArray_RFcLcLBinnedWorkspaceBuilder);
      instance.SetDestructor(&destruct_RFcLcLBinnedWorkspaceBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::BinnedWorkspaceBuilder*)
   {
      return GenerateInitInstanceLocal((::RF::BinnedWorkspaceBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::BinnedWorkspaceBuilder*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLBinnedWorkspaceBuilder_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::BinnedWorkspaceBuilder*)0x0)->GetClass();
      RFcLcLBinnedWorkspaceBuilder_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLBinnedWorkspaceBuilder_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLChannel_Dictionary();
   static void RFcLcLChannel_TClassManip(TClass*);
   static void *new_RFcLcLChannel(void *p = 0);
   static void *newArray_RFcLcLChannel(Long_t size, void *p);
   static void delete_RFcLcLChannel(void *p);
   static void deleteArray_RFcLcLChannel(void *p);
   static void destruct_RFcLcLChannel(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::Channel*)
   {
      ::RF::Channel *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::Channel));
      static ::ROOT::TGenericClassInfo 
         instance("RF::Channel", "ResonanceFinder/Channel.h", 19,
                  typeid(::RF::Channel), DefineBehavior(ptr, ptr),
                  &RFcLcLChannel_Dictionary, isa_proxy, 0,
                  sizeof(::RF::Channel) );
      instance.SetNew(&new_RFcLcLChannel);
      instance.SetNewArray(&newArray_RFcLcLChannel);
      instance.SetDelete(&delete_RFcLcLChannel);
      instance.SetDeleteArray(&deleteArray_RFcLcLChannel);
      instance.SetDestructor(&destruct_RFcLcLChannel);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::Channel*)
   {
      return GenerateInitInstanceLocal((::RF::Channel*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::Channel*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLChannel_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::Channel*)0x0)->GetClass();
      RFcLcLChannel_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLChannel_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLDownVariationAdder_Dictionary();
   static void RFcLcLDownVariationAdder_TClassManip(TClass*);
   static void *new_RFcLcLDownVariationAdder(void *p = 0);
   static void *newArray_RFcLcLDownVariationAdder(Long_t size, void *p);
   static void delete_RFcLcLDownVariationAdder(void *p);
   static void deleteArray_RFcLcLDownVariationAdder(void *p);
   static void destruct_RFcLcLDownVariationAdder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::DownVariationAdder*)
   {
      ::RF::DownVariationAdder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::DownVariationAdder));
      static ::ROOT::TGenericClassInfo 
         instance("RF::DownVariationAdder", "ResonanceFinder/DownVariationAdder.h", 18,
                  typeid(::RF::DownVariationAdder), DefineBehavior(ptr, ptr),
                  &RFcLcLDownVariationAdder_Dictionary, isa_proxy, 0,
                  sizeof(::RF::DownVariationAdder) );
      instance.SetNew(&new_RFcLcLDownVariationAdder);
      instance.SetNewArray(&newArray_RFcLcLDownVariationAdder);
      instance.SetDelete(&delete_RFcLcLDownVariationAdder);
      instance.SetDeleteArray(&deleteArray_RFcLcLDownVariationAdder);
      instance.SetDestructor(&destruct_RFcLcLDownVariationAdder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::DownVariationAdder*)
   {
      return GenerateInitInstanceLocal((::RF::DownVariationAdder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::DownVariationAdder*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLDownVariationAdder_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::DownVariationAdder*)0x0)->GetClass();
      RFcLcLDownVariationAdder_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLDownVariationAdder_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLHistoCollection_Dictionary();
   static void RFcLcLHistoCollection_TClassManip(TClass*);
   static void *new_RFcLcLHistoCollection(void *p = 0);
   static void *newArray_RFcLcLHistoCollection(Long_t size, void *p);
   static void delete_RFcLcLHistoCollection(void *p);
   static void deleteArray_RFcLcLHistoCollection(void *p);
   static void destruct_RFcLcLHistoCollection(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::HistoCollection*)
   {
      ::RF::HistoCollection *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::HistoCollection));
      static ::ROOT::TGenericClassInfo 
         instance("RF::HistoCollection", "ResonanceFinder/HistoCollection.h", 26,
                  typeid(::RF::HistoCollection), DefineBehavior(ptr, ptr),
                  &RFcLcLHistoCollection_Dictionary, isa_proxy, 0,
                  sizeof(::RF::HistoCollection) );
      instance.SetNew(&new_RFcLcLHistoCollection);
      instance.SetNewArray(&newArray_RFcLcLHistoCollection);
      instance.SetDelete(&delete_RFcLcLHistoCollection);
      instance.SetDeleteArray(&deleteArray_RFcLcLHistoCollection);
      instance.SetDestructor(&destruct_RFcLcLHistoCollection);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::HistoCollection*)
   {
      return GenerateInitInstanceLocal((::RF::HistoCollection*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::HistoCollection*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLHistoCollection_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::HistoCollection*)0x0)->GetClass();
      RFcLcLHistoCollection_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLHistoCollection_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLHistoLumiRescaler_Dictionary();
   static void RFcLcLHistoLumiRescaler_TClassManip(TClass*);
   static void *new_RFcLcLHistoLumiRescaler(void *p = 0);
   static void *newArray_RFcLcLHistoLumiRescaler(Long_t size, void *p);
   static void delete_RFcLcLHistoLumiRescaler(void *p);
   static void deleteArray_RFcLcLHistoLumiRescaler(void *p);
   static void destruct_RFcLcLHistoLumiRescaler(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::HistoLumiRescaler*)
   {
      ::RF::HistoLumiRescaler *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::HistoLumiRescaler));
      static ::ROOT::TGenericClassInfo 
         instance("RF::HistoLumiRescaler", "ResonanceFinder/HistoLumiRescaler.h", 16,
                  typeid(::RF::HistoLumiRescaler), DefineBehavior(ptr, ptr),
                  &RFcLcLHistoLumiRescaler_Dictionary, isa_proxy, 0,
                  sizeof(::RF::HistoLumiRescaler) );
      instance.SetNew(&new_RFcLcLHistoLumiRescaler);
      instance.SetNewArray(&newArray_RFcLcLHistoLumiRescaler);
      instance.SetDelete(&delete_RFcLcLHistoLumiRescaler);
      instance.SetDeleteArray(&deleteArray_RFcLcLHistoLumiRescaler);
      instance.SetDestructor(&destruct_RFcLcLHistoLumiRescaler);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::HistoLumiRescaler*)
   {
      return GenerateInitInstanceLocal((::RF::HistoLumiRescaler*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::HistoLumiRescaler*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLHistoLumiRescaler_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::HistoLumiRescaler*)0x0)->GetClass();
      RFcLcLHistoLumiRescaler_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLHistoLumiRescaler_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLIAnalysisRunner_Dictionary();
   static void RFcLcLIAnalysisRunner_TClassManip(TClass*);
   static void delete_RFcLcLIAnalysisRunner(void *p);
   static void deleteArray_RFcLcLIAnalysisRunner(void *p);
   static void destruct_RFcLcLIAnalysisRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::IAnalysisRunner*)
   {
      ::RF::IAnalysisRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::IAnalysisRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::IAnalysisRunner", "ResonanceFinder/IAnalysisRunner.h", 26,
                  typeid(::RF::IAnalysisRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLIAnalysisRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::IAnalysisRunner) );
      instance.SetDelete(&delete_RFcLcLIAnalysisRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLIAnalysisRunner);
      instance.SetDestructor(&destruct_RFcLcLIAnalysisRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::IAnalysisRunner*)
   {
      return GenerateInitInstanceLocal((::RF::IAnalysisRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::IAnalysisRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLIAnalysisRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::IAnalysisRunner*)0x0)->GetClass();
      RFcLcLIAnalysisRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLIAnalysisRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLIBinnedAnalysisRunner_Dictionary();
   static void RFcLcLIBinnedAnalysisRunner_TClassManip(TClass*);
   static void delete_RFcLcLIBinnedAnalysisRunner(void *p);
   static void deleteArray_RFcLcLIBinnedAnalysisRunner(void *p);
   static void destruct_RFcLcLIBinnedAnalysisRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::IBinnedAnalysisRunner*)
   {
      ::RF::IBinnedAnalysisRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::IBinnedAnalysisRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::IBinnedAnalysisRunner", "ResonanceFinder/IBinnedAnalysisRunner.h", 19,
                  typeid(::RF::IBinnedAnalysisRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLIBinnedAnalysisRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::IBinnedAnalysisRunner) );
      instance.SetDelete(&delete_RFcLcLIBinnedAnalysisRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLIBinnedAnalysisRunner);
      instance.SetDestructor(&destruct_RFcLcLIBinnedAnalysisRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::IBinnedAnalysisRunner*)
   {
      return GenerateInitInstanceLocal((::RF::IBinnedAnalysisRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::IBinnedAnalysisRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLIBinnedAnalysisRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::IBinnedAnalysisRunner*)0x0)->GetClass();
      RFcLcLIBinnedAnalysisRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLIBinnedAnalysisRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLIBinnedTreeAnalysisRunner_Dictionary();
   static void RFcLcLIBinnedTreeAnalysisRunner_TClassManip(TClass*);
   static void delete_RFcLcLIBinnedTreeAnalysisRunner(void *p);
   static void deleteArray_RFcLcLIBinnedTreeAnalysisRunner(void *p);
   static void destruct_RFcLcLIBinnedTreeAnalysisRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::IBinnedTreeAnalysisRunner*)
   {
      ::RF::IBinnedTreeAnalysisRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::IBinnedTreeAnalysisRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::IBinnedTreeAnalysisRunner", "ResonanceFinder/IBinnedTreeAnalysisRunner.h", 20,
                  typeid(::RF::IBinnedTreeAnalysisRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLIBinnedTreeAnalysisRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::IBinnedTreeAnalysisRunner) );
      instance.SetDelete(&delete_RFcLcLIBinnedTreeAnalysisRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLIBinnedTreeAnalysisRunner);
      instance.SetDestructor(&destruct_RFcLcLIBinnedTreeAnalysisRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::IBinnedTreeAnalysisRunner*)
   {
      return GenerateInitInstanceLocal((::RF::IBinnedTreeAnalysisRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::IBinnedTreeAnalysisRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLIBinnedTreeAnalysisRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::IBinnedTreeAnalysisRunner*)0x0)->GetClass();
      RFcLcLIBinnedTreeAnalysisRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLIBinnedTreeAnalysisRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLIHistoCollector_Dictionary();
   static void RFcLcLIHistoCollector_TClassManip(TClass*);
   static void delete_RFcLcLIHistoCollector(void *p);
   static void deleteArray_RFcLcLIHistoCollector(void *p);
   static void destruct_RFcLcLIHistoCollector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::IHistoCollector*)
   {
      ::RF::IHistoCollector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::IHistoCollector));
      static ::ROOT::TGenericClassInfo 
         instance("RF::IHistoCollector", "ResonanceFinder/IHistoCollector.h", 33,
                  typeid(::RF::IHistoCollector), DefineBehavior(ptr, ptr),
                  &RFcLcLIHistoCollector_Dictionary, isa_proxy, 0,
                  sizeof(::RF::IHistoCollector) );
      instance.SetDelete(&delete_RFcLcLIHistoCollector);
      instance.SetDeleteArray(&deleteArray_RFcLcLIHistoCollector);
      instance.SetDestructor(&destruct_RFcLcLIHistoCollector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::IHistoCollector*)
   {
      return GenerateInitInstanceLocal((::RF::IHistoCollector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::IHistoCollector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLIHistoCollector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::IHistoCollector*)0x0)->GetClass();
      RFcLcLIHistoCollector_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLIHistoCollector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLIHistoManipulator_Dictionary();
   static void RFcLcLIHistoManipulator_TClassManip(TClass*);
   static void delete_RFcLcLIHistoManipulator(void *p);
   static void deleteArray_RFcLcLIHistoManipulator(void *p);
   static void destruct_RFcLcLIHistoManipulator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::IHistoManipulator*)
   {
      ::RF::IHistoManipulator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::IHistoManipulator));
      static ::ROOT::TGenericClassInfo 
         instance("RF::IHistoManipulator", "ResonanceFinder/IHistoManipulator.h", 24,
                  typeid(::RF::IHistoManipulator), DefineBehavior(ptr, ptr),
                  &RFcLcLIHistoManipulator_Dictionary, isa_proxy, 0,
                  sizeof(::RF::IHistoManipulator) );
      instance.SetDelete(&delete_RFcLcLIHistoManipulator);
      instance.SetDeleteArray(&deleteArray_RFcLcLIHistoManipulator);
      instance.SetDestructor(&destruct_RFcLcLIHistoManipulator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::IHistoManipulator*)
   {
      return GenerateInitInstanceLocal((::RF::IHistoManipulator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::IHistoManipulator*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLIHistoManipulator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::IHistoManipulator*)0x0)->GetClass();
      RFcLcLIHistoManipulator_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLIHistoManipulator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLITreeRunner_Dictionary();
   static void RFcLcLITreeRunner_TClassManip(TClass*);
   static void delete_RFcLcLITreeRunner(void *p);
   static void deleteArray_RFcLcLITreeRunner(void *p);
   static void destruct_RFcLcLITreeRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::ITreeRunner*)
   {
      ::RF::ITreeRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::ITreeRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::ITreeRunner", "ResonanceFinder/ITreeRunner.h", 13,
                  typeid(::RF::ITreeRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLITreeRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::ITreeRunner) );
      instance.SetDelete(&delete_RFcLcLITreeRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLITreeRunner);
      instance.SetDestructor(&destruct_RFcLcLITreeRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::ITreeRunner*)
   {
      return GenerateInitInstanceLocal((::RF::ITreeRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::ITreeRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLITreeRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::ITreeRunner*)0x0)->GetClass();
      RFcLcLITreeRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLITreeRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLIWorkspaceBuilder_Dictionary();
   static void RFcLcLIWorkspaceBuilder_TClassManip(TClass*);
   static void delete_RFcLcLIWorkspaceBuilder(void *p);
   static void deleteArray_RFcLcLIWorkspaceBuilder(void *p);
   static void destruct_RFcLcLIWorkspaceBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::IWorkspaceBuilder*)
   {
      ::RF::IWorkspaceBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::IWorkspaceBuilder));
      static ::ROOT::TGenericClassInfo 
         instance("RF::IWorkspaceBuilder", "ResonanceFinder/IWorkspaceBuilder.h", 29,
                  typeid(::RF::IWorkspaceBuilder), DefineBehavior(ptr, ptr),
                  &RFcLcLIWorkspaceBuilder_Dictionary, isa_proxy, 0,
                  sizeof(::RF::IWorkspaceBuilder) );
      instance.SetDelete(&delete_RFcLcLIWorkspaceBuilder);
      instance.SetDeleteArray(&deleteArray_RFcLcLIWorkspaceBuilder);
      instance.SetDestructor(&destruct_RFcLcLIWorkspaceBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::IWorkspaceBuilder*)
   {
      return GenerateInitInstanceLocal((::RF::IWorkspaceBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::IWorkspaceBuilder*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLIWorkspaceBuilder_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::IWorkspaceBuilder*)0x0)->GetClass();
      RFcLcLIWorkspaceBuilder_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLIWorkspaceBuilder_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLManualHistoCollector_Dictionary();
   static void RFcLcLManualHistoCollector_TClassManip(TClass*);
   static void *new_RFcLcLManualHistoCollector(void *p = 0);
   static void *newArray_RFcLcLManualHistoCollector(Long_t size, void *p);
   static void delete_RFcLcLManualHistoCollector(void *p);
   static void deleteArray_RFcLcLManualHistoCollector(void *p);
   static void destruct_RFcLcLManualHistoCollector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::ManualHistoCollector*)
   {
      ::RF::ManualHistoCollector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::ManualHistoCollector));
      static ::ROOT::TGenericClassInfo 
         instance("RF::ManualHistoCollector", "ResonanceFinder/ManualHistoCollector.h", 21,
                  typeid(::RF::ManualHistoCollector), DefineBehavior(ptr, ptr),
                  &RFcLcLManualHistoCollector_Dictionary, isa_proxy, 0,
                  sizeof(::RF::ManualHistoCollector) );
      instance.SetNew(&new_RFcLcLManualHistoCollector);
      instance.SetNewArray(&newArray_RFcLcLManualHistoCollector);
      instance.SetDelete(&delete_RFcLcLManualHistoCollector);
      instance.SetDeleteArray(&deleteArray_RFcLcLManualHistoCollector);
      instance.SetDestructor(&destruct_RFcLcLManualHistoCollector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::ManualHistoCollector*)
   {
      return GenerateInitInstanceLocal((::RF::ManualHistoCollector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::ManualHistoCollector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLManualHistoCollector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::ManualHistoCollector*)0x0)->GetClass();
      RFcLcLManualHistoCollector_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLManualHistoCollector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLMultiplicativeFactor_Dictionary();
   static void RFcLcLMultiplicativeFactor_TClassManip(TClass*);
   static void *new_RFcLcLMultiplicativeFactor(void *p = 0);
   static void *newArray_RFcLcLMultiplicativeFactor(Long_t size, void *p);
   static void delete_RFcLcLMultiplicativeFactor(void *p);
   static void deleteArray_RFcLcLMultiplicativeFactor(void *p);
   static void destruct_RFcLcLMultiplicativeFactor(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::MultiplicativeFactor*)
   {
      ::RF::MultiplicativeFactor *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::MultiplicativeFactor));
      static ::ROOT::TGenericClassInfo 
         instance("RF::MultiplicativeFactor", "ResonanceFinder/MultiplicativeFactor.h", 18,
                  typeid(::RF::MultiplicativeFactor), DefineBehavior(ptr, ptr),
                  &RFcLcLMultiplicativeFactor_Dictionary, isa_proxy, 0,
                  sizeof(::RF::MultiplicativeFactor) );
      instance.SetNew(&new_RFcLcLMultiplicativeFactor);
      instance.SetNewArray(&newArray_RFcLcLMultiplicativeFactor);
      instance.SetDelete(&delete_RFcLcLMultiplicativeFactor);
      instance.SetDeleteArray(&deleteArray_RFcLcLMultiplicativeFactor);
      instance.SetDestructor(&destruct_RFcLcLMultiplicativeFactor);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::MultiplicativeFactor*)
   {
      return GenerateInitInstanceLocal((::RF::MultiplicativeFactor*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::MultiplicativeFactor*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLMultiplicativeFactor_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::MultiplicativeFactor*)0x0)->GetClass();
      RFcLcLMultiplicativeFactor_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLMultiplicativeFactor_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLSample_Dictionary();
   static void RFcLcLSample_TClassManip(TClass*);
   static void *new_RFcLcLSample(void *p = 0);
   static void *newArray_RFcLcLSample(Long_t size, void *p);
   static void delete_RFcLcLSample(void *p);
   static void deleteArray_RFcLcLSample(void *p);
   static void destruct_RFcLcLSample(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::Sample*)
   {
      ::RF::Sample *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::Sample));
      static ::ROOT::TGenericClassInfo 
         instance("RF::Sample", "ResonanceFinder/Sample.h", 29,
                  typeid(::RF::Sample), DefineBehavior(ptr, ptr),
                  &RFcLcLSample_Dictionary, isa_proxy, 0,
                  sizeof(::RF::Sample) );
      instance.SetNew(&new_RFcLcLSample);
      instance.SetNewArray(&newArray_RFcLcLSample);
      instance.SetDelete(&delete_RFcLcLSample);
      instance.SetDeleteArray(&deleteArray_RFcLcLSample);
      instance.SetDestructor(&destruct_RFcLcLSample);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::Sample*)
   {
      return GenerateInitInstanceLocal((::RF::Sample*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::Sample*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLSample_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::Sample*)0x0)->GetClass();
      RFcLcLSample_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLSample_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLSystematicsSmoother_Dictionary();
   static void RFcLcLSystematicsSmoother_TClassManip(TClass*);
   static void *new_RFcLcLSystematicsSmoother(void *p = 0);
   static void *newArray_RFcLcLSystematicsSmoother(Long_t size, void *p);
   static void delete_RFcLcLSystematicsSmoother(void *p);
   static void deleteArray_RFcLcLSystematicsSmoother(void *p);
   static void destruct_RFcLcLSystematicsSmoother(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::SystematicsSmoother*)
   {
      ::RF::SystematicsSmoother *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::SystematicsSmoother));
      static ::ROOT::TGenericClassInfo 
         instance("RF::SystematicsSmoother", "ResonanceFinder/SystematicsSmoother.h", 16,
                  typeid(::RF::SystematicsSmoother), DefineBehavior(ptr, ptr),
                  &RFcLcLSystematicsSmoother_Dictionary, isa_proxy, 0,
                  sizeof(::RF::SystematicsSmoother) );
      instance.SetNew(&new_RFcLcLSystematicsSmoother);
      instance.SetNewArray(&newArray_RFcLcLSystematicsSmoother);
      instance.SetDelete(&delete_RFcLcLSystematicsSmoother);
      instance.SetDeleteArray(&deleteArray_RFcLcLSystematicsSmoother);
      instance.SetDestructor(&destruct_RFcLcLSystematicsSmoother);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::SystematicsSmoother*)
   {
      return GenerateInitInstanceLocal((::RF::SystematicsSmoother*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::SystematicsSmoother*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLSystematicsSmoother_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::SystematicsSmoother*)0x0)->GetClass();
      RFcLcLSystematicsSmoother_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLSystematicsSmoother_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLTreeHistoCollector_Dictionary();
   static void RFcLcLTreeHistoCollector_TClassManip(TClass*);
   static void *new_RFcLcLTreeHistoCollector(void *p = 0);
   static void *newArray_RFcLcLTreeHistoCollector(Long_t size, void *p);
   static void delete_RFcLcLTreeHistoCollector(void *p);
   static void deleteArray_RFcLcLTreeHistoCollector(void *p);
   static void destruct_RFcLcLTreeHistoCollector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::TreeHistoCollector*)
   {
      ::RF::TreeHistoCollector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::TreeHistoCollector));
      static ::ROOT::TGenericClassInfo 
         instance("RF::TreeHistoCollector", "ResonanceFinder/TreeHistoCollector.h", 30,
                  typeid(::RF::TreeHistoCollector), DefineBehavior(ptr, ptr),
                  &RFcLcLTreeHistoCollector_Dictionary, isa_proxy, 0,
                  sizeof(::RF::TreeHistoCollector) );
      instance.SetNew(&new_RFcLcLTreeHistoCollector);
      instance.SetNewArray(&newArray_RFcLcLTreeHistoCollector);
      instance.SetDelete(&delete_RFcLcLTreeHistoCollector);
      instance.SetDeleteArray(&deleteArray_RFcLcLTreeHistoCollector);
      instance.SetDestructor(&destruct_RFcLcLTreeHistoCollector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::TreeHistoCollector*)
   {
      return GenerateInitInstanceLocal((::RF::TreeHistoCollector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::TreeHistoCollector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLTreeHistoCollector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::TreeHistoCollector*)0x0)->GetClass();
      RFcLcLTreeHistoCollector_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLTreeHistoCollector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLVVJJAnalysisRunner_Dictionary();
   static void RFcLcLVVJJAnalysisRunner_TClassManip(TClass*);
   static void delete_RFcLcLVVJJAnalysisRunner(void *p);
   static void deleteArray_RFcLcLVVJJAnalysisRunner(void *p);
   static void destruct_RFcLcLVVJJAnalysisRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::VVJJAnalysisRunner*)
   {
      ::RF::VVJJAnalysisRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::VVJJAnalysisRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::VVJJAnalysisRunner", "ResonanceFinder/VVJJAnalysisRunner.h", 18,
                  typeid(::RF::VVJJAnalysisRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLVVJJAnalysisRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::VVJJAnalysisRunner) );
      instance.SetDelete(&delete_RFcLcLVVJJAnalysisRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLVVJJAnalysisRunner);
      instance.SetDestructor(&destruct_RFcLcLVVJJAnalysisRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::VVJJAnalysisRunner*)
   {
      return GenerateInitInstanceLocal((::RF::VVJJAnalysisRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::VVJJAnalysisRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLVVJJAnalysisRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::VVJJAnalysisRunner*)0x0)->GetClass();
      RFcLcLVVJJAnalysisRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLVVJJAnalysisRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLVVJJValidationRunner_Dictionary();
   static void RFcLcLVVJJValidationRunner_TClassManip(TClass*);
   static void delete_RFcLcLVVJJValidationRunner(void *p);
   static void deleteArray_RFcLcLVVJJValidationRunner(void *p);
   static void destruct_RFcLcLVVJJValidationRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::VVJJValidationRunner*)
   {
      ::RF::VVJJValidationRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::VVJJValidationRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::VVJJValidationRunner", "ResonanceFinder/VVJJValidationRunner.h", 16,
                  typeid(::RF::VVJJValidationRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLVVJJValidationRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::VVJJValidationRunner) );
      instance.SetDelete(&delete_RFcLcLVVJJValidationRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLVVJJValidationRunner);
      instance.SetDestructor(&destruct_RFcLcLVVJJValidationRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::VVJJValidationRunner*)
   {
      return GenerateInitInstanceLocal((::RF::VVJJValidationRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::VVJJValidationRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLVVJJValidationRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::VVJJValidationRunner*)0x0)->GetClass();
      RFcLcLVVJJValidationRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLVVJJValidationRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLVVlvqqAnalysisRunner_Dictionary();
   static void RFcLcLVVlvqqAnalysisRunner_TClassManip(TClass*);
   static void delete_RFcLcLVVlvqqAnalysisRunner(void *p);
   static void deleteArray_RFcLcLVVlvqqAnalysisRunner(void *p);
   static void destruct_RFcLcLVVlvqqAnalysisRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::VVlvqqAnalysisRunner*)
   {
      ::RF::VVlvqqAnalysisRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::VVlvqqAnalysisRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::VVlvqqAnalysisRunner", "ResonanceFinder/VVlvqqAnalysisRunner.h", 18,
                  typeid(::RF::VVlvqqAnalysisRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLVVlvqqAnalysisRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::VVlvqqAnalysisRunner) );
      instance.SetDelete(&delete_RFcLcLVVlvqqAnalysisRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLVVlvqqAnalysisRunner);
      instance.SetDestructor(&destruct_RFcLcLVVlvqqAnalysisRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::VVlvqqAnalysisRunner*)
   {
      return GenerateInitInstanceLocal((::RF::VVlvqqAnalysisRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::VVlvqqAnalysisRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLVVlvqqAnalysisRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::VVlvqqAnalysisRunner*)0x0)->GetClass();
      RFcLcLVVlvqqAnalysisRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLVVlvqqAnalysisRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLVVlvqqValidationRunner_Dictionary();
   static void RFcLcLVVlvqqValidationRunner_TClassManip(TClass*);
   static void delete_RFcLcLVVlvqqValidationRunner(void *p);
   static void deleteArray_RFcLcLVVlvqqValidationRunner(void *p);
   static void destruct_RFcLcLVVlvqqValidationRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::VVlvqqValidationRunner*)
   {
      ::RF::VVlvqqValidationRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::VVlvqqValidationRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::VVlvqqValidationRunner", "ResonanceFinder/VVlvqqValidationRunner.h", 16,
                  typeid(::RF::VVlvqqValidationRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLVVlvqqValidationRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::VVlvqqValidationRunner) );
      instance.SetDelete(&delete_RFcLcLVVlvqqValidationRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLVVlvqqValidationRunner);
      instance.SetDestructor(&destruct_RFcLcLVVlvqqValidationRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::VVlvqqValidationRunner*)
   {
      return GenerateInitInstanceLocal((::RF::VVlvqqValidationRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::VVlvqqValidationRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLVVlvqqValidationRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::VVlvqqValidationRunner*)0x0)->GetClass();
      RFcLcLVVlvqqValidationRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLVVlvqqValidationRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLVVllqqAnalysisRunner_Dictionary();
   static void RFcLcLVVllqqAnalysisRunner_TClassManip(TClass*);
   static void delete_RFcLcLVVllqqAnalysisRunner(void *p);
   static void deleteArray_RFcLcLVVllqqAnalysisRunner(void *p);
   static void destruct_RFcLcLVVllqqAnalysisRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::VVllqqAnalysisRunner*)
   {
      ::RF::VVllqqAnalysisRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::VVllqqAnalysisRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::VVllqqAnalysisRunner", "ResonanceFinder/VVllqqAnalysisRunner.h", 18,
                  typeid(::RF::VVllqqAnalysisRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLVVllqqAnalysisRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::VVllqqAnalysisRunner) );
      instance.SetDelete(&delete_RFcLcLVVllqqAnalysisRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLVVllqqAnalysisRunner);
      instance.SetDestructor(&destruct_RFcLcLVVllqqAnalysisRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::VVllqqAnalysisRunner*)
   {
      return GenerateInitInstanceLocal((::RF::VVllqqAnalysisRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::VVllqqAnalysisRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLVVllqqAnalysisRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::VVllqqAnalysisRunner*)0x0)->GetClass();
      RFcLcLVVllqqAnalysisRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLVVllqqAnalysisRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLVVllqqValidationRunner_Dictionary();
   static void RFcLcLVVllqqValidationRunner_TClassManip(TClass*);
   static void delete_RFcLcLVVllqqValidationRunner(void *p);
   static void deleteArray_RFcLcLVVllqqValidationRunner(void *p);
   static void destruct_RFcLcLVVllqqValidationRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::VVllqqValidationRunner*)
   {
      ::RF::VVllqqValidationRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::VVllqqValidationRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::VVllqqValidationRunner", "ResonanceFinder/VVllqqValidationRunner.h", 16,
                  typeid(::RF::VVllqqValidationRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLVVllqqValidationRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::VVllqqValidationRunner) );
      instance.SetDelete(&delete_RFcLcLVVllqqValidationRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLVVllqqValidationRunner);
      instance.SetDestructor(&destruct_RFcLcLVVllqqValidationRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::VVllqqValidationRunner*)
   {
      return GenerateInitInstanceLocal((::RF::VVllqqValidationRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::VVllqqValidationRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLVVllqqValidationRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::VVllqqValidationRunner*)0x0)->GetClass();
      RFcLcLVVllqqValidationRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLVVllqqValidationRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLVHAnalysisRunner_Dictionary();
   static void RFcLcLVHAnalysisRunner_TClassManip(TClass*);
   static void delete_RFcLcLVHAnalysisRunner(void *p);
   static void deleteArray_RFcLcLVHAnalysisRunner(void *p);
   static void destruct_RFcLcLVHAnalysisRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::VHAnalysisRunner*)
   {
      ::RF::VHAnalysisRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::VHAnalysisRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::VHAnalysisRunner", "ResonanceFinder/VHAnalysisRunner.h", 15,
                  typeid(::RF::VHAnalysisRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLVHAnalysisRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::VHAnalysisRunner) );
      instance.SetDelete(&delete_RFcLcLVHAnalysisRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLVHAnalysisRunner);
      instance.SetDestructor(&destruct_RFcLcLVHAnalysisRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::VHAnalysisRunner*)
   {
      return GenerateInitInstanceLocal((::RF::VHAnalysisRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::VHAnalysisRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLVHAnalysisRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::VHAnalysisRunner*)0x0)->GetClass();
      RFcLcLVHAnalysisRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLVHAnalysisRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLVariation_Dictionary();
   static void RFcLcLVariation_TClassManip(TClass*);
   static void *new_RFcLcLVariation(void *p = 0);
   static void *newArray_RFcLcLVariation(Long_t size, void *p);
   static void delete_RFcLcLVariation(void *p);
   static void deleteArray_RFcLcLVariation(void *p);
   static void destruct_RFcLcLVariation(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::Variation*)
   {
      ::RF::Variation *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::Variation));
      static ::ROOT::TGenericClassInfo 
         instance("RF::Variation", "ResonanceFinder/Variation.h", 17,
                  typeid(::RF::Variation), DefineBehavior(ptr, ptr),
                  &RFcLcLVariation_Dictionary, isa_proxy, 0,
                  sizeof(::RF::Variation) );
      instance.SetNew(&new_RFcLcLVariation);
      instance.SetNewArray(&newArray_RFcLcLVariation);
      instance.SetDelete(&delete_RFcLcLVariation);
      instance.SetDeleteArray(&deleteArray_RFcLcLVariation);
      instance.SetDestructor(&destruct_RFcLcLVariation);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::Variation*)
   {
      return GenerateInitInstanceLocal((::RF::Variation*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::Variation*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLVariation_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::Variation*)0x0)->GetClass();
      RFcLcLVariation_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLVariation_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RooStatscLcLHypoTestInvTool_Dictionary();
   static void RooStatscLcLHypoTestInvTool_TClassManip(TClass*);
   static void *new_RooStatscLcLHypoTestInvTool(void *p = 0);
   static void *newArray_RooStatscLcLHypoTestInvTool(Long_t size, void *p);
   static void delete_RooStatscLcLHypoTestInvTool(void *p);
   static void deleteArray_RooStatscLcLHypoTestInvTool(void *p);
   static void destruct_RooStatscLcLHypoTestInvTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooStats::HypoTestInvTool*)
   {
      ::RooStats::HypoTestInvTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RooStats::HypoTestInvTool));
      static ::ROOT::TGenericClassInfo 
         instance("RooStats::HypoTestInvTool", "ResonanceFinder/HypoTestInvTool.h", 46,
                  typeid(::RooStats::HypoTestInvTool), DefineBehavior(ptr, ptr),
                  &RooStatscLcLHypoTestInvTool_Dictionary, isa_proxy, 0,
                  sizeof(::RooStats::HypoTestInvTool) );
      instance.SetNew(&new_RooStatscLcLHypoTestInvTool);
      instance.SetNewArray(&newArray_RooStatscLcLHypoTestInvTool);
      instance.SetDelete(&delete_RooStatscLcLHypoTestInvTool);
      instance.SetDeleteArray(&deleteArray_RooStatscLcLHypoTestInvTool);
      instance.SetDestructor(&destruct_RooStatscLcLHypoTestInvTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooStats::HypoTestInvTool*)
   {
      return GenerateInitInstanceLocal((::RooStats::HypoTestInvTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooStats::HypoTestInvTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RooStatscLcLHypoTestInvTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RooStats::HypoTestInvTool*)0x0)->GetClass();
      RooStatscLcLHypoTestInvTool_TClassManip(theClass);
   return theClass;
   }

   static void RooStatscLcLHypoTestInvTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RooStatscLcLFitTool_Dictionary();
   static void RooStatscLcLFitTool_TClassManip(TClass*);
   static void *new_RooStatscLcLFitTool(void *p = 0);
   static void *newArray_RooStatscLcLFitTool(Long_t size, void *p);
   static void delete_RooStatscLcLFitTool(void *p);
   static void deleteArray_RooStatscLcLFitTool(void *p);
   static void destruct_RooStatscLcLFitTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooStats::FitTool*)
   {
      ::RooStats::FitTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RooStats::FitTool));
      static ::ROOT::TGenericClassInfo 
         instance("RooStats::FitTool", "ResonanceFinder/FitTool.h", 19,
                  typeid(::RooStats::FitTool), DefineBehavior(ptr, ptr),
                  &RooStatscLcLFitTool_Dictionary, isa_proxy, 0,
                  sizeof(::RooStats::FitTool) );
      instance.SetNew(&new_RooStatscLcLFitTool);
      instance.SetNewArray(&newArray_RooStatscLcLFitTool);
      instance.SetDelete(&delete_RooStatscLcLFitTool);
      instance.SetDeleteArray(&deleteArray_RooStatscLcLFitTool);
      instance.SetDestructor(&destruct_RooStatscLcLFitTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooStats::FitTool*)
   {
      return GenerateInitInstanceLocal((::RooStats::FitTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooStats::FitTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RooStatscLcLFitTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RooStats::FitTool*)0x0)->GetClass();
      RooStatscLcLFitTool_TClassManip(theClass);
   return theClass;
   }

   static void RooStatscLcLFitTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLILimitTool_Dictionary();
   static void RFcLcLILimitTool_TClassManip(TClass*);
   static void delete_RFcLcLILimitTool(void *p);
   static void deleteArray_RFcLcLILimitTool(void *p);
   static void destruct_RFcLcLILimitTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::ILimitTool*)
   {
      ::RF::ILimitTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::ILimitTool));
      static ::ROOT::TGenericClassInfo 
         instance("RF::ILimitTool", "ResonanceFinder/ILimitTool.h", 22,
                  typeid(::RF::ILimitTool), DefineBehavior(ptr, ptr),
                  &RFcLcLILimitTool_Dictionary, isa_proxy, 0,
                  sizeof(::RF::ILimitTool) );
      instance.SetDelete(&delete_RFcLcLILimitTool);
      instance.SetDeleteArray(&deleteArray_RFcLcLILimitTool);
      instance.SetDestructor(&destruct_RFcLcLILimitTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::ILimitTool*)
   {
      return GenerateInitInstanceLocal((::RF::ILimitTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::ILimitTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLILimitTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::ILimitTool*)0x0)->GetClass();
      RFcLcLILimitTool_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLILimitTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLIWorkspaceCollector_Dictionary();
   static void RFcLcLIWorkspaceCollector_TClassManip(TClass*);
   static void delete_RFcLcLIWorkspaceCollector(void *p);
   static void deleteArray_RFcLcLIWorkspaceCollector(void *p);
   static void destruct_RFcLcLIWorkspaceCollector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::IWorkspaceCollector*)
   {
      ::RF::IWorkspaceCollector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::IWorkspaceCollector));
      static ::ROOT::TGenericClassInfo 
         instance("RF::IWorkspaceCollector", "ResonanceFinder/IWorkspaceCollector.h", 18,
                  typeid(::RF::IWorkspaceCollector), DefineBehavior(ptr, ptr),
                  &RFcLcLIWorkspaceCollector_Dictionary, isa_proxy, 0,
                  sizeof(::RF::IWorkspaceCollector) );
      instance.SetDelete(&delete_RFcLcLIWorkspaceCollector);
      instance.SetDeleteArray(&deleteArray_RFcLcLIWorkspaceCollector);
      instance.SetDestructor(&destruct_RFcLcLIWorkspaceCollector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::IWorkspaceCollector*)
   {
      return GenerateInitInstanceLocal((::RF::IWorkspaceCollector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::IWorkspaceCollector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLIWorkspaceCollector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::IWorkspaceCollector*)0x0)->GetClass();
      RFcLcLIWorkspaceCollector_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLIWorkspaceCollector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLIWorkspaceManipulator_Dictionary();
   static void RFcLcLIWorkspaceManipulator_TClassManip(TClass*);
   static void delete_RFcLcLIWorkspaceManipulator(void *p);
   static void deleteArray_RFcLcLIWorkspaceManipulator(void *p);
   static void destruct_RFcLcLIWorkspaceManipulator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::IWorkspaceManipulator*)
   {
      ::RF::IWorkspaceManipulator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::IWorkspaceManipulator));
      static ::ROOT::TGenericClassInfo 
         instance("RF::IWorkspaceManipulator", "ResonanceFinder/IWorkspaceManipulator.h", 23,
                  typeid(::RF::IWorkspaceManipulator), DefineBehavior(ptr, ptr),
                  &RFcLcLIWorkspaceManipulator_Dictionary, isa_proxy, 0,
                  sizeof(::RF::IWorkspaceManipulator) );
      instance.SetDelete(&delete_RFcLcLIWorkspaceManipulator);
      instance.SetDeleteArray(&deleteArray_RFcLcLIWorkspaceManipulator);
      instance.SetDestructor(&destruct_RFcLcLIWorkspaceManipulator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::IWorkspaceManipulator*)
   {
      return GenerateInitInstanceLocal((::RF::IWorkspaceManipulator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::IWorkspaceManipulator*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLIWorkspaceManipulator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::IWorkspaceManipulator*)0x0)->GetClass();
      RFcLcLIWorkspaceManipulator_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLIWorkspaceManipulator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLLimitRunner_Dictionary();
   static void RFcLcLLimitRunner_TClassManip(TClass*);
   static void delete_RFcLcLLimitRunner(void *p);
   static void deleteArray_RFcLcLLimitRunner(void *p);
   static void destruct_RFcLcLLimitRunner(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::LimitRunner*)
   {
      ::RF::LimitRunner *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::LimitRunner));
      static ::ROOT::TGenericClassInfo 
         instance("RF::LimitRunner", "ResonanceFinder/LimitRunner.h", 21,
                  typeid(::RF::LimitRunner), DefineBehavior(ptr, ptr),
                  &RFcLcLLimitRunner_Dictionary, isa_proxy, 0,
                  sizeof(::RF::LimitRunner) );
      instance.SetDelete(&delete_RFcLcLLimitRunner);
      instance.SetDeleteArray(&deleteArray_RFcLcLLimitRunner);
      instance.SetDestructor(&destruct_RFcLcLLimitRunner);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::LimitRunner*)
   {
      return GenerateInitInstanceLocal((::RF::LimitRunner*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::LimitRunner*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLLimitRunner_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::LimitRunner*)0x0)->GetClass();
      RFcLcLLimitRunner_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLLimitRunner_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLLimitTool_Dictionary();
   static void RFcLcLLimitTool_TClassManip(TClass*);
   static void delete_RFcLcLLimitTool(void *p);
   static void deleteArray_RFcLcLLimitTool(void *p);
   static void destruct_RFcLcLLimitTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::LimitTool*)
   {
      ::RF::LimitTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::LimitTool));
      static ::ROOT::TGenericClassInfo 
         instance("RF::LimitTool", "ResonanceFinder/LimitTool.h", 24,
                  typeid(::RF::LimitTool), DefineBehavior(ptr, ptr),
                  &RFcLcLLimitTool_Dictionary, isa_proxy, 0,
                  sizeof(::RF::LimitTool) );
      instance.SetDelete(&delete_RFcLcLLimitTool);
      instance.SetDeleteArray(&deleteArray_RFcLcLLimitTool);
      instance.SetDestructor(&destruct_RFcLcLLimitTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::LimitTool*)
   {
      return GenerateInitInstanceLocal((::RF::LimitTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::LimitTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLLimitTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::LimitTool*)0x0)->GetClass();
      RFcLcLLimitTool_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLLimitTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLPlotter_Dictionary();
   static void RFcLcLPlotter_TClassManip(TClass*);
   static void delete_RFcLcLPlotter(void *p);
   static void deleteArray_RFcLcLPlotter(void *p);
   static void destruct_RFcLcLPlotter(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::Plotter*)
   {
      ::RF::Plotter *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::Plotter));
      static ::ROOT::TGenericClassInfo 
         instance("RF::Plotter", "ResonanceFinder/Plotter.h", 19,
                  typeid(::RF::Plotter), DefineBehavior(ptr, ptr),
                  &RFcLcLPlotter_Dictionary, isa_proxy, 0,
                  sizeof(::RF::Plotter) );
      instance.SetDelete(&delete_RFcLcLPlotter);
      instance.SetDeleteArray(&deleteArray_RFcLcLPlotter);
      instance.SetDestructor(&destruct_RFcLcLPlotter);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::Plotter*)
   {
      return GenerateInitInstanceLocal((::RF::Plotter*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::Plotter*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLPlotter_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::Plotter*)0x0)->GetClass();
      RFcLcLPlotter_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLPlotter_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLSignalInjector_Dictionary();
   static void RFcLcLSignalInjector_TClassManip(TClass*);
   static void *new_RFcLcLSignalInjector(void *p = 0);
   static void *newArray_RFcLcLSignalInjector(Long_t size, void *p);
   static void delete_RFcLcLSignalInjector(void *p);
   static void deleteArray_RFcLcLSignalInjector(void *p);
   static void destruct_RFcLcLSignalInjector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::SignalInjector*)
   {
      ::RF::SignalInjector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::SignalInjector));
      static ::ROOT::TGenericClassInfo 
         instance("RF::SignalInjector", "ResonanceFinder/SignalInjector.h", 26,
                  typeid(::RF::SignalInjector), DefineBehavior(ptr, ptr),
                  &RFcLcLSignalInjector_Dictionary, isa_proxy, 0,
                  sizeof(::RF::SignalInjector) );
      instance.SetNew(&new_RFcLcLSignalInjector);
      instance.SetNewArray(&newArray_RFcLcLSignalInjector);
      instance.SetDelete(&delete_RFcLcLSignalInjector);
      instance.SetDeleteArray(&deleteArray_RFcLcLSignalInjector);
      instance.SetDestructor(&destruct_RFcLcLSignalInjector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::SignalInjector*)
   {
      return GenerateInitInstanceLocal((::RF::SignalInjector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::SignalInjector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLSignalInjector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::SignalInjector*)0x0)->GetClass();
      RFcLcLSignalInjector_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLSignalInjector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLStatisticalResults_Dictionary();
   static void RFcLcLStatisticalResults_TClassManip(TClass*);
   static void *new_RFcLcLStatisticalResults(void *p = 0);
   static void *newArray_RFcLcLStatisticalResults(Long_t size, void *p);
   static void delete_RFcLcLStatisticalResults(void *p);
   static void deleteArray_RFcLcLStatisticalResults(void *p);
   static void destruct_RFcLcLStatisticalResults(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::StatisticalResults*)
   {
      ::RF::StatisticalResults *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::StatisticalResults));
      static ::ROOT::TGenericClassInfo 
         instance("RF::StatisticalResults", "ResonanceFinder/StatisticalResults.h", 22,
                  typeid(::RF::StatisticalResults), DefineBehavior(ptr, ptr),
                  &RFcLcLStatisticalResults_Dictionary, isa_proxy, 0,
                  sizeof(::RF::StatisticalResults) );
      instance.SetNew(&new_RFcLcLStatisticalResults);
      instance.SetNewArray(&newArray_RFcLcLStatisticalResults);
      instance.SetDelete(&delete_RFcLcLStatisticalResults);
      instance.SetDeleteArray(&deleteArray_RFcLcLStatisticalResults);
      instance.SetDestructor(&destruct_RFcLcLStatisticalResults);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::StatisticalResults*)
   {
      return GenerateInitInstanceLocal((::RF::StatisticalResults*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::StatisticalResults*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLStatisticalResults_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::StatisticalResults*)0x0)->GetClass();
      RFcLcLStatisticalResults_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLStatisticalResults_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLStatisticalResultsCollection_Dictionary();
   static void RFcLcLStatisticalResultsCollection_TClassManip(TClass*);
   static void *new_RFcLcLStatisticalResultsCollection(void *p = 0);
   static void *newArray_RFcLcLStatisticalResultsCollection(Long_t size, void *p);
   static void delete_RFcLcLStatisticalResultsCollection(void *p);
   static void deleteArray_RFcLcLStatisticalResultsCollection(void *p);
   static void destruct_RFcLcLStatisticalResultsCollection(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::StatisticalResultsCollection*)
   {
      ::RF::StatisticalResultsCollection *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::StatisticalResultsCollection));
      static ::ROOT::TGenericClassInfo 
         instance("RF::StatisticalResultsCollection", "ResonanceFinder/StatisticalResultsCollection.h", 22,
                  typeid(::RF::StatisticalResultsCollection), DefineBehavior(ptr, ptr),
                  &RFcLcLStatisticalResultsCollection_Dictionary, isa_proxy, 0,
                  sizeof(::RF::StatisticalResultsCollection) );
      instance.SetNew(&new_RFcLcLStatisticalResultsCollection);
      instance.SetNewArray(&newArray_RFcLcLStatisticalResultsCollection);
      instance.SetDelete(&delete_RFcLcLStatisticalResultsCollection);
      instance.SetDeleteArray(&deleteArray_RFcLcLStatisticalResultsCollection);
      instance.SetDestructor(&destruct_RFcLcLStatisticalResultsCollection);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::StatisticalResultsCollection*)
   {
      return GenerateInitInstanceLocal((::RF::StatisticalResultsCollection*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::StatisticalResultsCollection*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLStatisticalResultsCollection_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::StatisticalResultsCollection*)0x0)->GetClass();
      RFcLcLStatisticalResultsCollection_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLStatisticalResultsCollection_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLWorkspaceCollection_Dictionary();
   static void RFcLcLWorkspaceCollection_TClassManip(TClass*);
   static void *new_RFcLcLWorkspaceCollection(void *p = 0);
   static void *newArray_RFcLcLWorkspaceCollection(Long_t size, void *p);
   static void delete_RFcLcLWorkspaceCollection(void *p);
   static void deleteArray_RFcLcLWorkspaceCollection(void *p);
   static void destruct_RFcLcLWorkspaceCollection(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::WorkspaceCollection*)
   {
      ::RF::WorkspaceCollection *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::WorkspaceCollection));
      static ::ROOT::TGenericClassInfo 
         instance("RF::WorkspaceCollection", "ResonanceFinder/WorkspaceCollection.h", 24,
                  typeid(::RF::WorkspaceCollection), DefineBehavior(ptr, ptr),
                  &RFcLcLWorkspaceCollection_Dictionary, isa_proxy, 0,
                  sizeof(::RF::WorkspaceCollection) );
      instance.SetNew(&new_RFcLcLWorkspaceCollection);
      instance.SetNewArray(&newArray_RFcLcLWorkspaceCollection);
      instance.SetDelete(&delete_RFcLcLWorkspaceCollection);
      instance.SetDeleteArray(&deleteArray_RFcLcLWorkspaceCollection);
      instance.SetDestructor(&destruct_RFcLcLWorkspaceCollection);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::WorkspaceCollection*)
   {
      return GenerateInitInstanceLocal((::RF::WorkspaceCollection*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::WorkspaceCollection*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLWorkspaceCollection_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::WorkspaceCollection*)0x0)->GetClass();
      RFcLcLWorkspaceCollection_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLWorkspaceCollection_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RFcLcLWorkspaceCollector_Dictionary();
   static void RFcLcLWorkspaceCollector_TClassManip(TClass*);
   static void *new_RFcLcLWorkspaceCollector(void *p = 0);
   static void *newArray_RFcLcLWorkspaceCollector(Long_t size, void *p);
   static void delete_RFcLcLWorkspaceCollector(void *p);
   static void deleteArray_RFcLcLWorkspaceCollector(void *p);
   static void destruct_RFcLcLWorkspaceCollector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RF::WorkspaceCollector*)
   {
      ::RF::WorkspaceCollector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RF::WorkspaceCollector));
      static ::ROOT::TGenericClassInfo 
         instance("RF::WorkspaceCollector", "ResonanceFinder/WorkspaceCollector.h", 20,
                  typeid(::RF::WorkspaceCollector), DefineBehavior(ptr, ptr),
                  &RFcLcLWorkspaceCollector_Dictionary, isa_proxy, 0,
                  sizeof(::RF::WorkspaceCollector) );
      instance.SetNew(&new_RFcLcLWorkspaceCollector);
      instance.SetNewArray(&newArray_RFcLcLWorkspaceCollector);
      instance.SetDelete(&delete_RFcLcLWorkspaceCollector);
      instance.SetDeleteArray(&deleteArray_RFcLcLWorkspaceCollector);
      instance.SetDestructor(&destruct_RFcLcLWorkspaceCollector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RF::WorkspaceCollector*)
   {
      return GenerateInitInstanceLocal((::RF::WorkspaceCollector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RF::WorkspaceCollector*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RFcLcLWorkspaceCollector_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RF::WorkspaceCollector*)0x0)->GetClass();
      RFcLcLWorkspaceCollector_TClassManip(theClass);
   return theClass;
   }

   static void RFcLcLWorkspaceCollector_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *PullPlotter_Dictionary();
   static void PullPlotter_TClassManip(TClass*);
   static void delete_PullPlotter(void *p);
   static void deleteArray_PullPlotter(void *p);
   static void destruct_PullPlotter(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PullPlotter*)
   {
      ::PullPlotter *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PullPlotter));
      static ::ROOT::TGenericClassInfo 
         instance("PullPlotter", "ResonanceFinder/PullPlotter.h", 48,
                  typeid(::PullPlotter), DefineBehavior(ptr, ptr),
                  &PullPlotter_Dictionary, isa_proxy, 0,
                  sizeof(::PullPlotter) );
      instance.SetDelete(&delete_PullPlotter);
      instance.SetDeleteArray(&deleteArray_PullPlotter);
      instance.SetDestructor(&destruct_PullPlotter);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PullPlotter*)
   {
      return GenerateInitInstanceLocal((::PullPlotter*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PullPlotter*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PullPlotter_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PullPlotter*)0x0)->GetClass();
      PullPlotter_TClassManip(theClass);
   return theClass;
   }

   static void PullPlotter_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLBackgroundTemplateAdder(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::BackgroundTemplateAdder : new ::RF::BackgroundTemplateAdder;
   }
   static void *newArray_RFcLcLBackgroundTemplateAdder(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::BackgroundTemplateAdder[nElements] : new ::RF::BackgroundTemplateAdder[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLBackgroundTemplateAdder(void *p) {
      delete ((::RF::BackgroundTemplateAdder*)p);
   }
   static void deleteArray_RFcLcLBackgroundTemplateAdder(void *p) {
      delete [] ((::RF::BackgroundTemplateAdder*)p);
   }
   static void destruct_RFcLcLBackgroundTemplateAdder(void *p) {
      typedef ::RF::BackgroundTemplateAdder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::BackgroundTemplateAdder

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLBinnedWorkspaceBuilder(void *p) {
      delete ((::RF::BinnedWorkspaceBuilder*)p);
   }
   static void deleteArray_RFcLcLBinnedWorkspaceBuilder(void *p) {
      delete [] ((::RF::BinnedWorkspaceBuilder*)p);
   }
   static void destruct_RFcLcLBinnedWorkspaceBuilder(void *p) {
      typedef ::RF::BinnedWorkspaceBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::BinnedWorkspaceBuilder

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLChannel(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::Channel : new ::RF::Channel;
   }
   static void *newArray_RFcLcLChannel(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::Channel[nElements] : new ::RF::Channel[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLChannel(void *p) {
      delete ((::RF::Channel*)p);
   }
   static void deleteArray_RFcLcLChannel(void *p) {
      delete [] ((::RF::Channel*)p);
   }
   static void destruct_RFcLcLChannel(void *p) {
      typedef ::RF::Channel current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::Channel

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLDownVariationAdder(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::DownVariationAdder : new ::RF::DownVariationAdder;
   }
   static void *newArray_RFcLcLDownVariationAdder(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::DownVariationAdder[nElements] : new ::RF::DownVariationAdder[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLDownVariationAdder(void *p) {
      delete ((::RF::DownVariationAdder*)p);
   }
   static void deleteArray_RFcLcLDownVariationAdder(void *p) {
      delete [] ((::RF::DownVariationAdder*)p);
   }
   static void destruct_RFcLcLDownVariationAdder(void *p) {
      typedef ::RF::DownVariationAdder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::DownVariationAdder

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLHistoCollection(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::HistoCollection : new ::RF::HistoCollection;
   }
   static void *newArray_RFcLcLHistoCollection(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::HistoCollection[nElements] : new ::RF::HistoCollection[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLHistoCollection(void *p) {
      delete ((::RF::HistoCollection*)p);
   }
   static void deleteArray_RFcLcLHistoCollection(void *p) {
      delete [] ((::RF::HistoCollection*)p);
   }
   static void destruct_RFcLcLHistoCollection(void *p) {
      typedef ::RF::HistoCollection current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::HistoCollection

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLHistoLumiRescaler(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::HistoLumiRescaler : new ::RF::HistoLumiRescaler;
   }
   static void *newArray_RFcLcLHistoLumiRescaler(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::HistoLumiRescaler[nElements] : new ::RF::HistoLumiRescaler[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLHistoLumiRescaler(void *p) {
      delete ((::RF::HistoLumiRescaler*)p);
   }
   static void deleteArray_RFcLcLHistoLumiRescaler(void *p) {
      delete [] ((::RF::HistoLumiRescaler*)p);
   }
   static void destruct_RFcLcLHistoLumiRescaler(void *p) {
      typedef ::RF::HistoLumiRescaler current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::HistoLumiRescaler

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLIAnalysisRunner(void *p) {
      delete ((::RF::IAnalysisRunner*)p);
   }
   static void deleteArray_RFcLcLIAnalysisRunner(void *p) {
      delete [] ((::RF::IAnalysisRunner*)p);
   }
   static void destruct_RFcLcLIAnalysisRunner(void *p) {
      typedef ::RF::IAnalysisRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::IAnalysisRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLIBinnedAnalysisRunner(void *p) {
      delete ((::RF::IBinnedAnalysisRunner*)p);
   }
   static void deleteArray_RFcLcLIBinnedAnalysisRunner(void *p) {
      delete [] ((::RF::IBinnedAnalysisRunner*)p);
   }
   static void destruct_RFcLcLIBinnedAnalysisRunner(void *p) {
      typedef ::RF::IBinnedAnalysisRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::IBinnedAnalysisRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLIBinnedTreeAnalysisRunner(void *p) {
      delete ((::RF::IBinnedTreeAnalysisRunner*)p);
   }
   static void deleteArray_RFcLcLIBinnedTreeAnalysisRunner(void *p) {
      delete [] ((::RF::IBinnedTreeAnalysisRunner*)p);
   }
   static void destruct_RFcLcLIBinnedTreeAnalysisRunner(void *p) {
      typedef ::RF::IBinnedTreeAnalysisRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::IBinnedTreeAnalysisRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLIHistoCollector(void *p) {
      delete ((::RF::IHistoCollector*)p);
   }
   static void deleteArray_RFcLcLIHistoCollector(void *p) {
      delete [] ((::RF::IHistoCollector*)p);
   }
   static void destruct_RFcLcLIHistoCollector(void *p) {
      typedef ::RF::IHistoCollector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::IHistoCollector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLIHistoManipulator(void *p) {
      delete ((::RF::IHistoManipulator*)p);
   }
   static void deleteArray_RFcLcLIHistoManipulator(void *p) {
      delete [] ((::RF::IHistoManipulator*)p);
   }
   static void destruct_RFcLcLIHistoManipulator(void *p) {
      typedef ::RF::IHistoManipulator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::IHistoManipulator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLITreeRunner(void *p) {
      delete ((::RF::ITreeRunner*)p);
   }
   static void deleteArray_RFcLcLITreeRunner(void *p) {
      delete [] ((::RF::ITreeRunner*)p);
   }
   static void destruct_RFcLcLITreeRunner(void *p) {
      typedef ::RF::ITreeRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::ITreeRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLIWorkspaceBuilder(void *p) {
      delete ((::RF::IWorkspaceBuilder*)p);
   }
   static void deleteArray_RFcLcLIWorkspaceBuilder(void *p) {
      delete [] ((::RF::IWorkspaceBuilder*)p);
   }
   static void destruct_RFcLcLIWorkspaceBuilder(void *p) {
      typedef ::RF::IWorkspaceBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::IWorkspaceBuilder

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLManualHistoCollector(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::ManualHistoCollector : new ::RF::ManualHistoCollector;
   }
   static void *newArray_RFcLcLManualHistoCollector(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::ManualHistoCollector[nElements] : new ::RF::ManualHistoCollector[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLManualHistoCollector(void *p) {
      delete ((::RF::ManualHistoCollector*)p);
   }
   static void deleteArray_RFcLcLManualHistoCollector(void *p) {
      delete [] ((::RF::ManualHistoCollector*)p);
   }
   static void destruct_RFcLcLManualHistoCollector(void *p) {
      typedef ::RF::ManualHistoCollector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::ManualHistoCollector

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLMultiplicativeFactor(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::MultiplicativeFactor : new ::RF::MultiplicativeFactor;
   }
   static void *newArray_RFcLcLMultiplicativeFactor(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::MultiplicativeFactor[nElements] : new ::RF::MultiplicativeFactor[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLMultiplicativeFactor(void *p) {
      delete ((::RF::MultiplicativeFactor*)p);
   }
   static void deleteArray_RFcLcLMultiplicativeFactor(void *p) {
      delete [] ((::RF::MultiplicativeFactor*)p);
   }
   static void destruct_RFcLcLMultiplicativeFactor(void *p) {
      typedef ::RF::MultiplicativeFactor current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::MultiplicativeFactor

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLSample(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::Sample : new ::RF::Sample;
   }
   static void *newArray_RFcLcLSample(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::Sample[nElements] : new ::RF::Sample[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLSample(void *p) {
      delete ((::RF::Sample*)p);
   }
   static void deleteArray_RFcLcLSample(void *p) {
      delete [] ((::RF::Sample*)p);
   }
   static void destruct_RFcLcLSample(void *p) {
      typedef ::RF::Sample current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::Sample

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLSystematicsSmoother(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::SystematicsSmoother : new ::RF::SystematicsSmoother;
   }
   static void *newArray_RFcLcLSystematicsSmoother(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::SystematicsSmoother[nElements] : new ::RF::SystematicsSmoother[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLSystematicsSmoother(void *p) {
      delete ((::RF::SystematicsSmoother*)p);
   }
   static void deleteArray_RFcLcLSystematicsSmoother(void *p) {
      delete [] ((::RF::SystematicsSmoother*)p);
   }
   static void destruct_RFcLcLSystematicsSmoother(void *p) {
      typedef ::RF::SystematicsSmoother current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::SystematicsSmoother

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLTreeHistoCollector(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::TreeHistoCollector : new ::RF::TreeHistoCollector;
   }
   static void *newArray_RFcLcLTreeHistoCollector(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::TreeHistoCollector[nElements] : new ::RF::TreeHistoCollector[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLTreeHistoCollector(void *p) {
      delete ((::RF::TreeHistoCollector*)p);
   }
   static void deleteArray_RFcLcLTreeHistoCollector(void *p) {
      delete [] ((::RF::TreeHistoCollector*)p);
   }
   static void destruct_RFcLcLTreeHistoCollector(void *p) {
      typedef ::RF::TreeHistoCollector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::TreeHistoCollector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLVVJJAnalysisRunner(void *p) {
      delete ((::RF::VVJJAnalysisRunner*)p);
   }
   static void deleteArray_RFcLcLVVJJAnalysisRunner(void *p) {
      delete [] ((::RF::VVJJAnalysisRunner*)p);
   }
   static void destruct_RFcLcLVVJJAnalysisRunner(void *p) {
      typedef ::RF::VVJJAnalysisRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::VVJJAnalysisRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLVVJJValidationRunner(void *p) {
      delete ((::RF::VVJJValidationRunner*)p);
   }
   static void deleteArray_RFcLcLVVJJValidationRunner(void *p) {
      delete [] ((::RF::VVJJValidationRunner*)p);
   }
   static void destruct_RFcLcLVVJJValidationRunner(void *p) {
      typedef ::RF::VVJJValidationRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::VVJJValidationRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLVVlvqqAnalysisRunner(void *p) {
      delete ((::RF::VVlvqqAnalysisRunner*)p);
   }
   static void deleteArray_RFcLcLVVlvqqAnalysisRunner(void *p) {
      delete [] ((::RF::VVlvqqAnalysisRunner*)p);
   }
   static void destruct_RFcLcLVVlvqqAnalysisRunner(void *p) {
      typedef ::RF::VVlvqqAnalysisRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::VVlvqqAnalysisRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLVVlvqqValidationRunner(void *p) {
      delete ((::RF::VVlvqqValidationRunner*)p);
   }
   static void deleteArray_RFcLcLVVlvqqValidationRunner(void *p) {
      delete [] ((::RF::VVlvqqValidationRunner*)p);
   }
   static void destruct_RFcLcLVVlvqqValidationRunner(void *p) {
      typedef ::RF::VVlvqqValidationRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::VVlvqqValidationRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLVVllqqAnalysisRunner(void *p) {
      delete ((::RF::VVllqqAnalysisRunner*)p);
   }
   static void deleteArray_RFcLcLVVllqqAnalysisRunner(void *p) {
      delete [] ((::RF::VVllqqAnalysisRunner*)p);
   }
   static void destruct_RFcLcLVVllqqAnalysisRunner(void *p) {
      typedef ::RF::VVllqqAnalysisRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::VVllqqAnalysisRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLVVllqqValidationRunner(void *p) {
      delete ((::RF::VVllqqValidationRunner*)p);
   }
   static void deleteArray_RFcLcLVVllqqValidationRunner(void *p) {
      delete [] ((::RF::VVllqqValidationRunner*)p);
   }
   static void destruct_RFcLcLVVllqqValidationRunner(void *p) {
      typedef ::RF::VVllqqValidationRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::VVllqqValidationRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLVHAnalysisRunner(void *p) {
      delete ((::RF::VHAnalysisRunner*)p);
   }
   static void deleteArray_RFcLcLVHAnalysisRunner(void *p) {
      delete [] ((::RF::VHAnalysisRunner*)p);
   }
   static void destruct_RFcLcLVHAnalysisRunner(void *p) {
      typedef ::RF::VHAnalysisRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::VHAnalysisRunner

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLVariation(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::Variation : new ::RF::Variation;
   }
   static void *newArray_RFcLcLVariation(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::Variation[nElements] : new ::RF::Variation[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLVariation(void *p) {
      delete ((::RF::Variation*)p);
   }
   static void deleteArray_RFcLcLVariation(void *p) {
      delete [] ((::RF::Variation*)p);
   }
   static void destruct_RFcLcLVariation(void *p) {
      typedef ::RF::Variation current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::Variation

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooStatscLcLHypoTestInvTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RooStats::HypoTestInvTool : new ::RooStats::HypoTestInvTool;
   }
   static void *newArray_RooStatscLcLHypoTestInvTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RooStats::HypoTestInvTool[nElements] : new ::RooStats::HypoTestInvTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooStatscLcLHypoTestInvTool(void *p) {
      delete ((::RooStats::HypoTestInvTool*)p);
   }
   static void deleteArray_RooStatscLcLHypoTestInvTool(void *p) {
      delete [] ((::RooStats::HypoTestInvTool*)p);
   }
   static void destruct_RooStatscLcLHypoTestInvTool(void *p) {
      typedef ::RooStats::HypoTestInvTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooStats::HypoTestInvTool

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooStatscLcLFitTool(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RooStats::FitTool : new ::RooStats::FitTool;
   }
   static void *newArray_RooStatscLcLFitTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RooStats::FitTool[nElements] : new ::RooStats::FitTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooStatscLcLFitTool(void *p) {
      delete ((::RooStats::FitTool*)p);
   }
   static void deleteArray_RooStatscLcLFitTool(void *p) {
      delete [] ((::RooStats::FitTool*)p);
   }
   static void destruct_RooStatscLcLFitTool(void *p) {
      typedef ::RooStats::FitTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooStats::FitTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLILimitTool(void *p) {
      delete ((::RF::ILimitTool*)p);
   }
   static void deleteArray_RFcLcLILimitTool(void *p) {
      delete [] ((::RF::ILimitTool*)p);
   }
   static void destruct_RFcLcLILimitTool(void *p) {
      typedef ::RF::ILimitTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::ILimitTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLIWorkspaceCollector(void *p) {
      delete ((::RF::IWorkspaceCollector*)p);
   }
   static void deleteArray_RFcLcLIWorkspaceCollector(void *p) {
      delete [] ((::RF::IWorkspaceCollector*)p);
   }
   static void destruct_RFcLcLIWorkspaceCollector(void *p) {
      typedef ::RF::IWorkspaceCollector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::IWorkspaceCollector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLIWorkspaceManipulator(void *p) {
      delete ((::RF::IWorkspaceManipulator*)p);
   }
   static void deleteArray_RFcLcLIWorkspaceManipulator(void *p) {
      delete [] ((::RF::IWorkspaceManipulator*)p);
   }
   static void destruct_RFcLcLIWorkspaceManipulator(void *p) {
      typedef ::RF::IWorkspaceManipulator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::IWorkspaceManipulator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLLimitRunner(void *p) {
      delete ((::RF::LimitRunner*)p);
   }
   static void deleteArray_RFcLcLLimitRunner(void *p) {
      delete [] ((::RF::LimitRunner*)p);
   }
   static void destruct_RFcLcLLimitRunner(void *p) {
      typedef ::RF::LimitRunner current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::LimitRunner

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLLimitTool(void *p) {
      delete ((::RF::LimitTool*)p);
   }
   static void deleteArray_RFcLcLLimitTool(void *p) {
      delete [] ((::RF::LimitTool*)p);
   }
   static void destruct_RFcLcLLimitTool(void *p) {
      typedef ::RF::LimitTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::LimitTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RFcLcLPlotter(void *p) {
      delete ((::RF::Plotter*)p);
   }
   static void deleteArray_RFcLcLPlotter(void *p) {
      delete [] ((::RF::Plotter*)p);
   }
   static void destruct_RFcLcLPlotter(void *p) {
      typedef ::RF::Plotter current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::Plotter

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLSignalInjector(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::SignalInjector : new ::RF::SignalInjector;
   }
   static void *newArray_RFcLcLSignalInjector(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::SignalInjector[nElements] : new ::RF::SignalInjector[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLSignalInjector(void *p) {
      delete ((::RF::SignalInjector*)p);
   }
   static void deleteArray_RFcLcLSignalInjector(void *p) {
      delete [] ((::RF::SignalInjector*)p);
   }
   static void destruct_RFcLcLSignalInjector(void *p) {
      typedef ::RF::SignalInjector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::SignalInjector

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLStatisticalResults(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::StatisticalResults : new ::RF::StatisticalResults;
   }
   static void *newArray_RFcLcLStatisticalResults(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::StatisticalResults[nElements] : new ::RF::StatisticalResults[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLStatisticalResults(void *p) {
      delete ((::RF::StatisticalResults*)p);
   }
   static void deleteArray_RFcLcLStatisticalResults(void *p) {
      delete [] ((::RF::StatisticalResults*)p);
   }
   static void destruct_RFcLcLStatisticalResults(void *p) {
      typedef ::RF::StatisticalResults current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::StatisticalResults

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLStatisticalResultsCollection(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::StatisticalResultsCollection : new ::RF::StatisticalResultsCollection;
   }
   static void *newArray_RFcLcLStatisticalResultsCollection(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::StatisticalResultsCollection[nElements] : new ::RF::StatisticalResultsCollection[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLStatisticalResultsCollection(void *p) {
      delete ((::RF::StatisticalResultsCollection*)p);
   }
   static void deleteArray_RFcLcLStatisticalResultsCollection(void *p) {
      delete [] ((::RF::StatisticalResultsCollection*)p);
   }
   static void destruct_RFcLcLStatisticalResultsCollection(void *p) {
      typedef ::RF::StatisticalResultsCollection current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::StatisticalResultsCollection

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLWorkspaceCollection(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::WorkspaceCollection : new ::RF::WorkspaceCollection;
   }
   static void *newArray_RFcLcLWorkspaceCollection(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::WorkspaceCollection[nElements] : new ::RF::WorkspaceCollection[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLWorkspaceCollection(void *p) {
      delete ((::RF::WorkspaceCollection*)p);
   }
   static void deleteArray_RFcLcLWorkspaceCollection(void *p) {
      delete [] ((::RF::WorkspaceCollection*)p);
   }
   static void destruct_RFcLcLWorkspaceCollection(void *p) {
      typedef ::RF::WorkspaceCollection current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::WorkspaceCollection

namespace ROOT {
   // Wrappers around operator new
   static void *new_RFcLcLWorkspaceCollector(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::WorkspaceCollector : new ::RF::WorkspaceCollector;
   }
   static void *newArray_RFcLcLWorkspaceCollector(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::RF::WorkspaceCollector[nElements] : new ::RF::WorkspaceCollector[nElements];
   }
   // Wrapper around operator delete
   static void delete_RFcLcLWorkspaceCollector(void *p) {
      delete ((::RF::WorkspaceCollector*)p);
   }
   static void deleteArray_RFcLcLWorkspaceCollector(void *p) {
      delete [] ((::RF::WorkspaceCollector*)p);
   }
   static void destruct_RFcLcLWorkspaceCollector(void *p) {
      typedef ::RF::WorkspaceCollector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RF::WorkspaceCollector

namespace ROOT {
   // Wrapper around operator delete
   static void delete_PullPlotter(void *p) {
      delete ((::PullPlotter*)p);
   }
   static void deleteArray_PullPlotter(void *p) {
      delete [] ((::PullPlotter*)p);
   }
   static void destruct_PullPlotter(void *p) {
      typedef ::PullPlotter current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PullPlotter

namespace {
  void TriggerDictionaryInitialization_ResonanceFinderCINT_Impl() {
    static const char* headers[] = {
"ResonanceFinder/BackgroundTemplateAdder.h",
"ResonanceFinder/IHistoManipulator.h",
"ResonanceFinder/HistoCollection.h",
"ResonanceFinder/BinnedWorkspaceBuilder.h",
"ResonanceFinder/IWorkspaceBuilder.h",
"ResonanceFinder/Channel.h",
"ResonanceFinder/Sample.h",
"ResonanceFinder/Variation.h",
"ResonanceFinder/MultiplicativeFactor.h",
"ResonanceFinder/DownVariationAdder.h",
"ResonanceFinder/HistoLumiRescaler.h",
"ResonanceFinder/IAnalysisRunner.h",
"ResonanceFinder/WorkspaceCollector.h",
"ResonanceFinder/IWorkspaceCollector.h",
"ResonanceFinder/WorkspaceCollection.h",
"ResonanceFinder/LimitRunner.h",
"ResonanceFinder/LimitTool.h",
"ResonanceFinder/ILimitTool.h",
"ResonanceFinder/StatisticalResults.h",
"ResonanceFinder/HypoTestInvTool.h",
"ResonanceFinder/FitTool.h",
"ResonanceFinder/StatisticalResultsCollection.h",
"ResonanceFinder/SignalInjector.h",
"ResonanceFinder/IWorkspaceManipulator.h",
"ResonanceFinder/Plotter.h",
"ResonanceFinder/IBinnedAnalysisRunner.h",
"ResonanceFinder/IHistoCollector.h",
"ResonanceFinder/IBinnedTreeAnalysisRunner.h",
"ResonanceFinder/ITreeRunner.h",
"ResonanceFinder/TreeHistoCollector.h",
"ResonanceFinder/ManualHistoCollector.h",
"ResonanceFinder/SystematicsSmoother.h",
"ResonanceFinder/VVJJAnalysisRunner.h",
"ResonanceFinder/VVJJValidationRunner.h",
"ResonanceFinder/VVlvqqAnalysisRunner.h",
"ResonanceFinder/VVlvqqValidationRunner.h",
"ResonanceFinder/VVllqqAnalysisRunner.h",
"ResonanceFinder/VVllqqValidationRunner.h",
"ResonanceFinder/VHAnalysisRunner.h",
"ResonanceFinder/PullPlotter.h",
0
    };
    static const char* includePaths[] = {
"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root",
"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  BackgroundTemplateAdder;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  BinnedWorkspaceBuilder;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  Channel;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  DownVariationAdder;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  HistoCollection;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  HistoLumiRescaler;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  IAnalysisRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  IBinnedAnalysisRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  IBinnedTreeAnalysisRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  IHistoCollector;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  IHistoManipulator;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  ITreeRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  IWorkspaceBuilder;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  ManualHistoCollector;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  MultiplicativeFactor;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  Sample;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  SystematicsSmoother;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  TreeHistoCollector;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  VVJJAnalysisRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  VVJJValidationRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  VVlvqqAnalysisRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  VVlvqqValidationRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  VVllqqAnalysisRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  VVllqqValidationRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  VHAnalysisRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  Variation;}
namespace RooStats{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  HypoTestInvTool;}
namespace RooStats{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  FitTool;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  ILimitTool;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  IWorkspaceCollector;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  IWorkspaceManipulator;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  LimitRunner;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  LimitTool;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  Plotter;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  SignalInjector;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  StatisticalResults;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  StatisticalResultsCollection;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  WorkspaceCollection;}
namespace RF{class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  WorkspaceCollector;}
class __attribute__((annotate("$clingAutoload$/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/Root/LinkDef.h")))  PullPlotter;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 24
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725"
#endif
#ifndef ASG_TEST_FILE_DATA
  #define ASG_TEST_FILE_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MC
  #define ASG_TEST_FILE_MC "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MCAFII
  #define ASG_TEST_FILE_MCAFII ""
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "ResonanceFinder"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "ResonanceFinder/BackgroundTemplateAdder.h"
#include "ResonanceFinder/IHistoManipulator.h"
#include "ResonanceFinder/HistoCollection.h"
#include "ResonanceFinder/BinnedWorkspaceBuilder.h"
#include "ResonanceFinder/IWorkspaceBuilder.h"
#include "ResonanceFinder/Channel.h"
#include "ResonanceFinder/Sample.h"
#include "ResonanceFinder/Variation.h"
#include "ResonanceFinder/MultiplicativeFactor.h"
#include "ResonanceFinder/DownVariationAdder.h"
#include "ResonanceFinder/HistoLumiRescaler.h"
#include "ResonanceFinder/IAnalysisRunner.h"
#include "ResonanceFinder/WorkspaceCollector.h"
#include "ResonanceFinder/IWorkspaceCollector.h"
#include "ResonanceFinder/WorkspaceCollection.h"
#include "ResonanceFinder/LimitRunner.h"
#include "ResonanceFinder/LimitTool.h"
#include "ResonanceFinder/ILimitTool.h"
#include "ResonanceFinder/StatisticalResults.h"
#include "ResonanceFinder/HypoTestInvTool.h"
#include "ResonanceFinder/FitTool.h"
#include "ResonanceFinder/StatisticalResultsCollection.h"
#include "ResonanceFinder/SignalInjector.h"
#include "ResonanceFinder/IWorkspaceManipulator.h"
#include "ResonanceFinder/Plotter.h"
#include "ResonanceFinder/IBinnedAnalysisRunner.h"
#include "ResonanceFinder/IHistoCollector.h"
#include "ResonanceFinder/IBinnedTreeAnalysisRunner.h"
#include "ResonanceFinder/ITreeRunner.h"
#include "ResonanceFinder/TreeHistoCollector.h"
#include "ResonanceFinder/ManualHistoCollector.h"
#include "ResonanceFinder/SystematicsSmoother.h"
#include "ResonanceFinder/VVJJAnalysisRunner.h"
#include "ResonanceFinder/VVJJValidationRunner.h"
#include "ResonanceFinder/VVlvqqAnalysisRunner.h"
#include "ResonanceFinder/VVlvqqValidationRunner.h"
#include "ResonanceFinder/VVllqqAnalysisRunner.h"
#include "ResonanceFinder/VVllqqValidationRunner.h"
#include "ResonanceFinder/VHAnalysisRunner.h"
#include "ResonanceFinder/PullPlotter.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"PullPlotter", payloadCode, "@",
"RF::BackgroundTemplateAdder", payloadCode, "@",
"RF::BinnedWorkspaceBuilder", payloadCode, "@",
"RF::Channel", payloadCode, "@",
"RF::DownVariationAdder", payloadCode, "@",
"RF::HistoCollection", payloadCode, "@",
"RF::HistoLumiRescaler", payloadCode, "@",
"RF::IAnalysisRunner", payloadCode, "@",
"RF::IBinnedAnalysisRunner", payloadCode, "@",
"RF::IBinnedTreeAnalysisRunner", payloadCode, "@",
"RF::IHistoCollector", payloadCode, "@",
"RF::IHistoManipulator", payloadCode, "@",
"RF::ILimitTool", payloadCode, "@",
"RF::ITreeRunner", payloadCode, "@",
"RF::IWorkspaceBuilder", payloadCode, "@",
"RF::IWorkspaceCollector", payloadCode, "@",
"RF::IWorkspaceManipulator", payloadCode, "@",
"RF::LimitRunner", payloadCode, "@",
"RF::LimitTool", payloadCode, "@",
"RF::ManualHistoCollector", payloadCode, "@",
"RF::MultiplicativeFactor", payloadCode, "@",
"RF::Plotter", payloadCode, "@",
"RF::Sample", payloadCode, "@",
"RF::SignalInjector", payloadCode, "@",
"RF::StatisticalResults", payloadCode, "@",
"RF::StatisticalResultsCollection", payloadCode, "@",
"RF::SystematicsSmoother", payloadCode, "@",
"RF::TreeHistoCollector", payloadCode, "@",
"RF::VHAnalysisRunner", payloadCode, "@",
"RF::VVJJAnalysisRunner", payloadCode, "@",
"RF::VVJJValidationRunner", payloadCode, "@",
"RF::VVllqqAnalysisRunner", payloadCode, "@",
"RF::VVllqqValidationRunner", payloadCode, "@",
"RF::VVlvqqAnalysisRunner", payloadCode, "@",
"RF::VVlvqqValidationRunner", payloadCode, "@",
"RF::Variation", payloadCode, "@",
"RF::WorkspaceCollection", payloadCode, "@",
"RF::WorkspaceCollector", payloadCode, "@",
"RooStats::FitTool", payloadCode, "@",
"RooStats::HypoTestInvTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("ResonanceFinderCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_ResonanceFinderCINT_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_ResonanceFinderCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_ResonanceFinderCINT() {
  TriggerDictionaryInitialization_ResonanceFinderCINT_Impl();
}
