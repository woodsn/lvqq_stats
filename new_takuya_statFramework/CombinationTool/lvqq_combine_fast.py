#!/usr/bin/env python

# Load the shared library. To compile it, follow the instructions given in the
# README.
import ROOT
import sys
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem

try:
    gSystem.Load("lib/libCombinationTool.so")
except:
    print ("Could not load library. Make sure that it was compiled correctly.")

fFile1  = sys.argv[1]
fFile2 = sys.argv[2]
fFile3    = sys.argv[3]


isThreeFile = False
if len(sys.argv) > 4 :
    isThreeFile = True

oFileName = "output.root"


print('Using file1: %s'%fFile1)
print('Using file2: %s'%fFile2)
print('Using file3: %s'%fFile3)
  

# Define
combined = ROOT.CombinedMeasurement("combined_master", "combined", "ModelConfig", "obsData")

#Add the first measurement to the combination
fileOne = ROOT.Measurement("fileOne")
fileOne.SetSnapshotName("NominalParamValues")
fileOne.SetFileName(fFile1)
fileOne.SetWorkspaceName("combined")
fileOne.SetModelConfigName("ModelConfig")
fileOne.SetDataName("obsData")
combined.AddMeasurement(fileOne)

#Add the second measurement to the combiantion
fileTwo = ROOT.Measurement("fileTwo")
fileTwo.SetSnapshotName("NominalParamValues")
fileTwo.SetFileName(fFile2)
fileTwo.SetWorkspaceName("combined")
fileTwo.SetModelConfigName("ModelConfig")
fileTwo.SetDataName("obsData")
combined.AddMeasurement(fileTwo)


#Add the second measurement to the combiantion
fileThree = ROOT.Measurement("fileThree")
fileThree.SetSnapshotName("NominalParamValues")
fileThree.SetFileName(fFile3)
fileThree.SetWorkspaceName("combined")
fileThree.SetModelConfigName("ModelConfig")
fileThree.SetDataName("obsData")
combined.AddMeasurement(fileThree)




# Define a correlation scheme that should be used when combining the specified
# measurements. Parameters in the scheme that are not present in the
# measurements will be ignored.
correlation = ROOT.CorrelationScheme("CorrelationScheme")

# Define parameters of interest for the combined measurement. Only parameters
# present in the final workspace will be considered.
correlation.SetParametersOfInterest("mu")

# Signal strengths for different production modes. Use a placeholder for the
# ditau signal strength as it needs to be corrected for using 125.5 GeV.
# Signal strength parameters are unconstrained by default.
correlation.CorrelateParameter("fileOne::mu,fileTwo::mu,fileThree::mu", "mu")
#correlation.CorrelateParameter("fileOne::alpha_TAUS_TRUEHADTAU_SME_TES_DETECTOR,fileTwo::alpha_TAUS_TRUEHADTAU_SME_TES_DETECTOR", "alpha_TAUS_TRUEHADTAU_SME_TES_DETECTOR")
#correlation.CorrelateParameter("fileOne::alpha_TAUS_TRUEHADTAU_SME_TES_INSITU,fileTwo::alpha_TAUS_TRUEHADTAU_SME_TES_INSITU", "alpha_TAUS_TRUEHADTAU_SME_TES_INSITU")
#correlation.CorrelateParameter("fileOne::alpha_TAUS_TRUEHADTAU_SME_TES_MODEL,fileTwo::alpha_TAUS_TRUEHADTAU_SME_TES_MODEL", "alpha_TAUS_TRUEHADTAU_SME_TES_MODEL")
#correlation.CorrelateParameter("fileOne::alpha_FATJET_Medium_JET_Rtrk_TotalStat_D2,fileTwo::alpha_FATJET_Medium_JET_Rtrk_TotalStat_D2", "alpha_FATJET_Medium_JET_Rtrk_TotalStat_D2")
#correlation.CorrelateParameter("fileOne::alpha_JET_21NP_JET_EffectiveNP_6,fileTwo::alpha_JET_21NP_JET_EffectiveNP_6", "alpha_JET_21NP_JET_EffectiveNP_6")
#correlation.CorrelateParameter("fileOne::alpha_JET_21NP_JET_EffectiveNP_7,fileTwo::alpha_JET_21NP_JET_EffectiveNP_7", "alpha_JET_21NP_JET_EffectiveNP_7")

correlation.CorrelateParameter("fileOne::XS_Top,fileTwo::XS_Top_LP","XS_Top")
correlation.CorrelateParameter("fileOne::XS_Wjets,fileTwo::XS_Wjets_LP","XS_Wjets")


#CorrP



resolvedParams = [
'alpha_EG_RESOLUTION_ALL',
'alpha_EG_SCALE_ALL',
'alpha_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',
'alpha_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
'alpha_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
'alpha_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',
'alpha_FATJET_D2R',
'alpha_FATJET_JER',
'alpha_FATJET_JMR',
'alpha_FATJET_Medium_JET_Comb_Baseline_Kin',
'alpha_FATJET_Medium_JET_Comb_Modelling_Kin',
'alpha_FATJET_Medium_JET_Comb_TotalStat_Kin',
'alpha_FATJET_Medium_JET_Comb_Tracking_Kin',
'alpha_FATJET_Weak_JET_Rtrk_Baseline_D2Beta1',
'alpha_FATJET_Weak_JET_Rtrk_Modelling_D2Beta1',
'alpha_FATJET_Weak_JET_Rtrk_TotalStat_D2Beta1',
'alpha_FATJET_Weak_JET_Rtrk_Tracking_D2Beta1',
'alpha_JET_21NP_JET_BJES_Response',
'alpha_JET_21NP_JET_EffectiveNP_1',
'alpha_JET_21NP_JET_EffectiveNP_2',
'alpha_JET_21NP_JET_EffectiveNP_3',
'alpha_JET_21NP_JET_EffectiveNP_4',
'alpha_JET_21NP_JET_EffectiveNP_5',
'alpha_JET_21NP_JET_EffectiveNP_6',
'alpha_JET_21NP_JET_EffectiveNP_7',
'alpha_JET_21NP_JET_EffectiveNP_8restTerm',
'alpha_JET_21NP_JET_EtaIntercalibration_Modelling',
'alpha_JET_21NP_JET_EtaIntercalibration_NonClosure',
'alpha_JET_21NP_JET_EtaIntercalibration_TotalStat',
'alpha_JET_21NP_JET_Flavor_Composition',
'alpha_JET_21NP_JET_Flavor_Response',
'alpha_JET_21NP_JET_Pileup_OffsetMu',
'alpha_JET_21NP_JET_Pileup_OffsetNPV',
'alpha_JET_21NP_JET_Pileup_PtTerm',
'alpha_JET_21NP_JET_Pileup_RhoTopology',
'alpha_JET_21NP_JET_PunchThrough_MC15',
'alpha_JET_21NP_JET_SingleParticle_HighPt',
'alpha_JET_JER_SINGLE_NP',
'alpha_JET_JvtEfficiency',
'alpha_LumiNP',
'alpha_METTrigStat',
'alpha_METTrigTop',
'alpha_MET_SoftTrk_ResoPara',
'alpha_MET_SoftTrk_ResoPerp',
'alpha_MET_SoftTrk_Scale',
'alpha_MUON_EFF_STAT',
'alpha_MUON_EFF_STAT_LOWPT',
'alpha_MUON_EFF_SYS',
'alpha_MUON_EFF_SYS_LOWPT',
'alpha_MUON_ID',
'alpha_MUON_ISO_STAT',
'alpha_MUON_ISO_SYS',
'alpha_MUON_MS',
'alpha_MUON_SAGITTA_RESBIAS',
'alpha_MUON_SAGITTA_RHO',
'alpha_MUON_SCALE',
'alpha_MUON_TTVA_STAT',
'alpha_MUON_TTVA_SYS',
'alpha_PRW_DATASF',
'alpha_Signal_ISR_FSR',
'alpha_Signal_PDF',
'alpha_TopModeling_Herwig',
'alpha_TopModeling_MCatNLO',
'alpha_TopModeling_Rad',
'alpha_WjetsModeling_AlphaS',
'alpha_WjetsModeling_CKKW15',
'alpha_WjetsModeling_CKKW30',
'alpha_WjetsModeling_MadGraph',
'alpha_WjetsModeling_PDF',
'alpha_WjetsModeling_Scale',
'alpha_XS_Dibosons',
'alpha_XS_SingleTop',
'alpha_XS_Zjets',
'alpha_Res_FT_EFF_Eigen_B_0_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_B_1_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_B_2_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_C_0_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_C_1_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_C_2_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_C_3_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_extrapolation_AntiKt4EMTopoJets',
'alpha_Res_FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets',
]

boostedParams=[
'alpha_EG_RESOLUTION_ALL',
'alpha_EG_SCALE_ALL',
'alpha_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',
'alpha_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
'alpha_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
'alpha_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',
'alpha_FATJET_D2R',
'alpha_FATJET_JER',
'alpha_FATJET_JMR',
'alpha_FATJET_Medium_JET_Comb_Baseline_Kin',
'alpha_FATJET_Medium_JET_Comb_Modelling_Kin',
'alpha_FATJET_Medium_JET_Comb_TotalStat_Kin',
'alpha_FATJET_Medium_JET_Comb_Tracking_Kin',
'alpha_FATJET_Weak_JET_Rtrk_Baseline_D2Beta1',
'alpha_FATJET_Weak_JET_Rtrk_Modelling_D2Beta1',
'alpha_FATJET_Weak_JET_Rtrk_TotalStat_D2Beta1',
'alpha_FATJET_Weak_JET_Rtrk_Tracking_D2Beta1',
'alpha_JET_21NP_JET_BJES_Response',
'alpha_JET_21NP_JET_EffectiveNP_1',
'alpha_JET_21NP_JET_EffectiveNP_2',
'alpha_JET_21NP_JET_EffectiveNP_3',
'alpha_JET_21NP_JET_EffectiveNP_4',
'alpha_JET_21NP_JET_EffectiveNP_5',
'alpha_JET_21NP_JET_EffectiveNP_6',
'alpha_JET_21NP_JET_EffectiveNP_7',
'alpha_JET_21NP_JET_EffectiveNP_8restTerm',
'alpha_JET_21NP_JET_EtaIntercalibration_Modelling',
'alpha_JET_21NP_JET_EtaIntercalibration_NonClosure',
'alpha_JET_21NP_JET_EtaIntercalibration_TotalStat',
'alpha_JET_21NP_JET_Flavor_Composition',
'alpha_JET_21NP_JET_Flavor_Response',
'alpha_JET_21NP_JET_Pileup_OffsetMu',
'alpha_JET_21NP_JET_Pileup_OffsetNPV',
'alpha_JET_21NP_JET_Pileup_PtTerm',
'alpha_JET_21NP_JET_Pileup_RhoTopology',
'alpha_JET_21NP_JET_PunchThrough_MC15',
'alpha_JET_21NP_JET_SingleParticle_HighPt',
'alpha_JET_JER_SINGLE_NP',
'alpha_JET_JvtEfficiency',
'alpha_LumiNP',
'alpha_METTrigStat',
'alpha_METTrigTop',
'alpha_MET_SoftTrk_ResoPara',
'alpha_MET_SoftTrk_ResoPerp',
'alpha_MET_SoftTrk_Scale',
'alpha_MUON_EFF_STAT',
'alpha_MUON_EFF_STAT_LOWPT',
'alpha_MUON_EFF_SYS',
'alpha_MUON_EFF_SYS_LOWPT',
'alpha_MUON_ID',
'alpha_MUON_ISO_STAT',
'alpha_MUON_ISO_SYS',
'alpha_MUON_MS',
'alpha_MUON_SAGITTA_RESBIAS',
'alpha_MUON_SAGITTA_RHO',
'alpha_MUON_SCALE',
'alpha_MUON_TTVA_STAT',
'alpha_MUON_TTVA_SYS',
'alpha_PRW_DATASF',
'alpha_Signal_ISR_FSR',
'alpha_Signal_PDF',
'alpha_TopModeling_Herwig',
'alpha_TopModeling_MCatNLO',
'alpha_TopModeling_Rad',
'alpha_WjetsModeling_AlphaS',
'alpha_WjetsModeling_CKKW15',
'alpha_WjetsModeling_CKKW30',
'alpha_WjetsModeling_MadGraph',
'alpha_WjetsModeling_PDF',
'alpha_WjetsModeling_Scale',
'alpha_XS_Dibosons',
'alpha_XS_SingleTop',
'alpha_XS_Zjets',
'alpha_FT_EFF_Eigen_B_0_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_B_1_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_B_2_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_C_0_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_C_1_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_C_2_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_C_3_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets',
'alpha_FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets',
'alpha_FT_EFF_extrapolation_AntiKt4EMTopoJets',
'alpha_FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets',
]


for iPar_res, iPar_boos in zip(resolvedParams,boostedParams):
    #corrString = "fileOne::"+iPar+"fileTwo::"+iPar+",fileThree::"+iPar+",fileFour::"+iPar
    corrString = "fileOne::"+iPar_boos+",fileTwo::"+iPar_boos+",fileThree::"+iPar_res
    outString = iPar_boos
    outString = outString[6:]
    print 'correlation String issssssssssss: ' + corrString + " outstring: " + outString
    correlation.CorrelateParameter(corrString,outString);








    
# Use the correlation scheme for the combined measurement.
combined.SetCorrelationScheme(correlation)

# Run the combination. First all measurements are regularised, i.e. the
# structure of the PDF will be unified, parameters and constraint terms renamed
# according to a common convention, etc. Then a new simultaneous PDF and
# dataset is build.
combined.CollectMeasurements()
combined.CombineMeasurements()

print 'printing correlation schemmmmmeeeeeee'
correlation.Print()

# Add common signal strength parameter for all signal samples
signalstrength = ROOT.ParametrisationScheme("signalstrength")
#signalstrength.AddExpression("mu[1.0,-1000.0,1000.0]")
signalstrength.AddExpression("mu[1.0,-1000.0,1000.0]")


# Combine the different parametrisations
parametrisation = ROOT.ParametrisationSequence("parametrisation")
#parametrisation.AddScheme(corrections)
parametrisation.AddScheme(signalstrength)
parametrisation.SetParametersOfInterest("mu")

# Carry out the re-parametrisation
combined.SetParametrisationSequence(parametrisation)
combined.ParametriseMeasurements()

# Generate Asimov data (NP measured in unconditional fit, generated for mu = 0)
combined.MakeAsimovData(ROOT.kFALSE, ROOT.CombinedMeasurement.ucmles, ROOT.CombinedMeasurement.background)
#combined.MakeSnapshots(ROOT.CombinedMeasurement.nominal, ROOT.kTRUE)

# Save the combined workspace
combined.writeToFile(oFileName);

# Print useful information like the correlation scheme, re-namings, etc.
#combined.Print()


#code from here to the end is vestigial
'''corrParams = [#"alpha_XS_Top",
              #"alpha_XS_Wjets",
              #"alpha_XS_Top_LP", 
              #"alpha_XS_Wjets_LP",
              'alpha_XS_Dibosons',
              'alpha_XS_SingleTop',
              'alpha_XS_Zjets',
              'alpha_Signal_ISR_FSR',
              'alpha_LumiNP',
              'alpha_MUON_EFF_STAT',
              'alpha_MUON_EFF_SYS',
              'alpha_MUON_EFF_STAT_LOWPT',
              'alpha_MUON_EFF_SYS_LOWPT',
              'alpha_MUON_ISO_STAT',
              'alpha_MUON_ISO_SYS',
              'alpha_MUONS_ID',
              'alpha_MUONS_MS',
              'alpha_MUONS_SCALE',
              'alpha_MUON_SAGITTA_RESBIAS',
              'alpha_MUON_SAGITTA_RHO',
              'alpha_MUON_TTVA_STAT',
              'alpha_MUON_TTVA_SYS',
              'alpha_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',
              'alpha_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
              'alpha_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
              'alpha_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',
              'alpha_EG_RESOLUTION_ALL',
              'alpha_EG_SCALE_ALL',
              'alpha_MET_SoftTrk_Scale',
              'alpha_MET_SoftTrk_ResoPara',
              'alpha_MET_SoftTrk_ResoPerp',
              'alpha_METTrigStat',
              'alpha_METTrigTop',
              'alpha_JET_21NP_JET_BJES_Response',
              'alpha_JET_21NP_JET_EffectiveNP_1',
              'alpha_JET_21NP_JET_EffectiveNP_2',
              'alpha_JET_21NP_JET_EffectiveNP_3',
              'alpha_JET_21NP_JET_EffectiveNP_4',
              'alpha_JET_21NP_JET_EffectiveNP_5',
              'alpha_JET_21NP_JET_EffectiveNP_6',
              'alpha_JET_21NP_JET_EffectiveNP_7',
              'alpha_JET_21NP_JET_EffectiveNP_8restTerm',
              'alpha_JET_21NP_JET_EtaIntercalibration_Modelling',
              'alpha_JET_21NP_JET_EtaIntercalibration_NonClosure',
              'alpha_JET_21NP_JET_EtaIntercalibration_TotalStat',
              'alpha_JET_21NP_JET_Flavor_Composition',
              'alpha_JET_21NP_JET_Flavor_Response',
              'alpha_JET_21NP_JET_Pileup_OffsetMu',
              'alpha_JET_21NP_JET_Pileup_OffsetNPV',
              'alpha_JET_21NP_JET_Pileup_PtTerm',
              'alpha_JET_21NP_JET_Pileup_RhoTopology',
              'alpha_JET_21NP_JET_PunchThrough_MC15',
              'alpha_JET_21NP_JET_SingleParticle_HighPt',
              'alpha_JET_JER_SINGLE_NP',
              'alpha_JET_JvtEfficiency',
              'alpha_FATJET_Comb_Baseline_Kin',
              'alpha_FATJET_Comb_Modelling_Kin',
              'alpha_FATJET_Comb_TotalStat_Kin',
              'alpha_FATJET_Comb_Tracking_Kin',
              'alpha_FATJET_D2R',
              'alpha_FATJET_JER',
              'alpha_FATJET_JMR',
              'alpha_PRW_DATASF',
              'alpha_WjetsModeling_Scale',
              'alpha_WjetsModeling_AlphaS',
              'alpha_WjetsModeling_MadGraph',
              'alpha_WjetsModeling_CKKW15',
              'alpha_WjetsModeling_CKKW30',
              'alpha_WjetsModeling_PDF',
              'alpha_TopModeling_Rad',
              'alpha_TopModeling_MCatNLO',
              'alpha_TopModeling_Herwig'
                          ]

for iPar in corrParams:
    corrString = "fileOne::"+iPar+",fileTwo::"+iPar+"fileThree::"+iPar
    outString = iPar
    outString = outString[6:]
#    correlation.CorrelateParameter(corrString,outString);'''

