using namespace std;
using namespace RooFit;
using namespace RooStats;

void GetCorrMatrix(){

  gStyle->SetOptStat(0);

  RooWorkspace *w   = (RooWorkspace*) _file0->Get("ws_600p000"); //FIXME
  w->Print();
  RooStats::ModelConfig *mc = (RooStats::ModelConfig*) w->obj("ModelConfig");
  RooRealVar * firstPOI = dynamic_cast<RooRealVar*>(mc->GetParametersOfInterest()->first());
  firstPOI->setVal(0.0);
  
  RooSimultaneous *simPdf = (RooSimultaneous*)(mc->GetPdf());
  RooAbsData * data = w->data("obsData");
  
  RooCategory* channelCat = (RooCategory*) (&simPdf->indexCat());
  TIterator *iter = channelCat->typeIterator() ;
  RooCatType *tt  = NULL;
  
  while((tt=(RooCatType*) iter->Next()) ){
    cout << "category : " << tt->GetName() << " " << endl;
    string ttname = tt->GetName();
    if( ttname == "SRWW_Resolved_fileTwo" ){ //FIXME
      RooAbsPdf  *pdftmp  = simPdf->getPdf( tt->GetName() );
      RooArgSet  *obstmp  = pdftmp->getObservables( *mc->GetObservables() ) ;
      RooAbsData *datatmp = data->reduce(Form("%s==%s::%s",channelCat->GetName(),channelCat->GetName(),tt->GetName()));
      RooRealVar *obs = ((RooRealVar*) obstmp->first());
      
      ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
      
      int status = -99;
      int HessStatus = -99;
      double Edm = -99;
      
      RooArgSet* constrainedParams = pdftmp->getParameters(*data);
      const RooArgSet* glbObs = mc->GetGlobalObservables();
      
      RooRealVar * poi = (RooRealVar*) mc->GetParametersOfInterest()->first();
      cout << "Constatnt POI " << poi->isConstant() << endl;
      cout << "Value of POI  " << poi->getVal() << endl;
      
      RooStats::RemoveConstantParameters(constrainedParams);
      RooFit::Constrain(*constrainedParams);
      
      RooAbsReal * nll = pdftmp->createNLL(*datatmp, Constrain(*constrainedParams), GlobalObservables(*glbObs), Offset(1) );
      double nllval = nll->getVal();
      
      std::cout << "initial parameters" << std::endl;
      constrainedParams->Print("v");
      
      std::cout << "INITIAL NLL = " << nllval << std::endl;
      
      static int nrItr = 0;
      int maxRetries = 3;
      ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
      int strat = ROOT::Math::MinimizerOptions::DefaultStrategy();
      int save_strat = strat;
      RooMinimizer minim(*nll);
      minim.setStrategy(strat);
      minim.setPrintLevel(1);
      minim.setEps(1);
      
      status=-99;
      HessStatus=-99;
      Edm = -99;
      RooFitResult * r;
      
      ROOT::Math::MinimizerOptions::SetDefaultStrategy(save_strat);
      status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
      HessStatus= minim.hesse();

      r = minim.save();

      TCanvas *c = new TCanvas( ttname.c_str(), ttname.c_str(), 1200, 1000 );
      c->SetLeftMargin(0.24);
      r->correlationHist()->Draw("colz");
      c->Print( Form( "%s.eps", ttname.c_str() ) );
      
    }

  }

  return;

}


