#!/bin/bash
releaseDir='../Run/test_run'
analysis='VVlvqq_mc15_v2016'

#masses=(500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2200 2400 2600 2800 3000 3500 4000 4500 5000 )
masses=( 500 )
#srs=("HVTWW" "HVTWZ" "RSGWW" "ggHWWNWA") 
srs=("VBF")

#Loop over the masses
for sr in "${srs[@]}"
do
	for m1 in "${masses[@]}"
	do
    wsdir=$releaseDir"/"$analysis"/ws"
		python lvqq_combine_fast.py $wsdir $sr $m1  
    dout="../ws_combined/"$sr
		fout="ws_HPLP_"$m1".root" 
    mkdir -p $dout
		mv output.root $dout"/"$fout
	done
done
