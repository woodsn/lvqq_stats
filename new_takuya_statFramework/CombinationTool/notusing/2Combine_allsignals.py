 #!/usr/bin/env python
import condor, time, os, commands,sys


#signal=sys.argv[1]
#selection=sys.argv[2]
#mkdirstring='mkdir ../ws_combined/March2/%s/'%signal
#print 'mkdirstring is: ' + mkdirstring
#os.system(mkdirstring)
#masses =("500","600","700","800","900","1000","1200","1400")
#masses =("700","800")
#signals = ("ggHWWNWA", "VBFWWNWA", "HVTWW", "HVTWZ", "RSGWW")
signals=("VBF-HVTWZ", "VBF-HVTWW")
masses_higgs=("1600","1800","2000","2200","2400","2600","2800","3000")
#masses_nothiggs = ("1500","1600","1700","1800","1900","2000","2200", "2400", "2600", "2800", "3000","3500","4000","4500","5000" )
masses_nothiggs = ("1500","1600")
#masses =("600","700","800", "900", "1000", "1200", "1400")
#masses =("1000","1200")

directory_boosted='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Ryne_workspaces_april10/ws'





for s in signals:
	if s == "ggHWWNWA":
		selection = "RES"
		binning = "ggF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'*')
			file2 = commands.getoutput('ls ' + directory_boosted +'/'+ s +'_LP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'*')
			
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			runstring='python ryne_lvqq_combine_fast.py %s %s'%(file1,file2)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBFWWNWA":
		selection = "RESVBF"
		binning = "VBF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'*')
			file2 = commands.getoutput('ls ' + directory_boosted +'/'+ s +'_LP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'*')
			
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			runstring='python ryne_lvqq_combine_fast.py %s %s'%(file1,file2)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "HVTWW" or s == "HVTWZ" or s == "RSGWW":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + s + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'*')
			file2 = commands.getoutput('ls ' + directory_boosted +'/'+ s +'_LP/'+binning+'/VVlvqq_mc15_v2016_'+s+m+'*')
			
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			runstring='python ryne_lvqq_combine_fast.py %s %s'%(file1,file2)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBF-HVTWW":
		selection = "RESVBF"
		binning = "VBF"
		string = "VBF_HVTWW"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + string + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+string+m+'*')
			file2 = commands.getoutput('ls ' + directory_boosted +'/'+ string +'_LP/'+binning+'/VVlvqq_mc15_v2016_'+string+m+'*')
			
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			runstring='python ryne_lvqq_combine_fast.py %s %s'%(file1,file2)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBF-HVTWZ":
		selection = "RESVBF"
		binning = "VBF"
		string2 = "VBF_HVTWZ"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'/' + string2 + '_HP/'+binning+'/VVlvqq_mc15_v2016_'+string2+m+'*')
			file2 = commands.getoutput('ls ' + directory_boosted +'/'+ string2 +'_LP/'+binning+'/VVlvqq_mc15_v2016_'+string2+m+'*')
			
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			runstring='python ryne_lvqq_combine_fast.py %s %s'%(file1,file2)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)







