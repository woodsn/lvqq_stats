import condor, time, os, commands,sys


#signals = ("ggHWWNWA", "HVTWW", "HVTWZ", "RSGWW", "RSGWW-SmReW")
#signals = ("VBF-HVTWW", "VBF-HVTWZ")
signals = ("VBF-HVTWW", "VBF-HVTWZ", "VBFWWNWA") 
#signals = ("ggHWWNWA","HVTWW", "HVTWZ", "RSGWW", "VBFWWNWA", "VBF-HVTWW","VBF-HVTWZ", "RSGWW-SmReW")
#masses_higgs = ("12700","1000")
#masses_nothiggs=("1000","11000")
masses_higgs =("500", "600","700","800", "900", "1000", "1200", "1400")
masses_nothiggs =("500", "600","700","800", "900", "1000","1100", "1200", "1300","1400")
#mass_nothiggs =("1000","198")
outputdir='june16'
directory_resolved='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_v28_final/VBF2/'
#directory_boosted='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/takuto_boosted_june5/'
directory_boosted='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_final2/VBF/'

for s in signals:
	if s == "ggHWWNWA":
		selection = "RES"
		binning = "ggF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBFWWNWA":
		selection = "RESVBF"
		binning = "VBF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "HVTWW" or s == "HVTWZ":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "RSGWW" or s == "RSGWW-SmReW":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)

	if s == "VBF-HVTWW":
		selection = "RESVBF"
		binning = "VBF"
		string = "VBF-HVTWW"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBF-HVTWZ":
		selection = "RESVBF"
		binning = "VBF"
		string2 = "VBF-HVTWZ"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)




