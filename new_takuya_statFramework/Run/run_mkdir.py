#!/usr/bin/python


#___________________________________________________________________________
def createDirectoryStructure(input_dir, releaseDir, analysis, inputListTag, doCopy):
  import os
  import shutil
  import glob

  # create directories
  newdir = '{releaseDir}/{analysis}/data/{inputListTag}'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag)
  diagnosticsdir = '{releaseDir}/{analysis}/ws/diagnostics'.format(releaseDir=releaseDir, analysis=analysis ) 
  tmpdir = '{releaseDir}/{analysis}/tmp'.format(releaseDir=releaseDir, analysis=analysis)

  # first, we remove the previously existing directories
  dirs = [newdir, diagnosticsdir, tmpdir]
  for dir in dirs:
    if os.path.exists(dir):
      shutil.rmtree(dir)

  # we use makedirs, which takes care of creating intermediate directories
  os.makedirs(newdir)
  os.makedirs(diagnosticsdir)
  os.makedirs(tmpdir)
  
  # copy input trees from input location
  files = glob.glob(os.path.join(input_dir, '*.*'))
  ### commented out
  for file in files:
    if doCopy:
      print "Copying file: %s to %s"%(file,newdir)
      shutil.copy2(file, newdir)
   
   
#_________________________
if __name__ == '__main__':
 
  releaseDir   = './36p5ifb_v26_v01'
  analysis     = 'VVlvqq_mc15_v2016'
  inputListTag = 'itest01'
  doCopy       = False #FIXME  set to false if you don't want to copy the samples
  
  print "Creating directory structure"
  if doCopy:
    print "\nCopying inputs, may take a while, please be patient :)\n"
  createDirectoryStructure('/eos/atlas/unpledged/group-tokyo/users/tnobe/inputTree26_v01/', releaseDir, analysis, inputListTag, doCopy)

