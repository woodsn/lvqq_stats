import condor, time, os, commands,sys
timestr = time.strftime("%m%d_%H%M")

signal = sys.argv[1]
label = sys.argv[2]
#mass = sys.argv[3]

directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_combined/june14/%s/'%signal
files = commands.getoutput('ls ' + directory + 'ws_HPLP_1300.root')
files = files.split('\n')
print files
exe = 'run_limit_example.py'
argtemplate = ' %s'

for i, iFile in enumerate(files):
    files[i] = iFile
    dirname = label+ '_' + signal+ '_' + timestr
    print iFile
condor.run(exe,argtemplate,files, dirname, 1)

