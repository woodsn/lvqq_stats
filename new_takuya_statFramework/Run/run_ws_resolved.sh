#!/bin/bash

#MYPWD=`pwd`
source /export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/rcSetup.sh

signal=$1 #HVTWW RSGWW ggHWW VBFWW
mass=$2 # mass point e.g. 500
purity=( $3 ) # RES or RESVBF

for pr in "${purity[@]}"
do
	printf "\nProducing workspace for signal: "$signal"; mass: "$mass" GeV; purity: "$pr
	printf "\nLog stored in: log_ws_"$signal"_"$mass"_"$pr".txt\n" 
	python /export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/ResonanceFinder/python/lvqq_Resolved.py $signal $mass $pr | tee log_ws_${signal}_${mass}_${pr}.txt
done
