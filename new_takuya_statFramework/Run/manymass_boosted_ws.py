import condor, time, os, commands,sys
timestr = time.strftime("%m%d%Y_%H%M")

#signal = ["ggHWWNWA","HVTWW", "HVTWZ", "RSGWW","VBFWWNWA", "VBF_HVTWW", "VBF_HVTWZ"]
#signal = ["VBF-HVTWZ", "VBF-HVTWW"]
signal = ["HVTWW","HVTWZ"]
#mass = "700"
#mass_higgs = ["700","800"]
mass_higgs = ["300","400","500","600","700","800","900","1000","1200","1400"]
mass_HVT = ["300","400","500","600","700","800","900","1000","1100","1200","1300","1400","1500"]
mass_RSG = ["300","400","500","600","700","800","900","1000","1100","1200","1300","1400","1500"]
#purity = ["RES","RESVBF"]
purity = "RES"
purity2="RESVBF"
exe = 'run_ws_example.sh'
for s in signal:
	if s=="VBF-HVTWW" or s=="VBF-HVTWZ":
		for m in mass_HVT:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity2+timestr
			argtemplate = ' %s %s %s %s'%(s,m,purity2,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+purity2+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
	if s=="HVTWW" or s=="HVTWZ":
		for m in mass_HVT:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity+timestr
			argtemplate = ' %s %s %s'%(s,m,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
	if s == "RSGWW":
		for m in mass_RSG:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity+timestr
			argtemplate = ' %s %s %s %s'%(s,m,purity,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+purity+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
	if s == "ggHWWNWA":
		for m in mass_higgs:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity+timestr
			argtemplate = ' %s %s %s %s'%(s,m,purity,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+purity+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
	if s =="VBFWWNWA":
		for m in mass_higgs:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity2+timestr
			argtemplate = ' %s %s %s %s'%(s,m,purity2,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+purity2+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
