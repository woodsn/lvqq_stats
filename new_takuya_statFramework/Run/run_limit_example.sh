#!/bin/bash

signal="HVTWW"
mass="500"
ws="../ws_combined"

printf "\nProducing Limits for signal: "$signal"; mass: "$mass" GeV"
printf "\nLog stored in: log_limit_"$signal"_"$mass".txt\n" 
python getLimit.py $signal $mass $ws > log_limit_${signal}_${mass}.txt
