#!/bin/bash

source /export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/rcSetup.sh

signal=$1
mass=$2
#signal="ggHWWNWA"
#mass="700"
purity=( "HP" "LP" )

for pr in "${purity[@]}"
do
	printf "\nProducing workspace for signal: "$signal"; mass: "$mass" GeV; purity: "$pr
	printf "\nLog stored in: log_ws_"$signal"_"$mass"_"$pr".txt\n" 
	python /export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ResonanceFinder/python/lvqq_Main.py $signal $mass $pr >& log_ws_${signal}_${mass}_${pr}.txt
done
