// Root includes
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TAxis.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "Math/DistFunc.h"
#include "utils/AtlasLabels.C"
#include "utils/AtlasStyle.C"
// general includes
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <utility>
#include "stdio.h"
#include "stdlib.h"

// configuration
const bool debug     = true;
bool       doBlind   = false;  // blinding?
bool       doLims    = true; // run limits
bool       dop0      = false;  // make p0 plots
bool       doSens    = false; // compare sensitivity
int        purity    = 2;     // 0: lp 1: hp 2: combined
int        prod      = 0;     // 0: ggF 1: VBF 2:comb
double     gBr       = 0.439109;  // branching ratio (WW->lvqq; 2*W->lv*W->qq)
double     lumi      = 36.074566144;  // fb
int        npoints   = -1;
char*      pintern   = (char*)"Internal";

std::string srs[8]        = {"HVTWW","HVTWZ","RSGWW","VBFWWNWA","ggHWWNWA","RSGWW_ReWeight","VBF_HVTWW","VBF_HVTWZ"};
std::string purity_reg[3] = {"LP","HP","LPHP"};
std::string prod_reg[3]   = {"ggF","VBF","comb"};
std::map<std::string, std::string> x_title;
std::map<std::string, std::string> y_title;
std::map<std::string, std::vector<std::string> > xs_names;
std::map<std::string, std::vector<std::string> > leg_title;

// global
std::string      line;
std::string      name;
std::string      foldername;
std::vector<int> vmass; //was int
std::vector<double> dvmass; //was int
double *masspoint, *observed, *expected;
double *expectedp1, *expectedp2, *expectedm1; 
double *expectedm2, *zero, *prediction; 
double *temp_exp, *temp_exp2, *temp_exp3;
double *pval;

// functions
void runSensitivity  (std::string sig_reg);
void runLimits       (std::string sig_reg, std::string path);
void run_p0          (std::string sig_reg);
TGraph* getPrediction   (std::string sig_reg, int model_n=0);
void ReadInputMasses ();
bool set_Br          (std::string sig_reg);
void set_titles      ();
void dump_config     (std::string sig_reg);

//______________________________________
// Input the signal region:
//  sig_reg= HVTWW, HVTWZ, RSGWW, VBFWWNWA, ggHWWNWA
// Purity:
//  pr= 0 (LP), 1 (HP), 2 (LPHP)
// Production type: 
//  pd= 0 (ggF), 1 (VBF), 2 (comb)
int ryne_LimitPlotCombBR(std::string sig_reg = "All", std::string path = "setme",
                    int              pr = purity, 
                    int              pd = prod,
                    bool             dL = doLims,
                    bool             dP = dop0,
                    bool             dS = doSens,
                    bool             dB = doBlind){
  
  // Dump config
  purity  = pr;
  prod    = pd;
  doLims  = dL;
  dop0    = dP;
  doSens  = dS;
  doBlind = dB;
  dump_config(sig_reg);
  set_titles();

  // check if valid sr specified
  if( sig_reg.find("HVTWW")    != std::string::npos ||
      sig_reg.find("HVTWZ")    != std::string::npos ||
      sig_reg.find("ggHWWNWA") != std::string::npos || 
      sig_reg.find("VBFWWNWA") != std::string::npos || 
      sig_reg.find("RSGWW")    != std::string::npos ||
      sig_reg.find("RSGWW_ReWeight")  != std::string::npos ||
      sig_reg.find("VBF_HVTWW")!= std::string::npos ||
      sig_reg.find("VBF_HVTWZ")!= std::string::npos){
    
    // Set branching ratio
    if(!set_Br(sig_reg))
      return EXIT_FAILURE;

    if( doSens )
      runSensitivity( sig_reg );
    if( doLims )
      runLimits( sig_reg, path );
    if( dop0)
      run_p0( sig_reg);
  
  } // if all, loop over srs
  else if( sig_reg.find("All") != std::string::npos ){
    
    for( unsigned int s=0; s<7; s++){
      sig_reg = srs[s];
      // Set Branching ratio
      if(!set_Br(sig_reg))
        return EXIT_FAILURE;

      if( doSens )
        runSensitivity( sig_reg );
      if( doLims )
        runLimits( sig_reg, path ); 
      if( dop0 )
        run_p0( sig_reg );
    }
  } // else wrong input
  else{
    std::cout << "Invalid signal region: " << sig_reg 
              << "\nPlease select one of the following: HVTWW, HVTWZ, RSGWW, ggHWWNWA, VBFWWNWA, VBF_HVTWW, VBF_HVTWZ"
              << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}


//___________________________________
// Plot p0 vs signal mass
//
void run_p0(std::string sig_reg){

  std::cout << "\nWorking on signal region: " << sig_reg << std::endl;

  // Folder to read from (limits)
//  std::string f_base = "/eos/atlas/user/r/rcarbone/StatFramework/limits";
   std::string f_base = "/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/limits/";
  // input limit files
  foldername = Form("%s/%s/%s",f_base.data(),sig_reg.data(),prod_reg[prod].data());
  if( debug ) 
    std::cout << "Reading from folder: " << foldername << std::endl;

  //Read masses and sort vector
  std::cout << "Reading input masses" << std::endl;
  vmass.clear();
  ReadInputMasses();
  std::sort( vmass.begin(), vmass.end() );
  pval = new double[npoints];

  std::cout << "Mass\tP-value\n" 
            << "========================="
            << std::endl << std::endl;

  // loop over mass points
  for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){

    // open mass point
    TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
    if( !file ){
      std::cerr << "file : " << vmass.at(j0) 
                << " is missing " << std::endl;
      exit(6);
    }

    // read the tree 
    TTree *tree = (TTree*)file->Get( "stats" );
    tree->GetEntry(0);

    // catch limit if 0 
    //if( tree->GetLeaf("null_pvalue")->GetValue() == 0 ){  // FIXME
    if( tree->GetLeaf("pb_obs")->GetValue() == 0 ){
      std::cout << "mass point " << vmass.at(j0)
                << " will not be used" << std::endl;
      continue;
    }

    // save values
    masspoint[j0]   = vmass.at(j0); //natasha added
    //pval[j0]        = tree->GetLeaf("null_pvalue")->GetValue() ; //  / gBr; // FIXME
    pval[j0]        = tree->GetLeaf("pb_obs")->GetValue() ; //  / gBr;
    zero[j0]        = 0.0;
    
    std::cout << masspoint[j0] << "\t" << pval[j0] << std::endl;

  }// end loop over file

  // Declare p0 plot
  TCanvas *c1 = new TCanvas( Form("c_p0_%s",sig_reg.c_str()), Form("p0_%s",sig_reg.c_str()), 800, 700);
  TGraphAsymmErrors *g  = new TGraphAsymmErrors(npoints,masspoint,pval,zero,zero,zero,zero);
  g->SetNameTitle(Form("g_p0_%s",sig_reg.c_str()), "p0");

  // Set min/max
  double maxLimit = 1000;
  double minLimit = 1 - ROOT::Math::gaussian_cdf(6); // six sigma
  g->SetMinimum(minLimit);
  g->SetMaximum(maxLimit);

  // Draw graph
  std::string xtitle = "m(VV) [TeV]";
  std::string  ytitle = "Local p_{0}";
  g->GetXaxis()->SetTitle(xtitle.c_str());
  //g->GetXaxis()->SetNdivisions(505);
  g->GetYaxis()->SetTitle(ytitle.c_str());
  g->GetYaxis()->SetNdivisions(505);
  g->GetXaxis()->SetRangeUser( vmass.at(0)-0.001, vmass.at(npoints-1)+0.001 );
  g->GetXaxis()->SetLabelSize(0.04);
  g->Draw("apl");

  c1->SetRightMargin(0.1);
  c1->Update();

  // Atlas Labels
  ATLASLabel2(0.22, 0.88, pintern, kBlack);
  myText( 0.66, 0.88, kBlack, Form("#scale[0.7]{Data 2015 + 2016}"));
  myText( 0.66, 0.82, kBlack, Form("#scale[0.6]{%s}",sig_reg.c_str()));
//  myText( 0.19, 0.82, kBlack, Form("#scale[0.7]{#int Ldt = %.1f fb^{-1}, #sqrt{s} = 13 TeV}", lumi));
  myText( 0.19, 0.82, kBlack, Form("#scale[0.7]{#sqrt{s} = 13 TeV, # %.1f fb^{-1}}", lumi));
  // draw sigma lines
  TLine l;
  l.SetLineWidth(2);
  l.SetLineStyle(2);
  l.SetLineColor(kBlack);
  //const Double_t xmin = g->GetXaxis()->GetXmin();
  //const Double_t xmax = g->GetXaxis()->GetXmax();
  const Double_t xmin = vmass.at(0)-0.001;
  const Double_t xmax = vmass.at(npoints-1)+0.001;
  l.DrawLine(xmin, 1, xmax, 1);
  l.SetLineColor(kRed);
  l.DrawLine(xmin, 1 - ROOT::Math::gaussian_cdf(0), xmax, 1 - ROOT::Math::gaussian_cdf(0));
  // Draw sigma labels
  TLatex t;
  const Double_t delta = (xmax - xmin) * 0.02;
  t.DrawLatex(xmax + delta, (1 - ROOT::Math::gaussian_cdf(0)) * 0.8, "#color[2]{0#sigma}");
  // Draw rest of sigma lines
  int n = 1;
  while (minLimit < 1 - ROOT::Math::gaussian_cdf(n)) {
    l.DrawLine(xmin, 1 - ROOT::Math::gaussian_cdf(n), xmax, 1 - ROOT::Math::gaussian_cdf(n));
    std::string cmd = Form("#color[2]{%d#sigma}", n);
    t.DrawLatex(xmax + delta, (1 - ROOT::Math::gaussian_cdf(n)) * 0.4, cmd.c_str());
    n++;
  }
  c1->SetLogy(1);

  // Save plot
  std::cout << "Saving plot to output/" << std::endl;
  c1->SaveAs(Form("output/VVM_p0_%s_%s.eps",sig_reg.data(),prod_reg[prod].data()));
  c1->SaveAs(Form("output/VVM_p0_%s_%s.png",sig_reg.data(),prod_reg[prod].data()));

  //delete c1;
}

//________________________________________
// Make sensitivity plot
//
void runSensitivity(std::string sig_reg){

  std::cout << "\nWorking on signal region: " << sig_reg << std::endl;

  // folders to compare FIXME change to where limits stored
  std::string f_base[1] = {"/eos/atlas/user/r/rcarbone/StatFramework/limits"};
     // , "/eos/atlas/user/r/rcarbone/StatFramework/old_limits"};
  //std::string special_folders[3] = {"/vbf_binning","","/vbf_binning"};  
  //std::string p_reg[3] = {"ggF","VBF","comb"};
  std::string special_folders[3] = {"","/jet","/vbf_binning"};  
  std::string p_reg[3] = {"ggF","ggF","comb"};
  // loop over folders
  // FIXME was 3
  for( int i=0; i<2; i++){

    // input limit files
    foldername = Form("%s/%s/%s%s",f_base[0].data(),sig_reg.data(),p_reg[i].data(),special_folders[i].data());
    if( debug ) 
      std::cout << "Reading from folder: " << foldername << std::endl;

    //Read masses and sort vector
    std::cout << "Reading input masses" << std::endl;
    vmass.clear();
    ReadInputMasses();
    std::sort( vmass.begin(), vmass.end() );
    if( i==0 ) 
      temp_exp  = new double[npoints];
    else if( i==1 )      
      temp_exp2 = new double[npoints];
    else
      temp_exp3 = new double[npoints];
    
    // loop over mass points
    for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){

      // open mass point
      TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
      if( !file ){
        std::cerr << "file : " << vmass.at(j0) 
                  << " is missing " << std::endl;
        exit(6);
      }

      // read the tree 
      TTree *tree = (TTree*)file->Get( "stats" );
      tree->GetEntry(0);

      // catch limit if 0 
      if( tree->GetLeaf("exp_upperlimit")->GetValue() == 0 ){
        std::cout << "mass point " << vmass.at(j0)
                  << " will not be used" << std::endl;
        continue;
      }

      // save values
      masspoint[j0]     = vmass.at(j0);
      if( i==0) 
        temp_exp[j0]    = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
      else if (i==1) 
        temp_exp2[j0]   = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
      else  
        temp_exp3[j0]   = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
      zero[j0] = 0.0;

    }// end loop over files

  } // end loop over inputs
  
  // print limits
  if( debug ){
    std::cout << "Mass\t" << "Expected ggF\t" << "Expected VBF\t" << "Expected comb\n"  
      << "===============================================================" 
      << std::endl << std::endl;
    for( int ii=0; ii<npoints; ii++){
      std::cout << masspoint[ii] << std::setprecision(7) << std::fixed 
                << "\t" << temp_exp[ii] << "\t" << temp_exp2[ii]
                //<< "\t" << temp_exp3[ii] 
                << std::resetiosflags(ios::fixed) << std::endl;
    }
  }
  
  // Plot
  std::cout << "Making Plots" << std::endl;
  
  // expected tgraph
  TGraphAsymmErrors *grexpected = new TGraphAsymmErrors(npoints,masspoint,temp_exp,zero,zero,zero,zero);
  grexpected->SetLineWidth(2.0);
  grexpected->SetLineStyle(9);
  grexpected->SetLineColor(kMagenta+10);
  
  // expected2 tgraph
  TGraphAsymmErrors *grexpected2 = new TGraphAsymmErrors(npoints,masspoint,temp_exp2,zero,zero,zero,zero);
  grexpected2->SetLineWidth(2.0);
  grexpected2->SetLineStyle(9);
  grexpected2->SetLineColor(kSpring-7);
  
  // expected3 tgraph
  // FIXME commented out for now
  //TGraphAsymmErrors *grexpected3 = new TGraphAsymmErrors(npoints,masspoint,temp_exp3,zero,zero,zero,zero);
  //grexpected3->SetLineWidth(2.0);
  //grexpected3->SetLineStyle(9);
  //grexpected3->SetLineColor(kBlue);

  // Prediction
  TGraph *g_prediction = NULL;
  //g_prediction = getPrediction(sig_reg.data());
  //g_prediction->SetLineColor(kBlack);
  //g_prediction->SetLineWidth(2);

  // Put on a canvas
  TCanvas *MyC = new TCanvas("Sensitivity","Sensitivity",800,700);
  gPad->SetLogy();

  // Plot tgraphs together
  TMultiGraph *sens = new TMultiGraph("","; m_{Scalar} [TeV]; #sigma(qq#rightarrow H#rightarrow WW) [pb]");
  sens->Add(grexpected, "L");       // expected line
  sens->Add(grexpected2, "L");       // expected line
  //sens->Add(grexpected3, "L");       // expected line
  if(g_prediction)
    sens->Add(g_prediction, "C");     // predictions

  sens->Draw("AP");
  sens->GetXaxis()->SetRangeUser( vmass.at(0)-0.001, vmass.at(npoints-1)+0.001 );
  sens->GetYaxis()->SetRangeUser( 2.e-4, 5.e+1 );
  sens->GetXaxis()->SetLabelSize(0.04);
  MyC->Update();
  gPad->RedrawAxis();

  // Atlas Labels
  ATLASLabel2(0.22, 0.88, pintern, kBlack);
  myText( 0.19, 0.82, kBlack, Form("#scale[0.7]{#int Ldt = %.1f fb^{-1}, #sqrt{s} = 13 TeV}", lumi));
  myText( 0.22, 0.76, kBlack, Form("#scale[0.7]{Sensitivity Comparison}"));
  // Legend
  TLegend *legend = new TLegend(0.57,0.75,0.9,0.90);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetFillStyle(0);
  legend->SetTextSize(0.025);
  //FIXME put correct legend info here
  //legend->AddEntry(grexpected,"Expected (95\% CLs)","L");
  //legend->AddEntry(grexpected2,"Expected (95\% CLs) TA Mass","L");
  legend->AddEntry(grexpected,"Expected (95\% CLs) Current","L");
  legend->AddEntry(grexpected2,"Expected (95\% CLs) new Jet Uncertainties","L");
  //legend->AddEntry(grexpected3,"Expected (95\% CLs) combined","L");

  if(g_prediction){
    if( foldername.find("HVTWW") < foldername.length() )
      legend->AddEntry(g_prediction, "HVT Z'#rightarrowWW", "L" );
    if( foldername.find("HVTWZ") < foldername.length() )
      legend->AddEntry(g_prediction, "HVT W64#rightarrowWZ", "L" );
    if( foldername.find("RSGWW") < foldername.length() )
      legend->AddEntry(g_prediction, "RS G*#rightarrowWW", "L" );
    if( foldername.find("ggH") < foldername.length() )
      legend->AddEntry(g_prediction, "ggF H#rightarrowWW", "L" );
    if( foldername.find("VBF") < foldername.length() )
      legend->AddEntry(g_prediction, "VBF qq#rightarrowH#rightarrowWW", "L" );
  }
  legend->Draw();

  //Save
  std::cout << "Saving plot in output/" << std::endl;
  MyC->Print( Form("output/sensitivity_%s.eps", sig_reg.data() ) );
  MyC->Print( Form("output/sensitivity_%s.png", sig_reg.data() ) );

}


//__________________________________
// Make plot for specific sr
// To compare two predictions: 
//  > add loop and add tgraph for each prediction
void runLimits(std::string sig_reg, std::string path){

  std::cout << "\nWorking on signal region: " << sig_reg << std::endl;
  
  // Find input folder
  std::string purity_folder;
  if     (purity == 0 ) purity_folder = "lp_limits";
  else if(purity == 1 ) purity_folder = "hp_limits";
  else if(purity == 2 ) purity_folder = "limits";
  else  std::cout << "Invalid purity: " << purity << " (Choose 0(LP), 1(HP) or 2(HP/LP))" << std::endl;
  
  //FIXME
  foldername = Form("/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/limits/%s/", path.data());
  //foldername = Form("/eos/atlas/user/r/rcarbone/StatFramework/ws/%s_LP/%s/",sig_reg.data(),prod_reg[prod].data()); // From workspace, not limits (for quick hp/lp)

  if( debug ) 
    std::cout << "Reading from folder: " << foldername << std::endl;

  //Read masses and sort vector
  std::cout << "Reading input masses" << std::endl;
  vmass.clear();
  dvmass.clear();
  ReadInputMasses();
  std::sort( vmass.begin(), vmass.end() );
  
  if( debug ){
    std::cout << "Mass\t" 
              << "Observed\t"    << "Expecteed\t" 
              << "Expected m2\t" << "Expected m1\t"
              << "Expected p1\t" << "Expected p2" << std::endl;
    std::cout << "=====================================================" 
              << "====================================================="  
              << std::endl << std::endl;
  }
  // loop over mass points
  for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){
    
    // open mass point
    TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
    if( !file ){
      std::cerr << "file : " << vmass.at(j0) 
                << " is missing " << std::endl;
      exit(6);
    }
   std::cout << "reading tree " << std::endl;  
    // read the tree 
    TTree *tree = (TTree*)file->Get( "stats" );
    tree->GetEntry(0);
    
    // catch limit if 0 
    if( tree->GetLeaf("exp_upperlimit")->GetValue() == 0 ){
      std::cout << "mass point " << vmass.at(j0)
                << " will not be used" << std::endl;
      continue;
    }
    
    // save values
    masspoint[j0]  = double(double(vmass.at(j0))/double(1000)); //natasha added
    dvmass.push_back(masspoint[j0]);
    observed[j0]   = tree->GetLeaf("obs_upperlimit")->GetValue() / gBr;
    expected[j0]   = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
    expectedp1[j0] = tree->GetLeaf("exp_upperlimit_plus1")->GetValue() / gBr - expected[j0];
    expectedp2[j0] = tree->GetLeaf("exp_upperlimit_plus2")->GetValue() / gBr - expected[j0]; 
    expectedm1[j0] = expected[j0] - tree->GetLeaf("exp_upperlimit_minus1")->GetValue()/ gBr;
    expectedm2[j0] = expected[j0] - tree->GetLeaf("exp_upperlimit_minus2")->GetValue()/ gBr;
    zero[j0] = 0.0;
    
    // print limits
    if( debug ){
      std::cout << masspoint[j0] << std::setprecision(7) << std::fixed 
                << "\t" << observed[j0]   << "\t" << expected[j0] << "\t" 
                << expectedm2[j0] << "\t" << expectedm1[j0] << "\t"
                << expectedp1[j0] << "\t" << expectedp2[j0] << std::resetiosflags(ios::fixed) << std::endl;
    }
  }// end loop over files
std::cout << "done reading tree" << std::endl;


  // Plot
  std::cout << "Making Plots" << std::endl;
  // expected tgraph
  TGraphAsymmErrors *grexpected = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,zero,zero);
  grexpected->SetLineWidth(2.0);
  grexpected->SetLineStyle(2);
  grexpected->SetLineColor(1);
std::cout << "Expected created" << std::endl;
  // Save output
  //TFile * outfile = new TFile( Form("output/%s_%s_%s.root",sig_reg.data(),purity_reg[purity].data(),prod_reg[prod].data()), "RECREATE" );
  //grexpected->Write();
  //outfile->Close();
  
  // observed tgraph
  TGraphAsymmErrors *grobserved = new TGraphAsymmErrors(npoints,masspoint,observed,zero,zero,zero,zero);
  grobserved->SetMarkerStyle(20);
  grobserved->SetMarkerSize(1.0);
  grobserved->SetLineStyle(1);
  grobserved->SetLineColor(1);
  grobserved->SetLineWidth(2.0);
std::cout << "Observed created" << std::endl;
  // set 1 and 2 sigma bands on expected limits
  TGraphAsymmErrors *grexpected1sigma = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,expectedm1,expectedp1);
  TGraphAsymmErrors *grexpected2sigma = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,expectedm2,expectedp2);
  grexpected1sigma->SetFillColor(kGreen);
  grexpected2sigma->SetFillColor(kYellow);
std::cout << "error bands created" << std::endl;
  // Prediction
  TGraph *g_prediction = NULL; 
  if(sig_reg.find("VBF") == std::string::npos &&
     sig_reg.find("ggH") == std::string::npos ){
    std::cout << "Not VBF or ggH" << std::endl;
    g_prediction = getPrediction(sig_reg);
    g_prediction->SetLineColor(kRed+2);
    g_prediction->SetLineWidth(2.5);
  }
std::cout << "prediction 1 created" << std::endl;
  TGraph *g_prediction2 = NULL;
  if( sig_reg.find("VBF") == std::string::npos &&
      sig_reg.find("HVT") != std::string::npos ){ //||
     //sig_reg.find("RSG") != std::string::npos ||
     //sig_reg.find("ggH") != std::string::npos){
    g_prediction2 = getPrediction(sig_reg, 1);
    g_prediction2->SetLineColor(kBlue);
    g_prediction2->SetLineWidth(2.5);
  }

if(g_prediction2)g_prediction->SetLineStyle(kDashed);    
std::cout << "prediction 2 created" << std::endl;
  // Put on a canvas
  TCanvas *MyC = new TCanvas("Limit","Limit",1000,700);
  gPad->SetLogy();

  // Plot tgraphs together
std::cout << "at multigraph" << std::endl;
  TMultiGraph *likelihd_limit_0j = new TMultiGraph("",Form(";%s;%s",x_title.at(sig_reg).data(),y_title.at(sig_reg).data()));
std::cout << "after multigraphs" << std::endl;
  likelihd_limit_0j->Add(grexpected2sigma,"E3"); // 2 sigma band
  likelihd_limit_0j->Add(grexpected1sigma,"E3"); // 1 sigma band
  likelihd_limit_0j->Add(grexpected, "L");       // expected line
std::cout << "added expected to multigraphs" << std::endl;
  if(g_prediction)
    likelihd_limit_0j->Add(g_prediction, "C");     // prediction
  if(g_prediction2)
    likelihd_limit_0j->Add(g_prediction2, "C");     // prediction 2
std::cout << "added prediction to multigraphs" << std::endl;
  if( !doBlind )
    likelihd_limit_0j->Add(grobserved, "PL");    // data
std::cout << "added observed to multigraphs" << std::endl;
  likelihd_limit_0j->Draw("AP");
std::cout << "drew limits" << std::endl;
  //likelihd_limit_0j->GetXaxis()->SetRangeUser( vmass.at(0), vmass.at(npoints-1) );
  likelihd_limit_0j->GetXaxis()->SetRangeUser( 0.3, dvmass.at(npoints-1));
std::cout << "set x-axis range" << std::endl;
  likelihd_limit_0j->GetYaxis()->SetRangeUser( 2.e-4, 100.e+1 );
  likelihd_limit_0j->GetXaxis()->SetLabelSize(0.06); //was 0.04
  likelihd_limit_0j->GetYaxis()->SetLabelSize(0.06); //was 0.04
  likelihd_limit_0j->GetXaxis()->SetTitleSize(0.06); //was 0.04
  likelihd_limit_0j->GetXaxis()->SetTitleSize(0.06); //was 0.04
  likelihd_limit_0j->GetYaxis()->SetTitleSize(0.06); //was 0.04
  likelihd_limit_0j->GetYaxis()->SetTitleOffset(0.95); //was 0.04
  likelihd_limit_0j->GetXaxis()->SetTitleOffset(0.95); //was 0.04
  //likelihd_limit_0j->GetXaxis()->SetNdivisions(505);
  MyC->Update();
  MyC->SetTickx();
  MyC->SetTicky();
  gPad->RedrawAxis();
  gPad->SetBottomMargin(0.15);
  gPad->SetLeftMargin(0.13);
  //gPad->SetTopMargin(0.05);
  gPad->SetRightMargin(0.02);
  gPad->SetTopMargin(0.05);
  MyC->Update();
 //
std::cout << "Multigraph created" << std::endl;
  // Atlas Labels
  ATLASLabel2(0.18, 0.88, pintern, kBlack);
//  myText( 0.15, 0.79, kBlack, Form("#scale[0.7]{#int Ldt = %.1f fb^{-1}, #sqrt{s} = 13 TeV}", lumi));
  myText( 0.18, 0.83, kBlack, Form("#scale[1.0]{#sqrt{s} = 13 TeV, %.1f fb^{-1}}", lumi));
  TString anotherregionlabel;
  if(sig_reg.find("VBF")    != std::string::npos)myText( 0.18, 0.78, kBlack, "VBF Region");
  else myText( 0.18, 0.78, kBlack, "ggF Region");

//  myText( 0.15, 0.7, kBlack, anotherregionlabel);
  if(purity < 2){
    std::string p = (purity == 0 ? "Low Purity" : "High Purity");
    myText(0.47, 0.45, kRed, Form("#scale[0.7]{%s Region}",p.data()));
  }
  std::cout << "right before legend" << std::endl; 
  // Legend
  TLegend *legend = new TLegend(0.500,0.55,0.97,0.92);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetFillStyle(0);
  if( !doBlind )legend->AddEntry(grobserved,"Observed 95\% CL upper limit","PL");
  legend->AddEntry(grexpected,"Expected 95\% CL upper limit","L");
  legend->AddEntry(grexpected1sigma, "Expected limit (#pm 1#sigma)","F");
  legend->AddEntry(grexpected2sigma, "Expected limit (#pm 2#sigma)","F");
  if(g_prediction)
    legend->AddEntry(g_prediction, leg_title.at(sig_reg)[0].data(), "L" );
  if(g_prediction2)
    legend->AddEntry(g_prediction2, leg_title.at(sig_reg)[1].data(), "L" );
  
  legend->Draw();
  
  //Save
  std::cout << "Saving plot in output/" << std::endl;
  MyC->Print( Form("output/%s_%s_%s.eps",sig_reg.c_str(),purity_reg[purity].data(),prod_reg[prod].data() ) );
  MyC->Print( Form("output/%s_%s_%s.png",sig_reg.c_str(),purity_reg[purity].data(),prod_reg[prod].data() ) );
  MyC->Print( Form("output/%s_%s_%s.C",sig_reg.c_str(),purity_reg[purity].data(),prod_reg[prod].data() ) );
  MyC->Print( Form("output/%s_%s_%s.pdf",sig_reg.c_str(),purity_reg[purity].data(),prod_reg[prod].data() ) );

}


//______________________________________
// Count mass points and save in vector
//
void ReadInputMasses(){
  
  int    length;
  int    point;
  std::string spoint;
  
  // Print root files to get list of mass points
  if( !system(NULL) ) return;
  else{
   int i = system( Form("ls %s/*.root > temp.txt", foldername.c_str() ) );
   if( debug ) 
     std::cout << "system result: " << i << std::endl;  
  }
  ifstream infile ("temp.txt");
  // Read mass from file names
  if( infile.is_open() ){
    while( infile.good() ){
      getline( infile,line );
      if( line != "" && line.find("VV") == std::string::npos ){
        length = line.length();
        spoint = line.substr( length-9, length-5 );
        point = atoi( spoint.c_str() );
        name = line.substr( 0, length-9 );
        if( point == 0 ){
          spoint = line.substr( length-8, length-5 );
          point = atoi( spoint.c_str() );
          name = line.substr( 0, length-8 );
        }
        vmass.push_back( point );
      }
    }
    infile.close();
  }
  else 
    cout << "Unable to open temp.txt";

  npoints    = vmass.size();  
  masspoint  = new double[npoints]; 
  observed   = new double[npoints];
  expected   = new double[npoints];
  expectedp1 = new double[npoints];
  expectedp2 = new double[npoints];
  expectedm1 = new double[npoints];
  expectedm2 = new double[npoints];
  zero       = new double[npoints];
  prediction = new double[npoints];
}


//
//________________________________________________________
TGraph* getPrediction(std::string sig_reg, int model_n){
  
  double              xs;
  std::string         s0;
  std::vector<double> mxs;
  std::vector<double> points;

  // Read xs from file
  std::string xsFile = Form("xs/%s", xs_names.at(sig_reg)[model_n].data() );
  ifstream Sig_file(xsFile.data());
  if(debug)
    std::cout << "Reading prediction XS from file: " << xsFile << std::endl;
  
  // Parse through lines
  if( Sig_file.is_open() ){
    std::string in_line; 
    double      mass_point;
    while( Sig_file >> mass_point >> s0 >> xs >> s0){
      points.push_back(mass_point/1000);//natasha added
      mxs.push_back( xs*1000.);     
      if( debug )
        std::cout << "Mass: " << mass_point << "  Prediciton: " << xs*1000 << std::endl;
    }
  }// done reading file
  else{
    std::cout << "Couldn't locate xs file: " << xs_names.at(sig_reg)[model_n] << std::endl;
  }

  // Make TGraph
  // note, referencing the first point of vector as an array, but compiled
  // code should store the vector in continuous memory
  TGraph *t_return = new TGraph(points.size(), &points[0], &mxs[0]);
  return t_return;  
}



//
//__________________________________
bool set_Br(std::string sig_reg){
  
  // Final state decay fractions
  double BrWlv = 0.1075 + 0.1057 + 0.1125;
  double BrWqq = 0.6741;
  double BrZqq = 0.6991;

  if( sig_reg.find("WW") != std::string::npos ){
    gBr = 2 * BrWlv * BrWqq; // 2 is symmetry factor
    std::cout << sig_reg << " has final state WW, using Br: " 
              << gBr << std::endl;
  }else if (sig_reg.find("WZ") != std::string::npos){
    gBr = BrZqq * BrWlv;
    std::cout << sig_reg << " has final state WZ, using Br: " 
              << gBr << std::endl;
  }else{
    std::cout << sig_reg << " final state unknown, please check spelling"
              << std::endl;
    return false;
  }

  return true;
}


//
//__________________________________
void set_titles(){
  
  // X axis titles
  x_title.insert(std::pair<std::string,std::string>("HVTWW","m(Z\264) [TeV]"));
  x_title.insert(std::pair<std::string,std::string>("HVTWZ","m(W\264) [TeV]"));
  x_title.insert(std::pair<std::string,std::string>("RSGWW","m(G_{KK}) [TeV]"));
  x_title.insert(std::pair<std::string,std::string>("ggHWWNWA","m_{Scalar} [TeV]"));
  x_title.insert(std::pair<std::string,std::string>("VBFWWNWA","m_{Scalar} [TeV]"));
  x_title.insert(std::pair<std::string,std::string>("VBF_HVTWW","m(Z\264) [TeV]"));
  x_title.insert(std::pair<std::string,std::string>("VBF_HVTWZ","m(W\264) [TeV]"));
  x_title.insert(std::pair<std::string,std::string>("RSGWW_ReWeight","m(G_{KK}) [TeV]"));
  // Y axis titles
  y_title.insert(std::pair<std::string,std::string>("HVTWW","#sigma(pp#rightarrowZ\264#rightarrowWW) [pb]"));
  y_title.insert(std::pair<std::string,std::string>("HVTWZ","#sigma(pp#rightarrowW\264#rightarrowWZ) [pb]"));
  y_title.insert(std::pair<std::string,std::string>("RSGWW","#sigma(pp#rightarrowG_{KK}#rightarrowWW) [pb]"));
  y_title.insert(std::pair<std::string,std::string>("ggHWWNWA","#sigma(gg#rightarrowH#rightarrowWW) [pb]"));
  y_title.insert(std::pair<std::string,std::string>("VBFWWNWA","#sigma(qq#rightarrowH#rightarrowWW) [pb]"));
  y_title.insert(std::pair<std::string,std::string>("VBF_HVTWW","#sigma(qq#rightarrowZ\264#rightarrowWW) [pb]"));
  y_title.insert(std::pair<std::string,std::string>("VBF_HVTWZ","#sigma(qq#rightarrowW\264#rightarrowWZ) [pb]"));
  y_title.insert(std::pair<std::string,std::string>("RSGWW_ReWeight","#sigma(pp#rightarrowG_{KK}#rightarrowWW) [pb]"));

  // Legend names
  std::vector<std::string> hvtww_leg;
  hvtww_leg.push_back("#sigma(pp#rightarrowZ\264#rightarrowWW) HVT Model A, g_{v}=1");
  hvtww_leg.push_back("#sigma(pp#rightarrowZ\264#rightarrowWW) HVT Model B, g_{v}=3");
  leg_title.insert(std::pair<std::string,std::vector<std::string> >("HVTWW",hvtww_leg));
  std::vector<std::string> hvtwz_leg;
  hvtwz_leg.push_back("#sigma(pp#rightarrowW\264#rightarrowWZ) HVT Model A, g_{v}=1");
  hvtwz_leg.push_back("#sigma(pp#rightarrowW\264#rightarrowWZ) HVT Model B, g_{v}=3");
  leg_title.insert(std::pair<std::string,std::vector<std::string> >("HVTWZ",hvtwz_leg));
  std::vector<std::string> rsgww_leg;
  rsgww_leg.push_back("#sigma(pp#rightarrowG_{KK}#rightarrowWW) k/#bar{M}_{pl}=1");
  leg_title.insert(std::pair<std::string,std::vector<std::string> >("RSGWW",rsgww_leg));
  std::vector<std::string> rsgww_rw_leg;
  rsgww_rw_leg.push_back("#sigma(pp#rightarrowG_{KK}#rightarrowWW) k/#bar{M}_{pl}=0.5");
  leg_title.insert(std::pair<std::string,std::vector<std::string> >("RSGWW_ReWeight",rsgww_rw_leg));
  std::vector<std::string> gghwwnwa_leg;
  gghwwnwa_leg.push_back("#sigma(gg#rightarrowH#rightarrowWW) (NDA)");
  gghwwnwa_leg.push_back("#sigma(gg#rightarrowH#rightarrowWW) (Unsupp.)");
  leg_title.insert(std::pair<std::string,std::vector<std::string> >("ggHWWNWA",gghwwnwa_leg));
  std::vector<std::string> vbfwwnwa_leg;
  vbfwwnwa_leg.push_back("#sigma(qq#rightarrowH#rightarrowWW) (NWA)");
  leg_title.insert(std::pair<std::string,std::vector<std::string> >("VBFWWNWA",vbfwwnwa_leg));
  std::vector<std::string> vbfhvtww_leg;
  vbfhvtww_leg.push_back("#sigma(qq#rightarrowZ\264#rightarrowWW) (HVT)");
  leg_title.insert(std::pair<std::string,std::vector<std::string> >("VBF_HVTWW",vbfhvtww_leg));
  std::vector<std::string> vbfhvtwz_leg;
  vbfhvtwz_leg.push_back("#sigma(qq#rightarrowW\264#rightarrowWZ) (HVT)");
  leg_title.insert(std::pair<std::string,std::vector<std::string> >("VBF_HVTWZ",vbfhvtwz_leg));

  // XS file names:
  std::vector<std::string> HVTWW_names;
  HVTWW_names.push_back("HVTWWxs_agv1.txt");
  HVTWW_names.push_back("HVTWWxs_bgv3.txt");
  xs_names.insert(std::pair<std::string,std::vector<std::string> >("HVTWW",HVTWW_names));
  std::vector<std::string> HVTWZ_names;
  HVTWZ_names.push_back("HVTWZxs_agv1.txt");
  HVTWZ_names.push_back("HVTWZxs_bgv3.txt");
  xs_names.insert(std::pair<std::string,std::vector<std::string> >("HVTWZ",HVTWZ_names));
  std::vector<std::string> RSGWW_names;
  RSGWW_names.push_back("RSGWWxs_km10.txt");
  xs_names.insert(std::pair<std::string,std::vector<std::string> >("RSGWW",RSGWW_names));
  std::vector<std::string> RSGWW_rw_names;
  RSGWW_rw_names.push_back("RSGWWxs_km05.txt");
  xs_names.insert(std::pair<std::string,std::vector<std::string> >("RSGWW_ReWeight",RSGWW_rw_names));
  std::vector<std::string> ggHWWNWA_names;
  ggHWWNWA_names.push_back("ggHWWNWAxs_NDA.txt");
  ggHWWNWA_names.push_back("ggHWWNWAxs_un.txt");
  xs_names.insert(std::pair<std::string,std::vector<std::string> >("ggHWWNWA",ggHWWNWA_names));
  std::vector<std::string> VBFWWNWA_names;
  VBFWWNWA_names.push_back("VBFWWNWAxs.txt");
  xs_names.insert(std::pair<std::string,std::vector<std::string> >("VBFWWNWA",VBFWWNWA_names));
  std::vector<std::string> VBF_HVTWW_names;
  VBF_HVTWW_names.push_back("VBF_HVTWWxs.txt");
  xs_names.insert(std::pair<std::string,std::vector<std::string> >("VBF_HVTWW",VBF_HVTWW_names));
  std::vector<std::string> VBF_HVTWZ_names;
  VBF_HVTWZ_names.push_back("VBF_HVTWZxs.txt");
  xs_names.insert(std::pair<std::string,std::vector<std::string> >("VBF_HVTWZ",VBF_HVTWZ_names));

}

//
//___________________________________
void dump_config(std::string sig_reg){

  std::cout << "===========================================" << std::endl
            << "Running with the following configuration: " << std::endl
            << "      debug: " << debug   << std::endl
            << "    doBlind: " << doBlind << std::endl
            << "     Signal: " << sig_reg << std::endl
            << "    do Lims: " << doLims  << std::endl
            << "      do p0: " << dop0    << std::endl
            << "    do Sens: " << doSens  << std::endl
            << "     purity: " << purity_reg[purity] << std::endl
            << "       prod: " << prod_reg[prod]     << std::endl
            << "       lumi: " << lumi << "/fb" << std::endl
            << "==========================================="
            << std::endl << std::endl;
  return;
}
