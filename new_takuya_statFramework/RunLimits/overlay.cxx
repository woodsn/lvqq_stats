// Root includes
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
#include "TAxis.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TLegend.h"
#include "Math/DistFunc.h"
#include "utils/AtlasLabels.C"
#include "utils/AtlasStyle.C"
// general includes
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>
#include "stdio.h"
#include "stdlib.h"

// configuration
const bool doBlind   = false; // blinding?
const bool doSens    = false;  // compare sensitivity
const bool doLims    = true; // run limits
const bool dop0      = false;  // make p0 plots
const bool debug     = true;
//double     gBr       = 1.0;  // branching ratio
double     gBr       = 0.439109;  // branching ratio ---ONLY TRUE FOR WW SIGNALS
double     lumi      = 36.1; // fb
int        npoints   = -1;
char*      pintern   = (char*)"Internal";

std::string srs[3]   = {"HVTWW","HVTWZ","RSGWW"};

// global
std::string      line;
std::string      name;
std::string      foldername;
std::vector<int> vmass;
double *masspoint, *observed, *expected;
double *expectedp1, *expectedp2, *expectedm1; 
double *expectedm2, *zero, *prediction; 
double *temp_exp, *temp_exp2;
double *pval;

// functions
void runSensitivity  (std::string sig_reg);
void runLimits       (std::string sig_reg);
void run_p0          (std::string sig_reg);
void getPrediction   (std::string sig_reg);
void ReadInputMasses ();


//______________________________________
// Input the signal region:
//   HVTWW, HVTWZ, RSGWW
//
int overlay(std::string sig_reg = "All"){
  
  // check if valid sr specified
  if( 1 /*sig_reg.find("HVTWW")    != std::string::npos ||
      sig_reg.find("HVTWZ")    != std::string::npos ||
      sig_reg.find("ggHWWNWA") != std::string::npos || // is this correct?
      sig_reg.find("VBF") != std::string::npos || // or this? Natasha changed Feb 9 from VBFWWNWA to VBF
      sig_reg.find("ggHRES") != std::string::npos || // or this? Natasha changed Feb 9 from VBFWWNWA to VBF
      sig_reg.find("RSGWW")    != std::string::npos*/ ){
   if(sig_reg.find("WZ") != std::string::npos) gBr = 0.22769; //changing gBr for WZ signals!!!!
    if( doSens )
      runSensitivity( sig_reg );
    if( doLims )
      runLimits( sig_reg );
    if( dop0)
      run_p0( sig_reg);
  
  } // if all, loop over srs
  else if( sig_reg.find("All") != std::string::npos ){
    
    for( unsigned int s=0; s< 3; s++){
      if( doSens )
        runSensitivity( srs[s] );
      if( doLims )
        runLimits( srs[s] ); // FIXME increase past 3 if including more SRs
      if( dop0 )
        run_p0( srs[s] );
    }
  } // else wrong input
  else{
    std::cout << "Invalid signal region: " << sig_reg 
              << "\nPlease select one of the following: HVTWW, HVTWZ, RSGWW, ggHWWNWA, VBFWWNWA"
              << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}


//___________________________________
// Plot p0 vs signal mass
//
void run_p0(std::string sig_reg){

  std::cout << "\nWorking on signal region: " << sig_reg << std::endl;

  // folders to compare FIXME change to where limits stored
  //std::string f_base = "../hp_limits";
  std::string f_base = "../limits";

  // input limit files
  foldername = Form("%s/%s",f_base.c_str(),sig_reg.c_str());
  if( debug ) 
    std::cout << "Reading from folder: " << foldername << std::endl;

  //Read masses and sort vector
  std::cout << "Reading input masses" << std::endl;
  vmass.clear();
  ReadInputMasses();
  std::sort( vmass.begin(), vmass.end() );
  pval = new double[npoints];

  std::cout << "Mass\tP-value\n" 
            << "========================="
            << std::endl << std::endl;

  // loop over mass points
  for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){

    // open mass point
    TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
    if( !file ){
      std::cerr << "file : " << vmass.at(j0) 
        << " is missing " << std::endl;
      exit(6);
    }

    // read the tree 
    TTree *tree = (TTree*)file->Get( "stats" );
    tree->GetEntry(0);

    // catch limit if 0 
    //if( tree->GetLeaf("null_pvalue")->GetValue() == 0 ){  // FIXME
    if( tree->GetLeaf("pb_obs")->GetValue() == 0 ){
      std::cout << "mass point " << vmass.at(j0)
        << " will not be used" << std::endl;
      continue;
    }

    // save values
    masspoint[j0]   = vmass.at(j0);
    //pval[j0]        = tree->GetLeaf("null_pvalue")->GetValue() ; //  / gBr; // FIXME
    pval[j0]        = tree->GetLeaf("pb_obs")->GetValue() ; //  / gBr;
    zero[j0]        = 0.0;
    
    std::cout << masspoint[j0] << "\t" << pval[j0] << std::endl;

  }// end loop over file

  // Declare p0 plot
  TCanvas           *c1 = new TCanvas(Form("c_p0_%s",sig_reg.c_str()), Form("p0_%s",sig_reg.c_str()), 800, 700);
  TGraphAsymmErrors *g  = new TGraphAsymmErrors(npoints,masspoint,pval,zero,zero,zero,zero);
  g->SetNameTitle(Form("g_p0_%s",sig_reg.c_str()), "p0");

  // Set min/max
  double maxLimit = 50;
  double minLimit = 1 - ROOT::Math::gaussian_cdf(6); // six sigma
  g->SetMinimum(minLimit);
  g->SetMaximum(maxLimit);

  // Draw graph
  std::string xtitle = "m(VV) [GeV]";
  std::string  ytitle = "Local p_{0}";
  g->GetXaxis()->SetTitle(xtitle.c_str());
  //g->GetXaxis()->SetNdivisions(505);
  g->GetYaxis()->SetTitle(ytitle.c_str());
  g->GetYaxis()->SetNdivisions(505);
  g->Draw("apl");

  c1->SetRightMargin(0.1);
  c1->Update();

  // Atlas Labels
  ATLASLabel2(0.22, 0.68, pintern, kBlack);
  myText( 0.70, 0.88, kBlack, Form("#scale[0.7]{%s}",sig_reg.c_str()));
  //myText( 0.19, 0.82, kBlack, Form("#scale[0.7]{#int Ldt = %.1f fb^{-1}, #sqrt{s} = 13 TeV}", lumi));
  
  // draw sigma lines
  TLine l;
  l.SetLineWidth(2);
  l.SetLineStyle(2);
  l.SetLineColor(kBlack);
  const Double_t xmin = g->GetXaxis()->GetXmin();
  const Double_t xmax = g->GetXaxis()->GetXmax();
  l.DrawLine(xmin, 1, xmax, 1);

  l.SetLineColor(kRed);
  l.DrawLine(xmin, 1 - ROOT::Math::gaussian_cdf(0), xmax, 1 - ROOT::Math::gaussian_cdf(0));

  TLatex t;
  const Double_t delta = (xmax - xmin) * 0.02;
  t.DrawLatex(xmax + delta, (1 - ROOT::Math::gaussian_cdf(0)) * 0.8, "#color[2]{0#sigma}");

  int n = 1;
  while (minLimit < 1 - ROOT::Math::gaussian_cdf(n)) {
    l.DrawLine(xmin, 1 - ROOT::Math::gaussian_cdf(n), xmax, 1 - ROOT::Math::gaussian_cdf(n));
    std::string cmd = Form("#color[2]{%d#sigma}", n);
    t.DrawLatex(xmax + delta, (1 - ROOT::Math::gaussian_cdf(n)) * 0.4, cmd.c_str());
    n++;
  }
  c1->SetLogy(1);

  // Save plot
  std::cout << "Saving plot to output/" << std::endl;
  c1->SaveAs(Form("output/VVM_p0_%s.png",sig_reg.c_str()));

  //delete c1;

}

//________________________________________
// Make sensitivity plot
//
void runSensitivity(std::string sig_reg){

  std::cout << "\nWorking on signal region: " << sig_reg << std::endl;

  // folders to compare FIXME change to where limits stored
  std::string f_base[2] = {"../limits","../limits_TA"};
  
  // loop over folders
  for( int i=0; i<2; i++){

    // input limit files
    foldername = Form("%s/%s",f_base[i].c_str(),sig_reg.c_str());
    if( debug ) 
      std::cout << "Reading from folder: " << foldername << std::endl;

    //Read masses and sort vector
    std::cout << "Reading input masses" << std::endl;
    vmass.clear();
    ReadInputMasses();
    std::sort( vmass.begin(), vmass.end() );
    if( i==0 ) temp_exp  = new double[npoints];
    else       temp_exp2 = new double[npoints];
    
    // loop over mass points
    for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){

      // open mass point
      TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
      if( !file ){
        std::cerr << "file : " << vmass.at(j0) 
          << " is missing " << std::endl;
        exit(6);
      }

      // read the tree 
      TTree *tree = (TTree*)file->Get( "stats" );
      tree->GetEntry(0);

      // catch limit if 0 
      if( tree->GetLeaf("exp_upperlimit")->GetValue() == 0 ){
        std::cout << "mass point " << vmass.at(j0)
                  << " will not be used" << std::endl;
        continue;
      }

      // save values
      masspoint[j0]     = vmass.at(j0);
      if( i==0){ 
        temp_exp[j0]    = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
	cout << "gBr is : " << gBr << endl;
      }
      else  
        temp_exp2[j0]   = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
      zero[j0] = 0.0;

        }// end loop over files

  } // end loop over inputs
  
  // print limits
  if( debug ){
    std::cout << "Mass\t" << "Expected Calo\t" << "Expected TA\n"  
      << "=============================================" 
      << std::endl << std::endl;
    for( int ii=0; ii<npoints; ii++){
      std::cout << masspoint[ii] << std::setprecision(7) << std::fixed 
                << "\t" << temp_exp[ii] << "\t" << temp_exp2[ii]  
                << std::resetiosflags(ios::fixed) << std::endl;
    }
  }
  
  // Plot
  std::cout << "Making Plots" << std::endl;
  
  // expected tgraph
  TGraphAsymmErrors *grexpected = new TGraphAsymmErrors(npoints,masspoint,temp_exp,zero,zero,zero,zero);
  grexpected->SetLineWidth(2.0);
  grexpected->SetLineStyle(9);
  grexpected->SetLineColor(kMagenta+10);
  
  // expected2 tgraph
  TGraphAsymmErrors *grexpected2 = new TGraphAsymmErrors(npoints,masspoint,temp_exp2,zero,zero,zero,zero);
  grexpected2->SetLineWidth(2.0);
  grexpected2->SetLineStyle(9);
  grexpected2->SetLineColor(kSpring-7);

  // Prediction
  getPrediction(sig_reg);
  TGraph *g_prediction = new TGraph( npoints, masspoint, prediction );
  g_prediction->SetLineColor(kBlack);
  g_prediction->SetLineWidth(2);

  // Put on a canvas
  TCanvas *MyC = new TCanvas("Sensitivity","Sensitivity",800,700);
  gPad->SetLogy();

  // Plot tgraphs together
  TMultiGraph *sens = new TMultiGraph("","; m_{VV} [GeV]; #sigma(pp#rightarrow Z'#rightarrow WW) [pb]");
  sens->Add(grexpected, "L");       // expected line
  sens->Add(grexpected2, "L");       // expected line
  sens->Add(g_prediction, "C");     // prediction

  sens->Draw("AP");
  sens->GetXaxis()->SetRangeUser( vmass.at(0)-0.001, vmass.at(npoints-1)+0.001 );
  sens->GetYaxis()->SetRangeUser( 2.e-4, 5.e+1 );
  sens->GetXaxis()->SetLabelSize(0.04);
  MyC->Update();
  gPad->RedrawAxis();

  // Atlas Labels
  ATLASLabel2(0.22, 0.93, pintern, kBlack);
  myText( 0.19, 0.87, kBlack, Form("#scale[0.7]{#int Ldt = %.1f fb^{-1}, #sqrt{s} = 13 TeV}", lumi));
  myText( 0.22, 0.76, kBlack, Form("#scale[0.7]{Sensitivity Comparison}"));
  // Legend
  TLegend *legend = new TLegend(0.57,0.65,0.9,0.80);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.025);
  legend->AddEntry(grexpected,"Expected (95\% CLs)","L");
  legend->AddEntry(grexpected2,"Expected (95\% CLs) TA Mass","L");

  if( foldername.find("HVTWW") < foldername.length() )
    legend->AddEntry(g_prediction, "HVT #rightarrow WW", "L" );
  if( foldername.find("HVTWZ") < foldername.length() )
    legend->AddEntry(g_prediction, "HVT #rightarrow WZ", "L" );
  if( foldername.find("RSGWW") < foldername.length() )
    legend->AddEntry(g_prediction, "RSG #rightarrow WW", "L" );
  if( foldername.find("ggF") < foldername.length() )
    legend->AddEntry(g_prediction, "ggF H #rightarrow WW", "L" );
  if( foldername.find("VBF") < foldername.length() )
    legend->AddEntry(g_prediction, "VBF H #rightarrow WW", "L" );

  legend->Draw();

  //Save
  std::cout << "Saving plot in output/" << std::endl;
  MyC->Print( Form("output/sensitivity_%s.eps", sig_reg.c_str() ) );
  MyC->Print( Form("output/sensitivity_%s.png", sig_reg.c_str() ) );

}


//__________________________________
// Make plot for specific sr
// To compare two predictions: 
//  > add loop and add tgraph for each prediction
void runLimits(std::string sig_reg){
  std::cout << "gBr is : " << gBr << std::endl;
  TMultiGraph *likelihd_limit_0j = new TMultiGraph("","; m_{VV} [GeV]; 95% C.L. limit on xs");
  TCanvas *MyC = new TCanvas("Limit","Limit",800,700);
  std::cout << "\nWorking on signal region: " << sig_reg << std::endl;
  std::vector<std::string> selections = {"RES", "BOOSTED","Combined"}; 
//  std::vector<std::string> selections = {"BOOSTED", "Combined"}; 
  
// input limit files FIXME change to where limits are stored
  if( debug ) 
    std::cout << "Reading from folder: " << foldername << std::endl;

  std::string f_base2 = "../limits";


  //******************ggF**********************************************************************************
  foldername = Form("%s/%s/%s",f_base2.c_str(),sig_reg.c_str(),selections[0].c_str());
  std::cout << "foldername is " << foldername << std::endl;
  //Read masses and sort vector
  std::cout << "Reading input masses" << std::endl;
  vmass.clear();
  ReadInputMasses();
  std::sort( vmass.begin(), vmass.end() );
  
  if( debug ){
    std::cout << "Mass\t" << "Observed\t" << "Expecteed\t" 
	      << "Expected m2\t" << "Expected m1\t"
	      << "Expected p1\t" << "Expected p2" << std::endl;
    std::cout << "=====================================================" 
	      << "====================================================="  
	      << std::endl << std::endl;
  }
  // loop over mass points
  for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){
    
    // open mass point
    TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
    if( !file ){
      std::cerr << "file : " << vmass.at(j0) 
		<< " is missing " << std::endl;
      exit(6);
    }
    
    // read the tree 
    TTree *tree = (TTree*)file->Get( "stats" );
    tree->GetEntry(0);
    
    // catch limit if 0 
    if( tree->GetLeaf("exp_upperlimit")->GetValue() == 0 ){
      std::cout << "mass point " << vmass.at(j0)
		<< " will not be used" << std::endl;
      continue;
    }
    
    // save values
    masspoint[j0]  = vmass.at(j0);
    observed[j0]   = tree->GetLeaf("obs_upperlimit")->GetValue() / gBr;
    expected[j0]   = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
    expectedp1[j0] = (tree->GetLeaf("exp_upperlimit_plus1")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue()) / gBr;
    expectedp2[j0] = (tree->GetLeaf("exp_upperlimit_plus2")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue()) / gBr;
    expectedm1[j0] = -1*(tree->GetLeaf("exp_upperlimit_minus1")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue())/ gBr;
    expectedm2[j0] = -1*(tree->GetLeaf("exp_upperlimit_minus2")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue())/ gBr;
    zero[j0] = 0.0;
    
    // print limits
    if( debug ){
      std::cout << masspoint[j0] << std::setprecision(7) << std::fixed 
		<< "\t" << observed[j0]   << "\t" << expected[j0] << "\t" 
		<< expectedm2[j0] << "\t" << expectedm1[j0] << "\t"
		<< expectedp1[j0] << "\t" << expectedp2[j0] << std::resetiosflags(ios::fixed) << std::endl;
    }
  }// end loop over files


  // Plot
  std::cout << "Making Plots" << std::endl;
  // expected tgraph
  TGraphAsymmErrors *grexpected_0 = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,zero,zero);
  
  grexpected_0->SetLineWidth(2.0);
  grexpected_0->SetLineStyle(2);
  grexpected_0->SetLineColor(kBlack);
  
  TGraphAsymmErrors *grobserved_0 = new TGraphAsymmErrors(npoints,masspoint,observed,zero,zero,zero,zero);
//  grobserved_0->SetMarkerStyle(20);
  //grobserved_0->SetMarkerSize(1.0);
  grobserved_0->SetLineStyle(1);
  grobserved_0->SetLineColor(1);
  grobserved_0->SetLineWidth(2.0);
  gPad->SetLogy();



//********************VBF***********************************************************************
  foldername = Form("%s/%s/%s",f_base2.c_str(),sig_reg.c_str(),selections[1].c_str());
  std::cout << "foldername is " << foldername << std::endl;
  //Read masses and sort vector
  std::cout << "Reading input masses" << std::endl;
  vmass.clear();
  ReadInputMasses();
  std::sort( vmass.begin(), vmass.end() );
  
  if( debug ){
    std::cout << "Mass\t" << "Observed\t" << "Expecteed\t" 
	      << "Expected m2\t" << "Expected m1\t"
	      << "Expected p1\t" << "Expected p2" << std::endl;
    std::cout << "=====================================================" 
	      << "====================================================="  
	      << std::endl << std::endl;
  }
  // loop over mass points
  for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){
    
    // open mass point
    TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
    if( !file ){
      std::cerr << "file : " << vmass.at(j0) 
		<< " is missing " << std::endl;
      exit(6);
    }
    
    // read the tree 
    TTree *tree = (TTree*)file->Get( "stats" );
    tree->GetEntry(0);
    
    // catch limit if 0 
    if( tree->GetLeaf("exp_upperlimit")->GetValue() == 0 ){
      std::cout << "mass point " << vmass.at(j0)
		<< " will not be used" << std::endl;
      continue;
    }
    
    // save values
    masspoint[j0]  = vmass.at(j0);
    observed[j0]   = tree->GetLeaf("obs_upperlimit")->GetValue() / gBr;
    expected[j0]   = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
    expectedp1[j0] = (tree->GetLeaf("exp_upperlimit_plus1")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue()) / gBr;
    expectedp2[j0] = (tree->GetLeaf("exp_upperlimit_plus2")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue()) / gBr;
    expectedm1[j0] = -1*(tree->GetLeaf("exp_upperlimit_minus1")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue())/ gBr;
    expectedm2[j0] = -1*(tree->GetLeaf("exp_upperlimit_minus2")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue())/ gBr;
    zero[j0] = 0.0;
    
    // print limits
    if( debug ){
      std::cout << masspoint[j0] << std::setprecision(7) << std::fixed 
		<< "\t" << observed[j0]   << "\t" << expected[j0] << "\t" 
		<< expectedm2[j0] << "\t" << expectedm1[j0] << "\t"
		<< expectedp1[j0] << "\t" << expectedp2[j0] << std::resetiosflags(ios::fixed) << std::endl;
    }
  }// end loop over files


  // Plot
  std::cout << "Making Plots" << std::endl;
  // expected tgraph
  TGraphAsymmErrors *grexpected_1 = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,zero,zero);
  
  grexpected_1->SetLineWidth(2.0);
  grexpected_1->SetLineStyle(2);
  grexpected_1->SetLineColor(kOrange);

  TGraphAsymmErrors *grobserved_1 = new TGraphAsymmErrors(npoints,masspoint,observed,zero,zero,zero,zero);
//  grobserved_1->SetMarkerStyle(20);
  //grobserved_0->SetMarkerSize(1.0);
  grobserved_1->SetLineStyle(1);
  grobserved_1->SetLineColor(kOrange);
  grobserved_1->SetLineWidth(2.0);
  
//************************wjets and top modelling uncorr combination*******************************
/*

  foldername = Form("%s/%s/%s",f_base2.c_str(),sig_reg.c_str(),selections[3].c_str());
  std::cout << "foldername is " << foldername << std::endl;
  //Read masses and sort vector
  std::cout << "Reading input masses" << std::endl;
  vmass.clear();
  ReadInputMasses();
  std::sort( vmass.begin(), vmass.end() );
  
  if( debug ){
    std::cout << "Mass\t" << "Observed\t" << "Expecteed\t" 
	      << "Expected m2\t" << "Expected m1\t"
	      << "Expected p1\t" << "Expected p2" << std::endl;
    std::cout << "=====================================================" 
	      << "====================================================="  
	      << std::endl << std::endl;
  }
  // loop over mass points
  for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){
    
    // open mass point
    TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
    if( !file ){
      std::cerr << "file : " << vmass.at(j0) 
		<< " is missing " << std::endl;
      exit(6);
    }
    
    // read the tree 
    TTree *tree = (TTree*)file->Get( "stats" );
    tree->GetEntry(0);
    
    // catch limit if 0 
    if( tree->GetLeaf("exp_upperlimit")->GetValue() == 0 ){
      std::cout << "mass point " << vmass.at(j0)
		<< " will not be used" << std::endl;
      continue;
    }
    
    // save values
    masspoint[j0]  = vmass.at(j0);
    observed[j0]   = tree->GetLeaf("obs_upperlimit")->GetValue() / gBr;
    expected[j0]   = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
    expectedp1[j0] = (tree->GetLeaf("exp_upperlimit_plus1")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue()) / gBr;
    expectedp2[j0] = (tree->GetLeaf("exp_upperlimit_plus2")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue()) / gBr;
    expectedm1[j0] = -1*(tree->GetLeaf("exp_upperlimit_minus1")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue())/ gBr;
    expectedm2[j0] = -1*(tree->GetLeaf("exp_upperlimit_minus2")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue())/ gBr;
    zero[j0] = 0.0;
    
    // print limits
    if( debug ){
      std::cout << masspoint[j0] << std::setprecision(7) << std::fixed 
		<< "\t" << observed[j0]   << "\t" << expected[j0] << "\t" 
		<< expectedm2[j0] << "\t" << expectedm1[j0] << "\t"
		<< expectedp1[j0] << "\t" << expectedp2[j0] << std::resetiosflags(ios::fixed) << std::endl;
    }
  }// end loop over files


  // Plot
  std::cout << "Making Plots" << std::endl;
  // expected tgraph
  TGraphAsymmErrors *grexpected_a = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,zero,zero);
  
  grexpected_a->SetLineWidth(2.0);
  grexpected_a->SetLineStyle(2);
  grexpected_a->SetLineColor(kPink);

  
 



 
*/

//***********************combination**************************************

  foldername = Form("%s/%s/%s",f_base2.c_str(),sig_reg.c_str(),selections[2].c_str());
  std::cout << "foldername is " << foldername << std::endl;
  //Read masses and sort vector
  std::cout << "Reading input masses" << std::endl;
  vmass.clear();
  ReadInputMasses();
  std::sort( vmass.begin(), vmass.end() );
  
  if( debug ){
    std::cout << "Mass\t" << "Observed\t" << "Expecteed\t" 
	      << "Expected m2\t" << "Expected m1\t"
	      << "Expected p1\t" << "Expected p2" << std::endl;
    std::cout << "=====================================================" 
	      << "====================================================="  
	      << std::endl << std::endl;
  }
  // loop over mass points
  for( unsigned int j0 = 0; j0 < vmass.size(); j0++ ){
    
    // open mass point
    TFile* file = TFile::Open( Form( "%s%d.root", name.c_str(), vmass.at(j0)) , "READ" );
    if( !file ){
      std::cerr << "file : " << vmass.at(j0) 
		<< " is missing " << std::endl;
      exit(6);
    }
    
    // read the tree 
    TTree *tree = (TTree*)file->Get( "stats" );
    tree->GetEntry(0);
    
    // catch limit if 0 
    if( tree->GetLeaf("exp_upperlimit")->GetValue() == 0 ){
      std::cout << "mass point " << vmass.at(j0)
		<< " will not be used" << std::endl;
      continue;
    }
    
    // save values
    masspoint[j0]  = vmass.at(j0);
    observed[j0]   = tree->GetLeaf("obs_upperlimit")->GetValue() / gBr;
    expected[j0]   = tree->GetLeaf("exp_upperlimit")->GetValue() / gBr;
    expectedp1[j0] = (tree->GetLeaf("exp_upperlimit_plus1")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue()) / gBr;
    expectedp2[j0] = (tree->GetLeaf("exp_upperlimit_plus2")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue()) / gBr;
    expectedm1[j0] = -1*(tree->GetLeaf("exp_upperlimit_minus1")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue())/ gBr;
    expectedm2[j0] = -1*(tree->GetLeaf("exp_upperlimit_minus2")->GetValue() - tree->GetLeaf("exp_upperlimit")->GetValue())/ gBr;
    zero[j0] = 0.0;
    
    // print limits
    if( debug ){
      std::cout << masspoint[j0] << std::setprecision(7) << std::fixed 
		<< "\t" << observed[j0]   << "\t" << expected[j0] << "\t" 
		<< expectedm2[j0] << "\t" << expectedm1[j0] << "\t"
		<< expectedp1[j0] << "\t" << expectedp2[j0] << std::resetiosflags(ios::fixed) << std::endl;
    }
  }// end loop over files


  // Plot
  std::cout << "Making Plots" << std::endl;
  // expected tgraph
  TGraphAsymmErrors *grexpected_2 = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,zero,zero);
  
  grexpected_2->SetLineWidth(2.0);
  grexpected_2->SetLineStyle(2);
  grexpected_2->SetLineColor(kBlue);

  TGraphAsymmErrors *grobserved_2 = new TGraphAsymmErrors(npoints,masspoint,observed,zero,zero,zero,zero);
//  grobserved_2->SetMarkerStyle(20);
  //grobserved_0->SetMarkerSize(1.0);
  grobserved_2->SetLineStyle(1);
  grobserved_2->SetLineColor(kBlue);
  grobserved_2->SetLineWidth(2.0);
 
  // set 1 and 2 sigma bands on expected limits
  TGraphAsymmErrors *grexpected1sigma = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,expectedm1,expectedp1);
  TGraphAsymmErrors *grexpected2sigma = new TGraphAsymmErrors(npoints,masspoint,expected,zero,zero,expectedm2,expectedp2);
  grexpected1sigma->SetFillColor(kGreen);
  grexpected2sigma->SetFillColor(kYellow);










  // Prediction
  getPrediction(sig_reg);
  TGraph *g_prediction = new TGraph( npoints, masspoint, prediction );
  g_prediction->SetLineColor(kRed+2);
  g_prediction->SetLineWidth(1.5);




  // Plot tgraphs together

 //  likelihd_limit_0j->Add(grexpected2sigma,"E3"); // 2 sigma band
  // likelihd_limit_0j->Add(grexpected1sigma,"E3"); // 1 sigma band
  likelihd_limit_0j->Add(grexpected_0, "L");       // expected line
  likelihd_limit_0j->Add(grexpected_1, "L");       // expected line
  likelihd_limit_0j->Add(grexpected_2, "L");       // expected line
//  likelihd_limit_0j->Add(grexpected_a, "L");       // expected line
//  likelihd_limit_0j->Add(g_prediction, "C");     // prediction
if( !doBlind ){
   likelihd_limit_0j->Add(grobserved_0, "L");    // data
   likelihd_limit_0j->Add(grobserved_1, "L");    // data
   likelihd_limit_0j->Add(grobserved_2, "L");    // data

}
  //likelihd_limit_0j->SetMaximum(ymax);
  //likelihd_limit_0j->SetMinimum(0.1);
  likelihd_limit_0j->Draw("AP");
  likelihd_limit_0j->GetXaxis()->SetRangeUser( vmass.at(0)-0.001, vmass.at(npoints-1)+0.001 );
  //likelihd_limit_0j->GetYaxis()->SetRangeUser( 6.e-4, 1.e+2 );
  likelihd_limit_0j->GetYaxis()->SetRangeUser( 2.e-4, 5.e+1 );
  likelihd_limit_0j->GetXaxis()->SetLabelSize(0.04);
  //likelihd_limit_0j->GetXaxis()->SetNdivisions(505);
  //
  //
  if(sig_reg.find("RSGWW")!= std::string::npos){
  cout << "in RSGWW" << endl;
  likelihd_limit_0j->GetXaxis()->SetTitle("m_{G*} [GeV]");
  likelihd_limit_0j->GetYaxis()->SetTitle("#sigma(pp#rightarrow G*#rightarrow WW) [pb]");
}
  if(sig_reg.find("HVTWW")!= std::string::npos){
   cout << "in HVTWW" << endl;
  likelihd_limit_0j->GetXaxis()->SetTitle("m_{Z'} [GeV]");
  likelihd_limit_0j->GetYaxis()->SetTitle("#sigma(pp#rightarrow Z'#rightarrow WW) [pb]");
}
  if(sig_reg.find("HVTWZ")!= std::string::npos){
  likelihd_limit_0j->GetXaxis()->SetTitle("m_{W'} [GeV]");
  likelihd_limit_0j->GetYaxis()->SetTitle("#sigma(pp#rightarrow W'#rightarrow WZ) [pb]");
}
  if(sig_reg.find("ggHWWNWA")!= std::string::npos){
  likelihd_limit_0j->GetXaxis()->SetTitle("m_{Scalar} [GeV]");
  likelihd_limit_0j->GetYaxis()->SetTitle("#sigma(gg#rightarrow H#rightarrow WW) [pb]");
}
  if(sig_reg.find("VBFWWNWA")!= std::string::npos){
  likelihd_limit_0j->GetXaxis()->SetTitle("m_{Scalar} [GeV]");
  likelihd_limit_0j->GetYaxis()->SetTitle("#sigma(qq#rightarrow H#rightarrow WW) [pb]");
}
  if(sig_reg.find("VBFHVTWW")!= std::string::npos){
  likelihd_limit_0j->GetXaxis()->SetTitle("m_{Z'} [GeV]");
  likelihd_limit_0j->GetYaxis()->SetTitle("#sigma(qq#rightarrow Z'#rightarrow WW) [pb]");
}
  if(sig_reg.find("VBFHVTWZ")!= std::string::npos){
  likelihd_limit_0j->GetXaxis()->SetTitle("m_{W'} [GeV]");
  likelihd_limit_0j->GetYaxis()->SetTitle("#sigma(qq#rightarrow W'#rightarrow WZ) [pb]");
}
  MyC->Update();
  gPad->RedrawAxis();
  
  // Atlas Labels
  ATLASLabel2(0.13, 0.83, pintern, kBlack);
  myText( 0.13, 0.77, kBlack, Form("#scale[0.7]{#int Ldt = %.1f fb^{-1}, #sqrt{s} = 13 TeV}", lumi));
  // Legend
  TLegend *legend = new TLegend(0.50,0.45,0.9,0.87);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  if( !doBlind ){
	legend->AddEntry(grobserved_0,"Resolved Observed 95% CL upper limit","PL");
	legend->AddEntry(grobserved_1,"Boosted HP+LP Observed 95% CL upper limit","PL");
	legend->AddEntry(grobserved_2,"Combined Observed 95% CL upper limit","PL");
}
  legend->AddEntry(grexpected_0,"Resolved HP+LP Expected 95% CL uppper limit","L");
  legend->AddEntry(grexpected_1,"Boosted HP+LP Expected 95% CL upper limit","L");
  legend->AddEntry(grexpected_2,"Combined Expected 95% CL upper limit","L");
  //legend->AddEntry(grexpected_a,"Resolved ggF selection Expected (CLs)","L");
  //legend->AddEntry(grexpected1sigma, "#pm 1#sigma","F");
  //legend->AddEntry(grexpected2sigma, "#pm 2#sigma","F");
/* 
  if( foldername.find("HVTWW") < foldername.length() )
    legend->AddEntry(g_prediction, "HVT #rightarrow WW", "L" );
  if( foldername.find("HVTWZ") < foldername.length() )
    legend->AddEntry(g_prediction, "HVT #rightarrow WZ", "L" );
  if( foldername.find("RSGWW") < foldername.length() )
    legend->AddEntry(g_prediction, "RSG #rightarrow WW", "L" );
  if( foldername.find("ggH") < foldername.length() )
    legend->AddEntry(g_prediction, "H #rightarrow WW", "L" );
  if( foldername.find("ggF") < foldername.length() )
    legend->AddEntry(g_prediction, "ggF H #rightarrow WW", "L" );
  if( foldername.find("VBF") < foldername.length() )
    legend->AddEntry(g_prediction, "VBF H #rightarrow WW", "L" );
  */
  legend->Draw();
  //Save
  std::cout << "Saving plot in output/" << std::endl;
  MyC->Print( Form("output/%s.eps", sig_reg.c_str() ) );
  MyC->Print( Form("output/%s.png", sig_reg.c_str() ) );

  return;

}


//______________________________________
// Count mass points and save in vector
//
void ReadInputMasses(){
  
  int    length;
  int    point;
  string spoint;
  
  // Print root files to get list of mass points
  if( !system(NULL) ) return;
  else{
   int i = system( Form("ls %s/*.root > temp.txt", foldername.c_str() ) );
   if( debug ) 
     std::cout << "system result: " << i << std::endl;  
  }
  ifstream infile ("temp.txt");
  // Read mass from file names
  if( infile.is_open() ){
    while( infile.good() ){
      getline( infile,line );
      if( line != "" ){
        length = line.length();
        spoint = line.substr( length-9, length-5 );
        point = atoi( spoint.c_str() );
        name = line.substr( 0, length-9 );
        if( point == 0 ){
          spoint = line.substr( length-8, length-5 );
          point = atoi( spoint.c_str() );
          name = line.substr( 0, length-8 );
        }
        vmass.push_back( point );
      }
    }
    infile.close();
  }
  else 
    cout << "Unable to open temp.txt";

  npoints    = vmass.size();  
  masspoint  = new double[npoints]; 
  observed   = new double[npoints];
  expected   = new double[npoints];
  expectedp1 = new double[npoints];
  expectedp2 = new double[npoints];
  expectedm1 = new double[npoints];
  expectedm2 = new double[npoints];
  zero       = new double[npoints];
  prediction = new double[npoints];
}



//________________________
// Calculate prediction
//
void getPrediction(std::string sig_reg){
  int              point;
  double           xs;
  double           filter;
  std::string      s0;
  map<int, double> mxs;
  
  // XS file
  std::string xsFile = Form("xs/%sxs.txt",sig_reg.c_str()); 
  ifstream Sfile (xsFile.c_str());
  if( debug )
    std::cout << "Reading Prediciton XSs from file: " << xsFile << std::endl;

  // HVT/RSG xs
//  if( foldername.find("ggF") < foldername.length() ){  // FIXME   && foldername.find("VBF") > foldername.length() ){
    if(1){  
  if( debug )
      std::cout << "Signal is HVTWW, HVTWZ or RSGWW or VBF" << std::endl;

    // save xs for each mass point
    if( Sfile.is_open() ){
      while( Sfile.good() ){
        getline( Sfile,line);
        std::stringstream linestring(line);
        if(foldername.find("ggHWWNWA") > foldername.length() ){
          linestring >> point >> s0 >> xs >> s0;
          mxs[point] = xs;
        }else{
          linestring >> point >> s0 >> xs >> s0 >> filter;
          mxs[point] = xs * filter;
        }
      }
    }

    // branching ratios
    double BrWlv = 0.1075 + 0.1057 + 0.1125;
    double BrWqq = 0.6741;
    double BrZqq = 0.6991;
    double Br;
    if( foldername.find("WZ") > foldername.length() )
      //Br = 2.*BrWlv*BrWqq; // WW
      Br = 1;
    else  
      //Br = BrWlv*BrZqq; // WZ
      Br = 1;
    // calculate prediction
    for( unsigned int i1 = 0; i1 < vmass.size(); i1++ ){
      point = vmass.at(i1);
      prediction[i1] = mxs[point]/Br*1000.;
      if( debug )
        std::cout << "Mass: " << point << "  Prediciton: " << prediction[i1] << " read in: " << mxs[point] << std::endl;
    }
  } // ggF, vbf  (xs set to 1)
  else{  
    if( debug )
      std::cout << "Signal is ggF or vbf" << std::endl;
    
    for( unsigned int i1 = 0; i1 < vmass.size(); i1++ ){
      point = vmass.at(i1);
      prediction[i1] = 1./gBr; //Br(WW->lvqq) = 0.439109 
      if( debug )
        std::cout << "Mass: " << point << "  Prediciton: " << prediction[i1] << std::endl;
    }
  }

}


