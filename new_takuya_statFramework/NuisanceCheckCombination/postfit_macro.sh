#!/bin/bash
#does this change
rm *eps
rm *png
rm *pdf
rm *file*.C
rm -r plots
mkdir plots
cd plots
mkdir HVTWW HVTWZ VBF-HVTWW VBF-HVTWZ
cd ..

#--------HVTWW------------------------------------------------

#----------HVTWW 500 GeV Resolved--------------
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/condor_runs/NuisanceCheck_june15_firsttestmorews06152017_1323/job_001/results_f000_HVTWW_ws_HPLP_500.root/1/FitCrossChecks.root\""
rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_june26_500Gev_ggF_test06262017_1347/job_000/results_f000_HVTWW_ws_HPLP_500.root/1/FitCrossChecks.root\""
signal="\"HVT WW 500 GeV\""
xsec="1.93" #HVTWW 500 GeV

root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWW_Resolved_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Sideband_Resolved_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Btag_Resolved_fileThree",$rootfile,$signal,$xsec)
.q
EOF

#----------HVTWW 2000 GeV Boosted -------------
xsec="0.003612549743" #HVTWW 2TeV
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/condor_runs/NuisanceCheck_2TeV_ggF_workspaces06152017_1326/job_000/results_f000_HVTWW_ws_HPLP_2000.root/1/FitCrossChecks.root\""
rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/HVTWW/FitCrossChecks.root\""
signal="\"HVT WW 2000 GeV\""
root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWW_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
drawPostFit("SRWW_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)
.q
EOF

mv *eps plots/HVTWW
mv *pdf plots/HVTWW
mv *png plots/HVTWW
mv *root plots/HVTWW
mv *file*.C plots/HVTWW



#---------------------------------------------------------------------------------------------

#-----------------HVTWZ--------------------------
#----------HVTWZ 500 GeV Resolved--------------
xsec="2.11" #HVTWZ 500 GeV
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/condor_runs/NuisanceCheck_june15_firsttestmorews06152017_1323/job_003/results_f000_HVTWZ_ws_HPLP_500.root/1/FitCrossChecks.root\""
rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_june26_500Gev_ggF_test06262017_1347/job_001/results_f000_HVTWZ_ws_HPLP_500.root/1/FitCrossChecks.root\""
signal="\"HVT WZ 500 GeV\""

root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWZ_Resolved_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Sideband_Resolved_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Btag_Resolved_fileThree",$rootfile,$signal,$xsec)
.q
EOF

#----------HVTWZ 2000 GeV Boosted -------------
xsec="0.00406" #HVTWZ 2TeV
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/condor_runs/NuisanceCheck_2TeV_ggF_workspaces06152017_1326/job_001/results_f000_HVTWZ_ws_HPLP_2000.root/1/FitCrossChecks.root\""
rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/HVTWZ/FitCrossChecks.root\""
signal="\"HVT WZ 2000 GeV\""
root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWZ_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
drawPostFit("SRWZ_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)
.q
EOF

mv *eps plots/HVTWZ
mv *pdf plots/HVTWZ
mv *png plots/HVTWZ
mv *file*.C plots/HVTWZ

#---------------------------------------------------------------------------------------------

#-------VBF HVTWW-------------------------------
xsec="0.005033" #VBF HVTWZ 500 GeV

rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/condor_runs/NuisanceCheck_june15_firsttestmorews06152017_1323/job_005/results_f000_VBF-HVTWW_ws_HPLP_500.root/1/FitCrossChecks.root\""
signal="\"VBF HVT WW 500 GeV\""

root -l -b <<EOF

.L drawPostFit.C

drawPostFit("SRWW_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Sideband_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Btag_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)

.q

EOF

#----------VBF HVTWW 1000 GeV Boosted -------------
xsec="0.0002027" #VBFHVTWW 1TeV
rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/condor_runs/NuisanceCheck_june15_firsttestmorews06152017_1323/job_004/results_f000_VBF-HVTWW_ws_HPLP_1000.root/1/FitCrossChecks.root\""
signal="\"VBF HVT WW 1000 GeV\""
root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWW_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
drawPostFit("SRWW_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)
.q
EOF

mv *eps plots/VBF-HVTWW
mv *pdf plots/VBF-HVTWW
mv *png plots/VBF-HVTWW
mv *file*.C plots/VBF-HVTWW

#---------------------------------------------------------------------------------------------


#-------VBF HVTWZ-------------------------------

#--------Resolved 500GeV---------------
xsec="0.0041372" #VBF HVTWZ 500 GeV

rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/condor_runs/NuisanceCheck_june15_firsttestmorews06152017_1323/job_007/results_f000_VBF-HVTWZ_ws_HPLP_500.root/1/FitCrossChecks.root\""
signal="\"VBF HVT WZ 500 GeV\""

root -l -b <<EOF

.L drawPostFit.C

drawPostFit("SRWZ_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Sideband_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Btag_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)

.q

EOF

#----------VBF HVTWZ 1000 GeV Boosted -------------
xsec="0.0001543" #VBFHVTWW 1TeV
rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/condor_runs/NuisanceCheck_june1606162017_1437/job_001/results_f000_VBF-HVTWZ_ws_HPLP_1000.root/1/FitCrossChecks.root\""
signal="\"VBF HVT WZ 1000 GeV\""
root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWZ_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
drawPostFit("SRWZ_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)
.q
EOF

mv *pdf plots/VBF-HVTWZ
mv *png plots/VBF-HVTWZ
mv *file*.C plots/VBF-HVTWZ
mv *eps plots/VBF-HVTWZ
