#!/bin/bash

signals=( HVTWW HVTWZ VBF-HVTWW VBF-HVTWZ )
cd plots

for ((i=0;i<${#signals[@]};++i)); do
	cd ${signals[i]}
	cp * /export/share/gauss/woodsn/InternalNote/figures/Results/Combined/${signals[i]}
	cd ..

done

cd ..







