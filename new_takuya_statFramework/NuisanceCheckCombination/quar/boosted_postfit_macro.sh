#!/bin/bash


rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_june12_vbf_new_workspaces06122017_1332/job_002/results_f000_VBF-HVTWZ_ws_HPLP_1000.root/1/FitCrossChecks.root\""
signal="\"VBF HVTWZ 1000 GeV\""

#xsec="1.0"

#2TeV
#xsec="1.0"
#xsec="0.00361" #HVTWW 2TeV
#xsec="0.004018849756" #HVTWZ 2TeV
#xsec="0.003328"
#xsec="" #HVTWZ 1TeV
#xsec="" #VBFHVTWW 1TeV
#xsec="" #VBFHVTWZ 1TeV



#1TeV
#xsec="0.0001013024463" #HVTWW 1TeV
#xsec="0.0001087707948" #HVTWZ 1TeV
#xsec="2.03E-07" #VBFHVTWW 1TeV
xsec="1.54E-07" #VBFHVTWZ 1TeV




root -l -b <<EOF

.L boosted_drawPostFit.C

boosted_drawPostFit("SRWZ_fileOne",$rootfile,$signal,$xsec)
boosted_drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
boosted_drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
boosted_drawPostFit("SRWZ_LP_fileTwo",$rootfile,$signal,$xsec)
boosted_drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
boosted_drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)


.q

EOF








