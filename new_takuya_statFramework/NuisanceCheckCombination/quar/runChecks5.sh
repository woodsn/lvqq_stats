#!/bin/bash

root -l -b <<EOF

.L CombFitCrossCheckForLimits.C+

LimitCrossCheck::PlotFitCrossChecks("/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_combined/VBF-HVTWZ/ws_HPLP_1000.root", "results5/", "combined", "ModelConfig", "obsData" )  

.q

EOF



#LimitCrossCheck::PlotFitCrossChecks("/afs/cern.ch/work/k/kalliopi/private/lvqq_Resolved_WS/v17_Aug_2016/Combined/combined_t_VBFWWNWA700.root", "results/", "combined", "ModelConfig", "obsData" )
#LimitCrossCheck::PlotFitCrossChecks("../CombinationTool/ws_combined_stats/RSGWW/ws_HPLP_4000.root", "results/", "combined", "ModelConfig", "obsData" )  
