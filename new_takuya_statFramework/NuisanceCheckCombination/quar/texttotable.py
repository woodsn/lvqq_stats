import sys
import argparse
from collections import OrderedDict

yld={}
unc={}

region="WW"
#region="WZ"
#region="ZZ"
#region="WWWZ"
#region="WWZZ"

import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument("--WW"  , action='store_true')
parser.add_argument("--WZ"  , action='store_true')
parser.add_argument("--ZZ"  , action='store_true')
parser.add_argument("--WWWZ", action='store_true')
parser.add_argument("--WWZZ", action='store_true')
args=parser.parse_args()

if args.WW:
    region="WW"
elif args.WZ:
    region="WZ"
elif args.ZZ:
    region="ZZ"
elif args.WWWZ:
    region="WWWZ"
elif args.WWZZ:
    region="WWZZ"

colnamemap=OrderedDict()
nCRcol=6
nSRcol=8
if region=="WW":
    infile="test.txt"
    regionlabel="$WW$"
    #colnamemap[as it appears in text file] = how you want it to appear in table
    colnamemap["Sideband_model_lvqq_binned"]="$W$+jets"
    colnamemap["Btag_model_lvqq_binned"]="$\\ttbar$"
    #colnamemap["WSR_model_llqq_binned"]="$\\ell\\ell qq$"
    colnamemap["SRWW_model_lvqq_binned"]="$\\WW$ signal region"
    #colnamemap["SR_model_qqqq"]="$qqqq$"
    tabledef="| c | c@{\ $\pm$\ }c c@{\ $\pm$\ }c | c@{\ $\pm$\ }c  c@{\ $\pm$\ }c |"
    nCRcol=2
    nSRcol=1
'''elif region=="ZZ":
    infile="VVPostfitTables_v8/combined_VV_2000_RSG_ZZ_yields.tex"
    regionlabel="$ZZ$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distmVH_DmvWCR_model_vvqq_binned"]="$W$+jets"
    colnamemap["Sideband_model_lvqq_binned"]="$W$+jets"
    colnamemap["ZCR_model_llqq_binned"]="$Z$+jets"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distmVH_DmvTCR_model_vvqq_binned"]="$\\ttbar$"
    colnamemap["Btag_model_lvqq_binned"]="$\\ttbar$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distmVH_DvvZSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distMT_DWSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distMT_DZSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    colnamemap["ZSR_model_llqq_binned"]="$\\ell\\ell qq$"
    colnamemap["SR_model_qqqq"]="$qqqq$"
    tabledef="| c | c@{\ $\pm$\ }c c@{\ $\pm$\ }c c@{\ $\pm$\ }c | c@{\ $\pm$\ }c  c@{\ $\pm$\ }c c@{\ $\pm$\ }c |"
    nCRcol=6
    nSRcol=6
elif region=="WZ":
    infile="VVPostfitTables_v8/combined_VV_2000_HVT_WZ_yields.tex"
    regionlabel="$W'\\rightarrow WZ$"
    colnamemap["Sideband_model_lvqq_binned"]="$W$+jets"
    colnamemap["ZCR_model_llqq_binned"]="$Z$+jets"
    colnamemap["Btag_model_lvqq_binned"]="$\\ttbar$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distmVH_DvvWSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distMT_DZSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distMT_DWSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    colnamemap["WSR_model_llqq_binned"]="$\\ell\\ell qq$"
    colnamemap["SRWZ_model_lvqq_binned"]="$\\ell\\nu qq$"
    colnamemap["SR_model_qqqq"]="$qqqq$"
    tabledef="| c | c@{\ $\pm$\ }c c@{\ $\pm$\ }c c@{\ $\pm$\ }c | c@{\ $\pm$\ }c  c@{\ $\pm$\ }c c@{\ $\pm$\ }c c@{\ $\pm$\ }c |"
elif region=="WWWZ":
    infile="VVPostfitTables_v8/combined_VV_2000_HVT_comb_yields.tex"
    regionlabel="HVT $W'+Z'$"
    colnamemap["Sideband_model_lvqq_binned"]="$W$+jets"
    colnamemap["ZCR_model_llqq_binned"]="$Z$+jets"
    colnamemap["Btag_model_lvqq_binned"]="$\\ttbar$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distmVH_DvvWSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distMT_DZSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distMT_DWSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    colnamemap["WSR_model_llqq_binned"]="$\\ell\\ell qq$"
    colnamemap["SRComb_model_lvqq_binned"]="$\\ell\\nu qq$"
    colnamemap["SR_model_qqqq"]="$qqqq$"
    tabledef="| c | c@{\ $\pm$\ }c c@{\ $\pm$\ }c c@{\ $\pm$\ }c | c@{\ $\pm$\ }c  c@{\ $\pm$\ }c c@{\ $\pm$\ }c c@{\ $\pm$\ }c |"
elif region=="WWZZ":
    infile="VVPostfitTables_v8/combined_VV_2000_RSG_comb_yields.tex"
    regionlabel="Bulk-RS Graviton $WW+ZZ$"
    colnamemap["Sideband_model_lvqq_binned"]="$W$+jets"
    colnamemap["ZCR_model_llqq_binned"]="$Z$+jets"
    colnamemap["Btag_model_lvqq_binned"]="$\\ttbar$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distmVH_DvvZSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    #colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distMT_DWSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    colnamemap["Region_BMin0_incJet1_J0_incTag1_T0_isMVA0_L0_Y2015_distMT_DZSR_model_vvqq_binned"]="$\\nu\\nu qq$"
    colnamemap["ZSR_model_llqq_binned"]="$\\ell\\ell qq$"
    colnamemap["SRWW_model_lvqq_binned"]="$\\ell\\nu qq$"
    colnamemap["SR_model_qqqq"]="$qqqq$"
    tabledef="| c | c@{\ $\pm$\ }c c@{\ $\pm$\ }c c@{\ $\pm$\ }c | c@{\ $\pm$\ }c  c@{\ $\pm$\ }c c@{\ $\pm$\ }c c@{\ $\pm$\ }c |"
   ''' 
smpl=""
for line in open("%s" % infile):
    line=line.replace("+Jets","")
    line=line.replace("jets","")
    line=line.replace("Dibosons","Diboson")
    line=line.replace("diboson","Diboson")
    line=line.replace("ttbar","ttbar")
    line=line.replace("$\pm$","")
    line=line.replace("\\","")
    line=line.replace("&","")
    line=line.strip()

    spline=line.split()
    
    #print line
    
    if len(spline)==0:
        continue

    if len(spline)==1:
        #print "colname %s" % spline[0]
        smpl=spline[0]
        yld[smpl]={}
        unc[smpl]={}
    else:
        if spline[0]=="stop":
            bg="Top"
            yld[smpl][bg]+=float(spline[1])
            unc[smpl][bg]=(unc[smpl][bg]**2 + float(spline[2])**2)**0.5
        elif spline[0]=="background":
            continue
        else:
            bg=spline[0]
            yld[smpl][bg]=float(spline[1])
            if bg != "Data":
                unc[smpl][bg]=float(spline[2])

rownamemap=OrderedDict()
rownamemap["Diboson"]="SM Diboson"
rownamemap["ttbar"]="$\\ttbar$"
rownamemap["singletop"]="\\single-$t$"
rownamemap["Z"]="$Z$+jets"
rownamemap["W"]="$W$+jets"
rownamemap["multijet"]="multijet"
rownamemap["Total"]="Total Background"
rownamemap["Data"]="Observed"


print "\\begin{table}[tbp]"
print "\\renewcommand{\\arraystretch}{1.25}"
print "\\caption{Expected and observed yields in signal and control regions for the %s signal hypothesis.  Yields and uncertainties are evaluated after a background-only fit to the data in all regions indicated above.  The background for the $qqqq$ channel is evaluated \\textit{in situ} and only the total background yield is indicated.  The $W$+jets background for the $Z$+jets control region and the $\\ell\\ell qq$ signal region is negligible.  The uncertainty on the total background estimate can be smaller than the quadratic sum of the individual background contributions due to anti-correlations between the estimates of different background sources.}" % regionlabel
if region != "WW" and region != "ZZ":
    print "\\resizebox{\\textwidth}{!}{%"
else:
    print "\\begin{center}"
print "\\begin{tabular}{%s}" % tabledef

print "\\hline"
print "\\hline"
print "                      &\multicolumn{%d}{c|}{Control Regions}  &\multicolumn{%d}{c|}{Signal Region}\\\\" % (nCRcol,nSRcol)
print "\cline{1-%d}" % (1+nCRcol+nSRcol)
ln="%22s" % ""
for colname,colfancy in colnamemap.iteritems():
    rightbar=""
    if "qqqq" in colfancy or "Btag_model" in colname or (region=="ZZ" and "ttbar" in colfancy):
        rightbar="|"
    ln+="&\multicolumn{2}{c%s}{%17s}" % (rightbar,colfancy)
ln+="\\\\"
print ln
print "\\hline"
for rowname,rowfancy in rownamemap.iteritems():
    ln="%22s" % rowfancy
    for colname,colfancy in colnamemap.iteritems():
        rightbar=""
        if "qqqq" in colfancy or "Btag_model" in colname or (region=="ZZ" and "ttbar" in colfancy):
            rightbar="|"
        if rowname not in yld[colname]:
            ln += "&\multicolumn{2}{c%s}{%5s}%13s" % (rightbar,"","")
        elif rowname=="Data":
            ln += "&\multicolumn{2}{c%s}{%4d}%13s" % (rightbar,yld[colname][rowname],"")
        else:
            y=yld[colname][rowname]
            u=unc[colname][rowname]
            if y<10:
                if (y%0.1)>0.05:
                    y+=0.1
                if (u%0.1)>0.05:
                    u+=0.1
                ln+="&%23.1f &%12.1f" % (y,u)
            else:
                if (y%1)>0.5:
                    y+=1
                if (u%1)>0.5:
                    u+=1
                ln+="&%23d &%12d" % (y,u)
    ln+="\\\\"
    print ln
    if rowname=="W" or rowname=="Total" or rowname=="Data":
        print "\hline"

print "\\hline"
print "\\end{tabular}"
if region != "WW" and region != "ZZ":
    print "}"
print "\\label{tab:yields_%s}" % region
if region == "WW" or region == "ZZ":
    print "\end{center}"
print "\\end{table}"
