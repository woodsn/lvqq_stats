#./reco_combinednickSCT81116.sh RDO.09094178._000773.pool.root.1
import condor, time, os, commands
timestr = time.strftime("%m%d%Y_%H%M")


#signals = ['ggHWWNWA', 'VBFWWNWA', 'HVTWW', 'HVTWZ', 'RSGWW', 'VBF-HVTWW', 'VBF-HVTWZ']
signals = ['HVTWW', 'HVTWZ', 'VBF-HVTWW', 'VBF-HVTWZ']
mass = ['1000']
selection = 'combined'
files=[]
directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_combined/May5/'
#directory ='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_combined/May5/noJEScorr/'
for s in signals:
	for m in mass:
		this_file=directory+s+'/ws_HPLP_'+m+'.root'
		files.append(this_file)
#files = commands.getoutput('ls ' + directory + '/RDO*')
#files = files.split('\n')

for i, iFile in enumerate(files):
    files[i] = iFile
print files
#files = ['/export/share/gauss/woodsn/15k_step16/Inclined/mc15_14TeV.117050.PowhegPythia_P2011C_ttbar.recon.RDO.e2176_s2988_s3000_r8832/RDO.10041692._000027.pool.root.1']
#files = ['/export/share/gauss/woodsn/15k_step16/mc15_14TeV/RDO.10041674._000020.pool.root.1']
#files = ['/export/share/gauss/woodsn/step16/mc15_14TeV.117050.PowhegPythia_P2011C_ttbar.recon.RDO.e2176_s2987_s2999_r8712/RDO.09832411._000001.pool.root.1']
exe = 'condor_combined_runFitCrossCheck.py'
argtemplate = ' %s'
dirname = 'NuisanceCheck_' + directory[-7]+timestr
condor.run(exe,argtemplate,files, dirname, 1)







#inputfiles = ['/export/share/gauss/woodsn/samples/mc15_14TeV.117050.PowhegPythia_P2011C_ttbar.recon.RDO.e2176_s2943_t610_r8414_tid09342203_00/step1_5_RDO/RDO.09342203._000001.pool.root.1']
#inputfiles = ['RDO.09342203._000001.pool.root.1','RDO.09342203._000002.pool.root.1','RDO.09342203._000003.pool.root.1','RDO.09342203._000004.pool.root.1', 'RDO.09342203._000005.pool.root.1','RDO.09342203._000006.pool.root.1','RDO.09342203._000007.pool.root.1','RDO.09342203._000008.pool.root.1','RDO.09342203._000009.pool.root.1','RDO.09342203._000010.pool.root.1','RDO.09342203._000011.pool.root.1','RDO.09342203._000012.pool.root.1','RDO.09342203._000013.pool.root.1','RDO.09342203._000014.pool.root.1','RDO.09342203._000015.pool.root.1','RDO.09342203._000016.pool.root.1','RDO.09342203._000017.pool.root.1','RDO.09342203._000018.pool.root.1','RDO.09342203._000019.pool.root.1','RDO.09342203._000020.pool.root.1','RDO.09342203._000021.pool.root.1','RDO.09342203._000022.pool.root.1','RDO.09342203._000023.pool.root.1','RDO.09342203._000024.pool.root.1','RDO.09342203._000025.pool.root.1', 'RDO.09342203._000026.pool.root.1','RDO.09342203._000027.pool.root.1','RDO.09342203._000028.pool.root.1','RDO.09342203._000029.pool.root.1','RDO.09342203._000030.pool.root.1','RDO.09342203._000031.pool.root.1','RDO.09342203._000032.pool.root.1','RDO.09342203._000033.pool.root.1','RDO.09342203._000034.pool.root.1','RDO.09342203._000035.pool.root.1','RDO.09342203._000111.pool.root.1','RDO.09342203._000112.pool.root.1','RDO.09342203._000113.pool.root.1','RDO.09342203._000114.pool.root.1','RDO.09342203._000115.pool.root.1']
