#!/usr/bin/python
import sys, os, commands
import subprocess
from ROOT import gROOT

mass = "1000"
#mass = ("300", "900", "1400")
signal = ("ggHWWNWA", "HVTWW", "HVTWZ", "RSGWW", "VBFWWNWA")


for sig in signal:
        for m in mass:
                for s in selection:
                        cmdstring = 'python runFitCrossCheck.py %s %s %s %s' %(sig,m,purity,s)
                        print 'working on signal,mass,purity,selection: '+sig + ',' + m + ',' + purity + ',' + s
                        os.system(cmdstring)
                        print 'DONE :))))'








formatted_args = alg[0] + "," + str(alg[1]) + "," + str(alg[2]) + "," + alg[3] \
                              + ",\"" + workspace + "\",\"" + directory + "\",\"" \
                              + workspaceName + "\",\"" + modelConfigName + "\",\"" + ObsDataName + "\""

pids.append(subprocess.Popen(["root", "-l", "-b", "-q",
                                      "FitCrossCheckForLimits.C+("+formatted_args+")"],
                                     stderr=output_f, stdout = output_f))


workspace="/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_combined/%s/%s/ws_HPLP_%s.root",signal,selection,mass
root -l -b <<EOF

.L CombFitCrossCheckForLimits.C+

LimitCrossCheck::PlotFitCrossChecks(workspace,args, "combined", "ModelConfig", "obsData" )  

.q

EOF
