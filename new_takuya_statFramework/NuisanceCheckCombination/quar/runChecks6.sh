#!/bin/bash

root -l -b <<EOF

.L CombFitCrossCheckForLimits.C+

LimitCrossCheck::PlotFitCrossChecks("/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheckCombination/ws_HPLP_2000.root", "results6/", "combined", "ModelConfig", "obsData" )  

.q

EOF



#LimitCrossCheck::PlotFitCrossChecks("/afs/cern.ch/work/k/kalliopi/private/lvqq_Resolved_WS/v17_Aug_2016/Combined/combined_t_VBFWWNWA700.root", "results/", "combined", "ModelConfig", "obsData" )
#LimitCrossCheck::PlotFitCrossChecks("../CombinationTool/ws_combined_stats/RSGWW/ws_HPLP_4000.root", "results/", "combined", "ModelConfig", "obsData" )  
