#!/usr/bin/env python

import sys
import os

if len(sys.argv)<3:
    print """                                                                                                                                                                         
Usage:                                                                                                                                                                                
  python %prog [options]
                                                                                                                                                                                      
    -d [dir]: [dir] is a directory of files for tree merging (all are included!) 
    -f [file]: [file] is a file listing all the files for tree merging
"""
    sys.exit()

flag = sys.argv[1]

var = sys.argv[2]

infile = var
if flag=='-d':
    os.system('ls '+str(var)+'/*.root > merge.txt')
elif flag=='-f':
    os.system('cp '+str(var)+' merge.txt')
else:
    sys.exit()

os.system("root -l -q -b mergeListOfTrees.C")
