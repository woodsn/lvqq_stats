
if __name__ == '__main__':
  import ROOT

  for x in ['HVT_WW.txt', 'HVT_WZ.txt', 'Gravi_WW.txt', 'Gravi_ZZ.txt']:
    f = ROOT.TFile('minitrees/' + x.replace('txt', 'root'), 'RECREATE')
    t = ROOT.TTree('stats', 'stats')
    t.ReadFile('minitrees/' + x, 'point/F:obs_upperlimit/F:exp_upperlimit/F:null_pvalue/F')
    f.Write()
    
    f.IsA().Destructor(f)
  
  
