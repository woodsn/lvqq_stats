#include <TLatex.h>
#include <TMarker.h>
#include <TLine.h>
#include <TPave.h>
#include <TCanvas.h>
#include <TROOT.h>

const Double_t tsize = 0.04;

void ATLASLabel(Double_t x,Double_t y,TString  text,Color_t color) 
{
  TLatex *l = new TLatex(); //l->SetTextAlign(12); l->SetTextSize(tsize); 
  l->SetTextSize(tsize);
  l->SetNDC();
  l->SetTextFont(72);
  l->SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l->DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex *p = new TLatex(); 
    p->SetTextSize(tsize);
    p->SetNDC();
    p->SetTextFont(42);
    p->SetTextColor(color);
    p->DrawLatex(x+delx,y,text);
    //    p->DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}

void myText(Double_t x,Double_t y,Color_t color,TString text) 
{
  //Double_t tsize=0.05;
  TLatex *l = new TLatex(); //l->SetTextAlign(12); l->SetTextSize(tsize); 
  l->SetTextSize(tsize);
  l->SetNDC();
  l->SetTextColor(color);
  l->DrawLatex(x,y,text);
}

void myMarkerText(Double_t x,Double_t y,Int_t color,Int_t mstyle,TString text) 
{
  //  printf("**myMarker: text= %s\ m ",text);

  TMarker *marker = new TMarker(x-(0.8*tsize),y,8);
  marker->SetMarkerColor(color);  marker->SetNDC();
  marker->SetMarkerStyle(mstyle);
  marker->SetMarkerSize(2.0);
  marker->Draw();

  TLatex *l = new TLatex(); l->SetTextAlign(12); l->SetTextSize(tsize); 
  l->SetNDC();
  l->DrawLatex(x,y,text);
}


void myBoxText(Double_t x, Double_t y,Double_t boxsize,Int_t mcolor,Int_t lineStyle, Int_t lineColor, Int_t lineWidth, TString text) 
{

  TLatex *l = new TLatex(); l->SetTextAlign(12); l->SetTextSize(tsize); 
  l->SetNDC();
  l->DrawLatex(x,y,text);

  Double_t y1=y-0.25*tsize;
  Double_t y2=y+0.25*tsize;
  Double_t x2=x-0.3*tsize;
  Double_t x1=x2-boxsize;

  printf("x1= %f x2= %f y1= %f y2= %f \n",x1,x2,y1,y2);

  TPave *mbox= new TPave(x1,y1,x2,y2,0,"NDC");

  mbox->SetFillColor(mcolor);
  mbox->SetFillStyle(1001);
  mbox->Draw();

  TLine *mline = new TLine();
  mline->SetLineColor(lineColor); // VALERIO
  mline->SetLineStyle(lineStyle); // VALERIO
  mline->SetLineWidth(lineWidth);
  Double_t y_new=(y1+y2)/2.;
  mline->DrawLineNDC(x1,y_new,x2,y_new);

}

void labels(TString file, TString codename = "") {
  TString mode = "";
  if (file.Contains("limit")) mode = "limit";
  else if (file.Contains("combined")) mode = "p0comb";
  else if (file.Contains("p0")) mode = "p0";
  else throw std::runtime_error("WTF");

  TString channel = "";
  if (file.Contains("WW")) channel = "WW";
  else if (file.Contains("WZ")) channel = "WZ";
  else if (file.Contains("ZZ")) channel = "ZZ";

  TString particle = "";
  TString nickname = "";
  TString happysuffix = "";
  if (file.Contains("HVT")) {
    happysuffix = channel + "_HVT";
    if (channel == "WW") {
      particle = "Z'";
      nickname = "HVT Z'#rightarrow" + channel;
    }
    else if (channel == "WZ") {
      particle = "W'";
      nickname = "HVT W'#rightarrow" + channel;
    }
    else throw std::runtime_error("WTF HVT does not have ZZ");
  } else if (file.Contains("Gravi")) {
    happysuffix = channel + "_Gravi";
    particle = "G_{RS}";
    nickname = "G_{RS}#rightarrow" + channel + ", k/#bar{M}_{Pl}=1";
  } else if (file.Contains("combined") == kFALSE) throw std::runtime_error("WTF2");

  gROOT->ProcessLine(".x " + file);

  // attempt at y-axis tricks
  if (mode == "limit") {
    for (auto x: *gPad->GetListOfPrimitives()) {
      TGraph *gr = dynamic_cast<TGraph*> (x);
      if (gr) gr->GetYaxis()->SetTitle("#sigma(pp#rightarrow" + particle + "+X)#timesBR(" + particle + "#rightarrow" + channel + ") [fb]");
      else cout << x->ClassName() << endl;
    }
  }


  const TString codename_for_label = (codename == "aaron") ? "" : codename;
  ATLASLabel(0.2, 0.88, "Internal " + codename_for_label, kBlack);
  myText(0.2, 0.83, kBlack, "#sqrt{s} = 13 TeV, 3.2 fb^{-1}");

  // limit
  if (mode == "limit") {
  myMarkerText(0.6, 0.80, kBlack, 20, "Observed 95% CL");
  myBoxText(0.6, 0.75, 0.04, kWhite,  kDashed, kBlack, 4, "Expected 95% CL");
  myBoxText(0.6, 0.70, 0.04, kWhite,  kSolid,  kRed, 2, nickname);
//myBoxText(0.6, 0.70, 0.04, kGreen,  kSolid,  kGreen, 4, "#pm 1#sigma");
//myBoxText(0.6, 0.65, 0.04, kYellow, kSolid,  kYellow, 4, "#pm 2#sigma");
//myBoxText(0.6, 0.60, 0.04, kWhite,  kSolid,  kRed, 2, nickname);
  } else if (mode == "p0comb"){
  // combined p0 plot
  myBoxText(0.6, 0.85, 0.04, kWhite, 1, kBlack, 4, "HVT W'#rightarrowWZ");
  myBoxText(0.6, 0.80, 0.04, kWhite, 2, kBlue+1, 4, "HVT Z'#rightarrowWW");
  myBoxText(0.6, 0.75, 0.04, kWhite, 5, kCyan, 4, "G_{RS}#rightarrowWW");
  myBoxText(0.6, 0.70, 0.04, kWhite, 3, kMagenta, 4, "G_{RS}#rightarrowZZ");
  }

  const TString actual_codename = (codename == "") ? "" : ("_" + codename);
  for (auto obj: *gROOT->GetListOfCanvases()) {
    TCanvas *c = dynamic_cast<TCanvas*>(obj);
    if (mode != "limit") {
      c->SetRightMargin(0.085);
    }
    c->SaveAs("plots/" + happysuffix + "_" + mode + actual_codename + ".pdf");
    c->SaveAs("plots/" + happysuffix + "_" + mode + actual_codename + ".C");
  }
}
