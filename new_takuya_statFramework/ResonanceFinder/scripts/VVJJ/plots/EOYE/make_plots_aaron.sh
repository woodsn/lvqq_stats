root -b -q labels.C\(\"plots/WZ_HVT_limit.C\",\"\"\)
root -b -q labels.C\(\"plots/WW_HVT_limit.C\",\"\"\)
root -b -q labels.C\(\"plots/WW_Gravi_limit.C\",\"\"\)
root -b -q labels.C\(\"plots/ZZ_Gravi_limit.C\",\"\"\)

sed -i '' 's/nan/0.5/g' plots/WZ_HVT_p0.C
sed -i '' 's/nan/0.5/g' plots/WW_HVT_p0.C
sed -i '' 's/nan/0.5/g' plots/WW_Gravi_p0.C
sed -i '' 's/nan/0.5/g' plots/ZZ_Gravi_p0.C

root -b -q labels.C\(\"plots/WZ_HVT_p0.C\",\"\"\)
root -b -q labels.C\(\"plots/WW_HVT_p0.C\",\"\"\)
root -b -q labels.C\(\"plots/WW_Gravi_p0.C\",\"\"\)
root -b -q labels.C\(\"plots/ZZ_Gravi_p0.C\",\"\"\)
