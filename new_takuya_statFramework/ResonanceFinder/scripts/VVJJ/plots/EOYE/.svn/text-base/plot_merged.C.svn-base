#include <Plotter.h>
#include <StatisticalResultsCollection.h>

void plot_merged2(TString selection, TString interpretation, TString indir, TString outdir)
{
   RF::Plotter plotter(selection + "_" + interpretation, outdir);
   plotter.setDoObserved(kTRUE);
   plotter.setVarUnit("GeV");

         RF::StatisticalResultsCollection statRes("HVT_WZ");
         statRes.retrieve(indir + "/" + interpretation + "_" + selection + ".root");

   if (selection == "WZ") {
      const Double_t  bf = 0.699 * 0.676;

      if (interpretation == "HVT") {
         plotter.setVarName("m_{W\'}");
         plotter.setSpec(1000, bf, 201.7);
         plotter.setSpec(1100, bf, 132.9);
         plotter.setSpec(1200, bf, 90.14);
         plotter.setSpec(1300, bf, 62.72);
         plotter.setSpec(1400, bf, 44.36);
         plotter.setSpec(1500, bf, 31.95);
         plotter.setSpec(1600, bf, 23.32);
         plotter.setSpec(1700, bf, 17.28);
         plotter.setSpec(1800, bf, 12.98);
         plotter.setSpec(1900, bf, 9.819);
         plotter.setSpec(2000, bf, 7.5);
         plotter.setSpec(2200, bf, 4.454);
         plotter.setSpec(2400, bf, 2.703);
         plotter.setSpec(2600, bf, 1.679);
         plotter.setSpec(2800, bf, 1.054);
         plotter.setSpec(3000, bf, 0.6688);
         plotter.setSpec(3500, bf, 0.226);
         plotter.setSpec(4000, bf, 0.07822);
         plotter.setSpec(4500, bf, 0.02748);
      } else throw std::runtime_error("Interpretation for WZ not found");
   } else if (selection == "WW") {
      const Double_t bf = 0.676 * 0.676;
      if (interpretation == "Gravi") {
         plotter.setVarName("m_{G_{RS}}");
         plotter.setSpec(1000, bf, 33.21);
         plotter.setSpec(1100, bf, 19.17);
         plotter.setSpec(1200, bf, 11.53);
         plotter.setSpec(1300, bf, 7.176);
         plotter.setSpec(1400, bf, 4.584);
         plotter.setSpec(1500, bf, 3.002);
         plotter.setSpec(1600, bf, 2.004);
         plotter.setSpec(1700, bf, 1.363);
         plotter.setSpec(1800, bf, 0.9409);
         plotter.setSpec(1900, bf, 0.6583);
         plotter.setSpec(2000, bf, 0.4656);
         plotter.setSpec(2200, bf, 0.2401);
         plotter.setSpec(2400, bf, 0.1281);
         plotter.setSpec(2600, bf, 0.07022);
         plotter.setSpec(2800, bf, 0.03933);
         plotter.setSpec(3500, bf, 0.005847);
         plotter.setSpec(4000, bf, 0.001615);
         plotter.setSpec(4500, bf, 0.0004663);
         plotter.setSpec(5000, bf, 0.0001405);
      } else if (interpretation == "HVT") {
         plotter.setVarName("m_{Z\'}");
         plotter.setSpec(1000, bf, 93.51);
         plotter.setSpec(1100, bf, 61.32);
         plotter.setSpec(1200, bf, 41.37);
         plotter.setSpec(1300, bf, 28.65);
         plotter.setSpec(1400, bf, 20.24);
         plotter.setSpec(1500, bf, 14.46);
         plotter.setSpec(1600, bf, 10.6);
         plotter.setSpec(1700, bf, 7.851);
         plotter.setSpec(1800, bf, 5.808);
         plotter.setSpec(1900, bf, 4.395);
         plotter.setSpec(2000, bf, 3.345);
         plotter.setSpec(2200, bf, 1.968);
         plotter.setSpec(2400, bf, 1.196);
         plotter.setSpec(2600, bf, 0.7407);
         plotter.setSpec(2800, bf, 0.4671);
         plotter.setSpec(3000, bf, 0.296);
         plotter.setSpec(3500, bf, 0.09876);
         plotter.setSpec(4000, bf, 0.0349);
         plotter.setSpec(4500, bf, 0.01239);
         plotter.setSpec(5000, bf, 0.004408);
      } else throw std::runtime_error("WW interpretation not found");
   } else if (selection == "ZZ") {
      const Double_t bf = 0.699 * 0.699;
      if (interpretation == "Gravi") {
         plotter.setVarName("m_{G_{RS}}");
         plotter.setSpec(1000, bf, 18.33);
         plotter.setSpec(1100, bf, 10.53);
         plotter.setSpec(1200, bf, 6.327);
         plotter.setSpec(1300, bf, 3.9268);
         plotter.setSpec(1400, bf, 2.509);
         plotter.setSpec(1500, bf, 1.641);
         plotter.setSpec(1600, bf, 1.095);
         plotter.setSpec(1700, bf, 0.7433);
         plotter.setSpec(1800, bf, 0.5131);
         plotter.setSpec(1900, bf, 0.3585);
         plotter.setSpec(2000, bf, 0.2535);
         plotter.setSpec(2200, bf, 0.1307);
         plotter.setSpec(2400, bf, 0.0697);
         plotter.setSpec(2600, bf, 0.03809);
         plotter.setSpec(2800, bf, 0.02133);
         plotter.setSpec(3000, bf, 0.01216);
         plotter.setSpec(3500, bf, 0.003164);
         plotter.setSpec(4000, bf, 0.0008763);
         plotter.setSpec(4500, bf, 0.0002533);
         plotter.setSpec(5000, bf, 7.645e-05);
      } else throw std::runtime_error("ZZ interpretation not implemented");
   } else {
      throw std::runtime_error("Unknown selection");
   }

plotter.setOutputFormat(RF::Plotter::C);
plotter.process(statRes);
}

void plot_merged(TString indir = "minitrees", TString outdir = "plots")
{
   /*
   gROOT->ProcessLine(".L StatisticalResults.cxx+");
   gROOT->ProcessLine(".L StatisticalResultsCollection.cxx+");
   gROOT->ProcessLine(".L Plotter.cxx+");
   gROOT->ProcessLine(".x plot_merged.C+");
   */

   plot_merged2("WZ", "HVT", indir, outdir);
   plot_merged2("WW", "HVT", indir, outdir);
   plot_merged2("WW", "Gravi", indir, outdir);
   plot_merged2("ZZ", "Gravi", indir, outdir);
}
