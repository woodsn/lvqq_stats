def get_interesting(fname, new_graph):
  result = [ list(), list() ] # header, text

  with open(fname) as f:
    lines = f.readlines()

    save = False
    goodJustEnded = False
    for line in lines:
      if not save and 'Double_t' in line:
        save = True
      if save and 'Draw("apl")' in line:
        save = False
        goodJustEnded = True

      if save:
        result[1].append(line.replace('graph', 'graph_' + new_graph))
      else:
        if not goodJustEnded:
          result[0].append(line.replace('void ' + new_graph + '_p0', 'void combined_p0'))
        else:
          result[0].append('SOSTITUISCIMI')
          goodJustEnded = False

  return result

if __name__ == '__main__':
  to_take = {
  'WZ_HVT': 'plots/WZ_HVT_p0.C',
  'WW_HVT': 'plots/WW_HVT_p0.C',
  'WW_Gravi': 'plots/WW_Gravi_p0.C',
  'ZZ_Gravi': 'plots/ZZ_Gravi_p0.C',
  }

  newlines = '''
  /// BY HAND INSERTION
     graph_WZ_HVT->SetLineColor(kBlack);
     graph_WZ_HVT->SetLineStyle(1);
     graph_WZ_HVT->SetLineWidth(2);
     graph_WZ_HVT->Draw("al");

     graph_WW_HVT->SetLineColor(kBlue+1);
     graph_WW_HVT->SetLineStyle(2);
     graph_WW_HVT->SetLineWidth(2);
     graph_WW_HVT->Draw("lsame");

     graph_WW_Gravi->SetLineColor(kCyan);
     graph_WW_Gravi->SetLineStyle(5);
     graph_WW_Gravi->SetLineWidth(2);
     graph_WW_Gravi->Draw("lsame");

     graph_ZZ_Gravi->SetLineColor(kMagenta);
     graph_ZZ_Gravi->SetLineStyle(3);
     graph_ZZ_Gravi->SetLineWidth(2);
     graph_ZZ_Gravi->Draw("lsame");

     
  /// END BY HAND INSERTION
  '''

  body = []
  for i,key in enumerate(to_take.keys()):
    header, text = get_interesting(to_take[key], key)
    for l in text:
      body.append(l)

  body.append( newlines )

  print (''.join(header)).replace('SOSTITUISCIMI', ''.join(body))
