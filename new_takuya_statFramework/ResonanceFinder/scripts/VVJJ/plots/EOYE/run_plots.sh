python interpret.py
python prepare_tree.py 
root -b -q launch_plotter.C 
./make_plots_aaron.sh 
python getTables.py > plots/limits.tex

python combine_p0s.py > plots/combined_p0.C
root -l labels.C\(\"plots/combined_p0.C\"\)
