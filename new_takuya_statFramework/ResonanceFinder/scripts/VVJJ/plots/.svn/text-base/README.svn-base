Valerio Ippolito - Harvard University

HOW TO RUN

PLEASE >>>>>>>> USE ROOT 6.04/12  (THIS IS CRUCIAL)


- to produce limit and p0 plots for individual channels using ResonanceFinder's output:

  0) go in a subfolder of this folder (it's more convenient) and link the code,
       ../link_needed_code.sh
  1) download all results (plots, workspaces, etc) in a single folder
     for example:
        results_v4_3p2fb
  2) edit make_plots.sh to point to the .C limit and p0 plots contained therein
  3) mkdir plots
  4) ./make_plots.sh 
     (this will also replace nan's with 0.5, in the p0 plots)


- to rerun limits with Aaron's optimised macro:

  5) edit make_limits.sh to point to the actualWorkspaces files in the folder defined in
     step 1) above, making sure all mass points are represented for each model
  6) mkdir root-files
     mkdir logs
  7) ./make_limits.sh
     (this will create root files to be read with StatisticalResultsCollection)
  8) mkdir merged
  9) ./merge_limits.sh
  10) check plot_merged.C and StatisticalResultsCollection.cxx to make sure all points
      are defined and outputs from Aaron's code are read and written in proper folders
  11) mkdir plots_aaron
  12) run
        root -l
        gROOT->ProcessLine(".L StatisticalResults.cxx+");
        gROOT->ProcessLine(".L StatisticalResultsCollection.cxx+");
        gROOT->ProcessLine(".L Plotter.cxx+");
        gROOT->ProcessLine(".x plot_merged.C+");

  13) ./make_plots_aaron.sh
     (this will create plots in plots/, called *aaron.pdf)
        

- for the combined p0 plot

  14) edit combine_p0s.py to point to the files you want to combine
  15) run
        cd plots
        python ../../combine_p0s.py > combined_p0.C
        root -l ../../labels.C\(\"combined_p0.C\"\)

- for the LaTeX tables
  16) check getTables.py (especially the __main__ part) to make sure all
  mass points and cross sections are  defined, and that paths are correct

  17) run
        cd ..
        python getTables.py > limits.tex
