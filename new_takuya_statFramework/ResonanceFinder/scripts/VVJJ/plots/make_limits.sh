#!/bin/bash

python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 1200 &> logs/LOG_HVT_WZ_1200.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 1300 &> logs/LOG_HVT_WZ_1300.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 1400 &> logs/LOG_HVT_WZ_1400.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 1500 &> logs/LOG_HVT_WZ_1500.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 1600 &> logs/LOG_HVT_WZ_1600.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 1700 &> logs/LOG_HVT_WZ_1700.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 1800 &> logs/LOG_HVT_WZ_1800.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 1900 &> logs/LOG_HVT_WZ_1900.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 2000 &> logs/LOG_HVT_WZ_2000.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 2200 &> logs/LOG_HVT_WZ_2200.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WZ/ws/VVJJ_HVT_WZ_actualWorkspaces_v4_WZ_HVT_obs_wNtrk.root HVT_WZ 0 2400 &> logs/LOG_HVT_WZ_2400.txt

python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 1200 &> logs/LOG_HVT_WW_1200.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 1300 &> logs/LOG_HVT_WW_1300.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 1400 &> logs/LOG_HVT_WW_1400.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 1500 &> logs/LOG_HVT_WW_1500.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 1600 &> logs/LOG_HVT_WW_1600.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 1700 &> logs/LOG_HVT_WW_1700.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 1800 &> logs/LOG_HVT_WW_1800.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 1900 &> logs/LOG_HVT_WW_1900.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 2000 &> logs/LOG_HVT_WW_2000.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 2200 &> logs/LOG_HVT_WW_2200.txt
python getLimit.py results_v4_3p2fb/VVJJ_HVT_WW/ws/VVJJ_HVT_WW_actualWorkspaces_v4_WW_HVT_obs_wNtrk.root HVT_WW 0 2400 &> logs/LOG_HVT_WW_2400.txt

python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 1200 &> logs/LOG_Gravi_WW_1200.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 1300 &> logs/LOG_Gravi_WW_1300.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 1400 &> logs/LOG_Gravi_WW_1400.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 1500 &> logs/LOG_Gravi_WW_1500.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 1600 &> logs/LOG_Gravi_WW_1600.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 1700 &> logs/LOG_Gravi_WW_1700.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 1800 &> logs/LOG_Gravi_WW_1800.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 1900 &> logs/LOG_Gravi_WW_1900.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 2000 &> logs/LOG_Gravi_WW_2000.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 2200 &> logs/LOG_Gravi_WW_2200.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_WW/ws/VVJJ_GRAVI_WW_actualWorkspaces_v4_WW_Gravi_obs_wNtrk.root Gravi_WW 0 2400 &> logs/LOG_Gravi_WW_2400.txt

python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 1200 &> logs/LOG_Gravi_ZZ_1200.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 1300 &> logs/LOG_Gravi_ZZ_1300.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 1400 &> logs/LOG_Gravi_ZZ_1400.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 1500 &> logs/LOG_Gravi_ZZ_1500.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 1600 &> logs/LOG_Gravi_ZZ_1600.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 1700 &> logs/LOG_Gravi_ZZ_1700.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 1800 &> logs/LOG_Gravi_ZZ_1800.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 1900 &> logs/LOG_Gravi_ZZ_1900.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 2000 &> logs/LOG_Gravi_ZZ_2000.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 2200 &> logs/LOG_Gravi_ZZ_2200.txt
python getLimit.py results_v4_3p2fb/VVJJ_GRAVI_ZZ/ws/VVJJ_GRAVI_ZZ_actualWorkspaces_v4_ZZ_Gravi_obs_wNtrk.root Gravi_ZZ 0 2400 &> logs/LOG_Gravi_ZZ_2400.txt

