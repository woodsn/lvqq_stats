ln -s ../labels.C ../getLimit.py ../runAsymptoticsCLs.C ../plot_merged.C ../StatisticalResults* ../Plotter.* ../getTables.py ../combine_p0s.py .
ln -s ../make_plots.sh ../make_limits.sh ../merge_limits.sh ../make_plots_aaron.sh .

mkdir plots
mkdir root-files
mkdir logs
mkdir merged
mkdir plots_aaron
