root -b -q labels.C\(\"plots_aaron/WZ_HVT_limit.C\",\"aaron\"\)
root -b -q labels.C\(\"plots_aaron/WW_HVT_limit.C\",\"aaron\"\)
root -b -q labels.C\(\"plots_aaron/WW_Gravi_limit.C\",\"aaron\"\)
root -b -q labels.C\(\"plots_aaron/ZZ_Gravi_limit.C\",\"aaron\"\)

sed -i '' 's/nan/0.5/g' plots_aaron/WZ_HVT_p0.C
sed -i '' 's/nan/0.5/g' plots_aaron/WW_HVT_p0.C
sed -i '' 's/nan/0.5/g' plots_aaron/WW_Gravi_p0.C
sed -i '' 's/nan/0.5/g' plots_aaron/ZZ_Gravi_p0.C

root -b -q labels.C\(\"plots_aaron/WZ_HVT_p0.C\",\"aaron\"\)
root -b -q labels.C\(\"plots_aaron/WW_HVT_p0.C\",\"aaron\"\)
root -b -q labels.C\(\"plots_aaron/WW_Gravi_p0.C\",\"aaron\"\)
root -b -q labels.C\(\"plots_aaron/ZZ_Gravi_p0.C\",\"aaron\"\)
