#include <iostream>
#include <fstream>
#include "TList.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

void mergeListOfTrees(void){

  TList *list = new TList();
  TString fList = "merge.txt";
  TString outFile = "mergedTrees.root";

  string line;
  ifstream myfile (fList.Data());
  if (myfile.is_open()){
    while ( getline (myfile,line) ){
      cout << line << '\n';
      TFile* fin = new TFile(line.c_str());
      if(fin){
	TTree* tin = (TTree*)fin->Get("stats");
	if(tin) list->Add(tin);
      }
    }
    myfile.close();
  }
  else cout << "Unable to open file"; 

  TFile* fout = new TFile(outFile, "RECREATE");
  TTree* tout = TTree::MergeTrees(list);
  fout->Write();
  fout->Close();

}
