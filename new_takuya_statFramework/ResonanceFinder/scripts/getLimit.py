#!/usr/bin/env python

import sys
import ROOT
import os

if len(sys.argv)<2:
    print """
Usage: 
  python %prog [workspace] [ws flag] [exp/obs] [mass]

    expected = 1
    observed = 0
    default mass point = 125
"""
    sys.exit()

ws = sys.argv[1]

flag = sys.argv[2]

if len(sys.argv)>3:
    is_expected = bool(int(sys.argv[3]))
else:
    is_expected = False

if len(sys.argv)>4:
    mass = sys.argv[4]
else:
    mass = "125"

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L runAsymptoticsCLs.C+")

if is_expected:
    ROOT.doExpected(True)
    suff = "exp"
else:
    ROOT.doExpected(False)
    suff = "obs"

ROOT.doPvalues(True);

#FIXME should be an option
ROOT.doBetterBands(False)
#ROOT.doBetterBands(True)

ROOT.doInjection(False)

outdir = flag + "-" + suff

ROOT.runAsymptoticsCLs(ws, "combined","ModelConfig", "obsData", "", outdir, mass, 0.95)
#ROOT.runAsymptoticsCLs(ws, "combined","ModelConfig", "combData", "", outdir, mass, 0.95)

f=ROOT.TFile("root-files/"+outdir+"/"+mass+".root")
tree = f.Get("stats")

obsL = tree.GetLeaf("obs_upperlimit");
medL = tree.GetLeaf("exp_upperlimit");
m2L = tree.GetLeaf("exp_upperlimit_minus2");
m1L = tree.GetLeaf("exp_upperlimit_minus1");
p1L = tree.GetLeaf("exp_upperlimit_plus1");
p2L = tree.GetLeaf("exp_upperlimit_plus2");

tree.GetEntry(0);

med = medL.GetValue()
obs = obsL.GetValue()
p2 = p2L.GetValue()
p1 = p1L.GetValue()
m1 = m1L.GetValue()
m2 = m2L.GetValue()

#print "Injected limit:", inj
print "Expected limit:", med, "+", p1-med, "-", med-m1
print "Observed limit:", obs
print "{0:.2f}".format(med)

outfname = 'root-files/'+outdir+'/limit_'+mass+'.txt'
#os.system('echo "Injected limit: '+str(inj)+'" > '+ outfname)
os.system('echo "Expected limit: '+str(med)+' +'+str(p1-med)+' -'+str(med-m1)+'" >> '+ outfname)
os.system('echo "Observed limit: '+str(obs)+'" >> '+outfname)
os.system('echo "Expected limit: {0:.2f}^{{+{1:.2f}}}_{{-{2:.2f}}}" >> {3}'.format(med, p1-med, med-m1, outfname))
os.system('echo "Observed limit: {0:.2f}" >> {1}'.format(obs, outfname))
os.system('echo " " >> {0}'.format(outfname))
os.system('echo "obs -2s -1s exp +1s _2s" >> {0}'.format(outfname))
os.system('echo "{0:.2f} & {1:.2f} &  {2:.2f} & {3:.2f} & {4:.2f} & {5:.2f} \\\\" >> {6}'.format(obs, m2, m1, med, p1, p2, outfname))

