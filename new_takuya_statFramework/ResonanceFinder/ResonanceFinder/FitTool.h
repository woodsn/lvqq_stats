#ifndef __RF_FitTool_h__
#define __RF_FitTool_h__

// Wade Fisher -- Michigan State University

/* -*- mode: c++ -*- */
// Class housing functions to generate fits under various hypotheses, useful for creating post-fit plots.
//


#include "ResonanceFinder/RooStatsUtils.h"

#include "RooWorkspace.h"
#include <TString.h>
#include <string>

namespace RooStats {

   class FitTool {

   public:
      FitTool();
      ~FitTool() {};

      void generateSBFit(RooWorkspace * w, const char * modelSBName, const char * modelBName, const char * dataName);

      void generateBFit(){}

      void SetParameter(const char * name, const char * value);
      void SetParameter(const char * name, bool value);
      void SetParameter(const char * name, int value);
      void SetParameter(const char * name, double value);

   private:

      std::string mMinimizerType;                  // minimizer type (default is what is in ROOT::Math::MinimizerOptions::DefaultMinimizerType()
      std::string mSBFitFileName;
      std::string mBFitFileName;
 
   };

} // end namespace RooStats

#endif
