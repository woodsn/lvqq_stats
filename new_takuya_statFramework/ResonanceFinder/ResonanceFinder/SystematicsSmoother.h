#ifndef __RF_SystematicsSmoother_h__
#define __RF_SystematicsSmoother_h__

///
/// \brief SystematicsSmoother - perform rebinning to smooth systematic histograms
/// \author - code is taken from the HSG5 Run1 fitting code (WSMaker, BinningTool)
/// \date November, 2015
///
/// SystematicsSmoother is a simple class which takes an HistoCollection as an input,
/// and performs a smoothing of systematic histograms based on some pre-defined rebinning scheme. The chosen rebinnig scheeme is such that the ratio of the systematically varied histogram to
/// nominal has at most a certain number of local extrema, not counting first and last bins. An averaging overadjacent bins in addition is optional.

#include "ResonanceFinder/IHistoManipulator.h"

namespace RF {
   class SystematicsSmoother : public IHistoManipulator {
   public:
      SystematicsSmoother();
      ~SystematicsSmoother();

      virtual void manipulate(HistoCollection &hc);
      void setSmoothFlags(std::map<TString, int> variations);
      void setNumberOfLocalExtrema(int nole);
      void doApplyAveraging(bool doAvg);

   private:
      std::map<TString, int> m_smoothFlags; //smoothing flags
      int m_nole; //number of local extrema
      bool m_doAvg; //average bins

      /*
     * @brief Compute the rebinning needed for a syst histo so that its ratio to
     * nominal has at most @c nmax local extrema, not counting first and last bins
     *
     * This function implements a rebinning algorithm so that the ratio of @c hsys
     * over @c hnom contains at most @c nmax local extrema. The local extrema are removed
     * iteratively starting at the places where the ratio is more compatible with a flat
     * one, which is done by computing a simple chi2.
     *
     * A second pass is then performed, to merge the remaining bins so that the MC stat
     * uncertainty is less than 5% in all the final bins after merging.
     *
     * @param hnom Denominator histogram ("Nominal")
     * @param hsys Numerator histogram ("Systematic")
     * @param nmax Maximum number of local extra allowed in the ratio, not counting
     * the first and last bins
     * @return A vector of bins numbers defining the bin boundaries, in reverse order.
     */
      std::vector<int> getLocalExtremaBinning(RF::Histo_t* hnom, RF::Histo_t* hsys, unsigned int nmax);

     /*
     * @brief Compute a ratio histogram with ratios computed over ranges of bins
     *
     * Auxiliary functions for getLocalExtremaBinning.
     * Returns a new TH1 which is the ratio of nom and sys.
     * It is user's reseponsibility to delete the returned TH1
     *
     * @param hnom	Denominator histogram ("Nominal")
     * @param hsys	Numerator histogram ("Systematic variation")
     * @param bins  Edges of bins that are merged to compute the ratios
     * @return The ratio histogram. It has the same number of bins as the input histograms, but
     * adjacent bins can have the same value. The number of different values is equal to
     * @c bins.size()-1
     */
      TH1* getRatioHist(TH1* hnom, TH1* hsys, const std::vector<int>& bins);

      /*
     * @brief Compute a ratio histogram with ratios computed over ranges of bins
     *
     * Auxiliary functions for getLocalExtremaBinning.
     * The histo @c res is filled with the ratio of sys over nom. It needs to have
     * the same number of bins as the input histograms
     *
     * @param hnom	Denominator histogram ("Nominal")
     * @param hsys	Numerator histogram ("Systematic variation")
     * @param bins  Edges of bins that are merged to compute the ratios
     * @param res   The ratio histogram. It has the same number of bins as the input histograms, but
     * adjacent bins can have the same value. The number of different values is equal to
     * @c bins.size()-1
     */
      void getRatioHist(TH1* hnom, TH1* hsys, const std::vector<int>& bins, TH1* res);
      
      /*
     * @brief Find local extrema in a histogram
     *
     * A local extrema is defined in bin @c i by either:
     * * h[i-1] <= h[i] > h[i+1]
     * * h[i-1] >= h[i] < h[i+1]
     * By convention bins 1 and @c GetNbinsX() are added in the result
     *
     * @todo micro-bug to fix (?) : will return {1, 1} for a 1-bin histogram
     *
     * @param h	The histogram in which to find the extrema
     * @return Bin numbers of the extrema
     */
      std::vector<int> findExtrema(TH1* h);
      
      /*
     * @brief Find the position of an extremum in a sys/nom ratio where the chi2
     * between nom and sys between this extremum and the next is smallest
     *
     * When smoothing systematics histograms, one 'merges' together bins between
     * consecutive extrema of the ratio. In order to chose what bins to merge,
     * we start where at the place of 'lower resistance', i.e where nominal and
     * systematic histograms are already the most similar. This is obtained by
     * computing chi2 between nominal and systematic in the ranges between consecutive
     * extrema (@c computeChi2), then chosing the place where the chi2 is lowest
     *
     * @param hnom	Denominator histogram ("Nominal")
     * @param hsys  Numerator histogram ("Systematic")
     * @param extrema A vector of extrema positions in @c hsys/hnorm ratio, that can
     * be obtained with @c findExtrema
     * @return The position in @c extrema for which the chi2 in the range
     * @c [extrema[pos], extrema[pos+1]] is smallest
     */
      int findSmallerChi2(TH1* hnom, TH1* hsys, const std::vector<int>& extrema);

      /*
     * @brief Compute a compatibility chi2 between two histograms in some range
     *
     * Compute a chi2 indicating how much the syst/nom ratio is compatible with a
     * flat ratio.
     *
     * First the mean ratio over the range is computed. Then the chi2 is evaluated
     * by summing over the bins the difference between the ratio and the mean ratio,
     * where the error considered is the BinError in the nominal histogram.
     *
     * @param hnom  Denominator histograms ("Nominal")
     * @param hsys  Numerator histograms ("Systematic")
     * @param beg First bin (inclusive) for the calculation
     * @param end   Last bin (inclusive) for the calculation
     * @return The chi2 value
     */
      float computeChi2(TH1* hnom, TH1* hsys, int beg, int end);
      /*
     * @brief Remove values within a range in a sorted vector;
     *
     * In a sorted vector of values (typically bin numbers),
     * remove the values in the [lo, hi] range.
     *
     * @param lo Lower value to remove (inclusive)
     * @param hi Highest value to remove (inclusive)
     * @param bins Sorted vector of values
     */
      void mergeBins(int lo, int hi, std::vector<int>& bins);

      /*
     * @brief Compute the relative error of a histogram over a range
     *
     * Return error / value computed over a chosen range.
     *
     * @param hnom The histogram on which the error is computed
     * @param beg First bin (inclusive) for the calculation
     * @param end Last bin (exclusive) for the calculation
     * @return The relative error over the range
     */
      float statError(TH1* hnom, int beg, int end);

     /*
     * @brief Smooth a syst histogram using some rebinning
     *
     * This function performs the actual "smoothing" of a systematic histogram based on
     * some pre-defined rebinning scheme.
     *
     * The ratio histogram is computed for the binning defined with @c bins.
     * If @c smooth is @c true, an additional avering of neighbouring bins is performed
     * in addition.
     * Then the syst histogram is replaced by ratio times nominal, and its integral is
     * forced to be equal to its original one.
     *
     * @param hnom Nominal histogram
     * @param hsys Systematic histogram
     * @param bins Vector of bin edges for the rebinning used to smooth
     * @param smooth Whether to also apply a simple averaging on top of the rebinning
     */
      void smoothHisto(RF::Histo_t* hnom, RF::Histo_t* hsys, const std::vector<int>& bins, bool smooth);

  };
}

#endif

