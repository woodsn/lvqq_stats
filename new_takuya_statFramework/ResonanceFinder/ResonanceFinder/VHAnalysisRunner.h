#ifndef __RF_VHAnalysisRunner_h__
#define __RF_VHAnalysisRunner_h__

///
/// \brief VHAnalysisRunner - binned histogram class for the VH analysis
/// \author Wade Fisher
/// \date June 22 2015
///
/// VHAnalysisRunner runs the VH analysis using histograms
///

#include "ResonanceFinder/IBinnedAnalysisRunner.h"

namespace RF {
   class VHAnalysisRunner : public IBinnedAnalysisRunner {
   public:
      VHAnalysisRunner(TString analysis);
      ~VHAnalysisRunner();

      virtual void configInputGetter();
      virtual void manipulate();

      void setTagNominal(TString tag);
      void setTagData(TString tag);
      void setTagUp(TString tag);
      void setTagDown(TString tag);

      void setRebinFactor(TString chan, int factor);
      void setMergeCommand(TString chan, vector<TString> mer_chans, int flag, TString merged_chan_name);

      void doApplySmoothing(bool doSmooth);
      void setSystSmoothFlag(TString syst, int flag);

      void setIncludeOverflow(bool use);
      void setIncludeUnderflow(bool use);

      void setHistoUpperBound(TString chan, double value);
      void setHistoLowerBound(TString chan, double value);

      void scaleSignal(double sf,TString name);
      void scaleBkgd(double sf, TString name);
      void scaleData(double sf);
      void setLumiRescale(Double_t oldLumi, Double_t newLumi);      

      virtual void addOneSideVariation(TString variation, TString tagUp, TString tagDn);

  private:
      RF::Histo_t* autoRebin(int flag, TString chan, RF::HistoCollection& hc, RF::HistoMap_t& hm);
      void rebinHistos();
      double HVTreso(double mVH, int region, double minBin);

      void mergeCR();
      bool checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin);
      void mergeHighLowMHChan(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm,std::map<TString,RF::Channel>& chan_map);
      bool hasBeenMerged(TString chan,std::map<TString,RF::Channel>& chan_map);

      map<TString, double> m_sigScales;
      map<TString, double> m_bkgdScales;
      map<TString, double> m_rebinFactors;
      map<TString, vector<TString>>  m_mergeChans;
      map<TString, int>  m_mergeFlag;
      map<TString,TString> m_mergedChanName;
      map<TString, int> m_systSmoothFlags;

      map<TString, double> m_upperBound;
      map<TString, double> m_lowerBound;

      vector<TString> m_onesidevar;
      vector<TString> m_onesidetagUp;
      vector<TString> m_onesidetagDn;

      double m_dataScale;
      double m_oldLumi;
      double m_newLumi;

      bool m_doSmooth;
      bool m_useOverflow;
      bool m_useUnderflow;
  };
}

#endif
