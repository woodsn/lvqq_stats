#!/usr/bin/env/python

import os
import os.path
import re
import sys
import math
from array import array
import pickle
import copy
from ROOT import *
from array import *
from ROOT import RooFit as RF
import plotMaker as mkplots
import makeTables as tables

verbose = False


# TODO support toying with several workspaces
# TODO check for memleaks etc...
# TODO reorganize this script into sections:
#       dealing with / moving in workspaces
#       dealing with yields and everythgin that is integrated
#       plot retrieving and making
# FIXME script can't run with varios options and single option is required.
#       if options config: 0,1 => option 0 crash at the end producing the plots. option 1 => do not produce plots
#       if options config: 1,0 => option 1 produce the plots. option 0 plots are equal to option 1 
#       O_o
#       option 0 crash at the end needs to be fix


# Spyros: Added mass as argument
def main(version, directory=None, rfrname=None, is_conditional=False, is_asimov=False, mu=None,
         combined = False, summed = False, sumCR = False):
    """ main function

    Parameters:
    * version: version of the workspace which we want to make plots with
    * directory: where to find FitCrossChecks.root that contains the RooFitResult
    * is_conditional, is_asimov, mu: describe the type of fit chosen for the FitResult
    """

    ws, rfr, is_prefit, suffix, plotdir, g = initialize(version, directory, rfrname, is_conditional, is_asimov, mu)
    print "ws",ws
    print "rfr",rfr
    print "is_prefit",is_prefit
    print "suffix",suffix
    print "plotdir", plotdir
    print "g",g

    if rfrname:
      #try to get mass from rfrname
      try:
        mymass = (rfrname.split("_")[1]).split("p")[0]
      #mymass = mywsname.split("p")[0]
      except:
        mymass=2000
      print "mymass", mymass
      
    #mymass = (mywsname.split("_")[1]).split("p")[0]
    #exit(1)
    save_h = False

    print "Now start plotting"
    if combined:
        makePlots78(ws, rfr, is_prefit=is_prefit, suffix=suffix, plotdir=plotdir,
                    makeVpT = True, save_hists = save_h, restrict_to=["_T2_", "isMVA1"])
    elif summed:
        makePlotsSums(ws, rfr, is_prefit=is_prefit, suffix=suffix, plotdir=plotdir,
                      save_hists = save_h)
    elif sumCR:
        makePlotsSumsCR(ws, rfr, is_prefit=is_prefit, suffix=suffix,
                        plotdir=plotdir, save_hists = save_h)
    else:
        makePlots(ws, rfr, is_prefit=is_prefit, suffix=suffix, plotdir=plotdir,
                  makeVpT = False, save_hists = save_h, restrict_to=[], mass=mymass)
                  #makeVpT = True, save_hists = save_h, restrict_to=[])
    print "Plots made. Now exiting"
    g.Close()
    #Config.save_plot_objs()
    Config.reset()


# Spyros: Added mass as argument
def initialize(version, directory=None, rfrname=None, is_conditional=False, is_asimov=False, mu=None):
    RooMsgService.instance().setGlobalKillBelow(RF.ERROR)
    gROOT.SetBatch(True)
    gSystem.Load("libPlotUtils.so")

    print version
    #print fitstring
    ws,g = getWorkspace(version, rfrname)

    print "Preparing NP to the requested values"
    print version
    # Make postfit plots
    if directory:
        print "dir"
        rfr, suffix = getFitResult(directory, is_conditional, is_asimov, mu)
        transferResults(ws, rfr)
        #plotdir = "plots/{0}/postfit".format(version)
        plotdir = os.path.dirname(version)+'/postfit'
        is_prefit = False

    # Make prefit plots
    else:
        #plotdir = "plots/{0}/prefit".format(version)
        print "nodir"
        plotdir = os.path.dirname(version)+'/prefit'
        rfr = getInitialFitRes(ws)
        ws.loadSnapshot("vars_initial")
        is_prefit = True
        suffix = "Prefit"

    os.system("mkdir -vp "+plotdir)
    Config.yieldsfile = os.path.join(plotdir, "Yields_{0}.yik".format(suffix))
    Config.yields78file = os.path.join(plotdir, "Yields78_{0}.yik".format(suffix))
    Config.plot_objs_file = os.path.join(plotdir, "plotobjs_{0}.yik".format(suffix))

    return ws, rfr, is_prefit, suffix, plotdir, g

class Config(object):
    """ This actually serves as a global namespace to exchange things """
    yields = None
    yields78 = None
    comps = None
    plot_objs = None
    formats = [ 'eps', 'png', 'cxx' ]
    #formats = [ 'eps', 'png' ]
    force_mu_to_1 = False
    muhat = None

    @staticmethod
    def save_yields78():
        os.system("rm -f {0}".format(Config.yields78file))
        f = file(Config.yields78file, "w")
        pickle.dump(copy.deepcopy(Config.yields78), f)
        f.close()

    @staticmethod
    def read_yields78():
        if os.path.isfile(Config.yields78file):
            f = file(Config.yields78file, "r")
            yields78 = pickle.load(f)
            f.close()
        else:
            "yields78 file does not exist yet... Generate it."
            yields78 = {}
        Config.yields78 = yields78

    @staticmethod
    def save_yields():
        os.system("rm -f {0}".format(Config.yieldsfile))
        f = file(Config.yieldsfile, "w")
        pickle.dump(copy.deepcopy(Config.yields), f)
        f.close()

    @staticmethod
    def read_yields():
        if os.path.isfile(Config.yieldsfile):
            f = file(Config.yieldsfile, "r")
            yields = pickle.load(f)
            f.close()
        else:
            "Yields file does not exist yet... Generate it."
            yields = {}
        Config.yields = yields

    @staticmethod
    def save_plot_objs():
        if Config.plot_objs is not None:
            os.system("rm -f {0}".format(Config.plot_objs_file))
            f = file(Config.plot_objs_file, "w")
            pickle.dump(copy.deepcopy(Config.plot_objs), f)
            f.close()

    @staticmethod
    def read_plot_objs():
        if os.path.isfile(Config.plot_objs_file):
            f = file(Config.plot_objs_file, "r")
            print f
            plot_objs = pickle.load(f)
            f.close()
        else:
            "Plot objs file does not exist yet... Generate it."
            plot_objs = {}
        Config.plot_objs = plot_objs

    @staticmethod
    def reset():
        Config.yields = None
        Config.yields78 = None
        Config.comps = None

def getWorkspace(version, rfrname):
    #wsdir = "workspaces/{0}/combined".format(version)
    #mass += ".root"
    #wsf = os.path.join(wsdir,mass)
    wsf = version
    g = TFile.Open(wsf)
    print wsname
    print g
    print rfrname
    ws = g.Get(rfrname)
    print ws
    #hacked in to test. TODO FIXME
    #wsf="/afs/cern.ch/work/p/pmullen/DBLHaifegDebug/run2_current/ZHllbb_Zprime/ws/results_ZHllbb_Zprime_full_syst.root"
    #wsf="/afs/cern.ch/work/p/pmullen/DBLHaifegDebug/run2_current/ZHllbb_Zprime/ws/ZHllbb_Zprime_actualWorkspaces_flat_syst_Oct20.root"
    #ws = g.Get("ws_1000p000")
    #ws = g.ws_1000p000
    #print ws
    #exit(1)
    #ws = g
    return ws,g


def getInitialFitRes(ws):
    """ Create a prefit RooExpandedFitResult for this workspace """
    mc = ws.obj("ModelConfig")
    np = RooArgList(mc.GetNuisanceParameters())
    np.add(mc.GetParametersOfInterest())
    #removeGamma(np)
    it = np.createIterator()
    n = it.Next()
    while n:
        if "alpha" in n.GetName():
            n.setError(1)
        else:
            n.setError(1.e-4)
        #if "alpha_SysJetEResol" in n.GetName():
            #n.setVal(-1)
        n = it.Next()
    ws.saveSnapshot("vars_initial", RooArgSet(np))
    re = RooExpandedFitResult(np)
    if Config.force_mu_to_1:
        mc.GetParametersOfInterest().first().setVal(1)
    Config.muhat = mc.GetParametersOfInterest().first().getVal()
    if verbose:
        re.Print("v")
    return re


def transferResults(ws, rfr):
    """ Transfer results from the RooFitResult to the workspace, and make relevant snapshots """
    mc = ws.obj("ModelConfig")
    np = RooArgSet(mc.GetNuisanceParameters())
    np.add(mc.GetParametersOfInterest())
    if verbose:
        np.Print("v")
    # we want to be sure that the snapshot contains all we need, including POI
    ws.saveSnapshot("vars_initial", np)
    fpf = rfr.floatParsFinal().Clone()
    cp = rfr.constPars()
    fpf.add(cp) # add all const parameters of the RooFitResult to the floating ones
    if verbose:
        fpf.Print()
    np.assignValueOnly(fpf) # only sets the central value. Should be ok as VisualizeError uses errors from the RooFitResult, according to ROOT doc
    if Config.force_mu_to_1:
        #is_higgs_fit = True
        #it = np.createIterator()
        #n = it.Next()
        #while n:
        #    if "HiggsNorm" in n.GetName():
        #        n.setVal(0)
        #        is_higgs_fit = False
        #        break
        #    n = it.Next()
        Config.muhat = 1
        #if is_higgs_fit:
        mc.GetParametersOfInterest().first().setVal(1)
    else:
        Config.muhat = mc.GetParametersOfInterest().first().getVal()
    if verbose:
        np.Print("v")
    ws.saveSnapshot("vars_final", np)
    ws.loadSnapshot("vars_final")

def removeGamma(arglist):
    to_remove = RooArgList()
    it = arglist.createIterator()
    n = it.Next()
    while n:
        if n.GetName().startswith("gamma"):
            to_remove.add(n)
        n = it.Next()
    print "Will remove"
    to_remove.Print()
    arglist.remove(to_remove)


def getFitResult(directory, is_conditional=False, is_asimov=False, mu=None):
    """ Go and fetch RooFitResult in a FitCrossChecks.root file """
    #this one should contain the final alpha values
    #f = TFile.Open("{0}/FitCrossChecks.root".format(directory))
    #dirpath = os.path.dirname(directory)
    #filename = dirpath+"/"+fitpath
    #f = TFile.Open("/afs/cern.ch/work/p/pmullen/DBLHaifegDebug/run2_current/ZHllbb_Zprime/ws/diagnostics/sbfit_ZHllbb_Zprime_flat_syst_Oct20_1000p000.root")
    f = TFile.Open(directory)
    #TODO - Set up so we can handle both asimov and data fits
    #if is_asimov:
    #    f.cd("PlotsAfterFitToAsimov")
    #else:
    #    f.cd("PlotsAfterGlobalFit")
    #if is_conditional:
    #    gDirectory.cd("conditionnal_MuIsEqualTo_{0}".format(mu))
    #else:
    #    gDirectory.cd("unconditionnal")
    rfr = gDirectory.Get("fitresult_simPdf_obsData").Clone()
    # bonus: get a suffix corresponding to the setup
    #Not even sure what the point in this is
    #suffix = getPostfitSuffix(is_conditional, is_asimov, mu)
    suffix = "PostFit"
    f.Close()
    return rfr, suffix


def getComponents(w):
    """ Fetch all components (data, MC pdfs...) for a given workspace
    Organize all of this in a map
    """
    mc = w.obj("ModelConfig")
    data = w.data("obsData")
    simPdf = w.pdf("simPdf")
    channelCat = simPdf.indexCat()
    chanName = channelCat.GetName()

    comps = {}

    for tt in categories(channelCat):
        ttname = tt.GetName()
        pdftmp  = simPdf.getPdf( ttname )
        datatmp = data.reduce("{0}=={1}::{2}".format(chanName,chanName,ttname))
        obs  = pdftmp.getObservables( mc.GetObservables() ).first()
        obs_set = RooArgSet(obs)
        binWidth = pdftmp.getVariables().find("binWidth_obs_x_{0}_0".format(ttname))
        if verbose:
            print "    Bin Width : " , binWidth.getVal()
        comps[ttname] = [obs, binWidth, datatmp, {}, pdftmp]

        binning = obs.getBinning()
        stepsize = binning.averageBinWidth()
        low = binning.lowBound()
        high = binning.highBound()
        m = low
        while m<(high-1e-6):
            obs.setRange("bin"+str(m), m, m+stepsize)
            m += stepsize

        bkgList = RooArgList()
        sigList = RooArgList()
        totList = RooArgList()
        for c in components(pdftmp, ttname):
            compname = c.GetName()
            if is_signal(compname):
                sigList.add(c)
            else:
                bkgList.add(c)
            totList.add(c)
            comps[ttname][3][compname] = c
        sigSum = RooAddition( "Signal", "sig_Sum", sigList)
        comps[ttname][3]["Signal"] = sigSum
        bkgSum = RooAddition( "Bkg", "bkg_Sum", bkgList)
        comps[ttname][3]["Bkg"] = bkgSum
        totSum = RooAddition( "MC", "tot_Sum", totList)
        comps[ttname][3]["MC"] = totSum

    Config.comps = comps
    return comps


def getYields(w, rfr=None, onlyTot=False, window=None):
    """ Give map of yields (each category and total) for current snapshot of workspace
    If RooFitResult is given, the errors are computed
    If onlyTot = True, error is computed only on the sum of MC
    """
    if Config.comps is None:
        getComponents(w)

    comps = Config.comps

    if window:
        print "Will compute weights in window", window

    Config.read_yields()
    yields = Config.yields
    if len(Config.yields)>0 and rfr is None:
        return Config.yields

    if onlyTot:
        comp_rfr = None
    else:
        comp_rfr = rfr
    for ttname,comptt in comps.iteritems():
        print "    Computing yields for category : " , ttname
        obs_set = RooArgSet(comptt[0])
        yieldsChan = {}
        if window:
            rname = comptt[0].GetName()+"_range"
            comptt[0].setRange(rname, window[0], window[1])
            yieldsChan["data"] = comptt[2].sumEntries("1", rname)
        else:
            yieldsChan["data"] = comptt[2].sumEntries()

        for compname, comp in comptt[3].iteritems():
            if compname != "MC":
                yieldsChan[compname] = getValueAndError(comptt[0], comp, comptt[1], comp_rfr, ttname, window)
            else:
                yieldsChan[compname] = getValueAndError(comptt[0], comp, comptt[1], rfr, ttname, window)
            if compname == "Signal" and Config.muhat:
                yieldsChan["SignalExpected"] = [y/Config.muhat for y in yieldsChan[compname]]
        if yieldsChan["Bkg"][0] == 0:
          print "foun bkg yield of ZERO in", ttname
          print comptt
          yieldsChan["S/B"] = 0
        else:
          yieldsChan["S/B"] = yieldsChan["Signal"][0] / yieldsChan["Bkg"][0]
        if (yieldsChan["Bkg"][0]+yieldsChan["Signal"][0])**.5 == 0:
          print "foun sig yield of ZERO in",ttname
          print comptt
          yieldsChan["S/sqrt(S+B)"] = 0
        else:
          yieldsChan["S/sqrt(S+B)"] = yieldsChan["Signal"][0] / (yieldsChan["Bkg"][0]+yieldsChan["Signal"][0])**.5
        print "tot yield for",ttname,"is",yieldsChan
        yields[ttname] = yieldsChan
    Config.yields = yields
    Config.save_yields()

    return yields


def getValueAndError(obs, comp, binWidth, rfr, ttname, window=None):
    """ Try to be clever and not re-compute something that has already been computed """
    obs_set = RooArgSet(obs)
    compname = comp.GetName()
    bwidth = binWidth.getVal()
    compInt = None
    if ttname in Config.yields and compname in Config.yields[ttname]:
        print "getting from config.yields"
        Ntemp = Config.yields[ttname][compname][0]
    else:
        print "getting from obs_set"
        if window:
            obs.setRange("myrange", window[0], window[1])
            compInt = comp.createIntegral(obs_set, RF.Range("myrange"))
        else:
            compInt = comp.createIntegral(obs_set)
        print comp
        print obs_set
        print obs
        print "binwidth",bwidth
        print "Val",compInt.getVal()
        Ntemp=compInt.getVal() * bwidth
    error = -1
    if rfr:
        if ttname in Config.yields and compname in Config.yields[ttname] \
           and Config.yields[ttname][compname][1] != -1:
            error = Config.yields[ttname][compname][1]
        else:
            if not compInt:
                if window:
                    obs.setRange("myrange", window[0], window[1])
                    compInt = comp.createIntegral(obs_set, RF.Range("myrange"))
                else:
                    compInt = comp.createIntegral(obs_set)
            error = RU.getPropagatedError(compInt, rfr) * bwidth
    print "Found", Ntemp, "+/-", error, "for", compname, " in region", ttname
    return [Ntemp, error]


def getYields78(w, rfr=None, onlyTot=False, window=None):
    """ Give map of yields (each category and total) for current snapshot of workspace
    If RooFitResult is given, the errors are computed
    If onlyTot = True, error is computed only on the sum of MC
    """
    if Config.comps is None:
        getComponents(w)
    comps = Config.comps

    if window:
        print "Will compute weights in window", window

    Config.read_yields78()
    yields78 = Config.yields78
    if len(Config.yields78)>0 and rfr is None:
        return Config.yields78

    if onlyTot:
        comp_rfr = None
    else:
        comp_rfr = rfr
    browsed = set()
    for ttname,comptt in comps.iteritems():
        if ttname in browsed:
            continue
        alt_tt, catname = getAltCatName(ttname)

        print "    Computing yields for category : " , catname
        browsed.add(ttname)

        yieldsChan = {}

        if alt_tt in comps:
            browsed.add(alt_tt)
            altcomp = comps[alt_tt]
            obs1 = comptt[0]
            bw1 = comptt[1]
            obs2 = altcomp[0]
            bw2 = altcomp[1]
            if window:
                rname1 = obs1.GetName()+"_range"
                rname2 = obs2.GetName()+"_range"
                obs1.setRange(rname1, window[0], window[1])
                obs2.setRange(rname2, window[0], window[1])
                yieldsChan["data"] = comptt[2].sumEntries("1", rname1) + altcomp[2].sumEntries("1", rname2)
            else:
                yieldsChan["data"] = comptt[2].sumEntries() + altcomp[2].sumEntries()

            comp_items = { getCompName(k):v for k,v in comptt[3].iteritems() }
            alt_items = { getCompName(k):v for k,v in altcomp[3].iteritems() }

            # stupid things to deal with naming conventions
            compnames = {}
            # FIXME update with new naming conventions
            for k in (comptt[3].keys() + altcomp[3].keys()):
                short = getCompName(k)
                if short != k:
                    blocks = k.split('_')
                    blocks[6] = 'both'
                    full = '_'.join(blocks)
                else:
                    full = k
                compnames[short] = full

            #print compnames
            for compname,fullname in compnames.iteritems():
                complist = []
                if compname in comp_items:
                    complist.append((obs1, bw1.getVal(), comp_items[compname]))
                if compname in alt_items:
                    complist.append((obs2, bw2.getVal(), alt_items[compname]))
                if compname != "MC":
                    yieldsChan[fullname] = getSumAndError(complist, comp_rfr, window)
                else:
                    yieldsChan[fullname] = getSumAndError(complist, rfr, window)
                if compname == "Signal" and Config.muhat:
                    yieldsChan["SignalExpected"] = [y/Config.muhat for y in yieldsChan[compname]]
        else:
            obs_set = RooArgSet(comptt[0])
            if window:
                rname = comptt[0].GetName()+"_range"
                comptt[0].setRange(rname, window[0], window[1])
                yieldsChan["data"] = comptt[2].sumEntries("1", rname)
            else:
                yieldsChan["data"] = comptt[2].sumEntries()
            for compname, comp in comptt[3].iteritems():
                short = getCompName(compname)
                # FIXME update with new naming conventions
                if short != compname:
                    blocks = compname.split('_')
                    blocks[6] = 'both'
                    full = '_'.join(blocks)
                else:
                    full = compname
                complist = []
                complist.append((comptt[0], comptt[1].getVal(), comp))
                if compname != "MC":
                    yieldsChan[full] = getSumAndError(complist, comp_rfr, window)
                else:
                    yieldsChan[full] = getSumAndError(complist, rfr, window)
            if compname == "Signal" and Config.muhat:
                yieldsChan["SignalExpected"] = [y/Config.muhat for y in yieldsChan[compname]]
        yieldsChan["S/B"] = yieldsChan["Signal"][0] / yieldsChan["Bkg"][0]
        yieldsChan["S/sqrt(S+B)"] = yieldsChan["Signal"][0] / (yieldsChan["Bkg"][0]+yieldsChan["Signal"][0])**.5
        yields78[catname] = yieldsChan
    Config.yields78 = yields78
    Config.save_yields78()

    return yields78


def getSumAndError(list_comps, rfr, window=None):
    """ list_comps: list of tuples (obs, bwidth, comp)
    """
    complist = RooArgList()
    widthlist = RooArgList()
    if verbose:
        print list_comps
    for l in list_comps:
        if window:
            l[0].setRange("myrange", window[0], window[1])
            inte = l[2].createIntegral(RooArgSet(l[0]), RF.Range("myrange"))
        else:
            inte = l[2].createIntegral(RooArgSet(l[0]))
        complist.add(inte)
        widthlist.add(RF.RooConst(l[1]))
    roosum = RooAddition("sum", "sum", complist, widthlist)
    val = roosum.getVal()
    if rfr is not None:
        error = RU.getPropagatedError(roosum, rfr)
    else:
        error = -1
    return [val, error]


def makePlots(w, rfr, is_prefit, suffix, plotdir, makeVpT = False, save_hists = False, restrict_to=[], mass = "125"):
    """ Plot distributions for each subchannel """

    os.system("mkdir -vp "+plotdir)
    objs_dict = getAllPlotObjects(w, rfr, is_prefit, suffix, plotdir, restrict_to)

    print "Plotting Distributions for each subchannel"
    print objs_dict

    for ttname, objs in objs_dict.iteritems():
        if ttname.endswith("error"):
            continue
        plot(objs, ttname, suffix, plotdir, save_hists, plot_bkgsub = False, masspt=mass)
#        plot(objs, ttname, suffix, plotdir, save_hists, plot_bkgsub = True)

    Config.save_yields()
    if makeVpT:
        makepTbinsPlots(w, rfr, is_prefit, suffix, plotdir, Config.yields, save_hists)
    print "End plotting distributions !"


def makepTbinsPlots(w, rfr, is_prefit, suffix, plotdir, yields = None, save_hists = False, is_combined = False):
    """ Plot VpT distributions in each tag region """

    print "Plotting VpT Distributions"
    if is_prefit:
        w.loadSnapshot("vars_initial")
    else:
        w.loadSnapshot("vars_final")

    if not yields:
        print "Yields not provided. Compute them"
        if is_combined:
            getYields78(w, rfr, True)
            yields = Config.yields78
        else:
            getYields(w, rfr, True)
            yields = Config.yields

    os.system("mkdir -vp "+plotdir)

    ptbins_cut = array('d', [0, 90, 120, 160, 200, 250])
    hmodel_cut = TH1F("hmodel_cut", "hmodel_cut", 5, ptbins_cut)
    errmodel_x_cut = array('d', [0, 90, 90, 120, 120, 160, 160, 200, 200, 250,
                             250, 200, 200, 160, 160, 120, 120, 90, 90, 0])
    errmodel_y_cut = array('d', [0 for i in range(20)])
    errmodel_cut = TGraph(20, errmodel_x_cut, errmodel_y_cut)

    ptbins_mva = array('d', [0, 120, 250])
    hmodel_mva = TH1F("hmodel_mva", "hmodel_mva", 2, ptbins_mva)
    errmodel_x_mva = array('d', [0, 120, 120, 250, 250, 120, 120, 0])
    errmodel_y_mva = array('d', [0 for i in range(8)])
    errmodel_mva = TGraph(8, errmodel_x_mva, errmodel_y_mva)

    ptbins_cut_0lep = array('d', [0, 100, 120, 160, 200, 250])
    hmodel_cut_0lep = TH1F("hmodel_cut_0lep", "hmodel_cut_0lep", 5, ptbins_cut_0lep)
    errmodel_x_cut_0lep = array('d', [0, 100, 100, 120, 120, 160, 160, 200, 200, 250,
                             250, 200, 200, 160, 160, 120, 120, 100, 100, 0])
    errmodel_y_cut_0lep = array('d', [0 for i in range(20)])
    errmodel_cut_0lep = TGraph(20, errmodel_x_cut_0lep, errmodel_y_cut_0lep)

    ptbins_mva_0lep = array('d', [0, 100, 120, 250])
    hmodel_mva_0lep = TH1F("hmodel_mva_0lep", "hmodel_mva_0lep", 3, ptbins_mva_0lep)
    errmodel_x_mva_0lep = array('d', [0, 100, 100, 120, 120, 250, 250, 120, 120, 100, 100, 0])
    errmodel_y_mva_0lep = array('d', [0 for i in range(12)])
    errmodel_mva_0lep = TGraph(12, errmodel_x_mva_0lep, errmodel_y_mva_0lep)

    histos = {}
    mass = None

    for k,y in yields.iteritems():
        parts = k.split('_')
        # k: Region_Y2012_isMVA1_B2_J2_T2_L2_distmva_TType
        # find the bin:
        pos1 = k.find("_B")
        pos2 = k.find("_", pos1+1)
        if pos2 - pos1 != 3:
            print "ERROR: name of the region", k, "does not seem to be a standard one"
            print "_B should match the bin name"
            return
        # TODO: horrible. Need to take care of 0 lepton low MET bin properly, especially in MVA case where lowMET is still CUT
        regname = k[:pos1+2]+'9'+k[pos2:]
        bin = int(k[pos1+2])
        pos1 = regname.find("_dist")
        pos2 = regname.find("_", pos1+1)
        regname = regname[:pos1+5]+"VpT"+regname[pos2:]
        pos1 = regname.find("_isMVA")
        isMVA = int(regname[pos1+6])
        pos1 = k.find("_L")
        pos2 = k.find("_", pos1+1)
        nlep = int(k[pos1+2])
        print "Accumulating for region", regname
        ibin = bin + 1
        if isMVA and nlep != 0 and ibin == 3: # MVA has no B1
            ibin = 2
        if isMVA:
            if nlep == 0:
                hmodel = hmodel_mva_0lep
                errmodel = errmodel_mva_0lep
            else:
                hmodel = hmodel_mva
                errmodel = errmodel_mva
        else:
            if nlep == 0:
                hmodel = hmodel_cut_0lep
                errmodel = errmodel_cut_0lep
            else:
                hmodel = hmodel_cut
                errmodel = errmodel_cut
        if not regname in histos:
            histos[regname] = {}
            for s, v in y.iteritems():
                if s.startswith("L_x"):
                    sname = getCompName(s)
                    histos[regname][sname] = hmodel.Clone(sname)
                    if mass == None:
                        res = is_signal(sname)
                        if res:
                            mass = res
            histos[regname]["data"] = hmodel.Clone("data")
            histos[regname]["error"] = errmodel.Clone("error")
            histos[regname]["prefit"] = [hmodel.Clone("prefit"), ""]
        # now, fill the histos
        for s, v in y.iteritems():
            if s.startswith("L_x"):
                sname = getCompName(s)
                try:
                    histos[regname][getCompName(s)].SetBinContent(ibin, v[0])
                except KeyError: # case when a given component is 0 in the first found VpT bin, but !=0 elsewhere
                    sname = getCompName(s)
                    histos[regname][sname] = hmodel.Clone(sname)
                    histos[regname][sname].SetBinContent(ibin, v[0])
        histos[regname]["data"].SetBinContent(ibin, y["data"])
        (mcval, mcerr) = y["MC"]
        if mcerr == -1: # recompute case by case if not already computed somewhere
            mcerr = getMCerror(w, rfr, k)
            y["MC"] = [mcval, mcerr]
        X = Double()
        Y = Double()
        for i in [2*ibin-2, 2*ibin-1]:
            histos[regname]["error"].GetPoint(i, X, Y)
            histos[regname]["error"].SetPoint(i, X, mcval - mcerr)
        npoints = histos[regname]["error"].GetN()
        for i in [npoints+1-2*ibin, npoints-2*ibin]:
            histos[regname]["error"].GetPoint(i, X, Y)
            histos[regname]["error"].SetPoint(i, X, mcval + mcerr)
        # if doing postfit, add the prefit line
        if not is_prefit:
            histo, mulegend = getPrefitCurve(w, regname = k)
            histos[regname]["prefit"][0].SetBinContent(ibin, histo.Integral())
            print "PREFIT ", mulegend
            histos[regname]["prefit"][1] = mulegend

    for reg,h in histos.iteritems():
        h["data"] = TGraphAsymmErrors(h["data"]) # TODO use Poisson errors ?
        sm = mkplots.SetupMaker(reg, mass, muhat = Config.muhat)
        for k,v in h.iteritems():
            sm.add(k, v)

        cname = reg +"_"+suffix
        can = sm.setup.make_complete_plot(cname, True, ybounds=(0.85, 1.15))
        plotname = "{0}/{1}".format(plotdir, can.GetName())
        can2 = sm.setup.make_complete_plot(cname+"_logy", True, True, ybounds=(0.85, 1.15))
        plotname2 = "{0}/{1}".format(plotdir, can2.GetName())
        for f in Config.formats:
            can.Print(plotname+'.'+f)
            can2.Print(plotname2+'.'+f)

        # save histograms if requested
        if save_hists:
            afile = TFile.Open(plotname+".root", "recreate")
            can.Write(can.GetName())
            can2.Write(can2.GetName())
            for k,v in h.iteritems():
                if isinstance(v, TObject):
                    v.Write(v.GetName())
                if k == "prefit":
                    v[0].Write(v[0].GetName())

        can.Close()
        can2.Close()
        # free memory for some objects used in the plotting...
        mkplots.purge()
        # TODO check for things not deleted
    if is_combined:
        Config.save_yields78()
    else:
        Config.save_yields()


def makePlots78(w, rfr, is_prefit, suffix, plotdir, makeVpT = False, save_hists = False, restrict_to=[]):
    """ Plot distributions for each subchannel, summing 7 and 8 TeV """

    os.system("mkdir -vp "+plotdir)
    objs_dict = getAllPlotObjects(w, None, is_prefit, suffix, plotdir, restrict_to)
    if Config.comps is None:
        getComponents(w)
    comps = Config.comps

    print "Plotting combined Distributions for each subchannel"

    browsed = set()

    for k,v in objs_dict.iteritems():
        if k in browsed:
            continue
        if k.endswith("error"):
            continue
        alt_tt, catname = getAltCatName(k)
        browsed.add(k)
        complist = [comps[k]]

        if alt_tt in objs_dict:
            browsed.add(alt_tt)
            res = sumPlotObjects([v, objs_dict[alt_tt]])
            complist.append(comps[alt_tt])
        else:
            res = v

        error_name = catname+"_error"
        if error_name in Config.plot_objs:
            res["error"] = Config.plot_objs[error_name]
        else:
            res["error"] = getSumErrorBand(complist, rfr)
            Config.plot_objs[error_name] = res["error"]
        # Now do the plots
        plot(res, catname, suffix, plotdir, save_hists, plot_bkgsub = False)

    Config.save_yields()
    if makeVpT:
        makepTbinsPlots(w, rfr, is_prefit, suffix, plotdir, None, save_hists, True)
    print "End plotting distributions !"


def plotSumOfCats(rfr, suffix, plotdir, save_hists, list_objs, list_comps, name, weights=None):
    if len(list_objs)>0:
        res = sumPlotObjects(list_objs, weights)
        error_name = name+"_error"
        if error_name in Config.plot_objs:
            res["error"] = Config.plot_objs[error_name]
        else:
            res["error"] = getSumErrorBand(list_comps, rfr, weights)
            Config.plot_objs[error_name] = res["error"]
        plot(res, name, suffix, plotdir, save_hists)

def makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, name, restrict_to=[], exclude=[]):
    """ restrict_to is to be understand with AND """
    list_tot = []
    comps_tot = []
    weights_tot = []
    weights_dib_tot = []

    for k,v in objs_dict.iteritems():
        if "error" in k:
            continue
        if len(restrict_to)>0:
            if False in (r in k for r in restrict_to):
                continue
        if True in (r in k for r in exclude):
            continue
        s_o_b = abs(yields[k]["Signal"][0]) / abs(yields[k]["Bkg"][0])
        diboson = 0
        for r in yields[k]:
            if "WZ" in r or "ZZ" in r or "VZ" in r or "Diboson" in r or "diboson" in r:
                diboson += yields[k][r][0]
        if diboson:
            d_o_b = abs(diboson) / abs(yields[k]["Bkg"][0])
        else:
            d_o_b = 0
        list_tot.append(v)
        comps_tot.append(comps[k])
        weights_tot.append(s_o_b)
        weights_dib_tot.append(d_o_b)

    #for wlist in [ weights_tot, weights_dib_tot]:
        #sum_list = math.fsum(wlist)
        #for w in wlist:
            #w /= sum_list


    #print weights_tot
    #print weights_dib_tot

    plotSumOfCats(rfr, suffix, plotdir, save_hists, list_tot, comps_tot, name)
    plotSumOfCats(rfr, suffix, plotdir, save_hists, list_tot, comps_tot, name+"_Higgsweighted", weights_tot)
    plotSumOfCats(rfr, suffix, plotdir, save_hists, list_tot, comps_tot, name+"_Dibosonweighted", weights_dib_tot)

def makePlotsSums(w, rfr, is_prefit, suffix, plotdir, save_hists = False):
    """ Plot distributions for each subchannel, summing 7 and 8 TeV """

    os.system("mkdir -vp "+plotdir)
    objs_dict = getAllPlotObjects(w, None, is_prefit, suffix, plotdir, restrict_to=["_T2_"], excludes=["_Spctopemucr"])
    yields = Config.yields
    if Config.comps is None:
        getComponents(w)
    comps = Config.comps

    print "Plotting summed Distributions"
    makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L3_T2_B9_Y2012_distmjj",
                        restrict_to=["_T2"], exclude=["_Spctopemucr"])
    makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L3_T2_B0_Y2012_distmjj",
                        restrict_to=["_T2"], exclude=["_B2", "_B3", "_B4", "_Spctopemucr"])
    makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L3_T2_B2_Y2012_distmjj",
                        restrict_to=["_T2"], exclude=["_B0", "_B1", "_Spctopemucr"])
    makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L3_T2_B9_J2_Y2012_distmjj",
                        restrict_to=["_T2", "_J2"], exclude=["_Spctopemucr"])
    makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L3_T2_B9_J3_Y2012_distmjj",
                        restrict_to=["_T2", "_J3"], exclude=["_Spctopemucr"])
    makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L3_T2_B9_Y2012_TTypexx_distmjj",
                        restrict_to=["_T2"], exclude=["_TTypell", "_Spctopemucr"])
    for i in ["0", "1", "2"]:
        makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L"+i+"_T2_B9_Y2012_distmjj",
                            restrict_to=["_L"+i, "_T2"], exclude=["_Spctopemucr"])
        makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L"+i+"_T2_B0_Y2012_distmjj",
                            restrict_to=["_L"+i, "_T2"], exclude=["_B2", "_B3", "_B4", "_Spctopemucr"])
        makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L"+i+"_T2_B2_Y2012_distmjj",
                            restrict_to=["_L"+i, "_T2"], exclude=["_B0", "_B1", "_Spctopemucr"])
        makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L"+i+"_T2_B9_J2_Y2012_distmjj",
                            restrict_to=["_L"+i, "_T2", "_J2"], exclude=["_Spctopemucr"])
        makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L"+i+"_T2_B9_J3_Y2012_distmjj",
                            restrict_to=["_L"+i, "_T2", "_J3"], exclude=["_Spctopemucr"])
        makeWeightedSumPlot(rfr, suffix, plotdir, save_hists, objs_dict, yields, comps, "Region_L"+i+"_T2_B9_Y2012_TTypexx_distmjj",
                            restrict_to=["_L"+i, "_T2"], exclude=["_TTypell", "_Spctopemucr"])

    Config.save_yields()
    print "End plotting distributions !"


def makePlotsSumsCR(w, rfr, is_prefit, suffix, plotdir, save_hists = False):
    """ Plot distributions for each subchannel, summing 7 and 8 TeV """

    os.system("mkdir -vp "+plotdir)
    objs_dict = getAllPlotObjects(w, None, is_prefit, suffix, plotdir, restrict_to=["_T0_", "_T1_", "Spctop"])

    if Config.comps is None:
        getComponents(w)
    comps = Config.comps

    print "Plotting summed Distributions of backgrounds"

    # FIXME update with new conventions. Low priority since we are not updating 2011
    for flav in ["Zero", "One", "Two"]:
        for reg in ["0T2J", "0T3J", "1T2J", "1T3J", "topcr", "topemucr"]:
            list_cr =[]
            list_comps = []
            for k,v in objs_dict.iteritems():
                if k.endswith("error"):
                    continue
                if reg in k and flav in k:
                    list_cr.append(v)
                    list_comps.append(comps[k])

            if len(list_cr)>0:
                name = "{0}Lepton_{1}_B9_both_mjj".format(flav, reg)
                res = sumPlotObjects(list_cr)
                error_name = name+"_error"
                if error_name in Config.plot_objs:
                    res["error"] = Config.plot_objs[error_name]
                else:
                    res["error"] = getSumErrorBand(list_comps, rfr)
                    Config.plot_objs[error_name] = res["error"]
                plot(res, name, suffix, plotdir, save_hists, plot_bkgsub = False,
                    ybounds = (0.76, 1.24))

    Config.save_yields()
    print "End plotting distributions !"

def getPlotObjects(obs, pdf, data, components, ttyields, newBinSizes, doVarBin, fitres=None, objs={}):
    divisor=25
    #divisor=100
    bins = newBinSizes
    doHackbin = False
    #print "type info: obs",type(obs)
    frm = obs.frame()
    print "************************"

    for comp in components:
        print comp
        compname = comp.GetName()
        print compname
        if compname in objs:
            continue
        h = comp.createHistogram(compname, obs)
        if h.Integral()!=0:
            h.Scale(1/h.Integral())
            if compname in ttyields:
                h.Scale(ttyields[compname][0])
        if doVarBin: 
          print "Changing the binning of the histograms"
          if len(newBinSizes)-1 == h.GetNbinsX():
            #print len(newBinSizes)
            #print h.GetNbinsX()
            #print "passed",h.GetName()
            #create new hist with new bin sizes
            print bins
            newh=TH1F(h.GetName(),h.GetTitle(), h.GetNbinsX(), bins)
            #more pythony loop?
            i=0
            while i < h.GetNbinsX()+2:
              binwidth= newh.GetXaxis().GetBinWidth(i)             
              newh.SetBinContent(i, h.GetBinContent(i)/(binwidth/divisor))
              newh.SetBinError(i, h.GetBinError(i)/(binwidth/divisor))
              i=i+1
            objs[compname] = newh
          else:
            print len(newBinSizes)
            print newBinSizes
            print h.GetNbinsX()
            print "Are you trying to plot variable width histograms? Are you sure you got the number of bins correct?"
            print h.GetName()
            continue
            #TODO skipping these as a test. In future we should exit.
            #exit(1)
        else:
          #print "bin contents:",h.GetNbinsX(), compname, type(h)
          objs[compname] = h
        if not "mass" in objs:
            res = is_signal(compname)
            if res:
                objs["mass"] = res

    if not "data" in objs:
        #Do we want to change the hist binning?
        #RooFit cant deal with variable bin width histograms
        if doVarBin:
          #Hacking the data TGraph so that we can have variable bin widths
          data.plotOn(frm,RF.DataError(RooAbsData.Poisson))
          tgdata = frm.getHist()
          newdata=reBinTGraph(tgdata, newBinSizes, divisor)
          objs["data"] = newdata
        elif doHackbin:
          data.plotOn(frm,RF.DataError(RooAbsData.Poisson))
          tgdata = frm.getHist()
          #set newBinSizes to chris stuff
          newBinSizes = [0] + range(50, 501, 25)
          newdata=reBinTGraph(tgdata, newBinSizes, divisor)
          objs["data"] = newdata
        else:
          data.plotOn(frm,RF.DataError(RooAbsData.Poisson))
          tgdata = frm.getHist()
          i=0
          while i<tgdata.GetN():
            print "y value",tgdata.GetY()[i]
            i=i+1
          
          objs["data"] = tgdata

        #is this broken if we want to change our binning of the data?
        pdf.plotOn(frm, RF.Normalization(1, RooAbsReal.RelativeExpected))
        chi2 = frm.chiSquare()
        objs["chi2"] = chi2

    if fitres is not None and "error" not in objs:
        print "DOING ERRORTHING"
        data.plotOn(frm,RF.DataError(RooAbsData.Poisson))
        #print "type info: pdf",type(pdf)
        pdf.plotOn(frm, RF.VisualizeError(fitres,1), RF.Name("FitError_AfterFit"),
                   RF.Normalization(1, RooAbsReal.RelativeExpected))
        c = frm.getCurve()
        if doVarBin:
          i=0
          while i<c.GetN():
            #print "i, xval, yval",i,c.GetX()[i],c.GetY()[i]
            i=i+1
          newCurve=reBinCurve(c, newBinSizes, divisor)
          objs["error"] = newCurve 
        else:
          print "type info: errcurve",type(c)
          objs["error"] = c

    return objs

def reBinCurve(myGraph, myBins,divisor=1):
  newGraph=mkplots.clone(myGraph)
  print "err type", type(myGraph)
  print "mybins", myBins
  #A curve of a PDF stores a lot of points
  #two per bin edge including under and overflow
  #An extra point that is the upper edge(for us 1.0)
  #So for 10 bins between 0 and 1 we have 27 values
  #It also stores an upper error and lower error value for each of these
  #so we end up at 54
  #Its basically a closed shape that gets drawn over the plots to represent the errors
  if (((len(myBins)-1)*2)+7)*2 == newGraph.GetN():
    for i in range(newGraph.GetN()):
      print "bin no. ",i,"xval, yval",newGraph.GetX()[i],newGraph.GetY()[i]
      #could do something clever here using modulo division and negative list indices
      #but that seems error prone
      if i<2:
        print "underflow"
        binwidth=myBins[1]-myBins[0]
        newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)
        #set point to be in underflow
      elif i<(len(myBins)*2)+2:
        if i==2:
          binwidth=myBins[1]-myBins[0]
          newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)
        elif i==(len(myBins)*2)+1:
          binwidth=myBins[-1]-myBins[-2]
          newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)
        else:
          if i %2 ==0:
            topbin=(i-2)/2
            binwidth=myBins[topbin]-myBins[topbin-1]
            newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)
          else:
            topbin=(i-1)/2
            binwidth=myBins[topbin]-myBins[topbin-1]
            newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)
            
          
          
        #print "binning",int((i-2)/2)
        #try:
        #  binwidth=myBins[int((i-2)/2)+1]-myBins[int((i-2)/2)]
        #  newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/100)
        #  print "setting bin",i,"with width",binwidth,"to val",newGraph.GetY()[i]
        #except:
        #  binwidth=myBins[int((i-2)/2)]-myBins[int((i-2)/2)-1]
        #  newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/100)
        #  print "setting bin",i,"with width",binwidth,"to val",newGraph.GetY()[i]
       
        #upper error values for each bin
        if i %2 ==0:
          #set value to be slightly lower than bin edge
          newGraph.GetX()[i]=myBins[int((i-2)/2)]-1e-6
        else:
          #set value to be slightly higher than bin edge
          newGraph.GetX()[i]=myBins[int((i-2)/2)]+1e-6
      elif i<(len(myBins)*2)+3:
        newGraph.GetX()[i]=myBins[-1]
        print "placeholder"
        #upper edge
      elif i<(len(myBins)*2)+7:
        print "placeholder"
        #overflow bin points
      elif i<(len(myBins)*2)+8:
        newGraph.GetX()[i]=myBins[-1]
        print "placeholder"
        #upper edge again
      elif i<(len(myBins)*4)+8:
        if i==(len(myBins)*2)+8:
          binwidth=myBins[-1]-myBins[-2]
          newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)
        elif i==(len(myBins)*4)+7:
          binwidth=myBins[1]-myBins[0]
          newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)
        else:
          if i %2 ==0:
            displacement=newGraph.GetN()-1-i
            topbin=(displacement-1)/2
            binwidth=myBins[topbin]-myBins[topbin-1]
            newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)
          else:
            displacement=newGraph.GetN()-1-i
            topbin=(displacement-2)/2
            binwidth=myBins[topbin]-myBins[topbin-1]
            newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/divisor)

        #lower error values for each bin
        shiftFactor=(((len(myBins)*2)+8)/2)-1
        index=shiftFactor-(i/2)
        #try:
        #  binwidth=myBins[index]-myBins[index-1]
        #  newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/100)
        #except:
        #  binwidth=myBins[index+1]-myBins[index]
        #  newGraph.GetY()[i]=newGraph.GetY()[i]/(binwidth/100)
        
        if i %2 ==0:
          #index=shiftFactor-(i/2)
          newGraph.GetX()[i]=myBins[index]+1e-6
        else:
          #index=shiftFactor-(i/2)
          newGraph.GetX()[i]=myBins[index]-1e-6
      else: 
        newGraph.GetX()[i]=myBins[0]-10
        print "underflow"
        #set to be underflow
      print "new bin no. ",i,"xval, yval",newGraph.GetX()[i],newGraph.GetY()[i]
    return newGraph
  else:
    print "INDICES WORNG"
    return myGraph
      
   


def reBinTGraph(myGraph, myBins, divisor=1):
   newGraph=mkplots.clone(myGraph) 
   if len(myBins)-1 == newGraph.GetN():
     print "Changing the binning"
     i=0
     while i<newGraph.GetN():
       print "y value",myGraph.GetY()[i]
       binwidth=myBins[i+1]-myBins[i]
       newGraph.GetX()[i]=(myBins[i]+myBins[i+1])/2.
       newGraph.GetY()[i]=(newGraph.GetY()[i]*(1/0.967))/(binwidth/float(divisor))
       xerr=(myBins[i+1]-myBins[i])/2.
       #print newGraph.GetX()[i]
       print "bin i",myBins[i]
       print "bin i+1",myBins[i+1]
       print "diff",myBins[i+1]-myBins[i]
       print myGraph.GetErrorYlow(i)
       #yerr=myGraph.GetErrorY(i)
       newGraph.SetPointError(i, xerr, xerr, myGraph.GetErrorYlow(i)/(binwidth/float(divisor)),myGraph.GetErrorYhigh(i)/(binwidth/float(divisor))) 
       print "y value",newGraph.GetY()[i]
       print newGraph.GetErrorYlow(i)
       i=i+1
     return newGraph
   else:
     print newGraph.GetN()
     print len(myBins)-1
     print "The number of bins in the data hist does not match the number of bins in your bin width info" 
     return myGraph
     #exit(0)

def sumPlotObjects(objs_list, weights=None):
    res = {}
    aux = {}
    if weights is None:
        weights = [1]*len(objs_list)
    if len(weights) != len(objs_list):
        print "Numbers of regions and weights given do not match !"
        return

    for objs,w in zip(objs_list, weights):
        for k,v in objs.iteritems():
            if k == "error" or k == "chi2": # meaningless to sum them
                continue
            short = getCompName(k)
            if not short in aux:
                aux[short] = k
                # pay attention not to modify the objects we are working with. They might be reused elsewhere.
                if isinstance(v, TObject):
                    res[k] = v.Clone()
                    if isinstance(v, TGraphAsymmErrors): # data
                        scaleTGraphAsymmErrors(res[k], w)
                    else:
                        res[k].Scale(w)
                elif k == "prefit":
                    res[k] = [v[0].Clone(), v[1]]
                    res[k][0].Scale(w)
                else:
                    res[k] = v
            else:
                idx = aux[short]
                if short != k: # that's one standard component
                    res[idx].Add(v, w)
                elif k == "prefit":
                    res[idx][0].Add(v[0], w)
                elif k == "data":
                    if w == 1:
                        res[idx] = RooHist(res[idx], v)
                    else:
                        res[idx] = RooHist(res[idx], v, 1, w, RooAbsData.SumW2)
    return res

def getSumErrorBand(comps_list, rfr, weights=None):
    if rfr is None:
        return None
    if weights is None:
        weights = [1]*len(comps_list)
    if len(weights) != len(comps_list):
        print "Numbers of regions and weights given do not match !"
        return

    #v_pdfs = vector('RooAbsPdf*')()
    #v_obs = vector('RooRealVar*')()
    #v_weights = vector(Double)()
    #v_data = vector('RooAbsData*')()

    #print "Summing:"
    #for c,w in zip(comps_list, weights):
        #print c[4]
        #v_obs.push_back(c[0])
        #v_weights.push_back(w)
        #v_pdfs.push_back(c[4])
        #v_data.push_back(c[2])

    #curve = RU.plotOnWithErrorBand(v_pdfs, v_obs, v_weights, rfr, v_data)

    v_pdfs = []
    v_obs = []
    v_bw = []
    #print "Summing:"
    for c in comps_list:
        v_pdfs.append(c[3]["MC"])
        v_obs.append(c[0])
        v_bw.append(c[1])
    curve = getBinByBinErrorBand(v_pdfs, v_obs, v_bw, rfr, weights)

    return curve


def getBinByBinErrorBand(mc_comps, observables, binWidths, rfr, weights=None):
    if rfr is None:
        return None
    bins = []
    binning = observables[0].getBinning()
    stepsize = binning.averageBinWidth()
    low = binning.lowBound()
    high = binning.highBound()
    real_weights = RooArgList()
    if weights is not None:
        for bw, w in zip(binWidths, weights):
            real_weights.add(RF.RooConst(bw.getVal()*w))
    else:
        for bw in binWidths:
            real_weights.add(RF.RooConst(bw.getVal()))
    m = low
    while m<(high-1e-6):
        rname = "bin"+str(m)
        intes = RooArgList()
        for obs, mc in zip(observables, mc_comps):
            intes.add(mc.createIntegral(RooArgSet(obs), RF.Range(rname)))
        totbin = RooAddition("sumbin", "sumbin", intes, real_weights)
        val = totbin.getVal()
        err = RU.getPropagatedError(totbin, rfr)
        bins.append((val, err))
        print "Found error of", err
        m += stepsize
    yvals = []
    xvals = []
    for i,b in enumerate(bins):
        yvals.extend([b[0]+b[1], b[0]+b[1]])
        xvals.extend([binning.binLow(i), binning.binLow(i)])
    xvals.append(high)
    xvals = xvals[1:]
    xvals.extend(reversed(xvals))
    for b in reversed(bins):
        yvals.extend([b[0]-b[1], b[0]-b[1]])
    yvals_a = array('d', yvals)
    xvals_a = array('d', xvals)
    curve = TGraph(len(xvals), xvals_a, yvals_a)
    return curve


def getPrefitCurve(w, obs=None, pdf=None, regname=None):
    w.loadSnapshot("vars_initial")
    mc = w.obj("ModelConfig")
    mc.GetParametersOfInterest().first().setVal( 0 );
    mubeforefit = mc.GetParametersOfInterest().first().getVal()
    muValueBeforeFitLegend = "Pre-fit background"
    if regname is not None and "both" in regname:
        simPdf = w.pdf("simPdf")
        idx = regname.find("both")
        curves = []
        for y in ["2011", "2012"]:
            regnameyear = regname[:idx]+y+regname[idx+4:]
            pdf  = simPdf.getPdf( regnameyear )
            if pdf:
                obs  = pdf.getObservables( mc.GetObservables() ).first()
                preFitIntegral = pdf.expectedEvents(RooArgSet(obs))
                h = pdf.createHistogram(pdf.GetName(), obs)
                h.Scale(preFitIntegral/h.Integral())
                curves.append(h)
        h = curves[0]
        if len(curves)>1:
            h.Add(curves[1])
    elif regname is not None:
        simPdf = w.pdf("simPdf")
        pdf  = simPdf.getPdf( regname )
        obs  = pdf.getObservables( mc.GetObservables() ).first()
        preFitIntegral = pdf.expectedEvents(RooArgSet(obs))
        h = pdf.createHistogram(pdf.GetName(), obs)
        h.Scale(preFitIntegral/h.Integral())
    else:
        preFitIntegral = pdf.expectedEvents(RooArgSet(obs))
        h = pdf.createHistogram(pdf.GetName(), obs)
        if h.Integral() !=0:
          h.Scale(preFitIntegral/h.Integral())
    w.loadSnapshot("vars_final")
    return h, muValueBeforeFitLegend


def getCompName(name):
    if name.startswith("L_x_"):
        return name.split('_')[2]
    return name


def categories(chanCat):
    it = chanCat.typeIterator()
    tt = it.Next()
    while tt:
        yield tt
        tt = it.Next()


def components(pdf, ttname):
    modelName1 = ttname + "_model"
    pdfmodel1 = pdf.getComponents().find(modelName1)
    funcList1 =  pdfmodel1.funcList()
    funcIter1 = funcList1.iterator()
    comp1 = funcIter1.Next()
    while comp1 != None:
        yield comp1
        comp1 = funcIter1.Next()


def getPostfitSuffix(is_conditional, is_asimov, mu=None):
    if is_asimov:
        dirname = "AsimovFit_"
        if mu is None:
            mu = 1 # default value
    else:
        dirname = "GlobalFit_"
        if mu is None:
            mu = 0 # default value
    if is_conditional:
        condname = "conditionnal_"
    else:
        condname = "unconditionnal_"
    muname = "mu{0}".format(mu)
    mu = str(mu)
    postfit_suffix = "{0}{1}{2}".format(dirname, condname, muname)
    return postfit_suffix


def getMCerror(w, rfr, regname):
    if Config.comps is None:
        getComponents(w)
    comps = Config.comps
    if not "both" in regname:
        obs  = comps[regname][0]
        binWidth = comps[regname][1]
        MC = comps[regname][3]["MC"]
        val, error = getValueAndError(obs, MC, binWidth, rfr, regname)
    else:
        list_comps = []
        idx = regname.find("both")
        for y in ["2011", "2012"]:
            regnameyear = regname[:idx]+y+regname[idx+4:]
            if not regnameyear in comps:
                continue
            obs  = comps[regnameyear][0]
            binWidth = comps[regnameyear][1].getVal()
            MC = comps[regnameyear][3]["MC"]
            list_comps.append((obs, binWidth, MC))
        val, error = getSumAndError(list_comps, rfr)
    return error


def getAltCatName(ttname):
    blocks = ttname.rsplit('_Y',1)
    catname = blocks[0]+"_Yboth"+blocks[1][4:]
    year = blocks[1][:4]
    if year == "2011":
        altyear = "2012"
    else:
        altyear = "2011"
    alt_tt = blocks[0]+'_Y'+altyear+blocks[1][4:]
    return alt_tt, catname


def getAllPlotObjects(w, rfr, is_prefit, suffix, plotdir, restrict_to=[], excludes=[]):
    """ returns a map of plot objects, indexed by category name """
    if is_prefit:
        print "Loading initial snapshot"
        w.loadSnapshot("vars_initial")
    else:
        print "Loading final snapshot"
        w.loadSnapshot("vars_final")
    getYields(w)
    tables.TablesFromPlots(Config.yields, plotdir, suffix)
    yields = Config.yields
    if Config.comps is None:
        getComponents(w)
    comps = Config.comps
    if Config.plot_objs is None:
        Config.read_plot_objs()
    objs_dict = Config.plot_objs

    doVarBin = True
    #doVarBin = False
    if doVarBin:
      print "building binning dictionary"
      histBinDict={}
      histInputDir="/".join((plotdir.split("/")[0:-1]))#probably an easier way of removing the dir from the path
      #("_".join(a.split("_")[0:-1])).replace("sbfit","varBins")+.root
      histInputDir=histInputDir+"/diagnostics/"
      binfilename="varBins_ZHvvbb_Zprime_full_syst_Nov18_UniMergeBins.root"
      for i in os.listdir(histInputDir):
        if os.path.isfile(os.path.join(histInputDir,i)) and 'varBins' in i:
          binfilename=i
      binFile = TFile.Open(histInputDir+binfilename, "READ")
      print histInputDir+binfilename
      print histInputDir
      print plotdir
      print binfilename
      print binFile
      #binFile = TFile.Open(histInputDir+"/varBins_ZHvvbb_Zprime_full_syst_Nov18_UniMergeBins.root", "READ")
      for ttname, comptt in comps.iteritems():
        try: 
          thishist=binFile.Get(ttname)
          tmphist=(thishist.GetNbinsX())*[-1]
          tmphist.append(thishist.GetXaxis().GetBinUpEdge(thishist.GetNbinsX()))
          tmpbins = array("d",tmphist)
          thishist.GetLowEdge(tmpbins)
          histBinDict[ttname]=tmpbins
        except:
          print "Failed to get new binning for histogram",ttname
        #print ttname
      #use plotdir to find histos and build dict
      print histBinDict

    print "Getting Distributions for each subchannel"

    for ttname, comptt in comps.iteritems():
        if len(restrict_to)>0:
            if not True in (r in ttname for r in restrict_to):
                continue
        if len(excludes)>0:
            if True in (r in ttname for r in excludes):
                continue
        objs = objs_dict.get(ttname, {})

        print "Gathering plot primitives for region", ttname
        pdftmp  = comptt[4]
        datatmp = comptt[2]
        obs  = comptt[0]

        individual_comps = {k:v for k,v in comptt[3].iteritems()
                            if k!="MC" and k!="Signal" and k!="Bkg"
                            and k!="SignalExpected" and k!="S/B"
                            and k!="S/sqrt(S+B)"}



        try:
          newBinSizes=histBinDict[ttname]
          #newBinSizes=array("d",newBinSizes)
        except:
          print "failed to get hist binning for",ttname
          exit(0)
          #newBinSizes=[0, 300, 800, 1300, 1800, 2300, 2800, 3300, 3800, 4300, 5000]
        
        #bins = array("d", newBinSizes)

        #used to hack the binning in the mJet plots for EOYE 2015
        #newBinSizes = [0] + range(50, 501, 25)
        #newBinSizes= [0] + range(50, 471, 20) + [500]
        newBinSizes=array("d",newBinSizes)

        print "doing ",ttname
        print objs
        divisor=25
        #divisor=100
        objs = getPlotObjects(obs, pdftmp, datatmp, individual_comps.values(), yields[ttname], newBinSizes, doVarBin, rfr, objs)
        print "gotplotobjs"

        # if doing postfit, add the prefit line
        #newBinSizes = [0,10,20,30,40,50,60,70,80,90,100]
        #newBinSizes = [0,10,20,40,50,60,70,90,100,110,150]
        #doVarBin = True
        if not is_prefit and "prefit" not in objs:
            histo, mulegend = getPrefitCurve(w, obs, pdftmp)
            if doVarBin: 
              print "Changing the binning of the histograms"
              if len(newBinSizes)-1 == histo.GetNbinsX():
                #create new hist with new bin sizes
                newh=TH1F(histo.GetName(),histo.GetTitle(), histo.GetNbinsX(), newBinSizes)
                #more pythony loop?
                i=0
                while i < histo.GetNbinsX()+2:
                  binwidth= newh.GetXaxis().GetBinWidth(i)             
                  newh.SetBinContent(i, histo.GetBinContent(i)/(binwidth/divisor))
                  newh.SetBinError(i, histo.GetBinError(i)/(binwidth/divisor))
                  i=i+1
                objs["prefit"] = [newh, mulegend]
              else:
                print "Are you trying to plot variable width histograms? Are you sure you got the number of bins correct?"
                continue
                #TODO skipping these as a test. In future we should exit.
                #exit(1)
            else:
              objs["prefit"] = [histo, mulegend]

        if verbose:
            print objs
        objs_dict[ttname] = objs

    Config.save_plot_objs()

    to_remove = []
    if len(restrict_to)>0:
        for k in objs_dict:
            if not True in (r in k for r in restrict_to):
                to_remove.append(k)
            if True in (r in k for r in excludes):
                to_remove.append(k)
    objs_final = copy.deepcopy(objs_dict)
    for k in to_remove:
        objs_final.pop(k)
    return objs_final


def scaleTGraphAsymmErrors(tg, weight):
    yval = tg.GetY()
    errhi = tg.GetEYhigh()
    errlo = tg.GetEYlow()
    for i in range(tg.GetN()):
        yval[i] *= weight
        errhi[i] *= weight
        errlo[i] *= weight


def plot(objs, ttname, suffix, plotdir, save_hists, plot_bkgsub=True, ybounds=(0.4, 1.6), masspt="125" ):
    # Now do the plots
    print "setting up plotname",ttname
    sm = mkplots.SetupMaker(ttname, mass=masspt ,muhat = Config.muhat)
    if 'mass' in objs:
        print objs
        sm.add('mass', objs['mass'])
    for k,v in objs.iteritems():
        print "...", k, v
        sm.add(getCompName(k), v)

    # first, standard plot
    cname = ttname +"_"+suffix
    print cname
    if ttname.endswith("weighted"):
        can = sm.setup.make_complete_plot(cname, True, ytitle = "Weighted events",ybounds = ybounds)
        canlog = sm.setup.make_complete_plot(cname+'_logy', True,True, ytitle = "Weighted events",ybounds = ybounds)
    else:
        can = sm.setup.make_complete_plot(cname, True, ybounds = ybounds)
        canlog = sm.setup.make_complete_plot(cname+'_logy', True,True, ybounds = ybounds)
    plotname = "{0}/{1}".format(plotdir, can.GetName())

    if plot_bkgsub:
        # then, bkg-subtracted plot
        cname2 = ttname +"_BkgSub_"+suffix
        can2 = sm.setup.make_bkg_substr_plot(cname2)
        plotname2 = "{0}/{1}".format(plotdir, can2.GetName())
    for f in Config.formats:
        print "saving!"
        print f
        print plotname
        can.Print(plotname+'.'+f)
        canlog.Print(plotname+'log.'+f)
        #can.Print(plotname+'.C')
        #canlog.Print(plotname+'log.C')
        if plot_bkgsub:
            can2.Print(plotname2+'.'+f)
        print "Saved!"
    # save histograms if requested
    if save_hists:
        afile = TFile.Open(plotname+".root", "recreate")
        can.Write(can.GetName())
        canlog.Write(canlog.GetName())
        if plot_bkgsub:
            can2.Write(can.GetName())
        for k,v in objs.iteritems():
            if isinstance(v, TObject):
                v.Write(v.GetName())
            if k == "prefit":
                v[0].Write(v[0].GetName())
    can.Close()
    if plot_bkgsub:
        can2.Close()
    # free memory for some objects used in the plotting...
    mkplots.purge()
    # TODO check for things not deleted


def is_signal(compname):
    """ Check if a component is Higgs. If yes, return mass """
    print compname
    # Spyros: Add ggA to list of signal names - has to be first in list otherwise we get problems
    signames = ["HVT"]
    #signames = ["HVT","ggA", "ZvvH", "WlvH", "ZllH", "VH", "ZH", "WH"]

    #TODO  - Kill with fire. If signal contains HVT1000_ZH this fails badly unless you look for HVT before ZH


    mass = False
    # Spyros: if sg in compname matches also mVH so doesn't work for resonance analyses
    # remove mVH from compname
    compname = re.sub('mVH', '', compname)
    for sg in signames:
        if sg in compname:
            pos = compname.find(sg) + len(sg)
            print pos
	    # Spyros: This doesn't work if mass > 999
            #mass = int(compname[pos:pos+3])
            mass = int(re.sub("[^0-9]", "", compname[pos+4:pos+9]))
            print mass
            break
    return mass

if __name__ == "__main__":

    gROOT.LoadMacro("/afs/cern.ch/work/p/pmullen/DBLHaifegDebug/atlasstyle/AtlasStyle.C") 
    SetAtlasStyle()

    wsname = sys.argv[1]
    modes = [int(s) for s in sys.argv[2].split(',')]
    # modes:
    # 0 is prefit
    # 1 is bkg-only fit
    # 2 is S+B fit
    
    
    #Paul VHRes
    #example run:
    #python doPlotFromWS.py /afs/cern.ch/work/p/pmullen/DBLHaifegDebug/151106_Wade/run2_current_Nov5/ZHllbb_Zprime/ws/ZHllbb_Zprime_actualWorkspaces_full_syst_Nov5.root 0 /afs/cern.ch/work/p/pmullen/DBLHaifegDebug/151106_Wade/run2_current_Nov5/ZHllbb_Zprime/ws/diagnostics/sbfit_ZHllbb_Zprime_full_syst_Nov5_1000p000.root ws_1000p000
    #python doPlitFromWS.py {workspace} {plotting mode} {fit result} {workspace name}
    #should really have the fit result name as that changes depending on the type of fit

    if len(sys.argv)>3:
      fitres = sys.argv[3]
    else:
      if modes !=0:
        print "You REALLY dont want to run this without telling it where to find your fit results"
        print "Note: This should be the path fropm then relative to the ws directory and with the _mass.root removed from the end"
        print "for example: /home/results/ws/diagnostics/sbfit_ZHllbb_Zprime_flat_syst_Oct20_1000p000.root should become diagnostics/sbfit_ZHllbb_Zprime_flat_syst_Oct20"
        print "this is hacky and ugly so we should probably make this better at some point"
      else:
        fitres = "/afs/cern.ch/work/p/pmullen/DBLHaifegDebug/151106_Wade/run2_current_Nov5/ZHllbb_Zprime/ws/diagnostics/sbfit_ZHllbb_Zprime_flat_syst_Oct20"
    print fitres

    if len(sys.argv)>4:
      rfrname = sys.argv[4]
    else:
      if modes !=0:
        print "You REALLY dont want to run this without telling it the name of your fit result"
      else:
        rfrname="ws_1000p000"


    # Spyros: This was the 4th argument - moved it to 5th place since it's optional
    # We should better make the order interchangeable
    #if len(sys.argv)>5:
    #    fitres = sys.argv[5]
    #else:
    #    fitres = wsname

    # FIXME hacky
    #if fitres.startswith("/afs"):
    fcc = fitres
    #else:
    #    fcc = "FitCrossChecks_"+fitres+"_combined"

    for mode in modes:
        if mode == 0:
            print "Doing prefit plots"
            try:
                main(wsname, rfrname=rfrname )
            except TypeError:
                print "WARNIG error to be solved: object.__new__(PyROOT_NoneType) is not safe, use PyROOT_NoneType.__new__()"
        elif mode == 1:
            print "Doing bkg-only postfit plots"
            main(wsname, fcc, rfrname, is_conditional=True, is_asimov=False, mu=0)
        elif mode == 2:
            print "Doing s+b postfit plots"
            main(wsname, fcc, rfrname, is_conditional=False, is_asimov=False, mu=1)
#        elif mode == 10:
#            print "Doing combined prefit plots"
#            main(wsname, mass, combined=True)
#        elif mode == 11:
#            print "Doing combined bkg-only postfit plots"
#            main(wsname, mass, fcc, is_conditional=True, is_asimov=False, mu=0, combined=True)
#        elif mode == 12:
#            print "Doing combined s+b postfit plots"
#            main(wsname, mass, fcc, is_conditional=False, is_asimov=False, mu=1, combined=True)
#        elif mode == 20:
#            print "Doing summed prefit plots"
#            main(wsname, mass, summed=True)
#        elif mode == 21:
#            print "Doing summed bkg-only postfit plots"
#            main(wsname, mass, fcc, is_conditional=True, is_asimov=False, mu=0, summed=True)
#        elif mode == 22:
#            print "Doing summed s+b postfit plots"
#            main(wsname, mass, fcc, is_conditional=False, is_asimov=False, mu=1, summed=True)
#        elif mode == 30:
#            print "Doing summed prefit plots in CR"
#            main(wsname, mass, sumCR=True)
#        elif mode == 31:
#            print "Doing summed bkg-only postfit plots in CR"
#            main(wsname, mass, fcc, is_conditional=True, is_asimov=False, mu=0, sumCR=True)
#        elif mode == 32:
#            print "Doing summed s+b postfit plots in CR"
#            main(wsname, mass, fcc, is_conditional=False, is_asimov=False, mu=1, sumCR=True)
        else:
            print "Mode", mode, "is not recognized !"


