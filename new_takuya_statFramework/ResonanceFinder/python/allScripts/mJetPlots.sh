python VHPlotsMJet.py mJetPlots/ \
        /afs/cern.ch/user/c/cpollard/public/mJetWS/LLBB_HVTZHllqq2000_full_syst_mJet_outputs.root combined \
        /afs/cern.ch/user/c/cpollard/public/mJetWS/sbfit_2TeV.root > mJetllqq.log

python VHPlotsMJet.py mJetPlots/ \
        /afs/cern.ch/user/c/cpollard/public/mJetWS/VVBB_HVTWHlvqq2000-HVTZHvvqq2000_full_syst_mJet_outputs.root combined \
        /afs/cern.ch/user/c/cpollard/public/mJetWS/sbfit_2TeV.root > mJetvvqq.log

python VHPlotsMJet.py mJetPlots/ \
        /afs/cern.ch/user/c/cpollard/public/mJetWS/LVBB_HVTWHlvqq2000-HVTZHllqq2000_full_syst_mJet_outputs.root combined \
        /afs/cern.ch/user/c/cpollard/public/mJetWS/sbfit_2TeV.root > mJetlvqq.log

