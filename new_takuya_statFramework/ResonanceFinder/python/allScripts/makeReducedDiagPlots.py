#!/usr/bin/env python
#############################################
# Script called by comparePulls.py (committed here to avoid incompatibilities from on-going developments on WSMaker)
# Further details inside comparePulls.py.
# Adapted from:
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/WorkspaceMaker#Fit_Diagnostics
#############################################

import sys
import os

import ROOT
from ROOT import gDirectory, gROOT

import runFitCrossCheck

#extension = "png"
extension = "eps"

def main(version, directory, is_conditional=False, is_asimov=False, mu=None):
    # TODO this initialization is UGLY
    plotdir = "plots/{0}".format(version)
    try:
        os.mkdir(plotdir)
    except OSError:
        pass
    plotdir = "plots/{0}/fcc".format(version)
    try:
        os.mkdir(plotdir)
    except OSError:
        pass
    if is_asimov:
        dirname = "AsimovFit_"
        if mu is None:
            mu = 1 # default value
    else:
        dirname = "GlobalFit_"
        if mu is None:
            mu = 0 # default value
    if is_conditional:
        condname = "conditionnal_"
    else:
        condname = "unconditionnal_"
    muname = "mu{0}".format(mu)
    mu = str(mu)

    plotdir = "plots/{0}/fcc/{1}{2}{3}".format(version, dirname, condname, muname)
    try:
        os.mkdir(plotdir)
    except OSError:
        pass

    gROOT.SetBatch(True)
    ROOT.gSystem.Load("libPlotUtils.so")
    f = ROOT.TFile.Open("{0}/FitCrossChecks.root".format(directory))
    if is_asimov:
        f.cd("PlotsAfterFitToAsimov")
    else:
        f.cd("PlotsAfterGlobalFit")
    if is_conditional:
        gDirectory.cd("conditionnal_MuIsEqualTo_{0}".format(mu))
    else:
        gDirectory.cd("unconditionnal")

    p_chi2 = gDirectory.Get("Chi2PerChannel")
    if p_chi2 != None:
        hchi2 = p_chi2.GetListOfPrimitives().At(0)
        c2 = ROOT.TCanvas("c2","c2")
        hchi2.Draw()
        c2.Print("{1}/Chi2PerChannel.{0}".format(extension, plotdir))

    p_nuis = gDirectory.Get("can_NuisPara_{0}{1}{2}".format(dirname, condname, muname))
    p_corr = gDirectory.Get("can_CorrMatrix_{0}{1}{2}".format(dirname, condname, muname))

    # play with canvas of NP
    h = p_nuis.GetListOfPrimitives().At(0)
    axis = h.GetXaxis()
    g_2s = p_nuis.GetListOfPrimitives().At(1)
    g_1s = p_nuis.GetListOfPrimitives().At(2)
    nuis = p_nuis.GetListOfPrimitives().At(3)
    # first try to reproduce the existing plot
    #nuis_plot("origin", nuis.Clone(), g_2s.Clone(), g_2s.Clone(), h.Clone())
    # then sort the NP
    # function pointers not supported in PyROOT... have to workaround
    gROOT.ProcessLine("TGraph* n = (TGraph*)"+str(ROOT.AddressOf(nuis)[0]))
    gROOT.ProcessLine("TAxis* a = (TAxis*)"+str(ROOT.AddressOf(axis)[0]))
    gROOT.ProcessLine("PU::sortTGraphAxis(n, a, true, PU::comp_sysNames)")
    #nuis_plot("sorted", nuis.Clone(), g_2s.Clone(), g_2s.Clone(), h.Clone())
    # then plot only interesting things
    res = reduce_all(nuis, g_2s, g_1s, axis, excludes=vector_TString("HiggsNorm"))
    nuis_plot(plotdir, "all", *res)
    nuis = res[0]
    g_2s = res[1]
    g_1s = res[2]
    axis = nuis.GetXaxis().Clone()
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("SysJetE", "SysJetF", "SysJetM",
    #                                                                  "SysBJet", "SysJetBE"))
    #    nuis_plot(plotdir, "Jet1", *res)
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("SysJetN", "SysJetP", "SysJVF",
    #                                                                   "METReso", "METTrig"))
    #    nuis_plot(plotdir, "Jet2", *res)
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("TruthTagDR", "BTagB"))
    #    nuis_plot(plotdir, "BTagB", *res)
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("BTagC"))
    #    nuis_plot(plotdir, "BTagC", *res)
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("BTagL"))
    #    nuis_plot(plotdir, "BTagL", *res)
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("Elec", "Muon","Lep"))
    #    nuis_plot(plotdir, "Lepton", *res)
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("norm"))
    #    nuis_plot(plotdir, "Norm1", *res)
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("Norm","Ratio"))
    #    nuis_plot(plotdir, "Norm2", *res)
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("DPhi", "WMbb", "WbbMbb", "ZMbb", "ZPt", "WPt"))
    #    nuis_plot(plotdir, "ModelWZ", *res)
    
    #    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("VVJet", "VVMbb", "TopP", "Ttbar", "ttbarHigh", "TChan",
    #                                                                 "WtChan", "SChan"))
    #    nuis_plot(plotdir, "ModelTopVV", *res)
    #res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("norm"))
    #nuis_plot(plotdir, "norm", *res, hmin=0, hmax=2.5)
    #res = reduce_all(nuis, g_2s, g_1s, axis, excludes=vector_TString("SysJetE", "SysJetF", "SysJetM", "SysBJet",
    #                                                              "SysJetBE", "SysJetN",
    #                                                              "SysJetP", "SysJVF", "MET", 
    #                                                              "TruthTagDR", "BTag", "norm", "Norm",
    #                                                              "Ratio", "Elec", "Muon", "Lep", "DPhi", "WMbb",
    #                                                              "WbbMbb", "ZMbb", "ZPt", "WPt", "VVJet",
    #                                                              "VVMbb", "TopP", "Ttbar", "ttbarHigh", "TChan", "WtChan",
    #                                                              "SChan"))
    #nuis_plot(plotdir, "Rest", *res)

    # New clasification
    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("SysFT_EFF_Eigen", "SysFT_EFF_extrapolation"))
    nuis_plot(plotdir, "BTag", *res)


    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("SysWt", "SysTop", "SysTtbar"))
    nuis_plot(plotdir, "Top", *res)


    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("SysVV", "SysWM","SysZM","SysWD","SysZD","SysWP","SysZP"))
    nuis_plot(plotdir, "ModelBoson", *res)


    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("Norm","Ratio"))
    nuis_plot(plotdir, "Norm", *res)

    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("norm"))
    nuis_plot(plotdir, "norm", *res)

    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("SysMUON","SysEL","SysEG"))
    nuis_plot(plotdir, "Lepton", *res)

    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("SysJET","FATJET"))
    nuis_plot(plotdir, "Jet", *res)

    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("SysMET"))
    nuis_plot(plotdir, "MET", *res)


    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString("LUMI"))
    nuis_plot(plotdir, "LUMI", *res)


    tmp = shift_to_zero(nuis, axis)
    res = reduce_all(tmp, g_2s, g_1s, axis, excludes=vector_TString("blablabla"))
    nuis_plot(plotdir, "Shifted", *res)


    suspicious_NP = []
    suspicious_NP.extend(flag_suspicious_NP(nuis, axis, .5, .5))
    res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString(*suspicious_NP))
    #res = reduce_all(nuis, g_2s, g_1s, axis, includes=vector_TString(*flag_suspicious_NP(nuis, axis)))
    nuis_plot(plotdir, "Suspicious", *res)

    # play with correlation matrix
    h2 = p_corr.GetListOfPrimitives().At(0)
    corr_plot(plotdir, "origin", h2.Clone())
    # function pointers not supported in PyROOT... have to workaround
    gROOT.ProcessLine("TH2* c = (TH2*)"+str(ROOT.AddressOf(h2)[0]))
    gROOT.ProcessLine("PU::sortTHAxis(c, true, PU::comp_sysNames)")
    gROOT.ProcessLine("PU::sortTHAxis(c, false, PU::comp_sysNames, true)")
    corr_plot(plotdir, "sorted", h2.Clone())
    reduce_and_plot(plotdir, "noMCStat", h2, excludes=vector_TString("gamma"))
    reduce_and_plot(plotdir, "JES", h2, includes=vector_TString("SigX", "norm_", "Jet"))
    reduce_and_plot(plotdir, "BTag", h2, includes=vector_TString("SigX", "norm_", "BTag"))
    reduce_and_plot(plotdir, "Mbb", h2, includes=vector_TString("SigX", "norm_", "Mbb"))
    reduce_and_plot(plotdir, "Modelling", h2, includes=vector_TString("SigX", "norm_", "Norm", "Ratio", "PtBi"))
    reduce_and_plot(plotdir, "SF", h2, includes=vector_TString("SigX", "norm_"))
    reduce_and_plot(plotdir, "Norm", h2, includes=vector_TString("3JNorm", "norm_", "Norm", "Ratio"))
    # find all correlations > 25%
    systs = set()
    systs.add("SigX")
    nbins = h2.GetNbinsX()
    hasOneBin = False

    for i in range(1, nbins+1):
        for j in range(1, nbins+1):
            if i+j == nbins+1: # diagonal
                continue
            if abs(h2.GetBinContent(i,j))>0.25:
                hasOneBin = True
                systs.add(h2.GetXaxis().GetBinLabel(i))
                systs.add(h2.GetYaxis().GetBinLabel(j))
    if hasOneBin :
        reduce_and_plot(plotdir, "HighCorr", h2, includes=vector_TString(*systs))

    #systs = set()
    #systs.add("SigX")
    #nbins = h2.GetNbinsX()
    #for i in range(1, nbins+1):
    #    if not "LUMI" in h2.GetXaxis().GetBinLabel(i):
    #        continue
    #    for j in range(1, nbins+1):
    #        if i+j == nbins+1: # diagonal
    #            continue
    #        if abs(h2.GetBinContent(i,j))>0.1:
    #            systs.add(h2.GetXaxis().GetBinLabel(i))
    #            systs.add(h2.GetYaxis().GetBinLabel(j))
    #reduce_and_plot(plotdir, "Lumi", h2, includes=vector_TString(*systs))

    syst_to_study = ["JetEResol", "Mbb_Whf", "V_Whf", "METScale", "TChanP",
                     "ttbarHigh",
                     "BJetReso",
                     "ZblZbb",
                     "BTagB1",
                     "norm_Wbb",
                     "WblWbbRatio",
                    ]
    systsX = set()
    systsX.add("SigX")
    for s in syst_to_study:
        systsX.add(s)
    systsY = set()
    systsY.add("SigX")
    for s in syst_to_study:
        systsY.add(s)
    nbins = h2.GetNbinsX()
    for j in range(1, nbins+1):
        found = False
        sysname = h2.GetYaxis().GetBinLabel(j)
        for s in systsY:
            if s in sysname:
                found = True
                break
        if not found:
            continue
        for i in range(1, nbins+1):
            if i+j == nbins+1: # diagonal
                continue
            if abs(h2.GetBinContent(i,j))>0.15:
                systsX.add(h2.GetXaxis().GetBinLabel(i))
    reduce_and_plot_2D(plotdir, "HighSysts", h2, includesX=vector_TString(*systsX), includesY=vector_TString(*systsY))




def nuis_plot(plotdir, name, nuis, yellow, green, h=None, hmin=-5, hmax=5):
    c = ROOT.TCanvas("c","c",1000,400)
    if h is not None:
        h.SetMinimum(hmin)
        h.SetMaximum(hmax)
        ya = h.GetYaxis()
        h.Draw()
        nuis.Draw("p")
    else:
        nuis.SetMinimum(hmin)
        nuis.SetMaximum(hmax)
        nuis.Draw("pa")
        ya = nuis.GetYaxis()
    ya.SetTitle("pull")
    ya.SetTitleOffset(.5)
    yellow.SetFillColor(ROOT.kYellow)
    green.SetFillColor(ROOT.kGreen)
    yellow.Draw("f")
    green.Draw("f")
    nuis.Draw("p")
    ROOT.gPad.SetBottomMargin(ROOT.gPad.GetBottomMargin()*1.8)
    ROOT.gPad.SetLeftMargin(ROOT.gPad.GetLeftMargin()*.5)
    ROOT.gPad.SetRightMargin(ROOT.gPad.GetRightMargin()*.3)
    ROOT.gPad.Update()
    c.Print("{2}/NP_{0}.{1}".format(name, extension, plotdir))

def corr_plot(plotdir, name, hist):
    c = ROOT.TCanvas("c","c")
    hist.GetXaxis().LabelsOption("v")
    if hist.GetNbinsX() > 0:
        hist.GetXaxis().SetLabelSize(min(1.6/hist.GetNbinsX(), 0.03))
    if hist.GetNbinsY() > 0:
        hist.GetYaxis().SetLabelSize(min(1.4/hist.GetNbinsY(), 0.03))
    purge_label_names(hist.GetXaxis())
    purge_label_names(hist.GetYaxis())
    hist.Draw("colz")
    ROOT.gPad.SetBottomMargin(ROOT.gPad.GetBottomMargin()*1.4)
    ROOT.gPad.SetLeftMargin(ROOT.gPad.GetLeftMargin()*1.2)
    ROOT.gPad.Update()
    c.Print("{2}/corr_{0}.{1}".format(name, extension, plotdir))

def purge_label_names(axis):
    for i in range(1, axis.GetNbins()+1):
        label = axis.GetBinLabel(i)
        axis.SetBinLabel(i, label.replace("alpha_","").replace("ATLAS_","").replace("Sys",""))

def reduce_all(nuis, yellow, green, axis, excludes=None, includes=None):
    if excludes is not None:
        new_nuis = ROOT.PU.reduceTGraphAxisExclude(nuis, axis, True, excludes)
    elif includes is not None:
        new_nuis = ROOT.PU.reduceTGraphAxisInclude(nuis, axis, True, includes)
    max_axis = new_nuis.GetXaxis().GetXmax()
    new_y = yellow.Clone(yellow.GetName()+"_reduced")
    new_g = green.Clone(yellow.GetName()+"_reduced")
    ROOT.PU.removeTGraphPointsAbove(new_y, True, max_axis)
    ROOT.PU.removeTGraphPointsAbove(new_g, True, max_axis)
    return [new_nuis, new_y, new_g]

def shift_to_zero(nuis, axis):
    return ROOT.PU.shiftTGraphToZero(nuis, axis, True)

def reduce_and_plot(plotdir, name, hist, excludes=None, includes=None):
    if excludes is not None:
        tmp_h = ROOT.PU.reduceTHAxisExclude(hist, True, excludes)
        final_h = ROOT.PU.reduceTHAxisExclude(tmp_h, False, excludes)
    elif includes is not None:
        tmp_h = ROOT.PU.reduceTHAxisInclude(hist, True, includes)
        final_h = ROOT.PU.reduceTHAxisInclude(tmp_h, False, includes)
    corr_plot(plotdir, name, final_h)

def reduce_and_plot_2D(plotdir, name, hist, excludesX=None, includesX=None, excludesY=None, includesY=None):
    if excludesX is not None:
        tmp_h = ROOT.PU.reduceTHAxisExclude(hist, True, excludesX)
    elif includesX is not None:
        tmp_h = ROOT.PU.reduceTHAxisInclude(hist, True, includesX)
    if excludesY is not None:
        final_h = ROOT.PU.reduceTHAxisExclude(tmp_h, False, excludesY)
    elif includesY is not None:
        final_h = ROOT.PU.reduceTHAxisInclude(tmp_h, False, includesY)
    corr_plot(plotdir, name, final_h)

def flag_suspicious_NP(nuis, axis, thresh_y=.5, thresh_err=.5):
    names = []
    x = ROOT.Double()
    y = ROOT.Double()
    for i in range(nuis.GetN()):
        err = nuis.GetErrorY(i)
        nuis.GetPoint(i, x, y)
        label = axis.GetBinLabel(axis.FindBin(x))
        if label.startswith("norm_") or abs(y)>thresh_y or err<thresh_err:
            names.append(label)
            #print label, y, err
    return names

def vector_TString(*names):
    res = ROOT.std.vector(ROOT.TString)()
    for n in names:
        res.push_back(n)
    return res

def init():
    if len(sys.argv)<=1:
        print "You must provide a workspace version number !"
        return

    ROOT.gROOT.LoadMacro("$WORKDIR/macros/AtlasStyle.C")
    ROOT.SetAtlasStyle(0.03)
    ROOT.gStyle.SetPadRightMargin(0.12)

    version = sys.argv[1]
    directory = "fccs/FitCrossChecks_{0}_combined".format(version)
    print "Running makeReducedDiagPlots for workspace", version
    if len(sys.argv)>2:
        algnums = sys.argv[2]
        for algnum in algnums.split(','):
            alg = runFitCrossCheck.available_algs[int(algnum)]
            mu = int(alg[1])
            is_conditional = False
            if alg[3] == "true":
                is_conditional = True
            is_asimov = False
            if "Asimov" in alg[0]:
                is_asimov = True
            print "Running for conditional ?", is_conditional, "for asimov ?", is_asimov, "What mu ?", mu
            main(version, directory, is_conditional, is_asimov, mu)


if __name__ == "__main__":
    init()


