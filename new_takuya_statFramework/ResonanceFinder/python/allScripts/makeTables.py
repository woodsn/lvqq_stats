#!/usr/bin/env/python

import sys
import os
import os.path
import doPlotFromWS as plots
from ROOT import *
from ROOT import RooFit as RF


def main(version, directory="", is_conditional=False, is_asimov=False, mu=None, combined=False,
        #window=[90, 150]):
        window=None):
    RooMsgService.instance().setGlobalKillBelow(RF.ERROR)
    gROOT.SetBatch(True)
    gSystem.Load("libPlotUtils.so")

    ws,g = plots.getWorkspace(version)
    if directory: # if directory is present, then assumes we want postfit
        rfr, suffix = plots.getFitResult(directory, is_conditional, is_asimov, mu)
        plots.transferResults(ws, rfr)
        plotdir = "plots/{0}/postfit".format(version)
        tsuffix = "Postfit"
    else : # else, compute prefit yields
        plotdir = "plots/{0}/prefit".format(version)
        rfr = plots.getInitialFitRes(ws)
        ws.loadSnapshot("vars_initial")
        suffix = "Prefit"
        tsuffix = "Prefit"

    make_slides = False

    os.system("mkdir -vp "+plotdir)

    if not combined:
        if window:
            plots.Config.yieldsfile = os.path.join(plotdir, "Yields_{0}_{1}_{2}.yik".format(suffix, window[0],
                                                                                        window[1]))
        else:
            plots.Config.yieldsfile = os.path.join(plotdir, "Yields_{0}.yik".format(suffix))
        plots.getYields(ws, rfr, window=window)
        raw_yields = plots.Config.yields
    else:
        if window:
            plots.Config.yields78file = os.path.join(plotdir, "Yields78_{0}_{1}_{2}.yik".format(suffix, window[0],
                                                                                            window[1]))
        else:
            plots.Config.yields78file = os.path.join(plotdir, "Yields78_{0}.yik".format(suffix))
        plots.getYields78(ws, rfr, window=window)
        raw_yields = plots.Config.yields78
    TablesFromPlots(raw_yields, suffix)

#window
#combined
#suffix

def TablesFromPlots(raw_yields, plotdir, suffix, combined=False, window=None): 
    make_slides = True
    pretty_yields = make_pretty_yields_map(raw_yields)
    if plots.verbose:
        print pretty_yields
    tables = make_tables(pretty_yields, print_errors=False)
    #final_text = r"\newcolumntype{d}{D{+}{\hspace{-3pt}\;\pm\;}{-1}}"+'\n'
    final_text = r"% \newcolumntype{d}{D{+}{\hspace{-3pt}\;\pm\;}{-1}}"+'\n'
    if make_slides: # suitable for beamer
        for t in sorted(tables.keys()):
            #[year, chan, reg] = t.split('_')
            #final_text += r"\begin{frame}{"+chan+', '+reg+', '+year+"}\n"
            final_text += r"\begin{frame}{"+t+"}\n"
            final_text += "\\tiny\n"
            final_text += tables[t]
            final_text += r"\end{frame}"+"\n\n\n"
    else:   # suitable for CONF note or whatever
        for t in sorted(tables.keys()):
            #[year, chan, reg] = t.split('_')
            final_text += r"\begin{table}"+"\n"
            final_text += r"\centering"+"\n"
            final_text += "\\small"+"\n"
            final_text += tables[t]
            #final_text += r"\caption{The "+tsuffix+" yield on the "+chan+", "+reg+" category from "+year+"\label{"+tsuffix+chan+reg+year+"}}"+"\n"
            final_text += r"\end{table}"+"\n\n\n"
        pass
    if plots.verbose:
        print final_text

    if window:
        if not combined:
            latexfile = os.path.join(plotdir, "Yields_{0}_{1}_{2}.tex".format(suffix, window[0], window[1]))
        else:
            latexfile = os.path.join(plotdir, "Yields78_{0}_{1}_{2}.tex".format(suffix, window[0], window[1]))
    else:
        if not combined:
            latexfile = os.path.join(plotdir, "Yields_{0}.tex".format(suffix))
        else:
            latexfile = os.path.join(plotdir, "Yields78_{0}.tex".format(suffix))
    os.system("rm -f "+latexfile)
    f = file(latexfile, 'w')
    f.write(final_text)
    f.close()
    #g.Close()
    #plots.Config.reset()


priorities = {
    "data" : 80,
    "S/sqrt(S+B)" : 73,
    "S/B" : 72,
    "Bkg" : 60,
    "MC" : 75,
    "SignalExpected" : 71,
    "Signal" : 70,
    "VH125" : 70,
    "ZvvH125" : 67,
    "HVTZHllqq1000" : 67,
    "HVTZHvvqq1000" : 67,
    "HVTWHlvqq2000" : 67,
    "HVTZHvvqq2000" : 67,
    "ggZvvH125" : 67,
    "qqZvvH125" : 67,
    "WlvH125" : 68,
    "ZllH125" : 69,
    "ggZllH125" : 69,
    "qqZllH125" : 69,
    "ZvvH150" : 67,
    "ggZvvH150" : 67,
    "qqZvvH150" : 67,
    "WlvH150" : 68,
    "ZllH150" : 69,
    "ggZllH150" : 69,
    "qqZllH150" : 69,
    "TTbar" : 45,
    "stops" : 43,
    "stopt" : 42,
    "stopst" : 41,
    "stopWt" : 40,
    "STop" : 40,
    "Zhf" : 27,
    "Zb" : 24,
    "Zbl" : 25,
    "Zbb" : 27,
    "Zbc" : 26,
    "Zc" : 21,
    "Zcl" : 22,
    "Zcc" : 23,
    "Zl" : 20,
    "Whf" : 37,
    "Wb" : 34,
    "Wbl" : 35,
    "Wbb" : 37,
    "Wbc" : 36,
    "Wcc" : 33,
    "Wc" : 31,
    "Wcl" : 32,
    "Wl" : 30,
    "WZ" : 53,
    "ZZ" : 52,
    "VZ" : 51,
    "WW" : 50,
    "Diboson" : 50,
    "VH" : 50,
    "diboson" : 50,
    "multijet" : 45,
    "multijetEl" : 45,
    "multijetMu" : 45,
    "MJ0lep" : 45,
    "MJ1lep" : 45,
    "MJ2lep" : 45,
    "MJ2lepEl" : 45,
    "MJ1lepEl" : 45,
    "MJ1lepMu" : 45,
}


def make_pretty_yields_map(yields):
    """ Ugly logic to get things right """
    regions = sorted(yields.keys())
    samples = []
    for r in regions:
        samples.extend([plots.getCompName(y) for y in yields[r].keys()])
    samples = list(set(samples))
    print samples
    samples.sort(key = lambda(x): priorities[x])
    pretty_yields = [[s] for s in samples]
    for r in regions:
        compname_dict = {}
        for s in yields[r].keys():
            compname_dict[plots.getCompName(s)] = s
        for i,s in enumerate(samples):
            if s in compname_dict:
                pretty_yields[i].append( yields[r][compname_dict[s]] )
            else:
                pretty_yields[i].append( [0.0, 0.0] )
    regions.insert(0, "")
    pretty_yields.insert(0, regions)
    return pretty_yields


def make_tables(pretty_yields, print_errors=True):
    regions = {}
    for i,reg in enumerate(pretty_yields[0]):
        if reg == '':
            continue
        #blocks = reg.split('_')
        #region = blocks[-2] + '_' + blocks[0] + '_' + blocks[1]
        region = reg
        if not region in regions:
            regions[region] = [i]
        else:
            regions[region].append(i)

    tables = {}
    for reg in sorted(regions.keys()):
        print "Making table for region", reg, "with columns", regions[reg]
        tables[reg] = make_subtable(pretty_yields, regions[reg], print_errors)
    return tables

def make_subtable(yields, cols, print_errors=True):
    vptbin_dict = {0: r"p_{T}^{V}<90\,\mathrm{GeV}", 1: r"90<p_{T}^{V}<120\,\mathrm{GeV}",
                   2: r"120<p_{T}^{V}<160\,\mathrm{GeV}", 3: r"160<p_{T}^{V}<200\,\mathrm{GeV}",
                   4: r"p_{T}^{V}>200\,\mathrm{GeV}", 9: r"p_{T}^{V}>0"}

    #table = r"\begin{tabular}{l"+"|d"*len(cols)+"|}\n"
    table = r"\begin{tabular}{l"+"|c"*len(cols)+"|}\n"
    table += r"\cline{2-"+str(int(len(cols)+1))+"}\n"
    #decomp = yields[0][cols[0]].split('_')
    #region = decomp[0]+", "+decomp[1]
    region = yields[0][cols[0]]
    table += r" & \multicolumn{"+str(len(cols))+"}{c|}{"+region.replace('_', '\_')+r"}\\"+'\n'
    table += r"\cline{2-"+str(int(len(cols)+1))+"}\n"
    table += yields[0][0]
    for i in cols:
        #bin = int(yields[0][i].split('_')[2][1])
        #table += r" & \multicolumn{1}{c|}{$"+vptbin_dict[bin]+"$}"
        bin = yields[0][i]
        table += r" & \multicolumn{1}{c|}{"+bin.replace('_', '\_')+"}"
    table += r"\\ \hline"+"\n"
    for line in yields:
        if line[0] == "MC" or line[0] == '':
            continue
        if line[0] == "data":
            table += r"\hline"+"\n"
            table += line[0]
            for i in cols:
                ##table += r" & \multicolumn{1}{c|}{"+str(int(line[i]))+"}"
                table += r" & "+str(int(line[i]))
            table += r"\\ \hline"+"\n"
            continue
        if line[0] == "S/B":
            table += r"\hline"+"\n"
            table += line[0]
            for i in cols:
                table += r" & {0:.2e}".format(line[i])
            table += r"\\"+"\n"
            continue
        if line[0] == "S/sqrt(S+B)":
            table += line[0]
            for i in cols:
                table += r" & {0:.2e}".format(line[i])
            table += r"\\"+"\n"
            continue
        if line[0] == "Bkg" or line[0] == "Signal":
            table += r"\hline"+"\n"
        table = add_line(line, table, cols, print_errors)
        if line[0] == "Bkg":
            table += r"\hline"+"\n"
    table += r"\end{tabular}"+"\n"
    return table


def add_line(line, table, cols, print_errors=True):
    table += line[0]
    for i in cols:
        if line[i][0] != 0:
            ## table += " & {0:.2f}\\hspace{{3pt}}+{1:.2f}".format(line[i][0], line[i][1])
            if print_errors:
                table += " & {0:.2f} $\pm$ {1:.2f}".format(line[i][0], line[i][1])
            else:
                table += " & {0:.2f}".format(line[i][0])
        else:
            ##table += r" & \multicolumn{1}{c|}{--}"
            table += r" & --"
    table += r"\\"+"\n"
    return table


if __name__ == "__main__":

    wsname = sys.argv[1]
    modes = [int(s) for s in sys.argv[2].split(',')]
    # modes:
    # 0 is prefit
    # 1 is bkg-only fit
    # 2 is S+B fit
    if len(sys.argv)>3:
        fitres = sys.argv[3]
    else:
        fitres = wsname
    fcc = "FitCrossChecks_"+fitres+"_combined"
    for mode in modes:
        if mode == 0:
            print "Doing prefit tables"
            main(wsname)
        elif mode == 1:
            print "Doing bkg-only postfit tables"
            main(wsname, fcc, is_conditional=True, is_asimov=False, mu=0)
        elif mode == 2:
            print "Doing s+b postfit tables"
            main(wsname, fcc, is_conditional=False, is_asimov=False, mu=1)
        elif mode == 10:
            print "Doing combined prefit tables"
            main(wsname, combined=True)
        elif mode == 11:
            print "Doing combined bkg-only postfit tables"
            main(wsname, fcc, is_conditional=True, is_asimov=False, mu=0, combined=True)
        elif mode == 12:
            print "Doing combined s+b postfit tables"
            main(wsname, fcc, is_conditional=False, is_asimov=False, mu=1, combined=True)
        else:
            print "Mode", mode, "is not recognized !"

