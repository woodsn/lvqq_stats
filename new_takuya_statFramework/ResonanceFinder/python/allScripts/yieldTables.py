#!/usr/env python
from sys import argv, stdout
myargv = argv[:]

import ROOT
from ROOT import RooFit as RF
from ROOT import TFile

from math import sqrt

from VHRegionsAndChannels import formatProcessName, formatRegionName

from RooFitUtil import *

ROOT.RooMsgService.instance().setGlobalKillBelow(RF.WARNING)


# dictionary of region, component, (val, err)
def yields(ws, rfr):

    mc = ws.obj("ModelConfig")
    data = ws.data("obsData")
    combPdf = ws.pdf("combPdf")

    channelCat = combPdf.indexCat()
    channelCatName = channelCat.GetName()

    d = {}
    # accumulate 
    for chan in categories(channelCat):

        chanName = chan.GetName()

        regName = formatRegionName(chanName)
        print regName
        d[regName] = {}

        pdftmp = combPdf.getPdf(chanName)
        datatmp = data.reduce("{0}=={1}::{2}".format(channelCatName,channelCatName,chanName))

        obs = pdftmp.getObservables( mc.GetObservables() ).first()

        binning = obs.getBinning()
        stepsize = binning.averageBinWidth()

        obs_set = ROOT.RooArgSet(obs)

        bkgList = ROOT.RooArgList()
        sigList = ROOT.RooArgList()
        totList = ROOT.RooArgList()

        for c in components(pdftmp, chanName):
            compname = c.GetName()
            if "HVT" in compname or "ggA" in compname:
                sigList.add(c)
            else:
                bkgList.add(c)

            d[regName][formatProcessName(compname)] = \
                    sumAndUncert(c.createIntegral(obs_set), rfr, stepsize)

            totList.add(c)

        bkgSum = ROOT.RooAddition("Bkg", "bkg_Sum", bkgList)
        sigSum = ROOT.RooAddition("Signal", "sig_Sum", sigList)
        totSum = ROOT.RooAddition("MC", "tot_Sum", totList)

        bkgNorm = bkgSum.createIntegral(obs_set)
        sigNorm = sigSum.createIntegral(obs_set)
        totNorm = totSum.createIntegral(obs_set)

        d[regName]["backgrounds"] = sumAndUncert(bkgNorm, rfr, stepsize)
        d[regName]["signals"] = sumAndUncert(sigNorm, rfr, stepsize)
        d[regName]["total"] = sumAndUncert(totNorm, rfr, stepsize)
        d[regName]["data"] = \
                (datatmp.sumEntries(), sqrt(datatmp.sumEntries()))

    return d


def procLine(yieldsDict, proc, regions, yieldsDictRatio=None):
    line = proc.ljust(16)
    for reg in regions:
        s = "& $%0.2f \\pm %0.2f" % yieldsDict[reg].get(proc, (0, 0))
        if yieldsDictRatio != None:
            s += "\ (%0.2f)$" % \
                    safeDiv( yieldsDict[reg].get(proc, (0, 0))[0]
                           , yieldsDictRatio[reg].get(proc, (0, 0))[0]
                           )
        else:
            s += "$"

        line += s.ljust(32)
        continue

    line += "\\\\"
    return line


def makeTable(yieldsDict, regions=None, procs=None, yieldsDictRatio=None):

    if regions == None:
        regions = yieldsDict.keys()

    if procs == None:
        procs = yieldsDict[regions[0]].keys()

    lines = []
    lines.append("\\begin{tabular}{l" + "|r"*len(regions) + "}")

    regLabels = \
            map(lambda s: "& \\multicolumn{1}{|c}{%s}" % s, regions)

    lines.append(" " + " ".join(regLabels) + " \\\\")
    lines.append("\\hline")

    for p in procs:
        lines.append(procLine(yieldsDict, p, regions,
            yieldsDictRatio))
        continue

    lines.append("\\hline")

    for p in ["backgrounds", "data"]:
        lines.append(procLine(yieldsDict, p, regions,
            yieldsDictRatio))
        continue

    lines.append("\\end{tabular}")

    return "\n".join(lines)



def main(wsfname, wsname, fitfname, prefitratio=False):
    fws = TFile(wsfname)
    ws = fws.Get(wsname)

    # e.g.
    # /ws/diagnostics/sbfit_*.root
    fitfname = myargv[3]

    f = TFile.Open(fitfname)
    rfr = f.Get("fitresult_combPdf_obsData").Clone()

    mc = ws.obj("ModelConfig")
    np = ROOT.RooArgSet(mc.GetNuisanceParameters())
    pois = mc.GetParametersOfInterest()
    for n in args(pois):
        np.add(n)

    ws.saveSnapshot("vars_initial", np)
    fpf = rfr.floatParsFinal().Clone()
    cp = rfr.constPars()

    # add all const parameters of the RooFitResult to the floating
    # ones
    fpf.add(cp)

    np.assignFast(fpf)

    ws.saveSnapshot("vars_final", np)

    ws.loadSnapshot("vars_final")
    yields_final = yields(ws, rfr)

    print yields_final; stdout.flush()
    return

    procs = [ "$t\\bar{t}$", "single top"
            # not for 2 lepton
            , "$W+b$", "$W+c$", "$W+l$"
            , "diboson", "$VH$"
            , "$Z+b$", "$Z+c$", "$Z+l$"
            ]

    regs = [ "$\\ell\\ell b\\bar{b}$, $75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$, 1 $b$-tag, 0 add. $b$-tags"
           , "$\\ell\\ell b\\bar{b}$, $75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$, 2 $b$-tags, 0 add. $b$-tags"
           , "$\\ell\\nu b\\bar{b}$, $75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$, 1 $b$-tag, 0 add. $b$-tags"
           , "$\\ell\\nu b\\bar{b}$, $75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$, 2 $b$-tags, 0 add. $b$-tags"
           , "$\\nu\\nu b\\bar{b}$, $75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$, 1 $b$-tag, 0 add. $b$-tags"
           , "$\\nu\\nu b\\bar{b}$, $75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$, 2 $b$-tags, 0 add. $b$-tags"
           ]

    if prefitratio:
        ws.loadSnapshot("vars_initial")
        yields_initial = yields(ws, rfr)

        print makeTable(yields_final, procs=procs, regions=regs,
                yieldsDictRatio=yields_initial)

    else:
        print makeTable(yields_final, procs=procs, regions=regs)


    return
if __name__ == "__main__":
    main(argv[1], argv[2], argv[3], len(argv) > 4)
