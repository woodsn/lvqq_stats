from array import array
import ROOT


def ratioArrows(dataratio, max, min):
    xs = []
    for iBin in xrange(1, dataratio.GetNbinsX()+1):
        if dataratio.GetBinContent(iBin) > max:
            xs.append(dataratio.GetBinCenter(iBin))
        continue

    n = len(xs)
    xarr = array('f', xs)
    yarr = array('f', [(max-min)*0.9]*n)
    yerrsl = array('f', [(max-min)*0.33]*n)
    noerrs = array('f', [0.0]*n)

    if not n:
        return None
    else:
        g = ROOT.TGraphAsymmErrors(n, xarr, yarr,
                noerrs, noerrs, yerrsl, noerrs)

        g.SetMarkerStyle(22)
        g.SetMarkerSize(2)
        g.SetLineWidth(2)
        return g


def stripUncert(h):
    hnew = h.Clone()
    for iBin in xrange(hnew.GetNbinsX()+2):
        hnew.SetBinError(iBin, 0.0)
        continue

    return hnew


def maxWithUncert(h):
    mx = 0.0
    for iBin in xrange(1, h.GetNbinsX()+1):
        mx = max(mx, h.GetBinContent(iBin) + h.GetBinError(iBin))
        continue

    return mx


def minH(h, mn):
    for iBin in xrange(1, h.GetNbinsX()+1):
        bc = h.GetBinContent(iBin)
        if bc <= 0:
            continue
        mn = min(mn, bc)
        continue

    return mn


def forceXmax(h, mx):
    edges = []
    for iBin in xrange(1, h.GetNbinsX()+1):
        edges.append(h.GetBinLowEdge(iBin))
        if h.GetBinLowEdge(iBin) + h.GetBinWidth(iBin) >= mx:
            break;
        else:
            continue

    edges.append(mx)

    hnew = h.Clone()
    hnew.SetBins(len(edges)-1, array('d', edges))

    for iBin in xrange(1, hnew.GetNbinsX()+1):
        hnew.SetBinContent(iBin, h.GetBinContent(iBin))
        hnew.SetBinError(iBin, h.GetBinError(iBin))
        continue

    return hnew



def drawText(x, y, s, font=72, size=None):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextColor(1)
    l.SetTextAlign(11)
    l.SetTextFont(font)
    if size:
        l.SetTextSize(size)

    l.DrawLatex(x, y, s)


def drawLumi(x, y):
    drawText(x, y,
            "#sqrt{s} = 13 TeV  #scale[0.75]{#int} Ldt = 3.2 fb^{-1}",
            font=42)
    return



def drawAtlasLabel(x, y, suffix="Internal"):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextColor(1)
    l.SetTextAlign(11)

    l.DrawLatex(x, y, "ATLAS")

    drawText(x + 0.13, y, suffix, font=42)

    return


def buildLegend(x1, y1, x2, y2, histOpts):

    leg = ROOT.TLegend(x1, y1, x2, y2)
    leg.SetTextSize(0.04)

    leg.SetFillColor(-1)
    leg.SetFillStyle(4000)
    leg.SetBorderSize(0)

    for h, opt in histOpts:
        leg.AddEntry(h, h.GetTitle(), opt)

    leg.Draw()
    return leg


def args(argSet):
    it = argSet.createIterator()
    tt = it.Next()
    while tt:
        yield tt
        tt = it.Next()


def divByBinWidth(h):
    hnew = h.Clone()
    for iBin in range(1, h.GetNbinsX()+1):
        binw = h.GetBinWidth(iBin)
        hnew.SetBinContent(iBin, h.GetBinContent(iBin)/binw)
        hnew.SetBinError(iBin, h.GetBinError(iBin)/binw)

    return hnew


def safeDiv(x, y):
    if x == 0 or y == 0:
        return 0

    return x/y


def scaleBins(h, hBin):
    if h.GetNbinsX() != hBin.GetNbinsX():
        print "incorrect number of bins between histogram", \
            h, "and bin histogram", hBin
        exit(-1)


    binEdges = map(hBin.GetBinLowEdge, xrange(1, hBin.GetNbinsX()+2))
    barray = array('d', binEdges)
    h.SetBins(h.GetNbinsX(), barray)
    return divByBinWidth(h)


def scaleBinsTG(tg, hBinning):
    if tg.GetN() != hBinning.GetNbinsX():
        print "incorrect number of bins between TGraph", \
            tg, "and bin histogram", hBinning
        exit(-1)

    h = hBinning.Clone()
    h.SetName(tg.GetName())
    h.SetTitle(tg.GetTitle())

    for tgBin in xrange(tg.GetN()):
        # TGraphs are 0-indexed.
        # TH1s are 1-indexed.
        hBin = tgBin + 1

        if tg.GetY()[tgBin] < 0.01:
            h.SetBinContent(hBin, 0.0)
            h.SetBinError(hBin, 0.0)

        else:
            h.SetBinContent(hBin, tg.GetY()[tgBin])

            err = (tg.GetErrorYhigh(tgBin) + tg.GetErrorYlow(tgBin)) / 2.0
            h.SetBinError(hBin, err)

        continue

    return divByBinWidth(h)


def categories(chanCat):
    it = chanCat.typeIterator()
    tt = it.Next()
    while tt:
        yield tt
        tt = it.Next()


def components(pdf, ttname):
    # dealing with a combined pdf...
    if "file" in ttname:
        modelName = ttname.replace("file", "model_file")
    # or a simple pdf
    else:
        modelName = ttname + "_model"

    pdfmodel = pdf.getComponents().find(modelName)

    iter = pdfmodel.funcList().iterator()

    i = iter.Next()
    while i != None:
        yield i
        i = iter.Next()


def sumComponents(name, title, cs):
    s = ROOT.RooArgList()
    for c in cs:
        s.add(c)

    return ROOT.RooAddition(name, title, s)


def compToHist(c, obs, rfr):
    """take a component of a pdf, an observable, and a RooFitResult,
    and return a histogram of that component"""

    stepsize = obs.getBinning().averageBinWidth()
    obsset = ROOT.RooArgSet(obs)

    h = c.createHistogram("hist_" + c.GetName(), obs)
    (s, e) = sumAndUncert(c.createIntegral(obsset), rfr, stepsize)
    h.Scale(safeDiv(s, h.Integral()))

    return h




def sumAndUncert(x, rfr, stepsize):
    return (x.getVal()/stepsize, x.getPropagatedError(rfr)/stepsize)


