#!/usr/env python
from sys import argv
myargv = argv[:]

import ROOT
from ROOT import RooFit as RF
from ROOT import TFile

from math import sqrt

from VHRegionsAndChannels import formatProcessName, formatRegionName

ROOT.RooMsgService.instance().setGlobalKillBelow(RF.WARNING)


def categories(chanCat):
    it = chanCat.typeIterator()
    tt = it.Next()
    while tt:
        yield tt
        tt = it.Next()


def args(argSet):
    it = argSet.createIterator()
    tt = it.Next()
    while tt:
        yield tt
        tt = it.Next()

def components(pdf, ttname):
    modelName1 = ttname + "_model"
    pdfmodel1 = pdf.getComponents().find(modelName1)
    funcList1 =  pdfmodel1.funcList()
    funcIter1 = funcList1.iterator()
    comp1 = funcIter1.Next()
    while comp1 != None:
        yield comp1
        comp1 = funcIter1.Next()


def sumAndUncert(x, rfr, stepsize):
    return tuple(map(lambda y: y/stepsize, (x.getVal(),
        x.getPropagatedError(rfr))))


def npLine(systDict, npname, regions):
    line = npname.replace("alpha_", "").replace("_", "\\_").ljust(24)
    maxdelt = 0
    for reg in regions:
        for s in ["backgrounds", "signal"]:
            nom = systDict[reg][""][s][0]
            var = systDict[reg][npname][s][0]
            delt = abs((var - nom) / nom * 100)
            s = "& $%0.2f$" % delt
                    
            if delt > maxdelt:
                maxdelt = delt

            line += s.ljust(8)
        continue

    line += " \\\\"
    return (maxdelt, line)


# dictionary of region, component, (val, err)
def systs(ws, rfr, regs=None, params=None):

    mc = ws.obj("ModelConfig")
    nps = ROOT.RooArgSet(mc.GetNuisanceParameters())
    nps.add(mc.GetParametersOfInterest())

    simPdf = ws.pdf("simPdf")
    channelCat = simPdf.indexCat()
    channelCatName = channelCat.GetName()

    if params is None:
        params = map(ROOT.TNamed.GetName, list(args(nps)))

    # always run "nominal"
    if "" not in params:
        params.append("")

    d = {}
    for chanName in map(lambda ch: ch.GetName(),
            list(categories(channelCat))):

        regName = formatRegionName(chanName)

        if regs is not None:
            if regName not in regs:
                continue

        d[regName] = {}

        for npname in params:
            d[regName][npname] = {}

            if npname == "":
                ws.loadSnapshot("vars_final")
            else:
                ws.loadSnapshot("vars_final_" + npname)

            simPdf = ws.pdf("simPdf")

            pdftmp = simPdf.getPdf(chanName)

            obs = pdftmp.getObservables( mc.GetObservables() ).first()

            binning = obs.getBinning()
            stepsize = binning.averageBinWidth()

            obs_set = ROOT.RooArgSet(obs)

            bkgList = ROOT.RooArgList()
            sigList = ROOT.RooArgList()
            totList = ROOT.RooArgList()

            for c in components(pdftmp, chanName):
                compname = c.GetName()
                if "HVT" in compname or "ggA" in compname:
                    sigList.add(c)
                else:
                    bkgList.add(c)

            bkgSum = ROOT.RooAddition( "Bkg", "bkg_Sum", bkgList)
            sigSum = ROOT.RooAddition( "Signal", "sig_Sum", sigList)


            bkgNorm = bkgSum.createIntegral(obs_set)
            sigNorm = sigSum.createIntegral(obs_set)

            d[regName][npname]["backgrounds"] = sumAndUncert(bkgNorm, rfr, stepsize)
            d[regName][npname]["signal"] = sumAndUncert(sigNorm, rfr, stepsize)
            continue
        continue

    return d

def makeTable(systDict, mindelt=0):

    # dangerous and prone to breaking
    regions = systDict.keys()
    npnames = systDict[regions[0]].keys()

    # don't want to show nominal
    npnames.remove("")
    # don't want to show mu!
    npnames.remove("mu")

    lines = []
    lines.append("\\begin{tabular}{l" + "|r"*len(regions)*2 + "}")

    regLabels = \
            map(lambda s: "& \\multicolumn{2}{|c}{%s}" % s, regions)
    sbLabels = \
            ["& background & signal"]*len(regions)

    lines.append(" " + " ".join(regLabels) + " \\\\")
    lines.append(" " + " ".join(sbLabels) + " \\\\")
    lines.append("\\hline")

    nplines = []
    for npname in npnames:
        x = npLine(systDict, npname, regions)
        if x[0] < mindelt:
            continue
        else:
            nplines.append(x)

    nplines = reversed(sorted(nplines))
    lines += map(lambda l: l[1], nplines)

    lines.append("\\hline")
    lines.append("\\end{tabular}")

    return "\n".join(lines)



def main(wsfname, pdfname, fitfname):
    fws = TFile(wsfname)
    ws = fws.Get(pdfname)

    mc = ws.obj("ModelConfig")
    nps = ROOT.RooArgSet(mc.GetNuisanceParameters())
    nps.add(mc.GetParametersOfInterest())

    ws.saveSnapshot("vars_final", nps)

    # e.g.
    # /ws/diagnostics/sbfit_*.root
    fitfname = myargv[3]

    f = TFile.Open(fitfname)
    rfr = f.Get("fitresult_simPdf_obsData").Clone()

    fpf = rfr.floatParsFinal().Clone()
    cp = rfr.constPars()

    # add all const parameters of the RooFitResult to the floating
    # ones
    fpf.add(cp)

    nps.assignFast(fpf)

    ws.saveSnapshot("vars_final", nps)

    for np in args(nps):
        ws.loadSnapshot("vars_final")

        uncert = np.getPropagatedError(rfr)
        np.setVal(np.getVal() + uncert)

        ws.saveSnapshot("vars_final_" + np.GetName(), nps)

    regs = [
            "$75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$, 1 $b$-tag, 0 add. $b$-tags",
            "$75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$, 2 $b$-tags, 0 add. $b$-tags"
            ]

    systDict = systs(ws, rfr, regs=regs)


    print makeTable(systDict, mindelt=2)
    return


if __name__ == "__main__":
    main(argv[1], argv[2], argv[3])
