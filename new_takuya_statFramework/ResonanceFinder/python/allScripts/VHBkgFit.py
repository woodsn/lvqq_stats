from sys import argv, stdout
import json
from itertools import product
import os
import shutil
from array import array

myargv = argv[:]

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptFit(111)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2");
ROOT.TH1.SetDefaultSumw2(True)

# TH1::Divide(TF1 *) is not doing what's expected...
# do it by hand.
# WARNING: this function assumes the uncertainty on the function is
# much smaller than that on the histogram.
def divide_func(h, f):
    for iBin in range(h.GetNbinsX()+2):
        ledge = h.GetBinLowEdge(iBin)
        width = h.GetBinWidth(iBin)
        fx = f.Eval(ledge + width/2.0)
        h.SetBinContent(iBin, h.GetBinContent(iBin) / fx)
        h.SetBinError(iBin, h.GetBinError(iBin) / fx)

    return h


def fill_with_func(h, f, therange):
    (xmin, xmax) = therange
    # loop over all bins in fit range;
    # set bin value to func value if it is not filled.
    for iBin in range(h.FindBin(xmin), h.GetNbinsX()+2):
        if h.GetBinContent(iBin) == 0.0 and h.GetBinError(iBin) == 0.0:
            ledge = h.GetBinLowEdge(iBin)
            width = h.GetBinWidth(iBin)
            fx = f.Eval(ledge + width/2.0)
            h.SetBinContent(iBin, fx)

        continue

    return h


def plot_fit(h, func, fname):
    c = ROOT.TCanvas("c", "c", 800, 600)
    c.Divide(2, 1)

    plotpad = c.cd(1)
    plotpad.SetPad(0.0, 0.3, 1.0, 1.0)
    plotpad.SetBottomMargin(0.15)
    plotpad.SetLeftMargin(0.15)
    plotpad.SetRightMargin(0.075)

    ratiopad = c.cd(2)
    ratiopad.Clear()
    ratiopad.SetPad(0.0, 0.05, 1.0, 0.3)
    ratiopad.SetTopMargin(0.02)
    ratiopad.SetLeftMargin(0.15)
    ratiopad.SetRightMargin(0.075)
    ratiopad.SetBottomMargin(0.3)

    plotpad.cd()
    plotpad.SetLogy()

    h.Draw()
    func.DrawCopy("same")
    h.GetYaxis().SetTitle("entries")
    h.GetXaxis().SetTitle("m_{VH} [GeV]")

    ratiopad.cd()
    ratiopad.Clear()

    hratio = h.Clone()
    hratio = divide_func(hratio, func)

    if hratio.GetListOfFunctions().GetSize():
        hratio.GetListOfFunctions().At(0).SetBit(ROOT.TF1.kNotDraw)

    hratio.SetStats(0)
    hratio.Draw()

    hratio.GetYaxis().SetRangeUser(0.5, 1.5)
    hratio.GetYaxis().SetTitle("ratio")
    hratio.GetYaxis().SetLabelSize(0.1)
    hratio.GetYaxis().SetTitleSize(0.15)
    hratio.GetYaxis().SetTitleOffset(0.25)
    hratio.GetYaxis().SetNdivisions(205)
    hratio.GetXaxis().SetLabelSize(0.1)
    hratio.GetXaxis().SetTitleSize(0.15)
    hratio.GetXaxis().SetTitleOffset(0.75)

    ratioLine = ROOT.TLine(hratio.GetBinLowEdge(1), 1,
            hratio.GetBinLowEdge(hratio.GetNbinsX()+1), 1)
    ratioLine.SetLineColor(ROOT.kRed)
    ratioLine.SetLineWidth(3)
    ratioLine.Draw("same")

    c.SaveAs(fname)

    return


def main(args):
    fconfig = open(args[1])
    config = json.load(fconfig)
    fconfig.close()

    fitfiles = set(
                ["%s_%s_%s.root" % x for x in
                product(
                    config["fitprocesses"],
                    config["fitregions"],
                    config["fitvariables"]
                    )]
                )

    infolder = config["infolder"]
    outfolder = config["outfolder"]

    os.makedirs(outfolder)
    if "plotfolder" in config:
        plotfolder = config["plotfolder"]
        os.makedirs(plotfolder)
    else:
        plotfolder = None

    if "fitnominal" in config:
        nominal = config["fitnominal"]
    else:
        nominal = None

    allfiles = os.listdir(infolder)
    (xmin, xmax) = config["fitrange"]

    for fname in allfiles:
        # ignore non-root files.
        if not ".root" in fname:
            continue

        fullfinname = os.path.join(infolder, fname)
        fullfoutname = os.path.join(outfolder, fname)

        if fname not in fitfiles:
            shutil.copyfile(fullfinname, fullfoutname)
            continue

        fin = ROOT.TFile(fullfinname)
        if not fin:
            print "ERROR: can't open file", fullfinname
            continue

        fout = ROOT.TFile(fullfoutname, "CREATE")
        if not fout:
            print "ERROR: can't open file", fullfoutname
            continue

        fout.cd()

        for hname in config["fithistograms"]:
            hin = fin.Get(hname)
            if not hin:
                print "ERROR: can't get histogram", hname, "from file", fullfinname
                continue

            h = hin.Clone()

            func = ROOT.TF1(hname+"func", config["fitfunction"])
            func.SetParameters(*tuple(config["fitinitialparams"]))

            fitresultptr = h.Fit(func, "QWLS", "", xmin, xmax)
            fitresult = fitresultptr.Get()

            if fitresult.Status() != 0:
                print "ERROR: fit did not converge for histogram", hname, "from file", fullfinname

            fill_with_func(h, func, (xmin, xmax))

            if plotfolder:
                plot_fit(h, func, os.path.join(plotfolder, fname) + hname + ".png")

            h.Write()

            # extrapolation systematics
            if hname == nominal:
                for (n, sfs) in zip(["EXTRAP__1up", "EXTRAP__1down"],
                        config["fituncertainty"]):

                    systhist = hin.Clone(n)

                    systfunc = ROOT.TF1(n+"func", config["fitfunction"])

                    for (i, sf) in enumerate(sfs):
                        systfunc.SetParameter(i,
                                func.GetParameter(i)*sf)

                    fill_with_func(systhist, systfunc, (xmin, xmax))

                    if plotfolder:
                        plot_fit(systhist, systfunc,
                                os.path.join(plotfolder, fname) + n + ".png")

                    systhist.Write()


            continue


        fout.Close()
        continue


    return

if __name__ == "__main__":
    main(myargv)
