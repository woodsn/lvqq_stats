from itertools import product
from sys import argv
# prevent ROOT from mangling arguments...
myargv = argv[:]

import ROOT
ROOT.gROOT.SetBatch(True)


inputfile = myargv[1]
outpath = myargv[2].rstrip('/') + '/'
if len(myargv) > 3:
    rebinF = int(myargv[3])
else:
    rebinF = 1

print "rebinning by a factor of", rebinF

# list of included signals

#sigMasses = range(500, 2001, 100) + range(2200, 3001, 200) + range(3500, 4501, 500)
sigMasses = range(1000, 2001, 100) + range(2200, 3001, 200) 

sigs = ["%s%d" % params \
        for params in product(["HVTWZvvqq", "HVTWZvvqq"], sigMasses)]

#bkgs = ["TTbar", "STop", "Wb", "Wc", "Wl", "Zc", "Zb", "Zl", "Diboson"]
bkgs = ["ttbar", "stop_s","stop_t","stop_Wt", "W", "Z", "WW", "WZ", "ZZ"]

procs = sigs + bkgs + ["data"]
#procs = ["TTbar"]

#nbtags = ["0", "1", "2"]
nbtags = ["0p", ]
#naddbtags = ["0", "1p"]
naddbtags = ["0p", ]
#mHregs = ["SR", "lowMH", "highMH"]
mHregs = ["vvWSR","mvWCR","mvTCR","mmZCR" ]
#mVHregs = ["blind", "unblind"]
#mVHregs = ["unblind"]
plots = ["mVH"]

folders = ["%s_%stag%sjet_0ptv_%s_%s" % params \
        for params in product(procs, nbtags, naddbtags, mHregs,  plots)]

# set of systematics
allSysts = set()
dummyHist = None

f = ROOT.TFile.Open(inputfile)

print "input file : ", f

# loop over folders and assemble list of systematics
for histKey in f.GetListOfKeys():    
    dummyHist = histKey.ReadObj()
    if not dummyHist.Class().InheritsFrom(ROOT.TDirectory.Class()): continue
    sname = dummyHist.GetName()
    allSysts.add(sname)
#    tokens = sname.split("__")
#    print tokens
#    if len(tokens)>1:
#        print tokens[0]
#        allSysts.add(tokens[0])


if not dummyHist:
    print "no histograms were registered; exiting!"
    exit(-2)

# loop over folders again, write systematic hists
for folder in folders:

    filename = outpath + folder + ".root"
    outfile = ROOT.TFile.Open(filename, "RECREATE")
    print "making file for ", folder

    print "syst. : ", allSysts

#    outfile.mkdir("nominal").cd()

    h = f.Get(folder)
    if h: 
        h.Rebin(rebinF)
        h.SetName("nominal")
        h.SetDirectory(outfile)

    for s in allSysts:
        sname = folder
        sname += "_"
        sname += s
        sdir = f.Get(s)
        h = sdir.Get(sname)

        print h
        
        if not h:
            if "data" in folder and not s == "Nominal":
                continue
            
            h = dummyHist.Clone()
            h.Reset()
            h.SetNameTitle(s,s)
            
            h.Rebin(rebinF)
            h.SetDirectory(outfile)
            
        else:
            if "data" in folder and not s == "Nominal":
                continue
            
            h.Rebin(rebinF)
            h.SetNameTitle(s,s);
            h.SetDirectory(outfile)


    outfile.Write()
    outfile.Close()
