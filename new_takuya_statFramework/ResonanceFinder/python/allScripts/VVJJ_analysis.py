# an example driver for the VVJJ analysis
# Valerio Ippolito - Harvard University

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(ROOT.gSystem.ExpandPathName('.x $ROOTCOREDIR/scripts/load_packages.C'))

from ROOT import RF as RF
from ROOT import PullPlotter as PullPlotter


def createDirectoryStructure(input_dir, releaseDir, analysis, inputListTag, outputWSTag, replaceRule):
  import os
  import shutil
  import glob

  # create directories
  newdir = '{releaseDir}/{analysis}/data/{inputListTag}'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)
  diagnosticsdir = '{releaseDir}/{analysis}/ws/diagnostics'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)
  tmpdir = '{releaseDir}/{analysis}/tmp'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)

  # first, we remove the previously existing directories
  dirs = [newdir, diagnosticsdir, tmpdir]
  for dir in dirs:
    if os.path.exists(dir):
      shutil.rmtree(dir)

  # we use makedirs, which takes care of creating intermediate directories
  os.makedirs(newdir)
  os.makedirs(diagnosticsdir)
  os.makedirs(tmpdir)
  
  # copy input trees from input location
  files = glob.glob(os.path.join(input_dir, '*.*'))
  for file in files:
    file_short = file.split('/')[-1] # e.g. take only data_SR.root
    if file_short not in replaceRule:
      shutil.copy2(file, newdir)
    else:
      if replaceRule[file_short] == None:
        print 'createDirectoryStructure is NOT copying file %s' % file
      else:
        replacement = '%s/%s' % (newdir, replaceRule[file_short])
        print 'createDirectoryStructure is RENAMING file %s to file %s' % (file, replacement)
        shutil.copy2(file, replacement)

  

if __name__ == '__main__':

  # LOCATIONS
  #
  # v4 (same as v3, only rescaled to different lumi and w/ifferent lumi uncertainty)
  # v3 (same as v2 but including missing signal points): /afs/cern.ch/work/v/vippolit/VVJJ/EOYE/v3
  # v2 (most recent signals, new data): /afs/cern.ch/work/v/vippolit/VVJJ/EOYE/v2
  # v1 (most recent signals but old data): /afs/cern.ch/work/v/vippolit/VVJJ/EOYE/v1
  #
  # signal and data inputs: /afs/cern.ch/work/c/cdelitzs/public/SignalHistograms/CONFNote_011215/WZ (ZZ, WW)

  # OLD LOCATIONS (if you want to use them, please go back on SVN to an older version of this driver)
  #  WZ data, with ntrk cut (default), /afs/cern.ch/work/y/yygao/public/VVJJStatInput/results_AntiKt10TrimmedPtFrac5SmallR20_3320ipb_noPRW/WZ/results_tree/syst_100/
  #  WW data, with ntrk cut (default), /afs/cern.ch/work/y/yygao/public/VVJJStatInput/results_AntiKt10TrimmedPtFrac5SmallR20_3320ipb_noPRW/WW/results_tree/syst_100
  #  WZ data, without ntrk cut, /afs/cern.ch/work/y/yygao/public/VVJJStatInput/results_AntiKt10TrimmedPtFrac5SmallR20_3320ipb_NoTrkCut_noPRW/WZ/results_tree/syst_100/

  # run settings
  doInjection = False 
  doObserved = True
  doExternalPdf = True
  producePull = False
  doToys = False

  # which analysis to run
  selection = 'WW' # WW, WZ, ZZ
  interpretation = 'HVT'# Wprime, HVT, Gravi

  # specification of analysis inputs
  treeLumi = 3.320 # 1/fb
  newLumi = 3.2 # 1/fb
  inputPredir = '/afs/cern.ch/work/v/vippolit/VVJJ/EOYE'
  analysisType = 'v3'
  inputListTag = 'v4'
  signal_mask = '%s_{mass}' % interpretation # e.g. Wprime_2000_SR.root -> Wprime_2000
  hasNtrk = True
  outputWSTag = 'v4' # the unique name for this configuration (things are appended later, automatically)

  # option parsing
  analysis = 'VVJJ_{interpretation}_{selection}'.format(interpretation = interpretation.upper(), selection = selection.upper()) # e.g. VVJJ_WPRIME_WZ
  inputdir = '%s/%s/%s' % (inputPredir, analysisType, selection)
  releaseDir = 'results_%s_%sfb' % (outputWSTag, str(newLumi).replace('.', 'p')) # the name of the local folder where inputs and outputs are stored

  # append output name
  outputWSTag = '%s_%s_%s' % (outputWSTag, selection, interpretation)
  if doObserved : outputWSTag = '%s_obs' % outputWSTag
  else : outputWSTag = '%s_exp' % outputWSTag
  if 'QCD' in inputdir: outputWSTag = '%s_QCD' % outputWSTag
  if hasNtrk : outputWSTag = '%s_wNtrk' % outputWSTag
  else: outputWSTag = '%s_noNtrk' % outputWSTag


  # specific analysis settings
  nbins = 15 # mJJ binning
  min_mjj = 1000
  max_mjj = 2500 
  muInject = 2.0 # injection settings
  massInject = '2000'
  lumiSys = 0.05 # relative luminosity uncertainty, on signals
  ntrkSys = 0.06 # relative ntrk uncertainty, on signals
  if selection == 'WZ': # decorrelation parameter, to have meaningful uncertainty bands
    xi_val = 8.33
    if hasNtrk: xi_val = 8.10
  elif selection == 'WW':
    if hasNtrk: xi_val = 8.2
    else: raise RuntimeError('xi value for WW without Ntrk is not implemented')
  elif selection == 'ZZ':
    if hasNtrk: xi_val = 8.27
    else: raise RuntimeError('xi value for ZZ without Ntrk is not implemented')
  else:
    raise RuntimeError('xi value for selection %s is not implemented' % selection)

  if ( doInjection == True ) :
    outputWSTag = '%s_siginj_mu%s' % (outputWSTag, str(muInject).replace('.', 'p'))
 

  # mass scan settings
  if selection == 'WZ':
    if 'QCD' in inputdir:
      masses = [1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500] # full available mass range
      raise RuntimeError('Please double-check the implementation of this feature, as it was done quickly')
    else:
      if interpretation.lower() == 'wprime':
        # WPRIME WZ
       #masses = [1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400] # mass range to be used for limit setting
        masses = [1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2300, 2400] # mass range to be used for limit setting
      else:
        # HVT WZ
        masses = [1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400] # mass range to be used for limit setting
  elif selection == 'WW':
    if interpretation.lower() == 'hvt':
      # HVT WW
      masses = [1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400]
    else:
      # GRAVI WW
      masses = [1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400]
  elif selection == 'ZZ':
    # GRAVI ZZ
    masses = [1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400]
  else:
    raise RuntimeError('Mass points not chosen for selection %s analysis %s' % (selection, analysis))


  # mu scan settings
  if interpretation.lower() == 'hvt' and selection == 'WZ':
    poi_setups = { 
      1100 : [75, 0, 20],
      1200 : [75, 0, 20],
      1300 : [75, 0, 20],
      1400 : [75, 0, 20],
      1500 : [75, 0, 20],
      1600 : [75, 0, 20],
      1700 : [75, 0, 20],
      1800 : [60, 0, 20],
      1900 : [60, 0, 20],
      2000 : [60, 0, 20],
      2100 : [60, 0, 20],
      2200 : [60, 0, 20],
      2300 : [60, 0, 50],
      2400 : [60, 0, 50],
      2500 : [60, 0, 50],
    }
  elif interpretation.lower() == 'hvt' and selection == 'WW':
    poi_setups = { 
      1100 : [75, 0, 20],
      1200 : [75, 0, 20],
      1300 : [75, 0, 20],
      1400 : [75, 0, 20],
      1500 : [75, 0, 20],
      1600 : [75, 0, 20],
      1700 : [75, 0, 20],
      1800 : [60, 0, 20],
      1900 : [60, 0, 20],
      2000 : [60, 0, 20],
      2100 : [60, 0, 20],
      2200 : [60, 0, 20],
      2300 : [60, 0, 40],
      2400 : [60, 0, 40],
      2500 : [60, 0, 40],
    }
  elif interpretation.lower() == 'gravi' and selection == 'WW':
    poi_setups = { 
      1100 : [75, 0, 50],
      1200 : [75, 0, 50],
      1300 : [75, 0, 50],
      1400 : [75, 0, 50],
      1500 : [75, 0, 80],
      1600 : [75, 0, 80],
      1700 : [75, 0, 80],
      1800 : [60, 0, 80],
      1900 : [60, 0, 100],
      2000 : [60, 0, 150],
      2100 : [60, 0, 150],
      2200 : [60, 0, 300],
      2300 : [60, 0, 300],
      2400 : [60, 0, 500],
      2500 : [60, 0, 500],
    }
  elif interpretation.lower() == 'gravi' and selection == 'ZZ':
    poi_setups = { 
      1100 : [75, 0, 100],
      1200 : [75, 0, 100],
      1300 : [75, 0, 100],
      1400 : [75, 0, 150],
      1500 : [75, 0, 150],
      1600 : [75, 0, 150],
      1700 : [75, 0, 200],
      1800 : [60, 0, 200],
      1900 : [60, 0, 200],
      2000 : [60, 0, 300],
      2100 : [60, 0, 300],
      2200 : [60, 0, 500],
      2300 : [60, 0, 500],
      2400 : [60, 0, 600],
      2500 : [60, 0, 600],
    }
  else:
    raise RuntimeError('You forgot to implement a proper POI scan')

  # pull plot settings
  pullMasses = [2000]

  # here we make sure multi-jet background is used instead of data when needed, and deleted otherwise
  # doObserved == False: replace data_SR.root with background_SR.root
  # doObserved == True: delete background_SR.root
  replaceRule = {}
  if doObserved:
    replaceRule['background_SR.root'] = None # this means: don't copy background_SR.root
  else:
    replaceRule['data_SR.root'] = None
    replaceRule['background_SR.root'] = 'data_SR.root' # this means: copy background_SR.root and rename it to data_SR.root
  createDirectoryStructure(inputdir, releaseDir, analysis, inputListTag, outputWSTag, replaceRule)


  ###
  ###
  ###

  vvjj = RF.VVJJAnalysisRunner(analysis)
  vvjj.setTreeObs('x')
  vvjj.setTreeWeight('weight')
  vvjj.setLumiRescale(treeLumi, newLumi)
  vvjj.setNbins(nbins)
  vvjj.setObsMin(min_mjj)
  vvjj.setObsMax(max_mjj)
  vvjj.setReleaseDir(releaseDir)
  vvjj.setInputListTag(inputListTag)
  vvjj.setOutputWSTag(outputWSTag)
  vvjj.doPull(producePull);
  vvjj.setPOI('mu')
  vvjj.addResolNP('ATLAS_FATJET_D2R') # to obtain down variation automatically as 2*nominal-up !
  vvjj.addResolNP('ATLAS_FATJET_JER')
  vvjj.addResolNP('ATLAS_FATJET_JMR')
  # create a new channel (will automatically add data)
  vvjj.addChannel('SR', '') # not using selection criteria


  # set up function for up/down shape variations, used in the envelope case
  # syntax is the same as RooWorkspace::factory
  # first argument is the factory command, second argument is the name of the observable
  # (dijet mass) used in this formula
  if not doExternalPdf:
    fitfunction = "CEXPR::b('pow(1 - x/sqrts, ATLAS_qqqq_bkg_p2-xi*ATLAS_qqqq_bkg_p3) * pow(x/sqrts, -ATLAS_qqqq_bkg_p3)', {x[%.0f,%f],sqrts[13000],xi[%.3f],ATLAS_qqqq_bkg_p2[5.28833e+01,-100,100],ATLAS_qqqq_bkg_p3[-1.72355e+00,-100,100]})" %(min_mjj, max_mjj, xi_val)
    vvjj.setFitFunction(fitfunction, "x") 
  else:
    # in principle we do not need this in case we use the external PDF
    # but in this way the files under ws/diagnostics/histos* will have the
    # correct envelope
    fitfunction = "Background::b(x[%f,%f],ATLAS_qqqq_bkg_p2[5.28833e+01,-100,100],ATLAS_qqqq_bkg_p3[-1.72355e+00,-100,100],xi[%.3f],1)" %(min_mjj, max_mjj, xi_val) 
    vvjj.setFitFunction(fitfunction, "x") 

  # loop over signals
  for mass in masses:
    thisSig = signal_mask.format(mass=mass)
    vvjj.channel('SR').addSample(thisSig)
    vvjj.channel('SR').sample(thisSig).multiplyBy('mu', 0.0, -1e3, 1e4)
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_FATJET_D2R')
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_FATJET_JER')
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_FATJET_JMR')
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_JET_WZ_CrossCalib')
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_JET_WZ_Run1_D2')
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_JET_WZ_Run1_mass')
    vvjj.channel('SR').sample(thisSig).addVariation('ATLAS_JET_WZ_Run1_pT')


    vvjj.channel('SR').sample(thisSig).multiplyBy('ATLAS_Lumi', 1.0, 1-lumiSys, 1+lumiSys, RF.MultiplicativeFactor.GAUSSIAN)

    # set the normalisation uncertainties 
    if hasNtrk:
      vvjj.channel('SR').sample(thisSig).multiplyBy('ATLAS_qqqq_sig_ntrk', 1.0, 1-ntrkSys, 1+ntrkSys, RF.MultiplicativeFactor.GAUSSIAN)

    # declare signal mass value
    vvjj.addSignal(thisSig, mass)
  #
  # add background 
  #
  vvjj.channel('SR').addSample('background')
  # make sure the fit range is sensible
  vvjj.channel('SR').sample('background').multiplyBy('ATLAS_norm_qqqq_bkg', 1.0, 0, 1000)

  if not doExternalPdf:
    vvjj.channel('SR').sample('background').addVariation('fit')
  else:
    # copy and load background from Run-1 analysis
    import os
    os.system('cp ResonanceFinder/scripts/VVJJ/Background.* .')
    ROOT.gROOT.ProcessLine('.L Background.cxx+')
    background_external_pdf = "Background::b(x[%f,%f],ATLAS_qqqq_bkg_p2[5.28833e+01,-100,100],ATLAS_qqqq_bkg_p3[-1.72355e+00,-100,100],xi[%.3f],1)" %(min_mjj, max_mjj, xi_val) 
    vvjj.channel("SR").sample("background").setFitFunction(background_external_pdf, "x")

  if doInjection:
    vvjj.setInjectionSample(signal_mask.format(mass=massInject))
    vvjj.setInjectionStrength(muInject)
  if doObserved:
    if doInjection:
      raise RuntimeError('Injection presumes you do not compute observed limits, in any case...')
    vvjj.setDoObserved(True)
  else:
    vvjj.setDoObserved(False)

  # setup limit runner
  vvjj.limitRunner().setPOIScan(100, 0, 100) # nsteps, min, max
  for mass in poi_setups:
    # deal with this signal in an optimised way
    vvjj.limitRunner().setPOIScan(mass, poi_setups[mass][0], poi_setups[mass][1], poi_setups[mass][2])

  # uncomment to get more verbose limit setting output
  #vvjj.limitRunner().limitTool().hypoTestInvTool().SetParameter('PrintLevel', 2)
  #vvjj.limitRunner().limitTool().setCalcType(RF.AsymptoticWithNominalAsimov)

  # optional: use toys instead of asymptotics
  if doToys:
    vvjj.limitTool().setCalcType(RF.Frequentist)
    vvjj.limitTool().setTestStatType(RF.PL1sided)
    vvjj.limitTool().setNToys(5000)
    vvjj.limitTool().hypoTestInvTool().SetParameter('UseProof', True)
    vvjj.limitTool().hypoTestInvTool().SetParameter('GenerateBinned', True) # speeds up

  vvjj.run()

  # plot
  ROOT.gROOT.ProcessLine('.L atlasstyle/AtlasStyle.C+')
  ROOT.SetAtlasStyle()
  plotter = RF.Plotter(releaseDir+'_'+inputListTag+'_'+outputWSTag, '.')
  plotter.setDoObserved(doObserved or doInjection) # will or will not plot observed limit, and p0
  plotter.setVarUnit('GeV')
  if selection == 'WZ':
    bf = 0.699*0.676
    if 'wprime' == interpretation.lower():
      plotter.setVarName('m_{W\'}')
      plotter.setSpec(1100, bf, 128.93) # in fb
      plotter.setSpec(1200, bf, 88.662)
      plotter.setSpec(1300, bf, 61.908)
      plotter.setSpec(1400, bf, 44.482)
      plotter.setSpec(1500, bf, 32.327)
      plotter.setSpec(1600, bf, 23.77)
      plotter.setSpec(1700, bf, 17.775)
      plotter.setSpec(1800, bf, 13.422)
      plotter.setSpec(1900, bf, 10.267)
      plotter.setSpec(2000, bf, 7.8715)
      plotter.setSpec(2100, bf, 6.118)
      plotter.setSpec(2200, bf, 4.7763)
      plotter.setSpec(2300, bf, 3.7325)
      plotter.setSpec(2400, bf, 2.9662)
      plotter.setSpec(2500, bf, 2.3708)
      plotter.setSpec(3000, bf, 0.80858)
    elif 'hvt' == interpretation.lower():
      plotter.setVarName('m_{W\'}')
      plotter.setSpec(1000, bf, 201.7) # in fb
      plotter.setSpec(1100, bf, 132.9)
      plotter.setSpec(1200, bf, 90.14)
      plotter.setSpec(1300, bf, 62.72)
      plotter.setSpec(1400, bf, 44.36)
      plotter.setSpec(1500, bf, 31.95)
      plotter.setSpec(1600, bf, 23.32)
      plotter.setSpec(1700, bf, 17.28)
      plotter.setSpec(1800, bf, 12.98)
      plotter.setSpec(1900, bf, 9.819)
      plotter.setSpec(2000, bf, 7.5)
      plotter.setSpec(2200, bf, 4.454)
      plotter.setSpec(2400, bf, 2.703)
      plotter.setSpec(2600, bf, 1.679)
      plotter.setSpec(2800, bf, 1.054)
      plotter.setSpec(3000, bf, 0.6688)
      plotter.setSpec(3500, bf, 0.226)
      plotter.setSpec(4000, bf, 0.07822)
      plotter.setSpec(4500, bf, 0.02748)
    else:
      raise RuntimeError('WZ interpretation %s not implemented', interpretation)

  elif selection == 'WW':
    bf = 0.676*0.676
    if 'gravi' == interpretation.lower():
      plotter.setVarName('m_{G_{RS}}')
      plotter.setSpec(1000, bf, 33.21) # in fb
      plotter.setSpec(1100, bf, 19.17)
      plotter.setSpec(1200, bf, 11.53)
      plotter.setSpec(1300, bf, 7.176)
      plotter.setSpec(1400, bf, 4.584)
      plotter.setSpec(1500, bf, 3.002)
      plotter.setSpec(1600, bf, 2.004)
      plotter.setSpec(1700, bf, 1.363)
      plotter.setSpec(1800, bf, 0.9409)
      plotter.setSpec(1900, bf, 0.6583)
      plotter.setSpec(2000, bf, 0.4656)
      plotter.setSpec(2200, bf, 0.2401)
      plotter.setSpec(2400, bf, 0.1281)
      plotter.setSpec(2600, bf, 0.07022)
      plotter.setSpec(2800, bf, 0.03933)
      plotter.setSpec(3500, bf, 0.005847)
      plotter.setSpec(4000, bf, 0.001615)
      plotter.setSpec(4500, bf, 0.0004663)
      plotter.setSpec(5000, bf, 0.0001405)
    elif 'hvt' == interpretation.lower():
      plotter.setVarName('m_{Z\'}')
      plotter.setSpec(1000, bf, 93.51) # in fb
      plotter.setSpec(1100, bf, 61.32)
      plotter.setSpec(1200, bf, 41.37)
      plotter.setSpec(1300, bf, 28.65)
      plotter.setSpec(1400, bf, 20.24)
      plotter.setSpec(1500, bf, 14.46)
      plotter.setSpec(1600, bf, 10.6)
      plotter.setSpec(1700, bf, 7.851)
      plotter.setSpec(1800, bf, 5.808)
      plotter.setSpec(1900, bf, 4.395)
      plotter.setSpec(2000, bf, 3.345)
      plotter.setSpec(2200, bf, 1.968)
      plotter.setSpec(2400, bf, 1.196)
      plotter.setSpec(2600, bf, 0.7407)
      plotter.setSpec(2800, bf, 0.4671)
      plotter.setSpec(3000, bf, 0.296)
      plotter.setSpec(3500, bf, 0.09876)
      plotter.setSpec(4000, bf, 0.0349)
      plotter.setSpec(4500, bf, 0.01239)
      plotter.setSpec(5000, bf, 0.004408)
    else:
      raise RuntimeError('WW interpretation %s not implemented', interpretation)

  elif selection == 'ZZ':
    bf = 0.699*0.699
    if 'gravi' == interpretation.lower():
      plotter.setVarName('m_{G_{RS}}')
      plotter.setSpec(1000, bf, 18.33) # in fb
      plotter.setSpec(1100, bf, 10.53)
      plotter.setSpec(1200, bf, 6.327)
      plotter.setSpec(1300, bf, 3.9268)
      plotter.setSpec(1400, bf, 2.509)
      plotter.setSpec(1500, bf, 1.641)
      plotter.setSpec(1600, bf, 1.095)
      plotter.setSpec(1700, bf, 0.7433)
      plotter.setSpec(1800, bf, 0.5131)
      plotter.setSpec(1900, bf, 0.3585)
      plotter.setSpec(2000, bf, 0.2535)
      plotter.setSpec(2200, bf, 0.1307)
      plotter.setSpec(2400, bf, 0.0697)
      plotter.setSpec(2600, bf, 0.03809)
      plotter.setSpec(2800, bf, 0.02133)
      plotter.setSpec(3000, bf, 0.01216)
      plotter.setSpec(3500, bf, 0.003164)
      plotter.setSpec(4000, bf, 0.0008763)
      plotter.setSpec(4500, bf, 0.0002533)
      plotter.setSpec(5000, bf, 7.645e-05)
    else:
      raise RuntimeError('ZZ interpretation %s not implemented', interpretation)
  else:
    raise RuntimeError('Cross sections for selection %s and analysis %s not implemented' % (selection, analysis))

  plotter.setOutputFormat(RF.Plotter.C)
  plotter.process(vvjj.getStatResults())
  
  if producePull : 
    pullplotter = PullPlotter('vvjjpulltest', '.')
  
    for i in pullMasses : pullplotter.addOneMassPoint(i) 
  
    pullplotter.setOutputFormat(RF.Plotter.pdf)
    pullplotter.process(vvjj.getStatResults())
