from itertools import product
from sys import argv
# prevent ROOT from mangling arguments...
myargv = argv[:]

import ROOT
ROOT.gROOT.SetBatch(True)


inputfile = myargv[1]
outpath = myargv[2].rstrip('/') + '/'
if len(myargv) > 3:
    rebinF = int(myargv[3])
else:
    rebinF = 1

print "rebinning by a factor of", rebinF

# list of included signals

sigMasses = range(500, 2001, 100) + range(2200, 3001, 200) + range(3500, 4501, 500)
sigs = ["%s%d" % params \
        for params in product(["HVTWHlvqq"], sigMasses)]
#        for params in product(["HVTWHlvqq", "HVTZHllqq"], sigMasses)]

bkgs = ["TTbar", "STop", "Wb", "Wc", "Wl", "Zb", "Zc", "Zl", "Diboson","qqWlvH125"]

procs = sigs + bkgs + ["data"]

print "Number of processes:", len(procs)
# choose one process for batch processing
if len(myargv) > 4:
    procs = [procs[int(myargv[4])]]

print "Running processes:", procs

nbtags = ["0", "1", "2"]
naddbtags = ["0", "1p"]
mHregs = ["SR", "lowMH", "highMH"]
mVHregs = ["unblind"]
dists = ["mVH"]

folders = ["%s_lvJ_%stag_%saddtag_%s_%s_%s" % params \
        for params in product(procs, nbtags, naddbtags, mVHregs, mHregs, dists)]

# set of systematics
allSysts = set()
# one dummy hist for each distribution
dummyHists = {}

print "Opening input file..."
f = ROOT.TFile.Open(inputfile)

# loop over folders and assemble list of systematics
print "Reading list of systematics..."
for histKey in f.GetListOfKeys():    
    dummyHist = histKey.ReadObj()
    sname = dummyHist.GetName()
    tokens = sname.split("___")
    if len(tokens)>1:
        allSysts.add(tokens[1])
    # find dummy histogram for each distribution
    for dist in dists:
        if dist in dummyHists:
            continue
        if tokens[0].endswith(dist):
            dummyHist.SetDirectory(0)
            dummyHists[dist] = dummyHist

allSysts = sorted(allSysts)
for sys in allSysts:
    print "  ", sys

print "Dummy histograms:"
for dname, dhist in dummyHists.iteritems():
    print "  ", dhist.GetTitle()

if len(dummyHists) != len(dists):
    print "Error: found", len(dummyHists), "for", len(dists), " distributions. Exiting!"
    exit(-2)

# loop over folders again, write systematic hists
for folder in folders:

    dummyHist = 0
    for dist in dists:
        if folder.endswith(dist):
            dummyHist = dummyHists[dist]
            dummyHist.SetDirectory(0)

    filename = outpath + folder + ".root"
    outfile = ROOT.TFile.Open(filename, "RECREATE")
    print "making file for ", folder


    for s in allSysts:
        if "data" in folder and not s == "Nominal":
            continue
            
        sname = folder
        sname += "___"
        sname += s
        h = f.Get(sname)
        
        if not h:
            h = dummyHist.Clone()
            h.Reset()

        h.Rebin(rebinF)
        h.SetNameTitle(s,s);
        h.SetDirectory(outfile)
        outfile.WriteTObject(h)

    outfile.Close()
