# cpollard@cern.ch
# for formatting channel and region names

# use formatProcessName() and formatRegionName()

import re
import ROOT

# name : color, fill style, line color, line width, line style
styles = { "$t\\bar{t}$": ("t#bar{t}",      ROOT.kOrange,   1001, ROOT.kBlack, 1, 1),
           "single top":  ("single top",    ROOT.kOrange-1, 1001, ROOT.kBlack, 1, 1),
           "diboson":     ("diboson",       ROOT.kGray+1,   1001, ROOT.kBlack, 1, 1),
           "$W+b$":       ("W+b",           ROOT.kGreen+3,  1001, ROOT.kBlack, 1, 1),
           "$W+c$":       ("W+c",           ROOT.kGreen+1,  1001, ROOT.kBlack, 1, 1),
           "$W+l$":       ("W+l",           ROOT.kGreen-9,  1001, ROOT.kBlack, 1, 1),
           "$Z+b$":       ("Z+b",           ROOT.kAzure-2,  1001, ROOT.kBlack, 1, 1),
           "$Z+c$":       ("Z+c",           ROOT.kAzure-4,  1001, ROOT.kBlack, 1, 1),
           "$Z+l$":       ("Z+l",           ROOT.kAzure-9,  1001, ROOT.kBlack, 1, 1),
           "SM VH":       ("SM VH",         ROOT.kViolet-9, 1001, ROOT.kBlack, 1, 1),
           "HVT":         ("2 TeV HVT",     ROOT.kRed,      0,    ROOT.kRed+1, 4, 2),
           "data":        ("data",          ROOT.kBlack,    0,    ROOT.kBlack, 2, 1),
           # "bkgsum":      ("sumbkg",        ROOT.kBlue,     0,    ROOT.kBlue,  4, 2),
           "bkgsum":      ("uncertainty",   ROOT.kBlack,    0, ROOT.kBlack,  0, 0)
         }

bkgOrder = dict(map(lambda (x, y): (y, x),
        enumerate( [ "t#bar{t}"
                   , "single top"
                   , "diboson"
                   , "W+b"
                   , "W+c"
                   , "W+l"
                   , "Z+b"
                   , "Z+c"
                   , "Z+l"
                   , "SM VH"
                   ]
                 )))

def cmpBkg(h1, h2):
    return cmp(bkgOrder[h1.GetTitle()], bkgOrder[h2.GetTitle()])


def setStyle(h):
    s = formatProcessName(h.GetName())
    (t, c, fs, lc, lw, ls) = styles[s]
    h.SetTitle(t)
    h.SetFillColor(c)
    h.SetFillStyle(fs)
    h.SetLineColor(lc)
    h.SetLineWidth(lw)
    h.SetLineStyle(ls)
    h.SetMarkerStyle(0)

    return h

lBkgProcessConvList = map(
        lambda (s, v): (re.compile(s), v),
        [
            ("TTbar", "$t\\bar{t}$"),
            ("STop", "single top"),
            ("Top", "$t\\bar{t}$"),
            ("Diboson", "diboson"),
            ("Wjets", "$W+$jets"),
            ("_VH", "SM VH"),
            ("SMZh", "SM VH"),
            ("H125", "SM VH"),
            ("W\\+Jets", "$W+$Jets"),
            ("Wb", "$W+b$"),
            ("Wc", "$W+c$"),
            ("Wl", "$W+l$"),
            ("Zjets", "$Z+$jets"),
            ("Z\\+Jets", "$Z+$Jets"),
            ("Zb", "$Z+b$"),
            ("Zc", "$Z+c$"),
            ("Zl", "$Z+l$"),
            ("HVT", "HVT"),
            ("sigsum", "HVT"),
            ("bkgsum", "bkgsum"),
            ("data", "data")
        ]
        )

lSigProcessConvList = map(
        lambda x:
                (re.compile("HVT.*%s" % x), "%.1f TeV HVT" % (x/1000.0)),
                # this needs to be reversed to avoid matching a 500
                # GeV HVT to a 2500 GeV HVT.
                reversed(range(500, 5000, 100))
        )


# should be read from config somewhere?
lConvProcessName = lBkgProcessConvList + lSigProcessConvList


lMHRegions = [
        ("SR", "$75~{\\rm GeV} < m_{H} < 145~{\\rm GeV}$"),
        ("lowMH", "$m_{H} < 75~{\\rm GeV}$"),
        ("highMH", "$m_{H} > 145~{\\rm GeV}$"),
        ("mergedMH", "$ m_{H} < 75~{\\rm GeV} || m_{H} > 145~{\\rm GeV}$")
        ]

lTagRegions = [
        # this is to remove the add. tag label in the 2 lepton channel
        ("0tag", "0 $b$-tags"),
        ("1tag", "1 $b$-tag"),
        ("2tag", "2 $b$-tags")
        ]

lAddTagRegions = [ ("0addtag", "")
                 , ("1paddtag", "t#bar{t} CR")
                 , ("1addtag", "t#bar{t} CR")
                 , ("emu", "t#bar{t} CR")
                 ]

lLepChannels = [ ("llJ", "$\\ell\\ell J$")
               , ("lvJ", "$\\ell\\nu J$")
               , ("vvJ", "$\\nu\\nu J$")
               ]


lMHRegionsROOT = [
            ("SR", "75 GeV < m_{jet} < 145 GeV"),
            ("lowMH", "m_{jet} < 75 GeV"),
            ("highMH", "m_{jet} > 145 GeV"),
            ("mergedMH", "m_{jet} < 75 GeV || m_{jet} > 145 GeV"),
            ]

lTagRegionsROOT = [ ("0tag", "0 b-tags"),
            ("1tag", "1 b-tag"),
            ("2tag", "2 b-tags"),
            ]

lAddTagRegionsROOT = [ ("0addtag", "")
                     , ("1paddtag", "t#bar{t} CR")
                     , ("1addtag", "t#bar{t} CR")
                     , ("emu", "t#bar{t} CR")
                     ]

lLepChannelsROOT = [ ("llJ", "llJ")
                   , ("lvJ", "l#nuJ")
                   , ("vvJ", "#nu#nuJ")
                   ]


def combineRE(xs):
    return re.compile(".*".join([""] + xs + [""]))


# UGLY
def makeRegionConvList(channels, mhregs, tagregs, addtagregs):
    l = []
    for chan, chans in channels:
        for mhr, mhrs in mhregs:
            for tr, trs in tagregs:
                for atr, atrs in addtagregs:
                    regex = combineRE([chan, tr, atr, mhr])
                    lab = ", ".join(filter(lambda s: s != "",
                                [mhrs, chans, trs, atrs]))
                    l.append((regex, lab))
                    continue
                continue
            continue
        continue

    return l


lConvRegionName = \
        makeRegionConvList(lLepChannels, lMHRegions, lTagRegions,
            lAddTagRegions)

lConvRegionNameROOT = \
        makeRegionConvList(lLepChannelsROOT, lMHRegionsROOT,
                lTagRegionsROOT, lAddTagRegionsROOT)


# converts a nasty name string into a nice one via regex matching
def formatNameWithList(s, l):
    for (k, v) in l:
        if re.search(k, s):
            return v

    return s


def formatProcessName(s):
    return formatNameWithList(s, lConvProcessName)

def formatRegionName(s):
    return formatNameWithList(s, lConvRegionName)

def formatRegionNameROOT(s):
    return formatNameWithList(s, lConvRegionNameROOT)
