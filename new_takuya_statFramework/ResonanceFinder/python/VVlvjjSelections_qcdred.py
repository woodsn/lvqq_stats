class VVlvjjSelection:
    def __init__(self):
        self.Selection = ""
SRWWHP = VVlvjjSelection()
SRWWHP.Selection = "passSRWWHP && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
SRWWLP = VVlvjjSelection()
SRWWLP.Selection = "passSRWWLP && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
SRWWResolved = VVlvjjSelection()
SRWWResolved.Selection = "!passSRWWHP && !passSRWWLP && passSRWWResolved"
SRWWResolvedVBF = VVlvjjSelection()
SRWWResolvedVBF.Selection = "!passSRWWHP && !passSRWWLP && passSRWWResolvedVBF"
SidebandHP = VVlvjjSelection()
SidebandHP.Selection = "!passSRWWResolved && passWRHP && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
BtagHP = VVlvjjSelection()
BtagHP.Selection = "!passSRWWResolved && passTRHP && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
SidebandLP = VVlvjjSelection()
SidebandLP.Selection = "!passSRWWResolved && passWRLP && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
BtagLP = VVlvjjSelection()
BtagLP.Selection = "!passSRWWResolved && passTRLP && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
SidebandResolved = VVlvjjSelection()
SidebandResolved.Selection = "!passSRWWHP && !passSRWWLP && !passWRHP && !passTRHP && !passWRLP && !passTRLP && passWRResolved"
BtagResolved = VVlvjjSelection()
BtagResolved.Selection = "!passSRWWHP && !passSRWWLP && !passWRHP && !passTRHP && !passWRLP && !passTRLP && passTRResolved"
SidebandResolvedVBF = VVlvjjSelection()
SidebandResolvedVBF.Selection = "!passSRWWHP && !passSRWWLP && !passWRHP && !passTRHP && !passWRLP && !passTRLP && passWRResolvedVBF"
BtagResolvedVBF = VVlvjjSelection()
BtagResolvedVBF.Selection = "!passSRWWHP && !passSRWWLP && !passWRHP && !passTRHP && !passWRLP && !passTRLP && passTRResolvedVBF"

SRWZHP = VVlvjjSelection()
SRWZHP.Selection = "passSRWZHP && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
SRCombHP = VVlvjjSelection()
SRCombHP.Selection = "(passSRWWHP || passSRWZHP) && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
SRWZLP = VVlvjjSelection()
SRWZLP.Selection = "passSRWZLP && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"
SRCombLP = VVlvjjSelection()
SRCombLP.Selection = "(passSRWWLP || passSRWZLP) && ( (leptonFlavor>0 && (met - leptonPt)/(met + leptonPt) > -0.6) || leptonFlavor<0 )"



#SRWW = VVlvjjSelection()
#SRWW.Selection = "passWMass && Nbjets==0"
#SRWZ = VVlvjjSelection()
#SRWZ.Selection = "passZass && Nbjets==0"
#Sideband = VVlvjjSelection()
#Sideband.Selection = "(!passWMass && !passZMass) && Nbjets==0"
#Btag = VVlvjjSelection()
#Btag.Selection = "(passWMass || passZMass) && Nbjets>0"
