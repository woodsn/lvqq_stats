class VVlvjjSelection:
    def __init__(self):
        self.Selection = ""
# SR WW
SRWWHP = VVlvjjSelection()
SRWWHP.Selection = "passSRWWHP"
SRWWLP = VVlvjjSelection()
SRWWLP.Selection = "passSRWWLP80"  #"passSRWWLP"
SRWWHPVBF = VVlvjjSelection()
SRWWHPVBF.Selection = "passSRWWHPVBF"
SRWWLPVBF = VVlvjjSelection()
SRWWLPVBF.Selection = "passSRWWLP80VBF"
# SR WZ
SRWZHP = VVlvjjSelection()
SRWZHP.Selection = "passSRWZHP"
SRWZLP = VVlvjjSelection()
SRWZLP.Selection = "passSRWZLP80" #"passSRWZLP"
SRWZHPVBF = VVlvjjSelection()
SRWZHPVBF.Selection = "passSRWZHPVBF"
SRWZLPVBF = VVlvjjSelection()
SRWZLPVBF.Selection = "passSRWZLP80VBF"
# SR WW/WZ
SRCombHP = VVlvjjSelection()
SRCombHP.Selection = "(passSRWWHP || passSRWZHP)"
SRCombLP = VVlvjjSelection()
SRCombLP.Selection = "(passSRWWLP80 || passSRWZLP80)" # "(passSRWWLP || passSRWZLP)"
SRCombHPVBF = VVlvjjSelection()
SRCombHPVBF.Selection = "(passSRWWHPVBF || passSRWZHPVBF)"
SRCombLPVBF = VVlvjjSelection()
SRCombLPVBF.Selection = "(passSRWWLP80VBF || passSRWZLP80VBF)"
# CR HP
SidebandHP = VVlvjjSelection()
SidebandHP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRHP80"
BtagHP = VVlvjjSelection()
BtagHP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRHP"
#$(O)"(BCR HP VBF
SidebandHPVBF = VVlvjjSelection()
SidebandHPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRHP80VBF"
BtagHPVBF = VVlvjjSelection()
BtagHPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRHPVBF"
# CR LP
SidebandLP = VVlvjjSelection()
SidebandLP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRLP80"
BtagLP = VVlvjjSelection()
BtagLP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRLP80"
#$(O)"(BCR LP VBF
SidebandLPVBF = VVlvjjSelection()
SidebandLPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRLP80VBF"
BtagLPVBF = VVlvjjSelection()
BtagLPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRLP80VBF"


## Resolved Selection
# SR WW
#SRWWResolved = VVlvjjSelection()
#SRWWResolved.Selection = "pass_ggFResolved_WW_SR && !"+SRCombHP+" && !"+SRCombLP+" && !"+SRCombHPVBF+" && !"+SRCombLPVBF
#SRWWResolvedVBF = VVlvjjSelection()
#SRWWResolvedVBF.Selection = "pass_VBFResolved_WW_SR && !"+SRCombHP+" && !"+SRCombLP+" && !"+SRCombHPVBF+" && !"+SRCombLPVBF
##$(O)"(BSR WZ
#SRWZResolved = VVlvjjSelection()
#SRWZResolved.Selection = "pass_ggFResolved_WZ_SR && !"+SRCombHP+" && !"+SRCombLP+" && !"+SRCombHPVBF+" && !"+SRCombLPVBF
#SRWZResolvedVBF = VVlvjjSelection()
#SRWZResolvedVBF.Selection = "pass_VBFResolved_WZ_SR && !"+SRCombHP+" && !"+SRCombLP+" && !"+SRCombHPVBF+" && !"+SRCombLPVBF
## CR
#SidebandResolved = VVlvjjSelection()
#SidebandResolved.Selection = "pass_ggFResolved_WCR && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF !"+SRCombHP+" && !"+SRCombLP+" && !"+SRCombHPVBF+" && !"+SRCombLPVBF
#BtagResolved = VVlvjjSelection()
#BtagResolved.Selection = "pass_ggFResolved_TopCR && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF && !"+SRCombHP+" && !"+SRCombLP+" && !"+SRCombHPVBF+" && !"+SRCombLPVBF
##$(O)"(BVBF Cr
#SidebandResolvedVBF = VVlvjjSelection()
#SidebandResolvedVBF.Selection = "pass_VBFResolved_WCR && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF && !"+SRCombHP+" && !"+SRCombLP+" && !"+SRCombHPVBF+" && !"+SRCombLPVBF
#BtagResolvedVBF = VVlvjjSelection()
#BtagResolvedVBF.Selection = "pass_VBFResolved_TopCR && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF && !"+SRCombHP+" && !"+SRCombLP+" && !"+SRCombHPVBF+" && !"+SRCombLPVBF
#


## TA selection
# SRWW
SRWWHPTA = VVlvjjSelection()
SRWWHPTA.Selection = 'passSRWWHP_TA'
SRWWLPTA = VVlvjjSelection()
SRWWLPTA.Selection = 'passSRWWLP_TA'
#$(O)"(BSRWZ
SRWZHPTA = VVlvjjSelection()
SRWZHPTA.Selection = 'passSRWZHP_TA'
SRWZLPTA = VVlvjjSelection()
SRWZLPTA.Selection = 'passSRWZLP_TA'
#$(O)"(BCR HP
SidebandHPTA = VVlvjjSelection()
SidebandHPTA.Selection = "!passSRWWResolved_TA && passWRHP_TA"
BtagHPTA = VVlvjjSelection()
BtagHPTA.Selection = "!passSRWWResolved_TA && passTRHP_TA"
#$(O)"(BCR LP
SidebandLPTA = VVlvjjSelection()
SidebandLPTA.Selection = "!passSRWWResolved_TA && passWRLP_TA"
BtagLPTA = VVlvjjSelection()
BtagLPTA.Selection = "!passSRWWResolved_TA && passTRLP_TA"

