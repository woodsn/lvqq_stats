# New list
Variations = [
'EG_RESOLUTION_ALL',
'EG_SCALE_ALL',
'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',
'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',
'FATJET_Comb_Baseline_Kin',
'FATJET_Comb_Modelling_Kin',
'FATJET_Comb_TotalStat_Kin',
'JET_Comb_Tracking_Kin',
'JET_21NP_JET_BJES_Response',
'JET_21NP_JET_EffectiveNP_1',
'JET_21NP_JET_EffectiveNP_2',
'JET_21NP_JET_EffectiveNP_3',
'JET_21NP_JET_EffectiveNP_4',
'JET_21NP_JET_EffectiveNP_5',
'JET_21NP_JET_EffectiveNP_6',
'JET_21NP_JET_EffectiveNP_7',
'JET_21NP_JET_EffectiveNP_8restTerm',
'JET_21NP_JET_EtaIntercalibration_Modelling',
'JET_21NP_JET_EtaIntercalibration_NonClosure',
'JET_21NP_JET_EtaIntercalibration_TotalStat',
'JET_21NP_JET_Flavor_Composition',
'JET_21NP_JET_Flavor_Response',
'JET_21NP_JET_Pileup_OffsetMu',
'JET_21NP_JET_Pileup_OffsetNPV',
'JET_21NP_JET_Pileup_PtTerm',
'JET_21NP_JET_Pileup_RhoTopology',
'JET_21NP_JET_PunchThrough_MC15',
'JET_21NP_JET_SingleParticle_HighPt',
'JET_JvtEfficiency',
'JET_SR1_JET_EtaIntercalibration_NonClosure',
'JET_SR1_JET_GroupedNP_1',
'JET_SR1_JET_GroupedNP_2',
'JET_SR1_JET_GroupedNP_3',
'MET_SoftTrk_Scale',
'MUON_EFF_STAT',
'MUON_EFF_STAT_LOWPT',
'MUON_EFF_SYS',
'MUON_EFF_SYS_LOWPT',
'MUONS_ID',
'MUON_ISO_STAT',
'MUON_ISO_SYS',
'MUONS_MS',
'MUONS_SCALE',
'MUON_TTVA_STAT',
'MUON_TTVA_SYS',
'JET_JER_SINGLE_NP',
'MET_SoftTrk_ResoPara',
'MET_SoftTrk_ResoPerp',
'FATJET_D2R',
'FATJET_JER',
'FATJET_JMR',
#'FT_EFF_Eigen_B_0', 'FT_EFF_Eigen_B_1', 'FT_EFF_Eigen_B_2', 'FT_EFF_Eigen_C_0', 'FT_EFF_Eigen_C_1', 'FT_EFF_Eigen_C_2', 'FT_EFF_Eigen_C_3', 'FT_EFF_Eigen_Light_0', 'FT_EFF_Eigen_Light_1', 'FT_EFF_Eigen_Light_2', 'FT_EFF_Eigen_Light_3', 'FT_EFF_Eigen_Light_4', 'FT_EFF_extrapolation', 'FT_EFF_extrapolation_from_charm' 
]

OneSided = [
    'JET_JER_SINGLE_NP',
    'FATJET_JER',
    'FATJET_JMR',
    'FATJET_D2R',
    'MET_SoftTrk_ResoPara',
    'MET_SoftTrk_ResoPerp',
    'TopModeling_MCatNLO',
    'TopModeling_Herwig',
    'TopModeling_Rad',
    'WjetsModeling_PDF',
    'WjetsModeling_MadGraph',
#'MUON_EFF_STAT_LOWPT', ?
#'MUON_EFF_SYS_LOWPT', ?
]

TopSyst = ['TopModeling_Rad', 'TopModeling_MCatNLO', 'TopModeling_Herwig']

ToSmooth = [
'JET_SR1_JET_GroupedNP_1','JET_SR1_JET_GroupedNP_2','JET_SR1_JET_GroupedNP_3',
#'FATJET_Medium_JET_Rtrk_Baseline_Kin','FATJET_Medium_JET_Rtrk_Baseline_D2',
#'FATJET_Medium_JET_Rtrk_Modelling_Kin','FATJET_Medium_JET_Rtrk_Modelling_D2',
#'FATJET_Medium_JET_Rtrk_Tracking_Kin','FATJET_Medium_JET_Rtrk_Tracking_D2',
'JET_JER_SINGLE_NP',
'FATJET_JER',
'FATJET_JMR',
'FATJET_D2R',
'MET_SoftTrk_Scale',
'MET_SoftTrk_ResoPara',
'MET_SoftTrk_ResoPerp'
]


# Reduced set of systematics #

ReducedVariations = ['JET_JER_SINGLE_NP', 'JET_WZ_Run1_D2', 'MUONS_MS', 'FT_EFF_Eigen_C_0', 'FATJET_JMR', 'FT_EFF_Eigen_Light_3', 'FT_EFF_Eigen_Light_2', 'FT_EFF_Eigen_Light_1', 'FT_EFF_Eigen_Light_0', 'JET_Rtrk_Tracking', 'JET_Rtrk_Baseline', 'FT_EFF_Eigen_B_1', 'FT_EFF_Eigen_B_0', 'FATJET_JER', 'JET_WZ_CrossCalib_D2', 'FATJET_D2R', 'JET_GroupedNP_2', 'JET_GroupedNP_3', 'JET_GroupedNP_1', 'JET_Rtrk_Modelling', 'FT_EFF_extrapolation'] 

ReducedToSmooth = ['JET_JER_SINGLE_NP', 'JET_WZ_Run1_D2', 'MUONS_MS', 'FATJET_JMR', 'JET_Rtrk_Tracking', 'JET_Rtrk_Baseline',  'FATJET_JER', 'JET_WZ_CrossCalib_D2', 'FATJET_D2R', 'JET_GroupedNP_2', 'JET_GroupedNP_3', 'JET_GroupedNP_1', 'JET_Rtrk_Modelling']

ReducedOneSided = ["JET_JER_SINGLE_NP","TopRad_new","TopHerwig_new","TopMCaNLOtt_new","VJetsMlvJShapeModeling","FATJET_JMR","FATJET_JER","FATJET_D2R"]


