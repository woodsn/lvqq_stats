import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(ROOT.gSystem.ExpandPathName('.x $ROOTCOREDIR/scripts/load_packages.C'))
from array import *

from ROOT import RF as RF
from ROOT import PullPlotter as PullPlotter


def createDirectoryStructure(input_dir, releaseDir, analysis, inputListTag, outputWSTag):
  import os
  import shutil
  import glob

  # create directories
  newdir = '{releaseDir}/{analysis}/data/{inputListTag}'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)
  diagnosticsdir = '{releaseDir}/{analysis}/ws/diagnostics'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)
  tmpdir = '{releaseDir}/{analysis}/tmp'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)

  # first, we remove the previously existing directories
  dirs = [newdir, diagnosticsdir, tmpdir]
  for dir in dirs:
    if os.path.exists(dir):
      shutil.rmtree(dir)

  # we use makedirs, which takes care of creating intermediate directories
  os.makedirs(newdir)
  os.makedirs(diagnosticsdir)
  os.makedirs(tmpdir)
  
  # copy input trees from input location
  files = glob.glob(os.path.join(input_dir, '*.*'))
  ### commented out
  for file in files:
    shutil.copy2(file, newdir)
   

if __name__ == '__main__':
 
  import sys
  configModel =  sys.argv[1] 
  masspoint   =  sys.argv[2]
  purity      =  'HP' # default HP unless specified
  if len(sys.argv) > 3:
    purity    =  sys.argv[3]


  ####################################################################################
  ###
  ### Configure the run here
  ###
  #################################################################################### 

  doConstrainBtagCR     = True
  doConstrainSidebandCR = True
  producePull           = False 
  doProfiling           = True
  doSyst                = True
  doInjection           = False
  doToys                = False
  doOptimisedScan       = True
  setUseStatError       = True
  doMakeWSOnly          = False   # FIXME change to make limits as well 
  doSmoothing           = True
  doTA                  = False  # FIXME change manually for now...
  doCopy                = False   # FIXME set to False after first run

  ####################################################################################
  ###
  ### Choose here the sample and signal region
  ###
  ####################################################################################

  import SignalSamples
  if configModel == "VBF_HVTWZ" : Model=SignalSamples.VBF_HVTWZ  #HVTWW HVTWZ RSGWW
  if configModel == "VBF_HVTWW" : Model=SignalSamples.VBF_HVTWW  #HVTWW HVTWZ RSGWW
  if configModel == "RSGWW" : Model=SignalSamples.RSGWW  #HVTWW HVTWZ RSGWW
  if configModel == "HVTWW" : Model=SignalSamples.HVTWW   #HVTWW HVTWZ RSGWW
  if configModel == "HVTWZ" : Model=SignalSamples.HVTWZ  #HVTWW HVTWZ RSGWW
  if configModel == "Comb" : Model=SignalSamples.Comb #
  if configModel == "ggHWWNWA" : Model=SignalSamples.ggHWWNWA
  print configModel
  print Model 
  print Model.Name
  
  # define which signal region/sample to use (WW or WZ)
  SignalRegion=Model.SR   #"WZ" #"WW"
  SignalSample=Model.Name      #"HVT"+SignalRegion


  releaseDir = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run'
  analysis = 'VVlvqq_mc15_v2016'
  inputListTag = 'itest01'
  outputWSTag = 'ws_'+SignalSample+'_'+purity+'_{mass}'.format(mass=masspoint)
  # sname = SignalSample+'{mass}'.format(mass=mass)
  TreeLumi = 1.
  DataLumi = 1.


 
  # print config 
  print "----------------------------------"
  print "----------------------------------"
  print "--- Running on sample     " , SignalSample
  print "--- with signal region    " , SignalRegion
  print "--- purity                " , purity
  print "----------------------------------"
  print "--- doConstrainBtagCR     " , doConstrainBtagCR   
  print "--- doConstrainSidebandCR " , doConstrainSidebandCR
  print "--- producePull           " , producePull
  print "--- doProfiling           " , doProfiling
  print "--- doSyst                " , doSyst
  print "--- doInjection           " , doInjection
  print "--- doToys                " , doToys
  print "--- doOptimisedScan       " , doOptimisedScan
  print "--- setUseStatError       " , setUseStatError
  print "--- doMakeWSOnly          " , doMakeWSOnly 
  print "--- doSmoothing           " , doSmoothing
  print "--- doTA                  " , doTA
  print "--- doCopy                " , doCopy
  print "----------------------------------"
  print "--- releaseDir            " , releaseDir
  print "--- analysis              " , analysis
  print "--- inputListTag          " , inputListTag
  print "--- outputWSTag           " , outputWSTag
  print "----------------------------------"
  print "----------------------------------"

  

  # import files and create directory structure
  # example inputs for HVTWW2000 are included in the input folder
  if( doCopy ):
    print "Creating directory structure"
    createDirectoryStructure('/afs/cern.ch/work/r/rcarbone/public/forStats/inputs/', releaseDir, analysis, inputListTag, outputWSTag)

  # chose do e only, mu only, combined or sum e+mu
  # e only
  #flav = ["EL"]
  # mu only
  #flav = ["MU"]
  # el and mu combined
  #flav = ["EL","MU"]
  # sum e+mu 
  flav = [""]

  # define running options
  #masses = Model.XS   # [1300]       #change this!
  masses = []
  masses.append(int(masspoint)) 

  poi_setups = { # for optimised mu scan
    5000 : [30, 0, 0.03],
    4500 : [30, 0, 0.03],
    4000 : [30, 0, 0.03], 
    3500 : [30, 0, 0.03],
    3000 : [30, 0, 0.03],
    2800 : [30, 0, 0.03],
    2600 : [30, 0, 0.03],
    2400 : [30, 0, 0.03],
    2200 : [30, 0, 0.03],
    2000 : [30, 0, 0.03],
    1900 : [30, 0, 0.1],
    1800 : [30, 0, 0.1],
    1700 : [30, 0, 0.1],
    1600 : [30, 0, 0.1],
    1500 : [30, 0, 0.3],
    1400 : [30, 0, 0.3],
    1300 : [30, 0, 0.3],
    1200 : [30, 0, 0.3],
    1100 : [30, 0, 0.3],
    1000 : [30, 0, 0.3],
    900  : [30, 0, 0.5],
    800  : [30, 0, 0.5],
    750  : [30, 0, 0.5],
    700  : [30, 0, 1],
    600  : [30, 0, 1],
    500  : [30, 0, 1],
    400  : [50, 0, 10],
    300  : [50, 0, 10],
    200  : [50, 0, 10]
    }
  
  
  Lumi = 1.
  LumiUncert = 0.05

     
  #### Define selection cuts:
  import VVlvjjSelections
  #### Signal and control region cuts for high purity
  if SignalRegion.count("WW"):
      if doTA:
        CutSR=VVlvjjSelections.SRWWHPTA.Selection
      else:
        CutSR=VVlvjjSelections.SRWWHP.Selection
  if SignalRegion.count("WZ"):
      if doTA:
        CutSR=VVlvjjSelections.SRWZHPTA.Selection
      else:
        CutSR=VVlvjjSelections.SRWZHP.Selection
  if SignalRegion.count("Comb"):
      CutSR=VVlvjjSelections.SRCombHP.Selection
  if doTA:
    CutSideband=VVlvjjSelections.SidebandHPTA.Selection
    CutBtag=VVlvjjSelections.BtagHPTA.Selection
  else:
    CutSideband=VVlvjjSelections.SidebandHP.Selection
    CutBtag=VVlvjjSelections.BtagHP.Selection

  #### Signal and control region cuts for low purity
  if SignalRegion.count("WW"):
      if doTA:
        CutSR_LP=VVlvjjSelections.SRWWLPTA.Selection
      else:
        CutSR_LP=VVlvjjSelections.SRWWLP.Selection
  if SignalRegion.count("WZ"):
      if doTA:
        CutSR_LP=VVlvjjSelections.SRWZLPTA.Selection
      else:
        CutSR_LP=VVlvjjSelections.SRWZLP.Selection
  if SignalRegion.count("Comb"):
      CutSR_LP=VVlvjjSelections.SRCombLP.Selection
  if doTA:
    CutSideband_LP=VVlvjjSelections.SidebandLPTA.Selection
    CutBtag_LP=VVlvjjSelections.BtagLPTA.Selection
  else:
    CutSideband_LP=VVlvjjSelections.SidebandLP.Selection
    CutBtag_LP=VVlvjjSelections.BtagLP.Selection

#  if SignalRegion.count("WW"):
#      CutSR_Resolved=VVlvjjSelections.SRWWResolved.Selection
#      CutSideband_Resolved=VVlvjjSelections.SidebandResolved.Selection
#      CutBtag_Resolved=VVlvjjSelections.BtagResolved.Selection


  #### Config background samples for High Purity category
  samples_setup_hp = { # configure here the fit
    # sample   : [ minmu , maxmu ,name           , constrianType                   , setNormByLumi, setUseStatError ]
    "ttbar"    : [ 0     , 2     ,"XS_Top"       , RF.MultiplicativeFactor.FREE    , False        , setUseStatError],
    "Wjets"    : [ 0     , 2     ,"XS_Wjets"     , RF.MultiplicativeFactor.FREE    , False        , setUseStatError],
    "SingleTop": [ 0.89  , 1.11  , "XS_SingleTop", RF.MultiplicativeFactor.GAUSSIAN, False        , False],
    "Diboson"  : [ 0.89  , 1.11  ,"XS_Dibosons"  , RF.MultiplicativeFactor.GAUSSIAN, False        , False],
    "Zjets"    : [ 0.89  , 1.11  ,"XS_Zjets"     , RF.MultiplicativeFactor.GAUSSIAN, False        , False]
    }

  #### Config background samples for Low Purity category
  samples_setup_lp = { # configure here the fit
  # sample     : [ minmu , maxmu ,name          , constrianType                   , setNormByLumi, setUseStatError ]
    "ttbar"    : [ 0     , 2     ,"XS_Top_LP"   , RF.MultiplicativeFactor.FREE    , False        , setUseStatError],
    "Wjets"    : [ 0     , 2     ,"XS_Wjets_LP" , RF.MultiplicativeFactor.FREE    , False        , setUseStatError],
    "SingleTop": [ 0.89  , 1.11  ,"XS_SingleTop", RF.MultiplicativeFactor.GAUSSIAN, False        , False],
    "Diboson"  : [ 0.89  , 1.11  ,"XS_Dibosons" , RF.MultiplicativeFactor.GAUSSIAN, False        , False],
    "Zjets"    : [ 0.89  , 1.11  ,"XS_Zjets"    , RF.MultiplicativeFactor.GAUSSIAN, False        , False]
  }

  #### Config background samples for Resolved category
  samples_setup_reso = { # configure here the fit
  # sample     : [ minmu , maxmu, name            , constrianType                   , setNormByLumi, setUseStatError ]
    "ttbar"    : [ 0     , 2    , "XS_Top_Reso"   , RF.MultiplicativeFactor.FREE    , False        , setUseStatError],
    "Wjets"    : [ 0     , 2    , "XS_Wjets_Reso" , RF.MultiplicativeFactor.FREE    , False        , setUseStatError],
    "SingleTop": [ 0.89  , 1.11 , "XS_SingleTop"  , RF.MultiplicativeFactor.GAUSSIAN, False        , False],
    "Diboson"  : [ 0.89  , 1.11 , "XS_Dibosons"   , RF.MultiplicativeFactor.GAUSSIAN, False        , False],
    "Zjets"    : [ 0.89  , 1.11 , "XS_Zjets"      , RF.MultiplicativeFactor.GAUSSIAN, False        , False]
  }

  #### DEFINE THE REGIONS ####
  #### Create a dictionary to map:   region,  selection,  sample_setup
  regions_lp ={
    "SR"+SignalRegion+"_LP" : [ CutSR_LP       , samples_setup_lp ],
    "Sideband_LP"           : [ CutSideband_LP , samples_setup_lp ],
    "Btag_LP"               : [ CutBtag_LP     , samples_setup_lp ]
  }
  regions_hp = {
    "SR"+SignalRegion       : [ CutSR          , samples_setup_hp ],
    "Sideband"              : [ CutSideband    , samples_setup_hp ], 
    "Btag"                  : [ CutBtag        , samples_setup_hp ]
  }
  #regions_res = {
  #  "SR"+SignalRegion+"_Resolved" : [ CutSR_Resolved       , samples_setup_reso ],
  #  "Sideband_Resolved"           : [ CutSideband_Resolved , samples_setup_reso ],
  #  "Btag_Resolved"               : [ CutBtag_Resolved     , samples_setup_reso ]
  #}

  regions = {}
  if purity == 'LP':
    regions = regions_lp
  elif purity == 'HP':
    regions = regions_hp
  #elif purity == 'RES':
  #  regions == regions_res



  lvqq = RF.VVlvqqAnalysisRunner(analysis)
  lvqq.doPull(producePull)
  lvqq.doApplySmoothing(doSmoothing)

  # set here systematics
  # these are systematics that apply to all samples and to all regions
  listSyst =[]
  listtopSyst  =[]
  if doSyst:
    import SystematicVariations
    listSyst = SystematicVariations.Variations
    # these are the 1 sided systematics
    one_sided_var = SystematicVariations.OneSided
    for one in one_sided_var:
       lvqq.addOneSideVariation(one)

    listtopSyst = SystematicVariations.TopSyst

  topSyst=set(listtopSyst)
  syst = set(listSyst)

  #### first set no smooth for all variations 
  if doSyst:
     for s in syst:
       lvqq.setSystSmoothFlag(s,0)

     for s in topSyst:
       lvqq.setSystSmoothFlag(s,0)

     ### then set smooth only for scale and resolution uncertainties
     for s in SystematicVariations.ToSmooth:
       lvqq.setSystSmoothFlag(s,1)	


  print syst
  print topSyst
  # define input tree format
  #fixme: running the TA mass instead of calo (lvJmass_TA)
  #lvqq.setTreeObs('lvJmass_TA*0.001') 
  lvqq.setTreeObs('lvJmass*0.001') # for boosted; tentatively resolved observable is hard-coded in TreeHistoCollector.cxx
  lvqq.setTreeWeight('weightBoosted') # for boosted; resovled weight is also hard-coded for now
  lvqq.setLumiRescale(TreeLumi, DataLumi)

  # define histogram binning
  bins = [500,575,660,755,860,975,1100,1235,1380,1535,1700,1875,2060,2255,2460,2675,2900,3135,3380,3800,5000] 
  #bins=[500,575,660,755,860,975,1100,1235,1380,1535,15000]
  lvqq.setNbins(21-1)
  binArray = array('d',bins)
  lvqq.setBins( binArray  ) 

  binsReso = [300,360,420,500,575,660,755,860,975,1100,1500,2000,5000]
  binArrayReso = array('d', binsReso)

  # apply directory structure
  lvqq.setReleaseDir(releaseDir)
  lvqq.setInputListTag(inputListTag)
  lvqq.setOutputWSTag(outputWSTag)


  scale=1.#5000.
  
  # loop over regions
  for region in regions: 
      samples_setup = regions[region][1]
      selection     = regions[region][0]
      # define the channels
      #if region.count("Resolved"):
      lvqq.addChannel(region, 20, binArray, selection)
      #else:
      #  lvqq.addChannel(region, selection)
      lvqq.channel(region).setStatErrorThreshold(0.05) # it was 0.05 means that errors < 5% will be ignored

      # loop over samples
      # signal samples
      for mass in masses:
          sname = SignalSample+'{mass}'.format(mass=mass)
          lvqq.addSignal(sname, mass)
          lvqq.channel(region).addSample(sname)
          lvqq.channel(region).sample(sname).multiplyBy('mu', scale, 0, 200)
#          lvqq.channel(region).sample(sname).setNormByLumi(False)
          lvqq.channel(region).sample(sname).multiplyBy('Signal_ISR_FSR', 1 , 0.94, 1.06,RF.MultiplicativeFactor.GAUSSIAN)
          lvqq.channel(region).sample(sname).multiplyBy('LumiNP',   Lumi, Lumi*(1-LumiUncert), Lumi*(1.+LumiUncert),RF.MultiplicativeFactor.GAUSSIAN)
          # add Syst
	  if doSyst:
             for var in syst:
               # if not profiling to not add syst in CR
                if not doProfiling and region is not "SR": continue
                #print region, " ", sname, " " , var
                lvqq.channel(region).sample(sname).addVariation(var)
      # background samples
      for sample  in samples_setup:
        muname = samples_setup[sample][2]
        muname = muname.replace("+","")
        lvqq.channel(region).addSample(sample)
        lvqq.channel(region).sample(sample).multiplyBy(muname, scale, samples_setup[sample][0],samples_setup[sample][1],samples_setup[sample][3])
        lvqq.channel(region).sample(sample).setUseStatError(samples_setup[sample][5])
        lvqq.channel(region).sample(sample).multiplyBy('LumiNP', Lumi, Lumi*(1-LumiUncert), Lumi*(1.+LumiUncert),RF.MultiplicativeFactor.GAUSSIAN)

        # add Syst
        if doSyst:
           for var in syst:
               # if not profiling to not add syst in CR
               if not doProfiling and region is not "SR": continue
               #print region, " ", sample, " " , var
               lvqq.channel(region).sample(sample).addVariation(var)
      # adding systematics relevant to Top only
      if doSyst:
         for ttvar in topSyst:
             print ttvar
             lvqq.channel(region).sample("ttbar").addVariation(ttvar) 

  #if doSyst:
     #lvqq.channel("SR"+SignalRegion).sample("W+Jets").addVariation("VJetsMlvJShapeModeling")
     #lvqq.channel("Sideband").sample("W+Jets").addVariation("VJetsMlvJShapeModeling")


  #lvqq.channel("SR"+SignalRegion).sample("W+Jets").addVariation("NoWeight")
  lvqq.Print()
  # define POI
  lvqq.setPOI('mu')

  #configure mu scan
  if not doOptimisedScan:
     lvqq.limitRunner().setPOIScan(20, 0, 20) # nsteps, min, max
  else:
     for mass in masses:
        # deal with this signal in an optimised way
        lvqq.limitRunner().setPOIScan(mass, poi_setups[mass][0], poi_setups[mass][1], poi_setups[mass][2])
  
  # optional: inject signal
  injmu = 2
  if doInjection:
    lvqq.setInjectionSample('HVT2000')
    lvqq.setInjectionStrength(injmu)  #W' is mu=1.44 HVT mu =2
  # optional: use toys instead of asymptotics
  if doToys:
    lvqq.limitTool().setCalcType(RF.Frequentist)
    lvqq.limitTool().setTestStatType(RF.PL1sided)
    lvqq.limitTool().setNToys(1000)
    lvqq.limitTool().hypoTestInvTool().SetParameter('UseProof', True)
    lvqq.limitTool().hypoTestInvTool().SetParameter('GenerateBinned', True) # speeds up

  if doMakeWSOnly:
    lvqq.produceWS()
    exit()

  # run
  lvqq.run()
  exit()

  # plot
  ROOT.gROOT.ProcessLine('.L AtlasStyle.C+')
  ROOT.SetAtlasStyle()

  # output name
  name ='lvqq_Unconstrained'
  if( doConstrainBtagCR and doConstrainSidebandCR  ) : name = 'lvqq_Constrained'
  if( doInjection ) : name+="_Injectingmu"
  name+="_"+SignalSample

  plotter = RF.Plotter(name, '.')
  plotter.setVarName('m_{X}')
  plotter.setVarUnit('GeV')

  k=1000 # 1000
  Br=Model.Br

  # adding mass points to the plotter
  for m in masses:
    plotter.setSpec(m, Br, Model.XS[m]*k)
 
  plotter.setOutputFormat(RF.Plotter.root)
  plotter.process(lvqq.getStatResults())



  pullMasses =  [2000]

  if producePull :
    pullplotter = PullPlotter('vvjjpulltest', '.')
    for i in pullMasses : pullplotter.addOneMassPoint(i)
    pullplotter.setOutputFormat(RF.Plotter.pdf)
    pullplotter.process(lvqq.getStatResults())


