class VVlvjjSelection:
    def __init__(self):
        self.Selection = ""
SRWWHP = VVlvjjSelection()
SRWWHP.Selection = "passSRWWHP"
SRWWLP = VVlvjjSelection()
SRWWLP.Selection = "passSRWWLP80"
SRWZHP = VVlvjjSelection()
SRWZHP.Selection = "passSRWZHP"
SRWZLP = VVlvjjSelection()
SRWZLP.Selection = "passSRWZLP80"
SRCombHP = VVlvjjSelection()
SRCombHP.Selection = "(passSRWWHP || passSRWZHP)"
SRCombLP = VVlvjjSelection()
SRCombLP.Selection = "(passSRWWLP80 || passSRWZLP80)"

SRWWHPVBF = VVlvjjSelection()
SRWWHPVBF.Selection = "passSRWWHPVBF"
SRWWLPVBF = VVlvjjSelection()
SRWWLPVBF.Selection = "passSRWWLP80VBF"
SRWZHPVBF = VVlvjjSelection()
SRWZHPVBF.Selection = "passSRWZHPVBF"
SRWZLPVBF = VVlvjjSelection()
SRWZLPVBF.Selection = "passSRWZLP80VBF"
SRCombHPVBF = VVlvjjSelection()
SRCombHPVBF.Selection = "(passSRWWHPVBF || passSRWZHPVBF)"
SRCombLPVBF = VVlvjjSelection()
SRCombLPVBF.Selection = "(passSRWWLP80VBF || passSRWZLP80VBF)"

SRWWResolved = VVlvjjSelection()
SRWWResolved.Selection = "pass_ggFResolved_WW_SR &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
SRWWResolvedVBF = VVlvjjSelection()
SRWWResolvedVBF.Selection = "pass_VBFResolved_WW_SR &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
SRWZResolved = VVlvjjSelection()
SRWZResolved.Selection = "pass_ggFResolved_WZ_SR && WleppT/lvjjmass_constZ>0.35 && WhadpT/lvjjmass_constZ>0.35 && !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
SRWZResolvedVBF = VVlvjjSelection()
SRWZResolvedVBF.Selection = "pass_VBFResolved_WZ_SR && WleppT/lvjjmass_constZ>0.3 && WhadpT/lvjjmass_constZ>0.3 && !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)

SidebandHP = VVlvjjSelection()
SidebandHP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRHP80"
BtagHP = VVlvjjSelection()
BtagHP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRHP"
SidebandLP = VVlvjjSelection()
SidebandLP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRLP80"
BtagLP = VVlvjjSelection()
BtagLP.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRLP80"

SidebandHPVBF = VVlvjjSelection()
SidebandHPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRHP80VBF"
BtagHPVBF = VVlvjjSelection()
BtagHPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRHPVBF"
SidebandLPVBF = VVlvjjSelection()
SidebandLPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passWRLP80VBF"
BtagLPVBF = VVlvjjSelection()
BtagLPVBF.Selection = "!pass_ggFResolved_WW_SR && !pass_VBFResolved_WW_SR && !pass_ggFResolved_WZ_SR && !pass_VBFResolved_WZ_SR && passTRLP80VBF"

SidebandResolved = VVlvjjSelection()
SidebandResolved.Selection = "pass_ggFResolved_WCR && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
BtagResolved = VVlvjjSelection()
BtagResolved.Selection = "pass_ggFResolved_TopCR && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
SidebandResolvedVBF = VVlvjjSelection()
SidebandResolvedVBF.Selection = "pass_VBFResolved_WCR && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
BtagResolvedVBF = VVlvjjSelection()
BtagResolvedVBF.Selection = "pass_VBFResolved_TopCR && !passSRWWHP && !passSRWWLP80 && !passWRHP80 && !passTRHP && !passWRLP80 && !passTRLP80 && !passWRHP80VBF && !passTRHPVBF && !passWRLP80VBF && !passTRLP80VBF &&  !"+str(SRCombHP.Selection)+" && !"+str(SRCombLP.Selection)+" && !"+str(SRCombHPVBF.Selection)+" && !"+str(SRCombLPVBF.Selection)
