#include "ResonanceFinder/TreeHistoCollector.h"

#include <TSystem.h>
#include <TTreeFormula.h>
#include <TTree.h>
#include <TFile.h>
#include <TIterator.h>
#include <TKey.h>
#include <stdexcept>
#include <iostream>
using namespace std;

RF::TreeHistoCollector::TreeHistoCollector() : m_obs(""), m_weight(""), m_nbins(0), m_obs_min(-1), m_obs_max(-1), m_bins(nullptr), m_use_bins(kFALSE)
{
}

RF::TreeHistoCollector::TreeHistoCollector(TString name) : m_obs(""), m_weight(""), m_nbins(0), m_obs_min(-1), m_obs_max(-1), m_bins(nullptr), m_use_bins(kFALSE)
{
   setName(name);
}

RF::TreeHistoCollector::~TreeHistoCollector()
{
}

TString RF::TreeHistoCollector::observable() const
{
   return m_obs;
}

void RF::TreeHistoCollector::setObservable(TString var)
{
   m_obs = var;
}

TString RF::TreeHistoCollector::weight() const
{
   return m_weight;
}

void RF::TreeHistoCollector::setWeight(TString var)
{
   m_weight = var;
}

Int_t RF::TreeHistoCollector::nbins() const
{
   return m_nbins;
}

void RF::TreeHistoCollector::setNbins(Int_t nbins)
{
   m_nbins = nbins;
}

Double_t RF::TreeHistoCollector::obsMin() const
{
   return m_obs_min;
}

void RF::TreeHistoCollector::setObsMin(Double_t min)
{
   m_obs_min = min;
}

Double_t RF::TreeHistoCollector::obsMax() const
{
   return m_obs_max;
}

void RF::TreeHistoCollector::setObsMax(Double_t max)
{
   m_obs_max = max;
}

void RF::TreeHistoCollector::setBins(Double_t* bins)
{
   m_bins = bins;
   m_use_bins =  kTRUE;
}

void RF::TreeHistoCollector::addChannel(TString channel, TString formula)
{
   m_selection[channel] = formula;
   addData(channel);
}

void RF::TreeHistoCollector::addChannel(TString channel, Int_t nbins, Double_t* bins, TString formula )
{
  BinningInfo info;
  info.N = nbins;
  info.Bins= bins;
  m_bininfo[channel] = info;
  addChannel(channel,formula);
}

void RF::TreeHistoCollector::addChannel(TString channel, Int_t nbins, Double_t min, Double_t max, TString formula)
{

  BinningInfo info;
  info.N = nbins;
  info.Min = min;
  info.Max = max;
  info.Bins=nullptr;
  m_bininfo[channel] = info;

  addChannel(channel,formula);



}



std::vector<TString> RF::TreeHistoCollector::getChannels()
{
   std::vector<TString> result;

   for (auto & kv : m_selection) {
      result.push_back(kv.first);
   }

   return result;
}

void RF::TreeHistoCollector::addSample(TString channel, TString sample)
{
   addSample(channel, sample, kFALSE);
}

void RF::TreeHistoCollector::addData(TString channel)
{
   addSample(channel, getHistoCollection().tagData(), kTRUE);
}

void RF::TreeHistoCollector::addSample(TString channel, TString sample, Bool_t isData)
{
   // here is the relevant part
  
   const TString filename = source() + "/" + sample + "_" + channel + ".root";
   const TString filenamesub = source() + "/" + sample + ".root";
   
   TFile* file;
   if ( gSystem->IsFileInIncludePath(filename) ){
     file=new TFile(filename);
   } else if ( gSystem->IsFileInIncludePath(filenamesub) ){
     file=new TFile(filenamesub);
   } else {
     cout<<"DegubP3  "<<filename<<" "<<filenamesub<<endl;
     throw std::runtime_error("File not found");
   }
   TIter nextKey(file->GetListOfKeys());
   TKey* key = nullptr;

   while ((key = dynamic_cast<TKey*>(nextKey()))) {
      const TString variation = key->GetName();
      
      //if( variation != "nominal" )
      //continue;

      std::cout << "building histogram for " << sample << " " << variation << " in " << channel <<  std::endl;

      if (TString(key->GetClassName()) == "TTree") {
         TTree *tree = dynamic_cast<TTree*>(key->ReadObj());
         RF::Histo_t *h = processTree(tree, channel);

         // protection against empty input
	 /*fixme
         if (h->Integral() == 0) {
	   const TString msg = "Histogram integral for channel " + channel + " sample " + sample + " in file " + filenamesub + " is empty, please check input tree " + variation;
            throw std::runtime_error(msg.Data());
	    }*/
	 
         if (isData == kFALSE) {
            // non-data histogram -> save always
            getHistoCollection().addHistogram(h, channel, sample, variation);
         } else {
            // data histogram: save only if nominal
            if (variation == getHistoCollection().tagNominal()) getHistoCollection().addData(h, channel);
            else throw std::logic_error("Data file contains input other than nominal");
         }

         delete h; // we took ownership
      }
   } // loop over variations

   delete file;
}

RF::Histo_t *RF::TreeHistoCollector::processTree(TTree *tree, TString channel)
{
   // this function returns also histogram ownership
   // TODO: use shared_ptr?
   // five decimal digits for min, max
   const TString hname = "tmpXjjDD"; // TODO: make unique?
   RF::Histo_t *h = nullptr; // this is the filled histogram

   Int_t l_nbins = nbins();
   Double_t l_obsMin = obsMin(); 
   Double_t l_obsMax = obsMax();


   Bool_t useVariableBins = (m_use_bins);
   double *bins = nullptr;
   if(useVariableBins) bins = m_bins;

   if( m_bininfo.count(channel) > 0   ){ 

     if( m_bininfo[channel].Bins!=nullptr   ) {  

       bins = m_bininfo[channel].Bins;
       l_nbins = m_bininfo[channel].N;  
       useVariableBins = true;

     }else{

       l_nbins  =  m_bininfo[channel].N;
       l_obsMin =  m_bininfo[channel].Min;
       l_obsMax =  m_bininfo[channel].Max;
       useVariableBins = false;

     }  

   }
   
   if (!useVariableBins) {
     h = new RF::Histo_t(hname, hname, l_nbins, l_obsMin, l_obsMax);
   } else { 
     h = new RF::Histo_t(hname, hname, l_nbins, bins ) ;
   }


/*
   const Bool_t useVariableBins = (m_use_bins);
   if (!useVariableBins) {
      h = new RF::Histo_t(hname, hname, nbins(), obsMin(), obsMax());
   } else {
      h = new RF::Histo_t(hname, hname, nbins() , m_bins) ;
      // we could also use sizeof(m_bins)/sizeof(Double_t), but this is cleaner
   }
*/

   const TString sel = m_selection[channel];
   TString how = weight(); // try only weight
   TString obs = observable();
   if( channel.Contains("Resolved") && !channel.Contains("SR") ){
     obs="lvjjmass_constW*0.001";
   }
   if (how == "") how = sel; // if no weight specified, try only selection
   else if (sel != "") how = "(" + how + ")*(" + sel + ")"; // if weight specified and also selection, take product
   tree->Project(hname, obs, how);
   std::cout << "Observation " << obs << ", cut " << how << std::endl;
   
   if (!useVariableBins) {

      h->SetDirectory(0);
      return h;

   } else {
      // remap

      const Int_t nb = h->GetNbinsX();

      TString newtitle = (TString)h->GetTitle() + "_remap";
      RF::Histo_t* hh = new RF::Histo_t(newtitle, newtitle, nb, 0., 1.); // using [0,1] as range (instead of m_obs_min,max) is safer to make sure people realise this is a remap

      for (int b = 0; b <= nb + 1; b++) { // do both under and overflow

         hh->SetBinContent(b, h->GetBinContent(b));
         hh->SetBinError(b, h->GetBinError(b));

	 if( h->GetBinContent(b) < 0 ) hh->SetBinContent(b, 0.);
      }
      SafeDelete(h);

      hh->SetDirectory(0);
      return hh;
   }

}
