#include "ResonanceFinder/VHAnalysisRunner.h"
#include "ResonanceFinder/SelectiveHistoCollector.h"
#include "ResonanceFinder/HistoLumiRescaler.h"
#include "ResonanceFinder/DownVariationAdder.h"
#include "ResonanceFinder/SystematicsSmoother.h"

RF::VHAnalysisRunner::VHAnalysisRunner(TString analysis) : RF::IBinnedAnalysisRunner(analysis, new RF::SelectiveHistoCollector())
{  
  m_dataScale = 1;
  m_oldLumi = 1;
  m_newLumi = 1;

  m_useUnderflow = 0;
  m_useOverflow = 1;

  m_doSmooth = false;
}

RF::VHAnalysisRunner::~VHAnalysisRunner()
{
}

void RF::VHAnalysisRunner::configInputGetter()
{
  RF::SelectiveHistoCollector *shc = dynamic_cast<RF::SelectiveHistoCollector*>(m_histoCollector);

   // manually add data (TreeHistoCollector automatically does that, but SelectiveHistoCollector
   // does not, as it is a low-level class allowing disrespect of rigid directory structure)

  for (auto &kv : m_selections) {
      // selection string is ignored
    shc->addSample(kv.first, shc->getHistoCollection().tagData());
  }
}

void RF::VHAnalysisRunner::setRebinFactor(TString chan, int factor) { m_rebinFactors[chan] = factor; }

void RF::VHAnalysisRunner::setMergeCommand(TString chan, vector<TString> mer_chan, int flag, TString merged_chan_name) {

  m_mergeChans[chan]=mer_chan;
  m_mergeFlag[chan]=flag;
  m_mergedChanName[chan]=merged_chan_name;
}

void RF::VHAnalysisRunner::doApplySmoothing(bool doSmooth) { m_doSmooth = doSmooth; }

void RF::VHAnalysisRunner::setSystSmoothFlag(TString syst, int flag){
  m_systSmoothFlags[syst] =  flag;
}

void RF::VHAnalysisRunner::setLumiRescale(Double_t oldLumi, Double_t newLumi) {
  m_oldLumi = oldLumi;
  m_newLumi = newLumi;
}

void RF::VHAnalysisRunner::setIncludeOverflow(bool use){ m_useOverflow = use; }
void RF::VHAnalysisRunner::setIncludeUnderflow(bool use){ m_useUnderflow = use; }

void RF::VHAnalysisRunner::setHistoUpperBound(TString chan, double value){
  m_upperBound[chan] = value;
}

void RF::VHAnalysisRunner::setHistoLowerBound(TString chan, double value){
  m_lowerBound[chan] = value;
}

void RF::VHAnalysisRunner::addOneSideVariation(TString variation, TString tagUp, TString tagDn)
{

  if (variation != "") {
    bool alreadyAdded =  std::find(m_onesidevar.begin(), m_onesidevar.end(), variation) != m_onesidevar.end();
    if (!alreadyAdded) {
      m_onesidevar.push_back(variation);
      m_onesidetagUp.push_back(tagUp);
      m_onesidetagDn.push_back(tagDn);
    }

  }
  return ;
}

void RF::VHAnalysisRunner::manipulate()
{

  RF::HistoCollection &hc = histoCollection();

  int idx = 0;
  for (auto kv : m_onesidevar) {
    // add down variations
    if (kv != "") {
      RF::DownVariationAdder dva;
      dva.setVariation(kv + m_onesidetagUp[idx], kv + m_onesidetagDn[idx]);
      dva.manipulate(hc);
    }
    idx++;
  }

  //perform merging of control regions if requested
  mergeCR();

  //perform automatic rebinning if requested
  rebinHistos();

  // first, rescale to luminosity
  if (m_oldLumi != m_newLumi) {
    RF::HistoLumiRescaler hlr;
    hlr.setLumi(m_oldLumi, m_newLumi);
    hlr.manipulate(hc);
  }

 
  for (auto &kv : hc.histos()){

    //Address negative bins
    double sumNeg = 0;
    double sumTot = kv.second->Integral();
    for(int bb=1; bb<=kv.second->GetNbinsX(); bb++){
      if(kv.second->GetBinContent(bb)<0){
	sumNeg += kv.second->GetBinContent(bb);
	kv.second->SetBinContent(bb,0);
      }
    }
    
    if(sumTot>0 && sumNeg!=0 && kv.second->Integral()>0) kv.second->Scale(sumTot/kv.second->Integral());
    else if(sumTot<0) kv.second->Scale(0);
    
    // apply data scale if it's data
    if(kv.first.Contains(hc.tagData())) kv.second->Scale(m_dataScale);

    for (auto &ks : m_sigScales){
      
      if(kv.first.Contains(ks.first)) kv.second->Scale(ks.second);      
      else if((ks.first == TString("All")) || (ks.first == TString("all"))){
	for(auto sig : signals()){
	  if(kv.first.Contains(sig)){ //this is a signal, oK to scale
	    kv.second->Scale(ks.second);	
	  }
	}
      }
    }  
    
    // apply any bkgd scales
    for (auto &kb : m_bkgdScales){
      for(auto &kc : channels()){
	TString searchStr = kc;
	searchStr = searchStr + "_" + kb.first + "_";
	if(kv.first.Contains(searchStr)){
	  kv.second->Scale(kb.second);	
	}
      }
    }
  }

  //After all other manipulations, apply systematics smoothing if requested
  if(m_doSmooth){                      					  
    RF::SystematicsSmoother SysS;					  
    SysS.setNumberOfLocalExtrema(1); //1: allows parabolic shape for mVH  
    SysS.doApplyAveraging(false); //false: no averaging of adjacent bins  
    SysS.setSmoothFlags(m_systSmoothFlags); //provide list of smoothing flags
    SysS.manipulate(hc);						  
  }                                                                    
  
}

void RF::VHAnalysisRunner::setTagData(TString tag)
{
  histoCollection().setTagData(tag);
}

void RF::VHAnalysisRunner::setTagNominal(TString tag)
{
  histoCollection().setTagNominal(tag);
}

void RF::VHAnalysisRunner::setTagUp(TString tag)
{
  histoCollection().setTagUp(tag);
}

void RF::VHAnalysisRunner::setTagDown(TString tag)
{
  histoCollection().setTagDown(tag);
}

void RF::VHAnalysisRunner::scaleSignal(double sf, TString name){
  if(sf == 1) return;
  if(name.IsNull()) return;

  for( auto sig : signals()){
    if(sig==name){
      if(m_sigScales.find(sig) == m_sigScales.end()){
	m_sigScales[sig] = sf;
      }
      else cout << "Error: already have a scale for this signal: " << name << endl;
    }
  }

  if((name==TString("All")) || (name==TString("all"))){
    if(m_sigScales.find(name) == m_sigScales.end()){
      m_sigScales[name] = sf;   
    }   
  }

}

void RF::VHAnalysisRunner::scaleBkgd(double sf, TString name){
  if(sf == 1) return;
  if(name.IsNull()) return;

  if(m_bkgdScales.find(name) == m_bkgdScales.end()) m_bkgdScales[name] = sf;
  else cout << "Error: already have a scale for this background: " << name << endl;

}

void RF::VHAnalysisRunner::scaleData(double sf){
  m_dataScale = sf;
}

void RF::VHAnalysisRunner::rebinHistos(){
  RF::HistoCollection &hc = histoCollection();
  RF::HistoMap_t &hm = hc.histos();

  //Perform uniform rebinnings, as requested
  for( auto &hist : hm){
    for( auto &rb : m_rebinFactors){
      if(rb.second>1 && hist.first.Contains(rb.first)) hist.second->Rebin(rb.second);
    }
    
    //merge overflow if requested
    if(m_useOverflow){
      int lbin = hist.second->GetNbinsX();
      hist.second->AddBinContent(lbin,hist.second->GetBinContent(lbin+1));
      hist.second->SetBinError(lbin, sqrt(pow(hist.second->GetBinError(lbin),2) + pow(hist.second->GetBinError(lbin+1),2)));
      hist.second->SetBinContent(lbin+1,0);
    }
    
    //merge underflow if requested
    if(m_useUnderflow){
      int lbin = 1;
      hist.second->AddBinContent(lbin,hist.second->GetBinContent(lbin-1));
      hist.second->SetBinError(lbin, sqrt(pow(hist.second->GetBinError(lbin),2) + pow(hist.second->GetBinError(lbin-1),2)));
      hist.second->SetBinContent(lbin-1,0);
    }    
  }  

  //Apply upperbound requests
  for( auto &uBound : m_upperBound){
    for( auto samp : hc.samples(uBound.first)){
      vector<TString> variations = hc.variations(uBound.first, samp);
      if(samp == hc.tagData()) variations.push_back("data");
      for( auto var : variations){
	Histo_t* hist = 0;

	if(var == "data") hist = hc.getData(uBound.first); 
	else hist = hc.getHistogram(uBound.first, samp, var);	         

	int ubin = hist->FindBin(uBound.second)-1;
	double sum = 0;
	double sumE = 0;
	for(int bb=ubin+1; bb<=hist->GetNbinsX()+1; bb++){
	  sum += hist->GetBinContent(bb);
	  sumE += pow(hist->GetBinError(bb),2);
	  hist->SetBinContent(bb,0);
	  hist->SetBinError(bb,0);
	}

	if(m_useOverflow){
	  hist->AddBinContent(ubin,sum);
	  hist->SetBinError(ubin,sqrt(sumE + pow(hist->GetBinError(ubin),2)));      
	}
	else{
	  hist->SetBinContent(hist->GetNbinsX()+1,sum);
	  hist->SetBinError(hist->GetNbinsX()+1,sqrt(sumE));
	}
      }
    }
  }

  //Apply lowerbound requests
  for( auto &lbound : m_lowerBound){
    for( auto samp : hc.samples(lbound.first)){
      vector<TString> variations = hc.variations(lbound.first, samp);
      if(samp == hc.tagData()) variations.push_back("data");
      for( auto var : variations){
	Histo_t* hist = 0;

	if(var == "data") hist = hc.getData(lbound.first); 
	else hist = hc.getHistogram(lbound.first, samp, var);	         
	
	int lbin = hist->FindBin(lbound.second)+1;
	double sum = 0;
	double sumE = 0;
	for(int bb=1; bb<=lbin-1; bb++){
	  sum += hist->GetBinContent(bb);
	  sumE += pow(hist->GetBinError(bb),2);
	  hist->SetBinContent(bb,0);
	  hist->SetBinError(bb,0);
	}
	
	if(m_useUnderflow){
	  hist->AddBinContent(lbin,sum);
	  hist->SetBinError(lbin,sqrt(sumE + pow(hist->GetBinError(lbin),2)));      
	}
	else{
	  hist->SetBinContent(0,sum);
	  hist->SetBinError(0,sqrt(sumE));
	}

      }
    }
  }
  
  //Perform variable-size rebinnings
  if(m_rebinFactors.size()>0){
    
    vector<Histo_t*> templates;
    for( auto &rb : m_rebinFactors){
      if(rb.second<1) templates.push_back(autoRebin(rb.second, rb.first, hc, hm));
    }
    
    //create storage container for template histograms
    TString nickname = analysis() + "_" + outputWSTag();  
    TString outHistFileName = getDiagnosticsDir() + "/varBins_" + nickname + ".root";
    TFile* outHistFile = new TFile(outHistFileName,"RECREATE");
    for(auto hh : templates) if(hh){ hh->Write();}
    outHistFile->Write();
    outHistFile->Close();
  }
  
}
  

double RF::VHAnalysisRunner::HVTreso(double mVH, int region, double minBin){
  // calculate 3x the HVT W' resolution
  double reso = 40. + (155.-40.)/(2800.-500.)*(mVH-500.);
  reso *= 3.0;
  
  //default to 250 GeV for SR, 500 GeV for CR
  if(minBin==-1){
    reso = 249.5;
    
    if(region==2 || region==4){ // bkgd region
      reso = 499.5;
    }
  }
  
  //user added the min bin width desired
  if(minBin>9) reso = minBin-0.5;
  
  return reso;
}

RF::Histo_t* RF::VHAnalysisRunner::autoRebin(int flag, TString chan, RF::HistoCollection& hc, RF::HistoMap_t& hm){
  
  ///Sum up the total backgrounds, careful to not include signals or data
  Histo_t* totBkgd=0;
  
  int region = 1;
  if(chan.Contains("SR") && chan.Contains("0addtag")) region=1;
  else region =2;

  for( auto &samp : hc.samples(chan)){
    Histo_t* hist = hc.getHistogram(chan, samp, hc.tagNominal());
    if(samp == hc.tagData()) continue;

    if(!hist){ cout << "VHAnalysisRunner::AutoRebin, Error accessing known histogram: " << samp << endl; continue;}

    bool keep=true;

    for(auto sig : signals()){
      if(samp.Contains(sig)){ //this is a signal, so don't include it
	keep = false;
      }
    }
    
    if(samp.Contains(hc.tagData())) keep = false;

    if(!keep) continue;

    if(totBkgd==0) totBkgd = dynamic_cast<RF::Histo_t*>(hist->Clone(TString(chan).Append("cloneBkgd")));
    else totBkgd->Add(hist);
		    
  }
  if(!totBkgd) return NULL;
  
  //use the total background rate to find bin edges
  vector<double> binEdges;
  double uBound = totBkgd->GetXaxis()->GetXmax();
  if(m_upperBound.find(chan) == m_upperBound.end()){
    binEdges.push_back(totBkgd->GetXaxis()->GetXmax());
  }  
  else{
    binEdges.push_back(m_upperBound[chan]);
    uBound = m_upperBound[chan];
  }

  int iRegion = region;
  double iReso = 0; //we only get here if the flag is negative
  if(flag<-2){
    iRegion += 2;
    iReso = -1;  //auto set 250/500 GeV bins in SR/CR
  }
  if(flag<-9) iReso = fabs(flag*1.0); //set bin width to what was passed in

  // for all flags, collapse bins with zero background
  // flag=-1: Base binning on resolution and statistics
  // flag=-2: base binning on resolution only
  // flag=-3: base binning on prescribed minimum, min bin size = 250/500 for SR/CR
  // flag<-9: base binning on user-specified minimum

  double sumWidth = 0;
  double sumInt = 0;
  double sumStat = 0;
  int maxBin = totBkgd->GetNbinsX();
  if(m_useOverflow) maxBin += 1;
  for(int bb=maxBin; bb>=1; bb--){

    if(totBkgd->GetBinLowEdge(bb)<uBound) sumWidth += totBkgd->GetBinWidth(bb);
    sumInt += totBkgd->GetBinContent(bb);
    sumStat = sqrt(sumStat*sumStat + totBkgd->GetBinError(bb)*totBkgd->GetBinError(bb));
    
    bool createBin = false;
    if(flag==-1){  //resolution and statistics
      createBin = (bb==1 || (sumInt>0 && (sumStat/(sumInt+1e-6))<0.75 && sumWidth>=HVTreso(totBkgd->GetBinCenter(bb), iRegion, iReso)));
    }
    else if(flag<-1){
      createBin = (bb==1 || (sumInt>0 && sumWidth>=HVTreso(totBkgd->GetBinCenter(bb), iRegion, iReso)));
    }
 
    //check to see if we're about to leave a lower bin that's too narrow
    if(bb>1){
      double lowInt = 0;
      double lowWidth = 0;
      for(int ii=1; ii<bb; ii++){
	lowInt += totBkgd->GetBinContent(ii);
	lowWidth += totBkgd->GetBinWidth(ii);
      }
      
      // if the width of what remains is too narrow or the content is too small, just sum it into the last bin being made
      if( ( lowWidth < HVTreso(totBkgd->GetBinCenter(bb-1), iRegion, iReso) ) || ( lowInt<0.1 )){
	createBin = false;
	cout << "Automerging lower bins below: " << totBkgd->GetBinLowEdge(bb) << " (" << bb << ")" << endl;
      }
    }

    if(createBin){
      binEdges.push_back(totBkgd->GetBinLowEdge(bb));
      sumWidth=0;
      sumInt = 0;
      sumStat = 0;
    }
  }

  //invert convenient top->bottom bin edge ordering
  vector<double> fBinEdges;
  for(int i=binEdges.size()-1; i>=0; i--){
    fBinEdges.push_back(binEdges[i]);
    cout << "Variable Bin Edge: " << chan << " " << binEdges[i] << endl;
  }

  //Create template to ease bin assignments
  Histo_t* templ = new Histo_t(chan,chan,fBinEdges.size()-1,&fBinEdges[0]);

  //Now rebin and replace
  for( auto &kh : hm){
    if(!kh.first.Contains(chan)) continue;
    Histo_t* hist = kh.second;
    
    TString hname = hist->GetName();
    TString htitle = hist->GetTitle();
    hist->SetName("tmp");

    //Create histogram with uniform bins
    Histo_t* hNew = new Histo_t(hname,htitle,fBinEdges.size()-1,0,1);
    
    //Fill new histogram
    for(int bb=1; bb<=hist->GetNbinsX(); bb++){
      int iBin = templ->FindBin(hist->GetBinCenter(bb));
      double ee = hist->GetBinError(bb);
      double oo = hNew->GetBinError(iBin);
      hNew->AddBinContent(iBin, hist->GetBinContent(bb));
      hNew->SetBinError(iBin,sqrt(ee*ee+oo*oo));
    }

    hm[kh.first] = hNew;
  }

  return templ;
}

bool RF::VHAnalysisRunner::checkHasNminExpEvts(TString chan, RF::HistoCollection& hc, double Nmin){
  ///Sum up the total backgrounds, careful to not include signals or data
  Histo_t* totBkgd=0;
  
  //only merge between lowMH and highMH of the same tag region, do not consider signal regions
  if( (chan.Contains("SR") && chan.Contains("0addtag")) || !(chan.Contains("lowMH") || chan.Contains("highMH"))) return true;
  
  for( auto &samp : hc.samples(chan)){
    Histo_t* hist = hc.getHistogram(chan, samp, hc.tagNominal());
    if(samp == hc.tagData()) continue;
    
    if(!hist){ cout << "VHAnalysisRunner::MergeCR, Error accessing known histogram: " << samp << endl; continue;}
    
    bool keep=true;
    
    for(auto sig : signals()){
      if(samp.Contains(sig)){ //this is a signal, so don't include it
	keep = false;
      }
    }
    
    if(samp.Contains(hc.tagData())) keep = false;
    
    if(!keep) continue;

    if(totBkgd==0) totBkgd = dynamic_cast<RF::Histo_t*>(hist->Clone(TString(chan).Append("clone")));
    else totBkgd->Add(hist);
    
  }
  if(!totBkgd) return false;
  
  double BkgContentChan=totBkgd->Integral();
  
  std::cout<<"BackgroundContent: "<<BkgContentChan<<std::endl;
  
  if(BkgContentChan<Nmin){
    std::cout<<"Background expectation too low, merge!"<<std::endl;
    return false;
  }
  
  return true;
}

void RF::VHAnalysisRunner::mergeHighLowMHChan(TString chan, RF::HistoCollection &hc,RF::HistoMap_t &hm, std::map<TString,RF::Channel>& chan_map){
  
  //containers to add and erase histogram pointers from map
  std::list<RF::HistoMap_t::iterator> iteratorEraseList;
  
  TString new_channel,new_sample,new_variation;
  
  TString refhisto_channel,refhisto_sample,refhisto_variation;
  
  std::vector<TString> histNames_add, channels_add, samples_add, variations_add;
  std::vector<RF::Histo_t*> histPtrAdd;
  
  //merge histograms of specified channels
  for( auto &kh : hm){
    if(!kh.first.Contains(chan)) continue;

    TString hist_newchan_name=kh.first;
    
    hc.getHistoNameTokens(kh.first,refhisto_channel,refhisto_sample,refhisto_variation);

    if(kh.first.Contains(hc.tagData())){

      iteratorEraseList.push_back(hm.find(kh.first));
      
      for(auto partner : m_mergeChans[refhisto_channel]){
	
	RF::Histo_t* hist=hc.getData(partner);
	if(!hist){
	  cout << "Error:  This histogram doesn't exist!" << endl;
	  continue;
	}
	if(TMath::Abs(hist->GetXaxis()->GetXmax()-hm[kh.first]->GetXaxis()->GetXmax())>1e-5){
	  cout<<"Cannot merge histos: "<<kh.first<<" "<<partner<<" -- different axis range!!!"<<endl;
	}
	
	hm[kh.first]->Add(hist);
	iteratorEraseList.push_back(hm.find(partner+"_data"));
	delete hist;
      }
    }
    else{      

      iteratorEraseList.push_back(hm.find(kh.first));
      
      for(auto partner : m_mergeChans[refhisto_channel]){
	
	RF::Histo_t* hist=hc.getHistogram(partner,refhisto_sample,refhisto_variation);
	if(!hist){
	  cout << "Error:  This histogram doesn't exist!" << endl;
	  continue;
	}

	if(TMath::Abs(hist->GetXaxis()->GetXmax()-hm[kh.first]->GetXaxis()->GetXmax())>1e-5){
	  cout<<"Cannot merge histos: "<<kh.first<<" "<<partner+"_"+refhisto_sample+"_"+refhisto_variation<<" -- different axis range!!!"<<endl;
	}

	hm[kh.first]->Add(hist);
	iteratorEraseList.push_back(hm.find(partner+"_"+refhisto_sample+"_"+refhisto_variation));
	delete hist;

      }
    }


    hc.getHistoNameTokens(hist_newchan_name,new_channel,new_sample,new_variation);

    if(!((m_mergedChanName[refhisto_channel]).EqualTo("unspecified"))){
     
      new_channel=m_mergedChanName[refhisto_channel];
    
      if(kh.first.Contains(hc.tagData())) hist_newchan_name=hc.getDataName(new_channel);
      else hist_newchan_name=hc.getHistoName(new_channel,new_sample,new_variation);

    }else{ //no merged channel name specified, assume MH sideband merging
      //get name of new merged histogram
      if(kh.first.Contains("highMH")){
	hist_newchan_name.ReplaceAll("highMH","mergedMH");
	new_channel.ReplaceAll("highMH","mergedMH");
      }else if(kh.first.Contains("lowMH")){
	hist_newchan_name.ReplaceAll("lowMH","mergedMH");
	new_channel.ReplaceAll("lowMH","mergedMH");
      }else if(kh.first.Contains("SR")){
	hist_newchan_name.ReplaceAll("SR","mergedMH");
	new_channel.ReplaceAll("SR","mergedMH");
      }
    }

    //add tokens to list for adding to map later
    histPtrAdd.push_back(hm[kh.first]);
    histNames_add.push_back(hist_newchan_name);
    channels_add.push_back(new_channel);
    samples_add.push_back(new_sample);
    variations_add.push_back(new_variation);


  }

  //erase all map elements (old histograms) at iterators in list
  for(auto i : iteratorEraseList){
    hm.erase(i);
  }

  //add all extra histograms
  for(unsigned int i=0; i<histPtrAdd.size(); i++){
    if(histNames_add[i].Contains("data")){
      hc.addData(histPtrAdd[i],channels_add[i]);
    }else{
      hc.addHistogram(histPtrAdd[i],channels_add[i],samples_add[i],variations_add[i]);
    }
  }

  std::cout<<"=================================="<<std::endl;
  std::cout<<"|| Merging Channels:"<<std::endl;
  std::cout<<"||   CH:"<<chan<<std::endl;
  for(auto &partner : m_mergeChans[chan]) std::cout<<"|| + CH:"<<partner<<std::endl;
  std::cout<<"|| = CHtot:"<<new_channel<<std::endl;
  std::cout<<"=================================="<<std::endl;
  
  //replace the two channels by the new merged one
  chan_map[new_channel]=Channel((this->m_channels)[chan]);
  chan_map[new_channel].setName(new_channel);

  //for now fix rebinning for the new channel, remove rebinning factors for old channels
  //TODO: choose binning of channel with larger stats?
  m_rebinFactors[new_channel]=m_rebinFactors[chan];
  m_rebinFactors.erase(chan);
  for(auto &partner : m_mergeChans[chan]) m_rebinFactors.erase(partner);

}


void RF::VHAnalysisRunner::mergeCR(){
  
  RF::HistoCollection &hc = histoCollection();
  RF::HistoMap_t &hm = hc.histos();
  
  std::map<TString,RF::Channel> chan_map; //will be filled in function mergeHighLowMHChan()
  
  //loop over channels listed to be merged
  for(auto &mch : m_mergeChans){
    
    TString chan=mch.first;
    
    if(!((m_mergedChanName[chan]).EqualTo("unspecified"))){

      if(!(chan_map.find(m_mergedChanName[chan])==chan_map.end())){
	continue;
      }

    }else{//no merged channel name specified, assume MH sideband merging
       //avoid double counting, if already merged, ignore
      if(!((chan_map.find(chan.ReplaceAll("lowMH","mergedMH"))==chan_map.end())
	 && (chan_map.find(chan.ReplaceAll("highMH","mergedMH"))==chan_map.end())
	 && (chan_map.find(chan.ReplaceAll("SR","mergedMH"))==chan_map.end()))){ 
	continue;                                                                                                                                                                                                                      
      }


    }   

    if(m_mergeFlag[mch.first]==-1){ //force merge
      mergeHighLowMHChan(mch.first,hc,hm,chan_map);
      
    }
    else if((m_mergeFlag[mch.first]>0)){ //stat based merge      
      //check if any of the channels have too little statistics
      bool chan_stat=(checkHasNminExpEvts(mch.first,hc,static_cast<double>(m_mergeFlag[mch.first])));
      for(auto &mer: mch.second){
	chan_stat=chan_stat && (checkHasNminExpEvts(mer,hc,static_cast<double>(m_mergeFlag[mch.first])));
      }
      
      if(!chan_stat)mergeHighLowMHChan(mch.first,hc,hm,chan_map);
    }
    
  }

  //find out which channels have not been merged, add them to the new channel map
  for(auto it1 = (this->m_channels).begin(); it1!=(this->m_channels).end(); it1++){
    TString chan=it1->first;
    
    if(!hasBeenMerged(chan,chan_map)){
      chan_map[it1->first]=it1->second;
    }else hc.removeChannel(chan);
  } 
  
  //replaceold with the new channel map
  this->m_channels=std::move(chan_map);
}



bool RF::VHAnalysisRunner::hasBeenMerged(TString chan,std::map<TString,RF::Channel>& chan_map){
  //check if channel has been merged (more difficult when including SR)
  bool couldBeMerged=false;
  TString base_chan;
  
  for(auto &mch : m_mergeChans){

    base_chan=mch.first;
    
    if(mch.first.EqualTo(chan)){
      couldBeMerged=true;
      break;
    }

    for(auto &merch : mch.second){
      
      if(merch.EqualTo(chan)){
	couldBeMerged=true;
	break;
      }
      
    }
  }


  if(!((m_mergedChanName[base_chan]).EqualTo("unspecified"))){

    if(couldBeMerged && !(chan_map.find(m_mergedChanName[base_chan])==chan_map.end())) return true;
  
  }else{//no merged channel name specified, assume MH sideband merging
    if(couldBeMerged
      && !((chan_map.find(chan.ReplaceAll("lowMH","mergedMH"))==chan_map.end())
      && (chan_map.find(chan.ReplaceAll("highMH","mergedMH"))==chan_map.end())	   
      && (chan_map.find(chan.ReplaceAll("SR","mergedMH"))==chan_map.end()))){
      return true;
    }
  }
  
  return false;
}
