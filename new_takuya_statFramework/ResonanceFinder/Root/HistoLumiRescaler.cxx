#include "ResonanceFinder/HistoLumiRescaler.h"
#include <TError.h>

RF::HistoLumiRescaler::HistoLumiRescaler() : m_originalLumi(1), m_targetLumi(1)
{
}

RF::HistoLumiRescaler::~HistoLumiRescaler()
{
}

void RF::HistoLumiRescaler::setLumi(Double_t original, Double_t target)
{
   m_originalLumi = original;
   m_targetLumi = target;
}

Double_t RF::HistoLumiRescaler::originalLumi() const
{
   return m_originalLumi;
}

void RF::HistoLumiRescaler::setOriginalLumi(Double_t lumi)
{
   m_originalLumi = lumi;
}

Double_t RF::HistoLumiRescaler::targetLumi() const
{
   return m_targetLumi;
}

void RF::HistoLumiRescaler::setTargetLumi(Double_t lumi)
{
   m_targetLumi = lumi;
}

void RF::HistoLumiRescaler::manipulate(HistoCollection &hc)
{
   for (auto kv : hc.histos()) {
     if (kv.first.Contains(hc.tagData()) == kFALSE) {
       rescale(kv.second);
       Info("manipulate", "Rescaled histogram %s", kv.first.Data());
     } else {
       Info("manipulate", "Not rescaling histogram %s (tagData=%s)", kv.first.Data(), hc.tagData().Data());
     }
   }
}

void RF::HistoLumiRescaler::rescale(RF::Histo_t *h)
{
   h->Scale(m_targetLumi / m_originalLumi);
}
