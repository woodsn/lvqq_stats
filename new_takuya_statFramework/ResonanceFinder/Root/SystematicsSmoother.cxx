#include "ResonanceFinder/SystematicsSmoother.h"
#include <algorithm>
#include <cassert>
#include <iostream>

RF::SystematicsSmoother::SystematicsSmoother():
  m_nole(1),
  m_doAvg(false)
{
}

RF::SystematicsSmoother::~SystematicsSmoother()
{
}

void RF::SystematicsSmoother::setSmoothFlags(std::map<TString, int> flags){
  m_smoothFlags = flags;
}

void RF::SystematicsSmoother::setNumberOfLocalExtrema(int nole){
  m_nole = nole;
}

void RF::SystematicsSmoother::doApplyAveraging(bool doAvg){
  m_doAvg = doAvg;
}

std::vector<int> RF::SystematicsSmoother::getLocalExtremaBinning(RF::Histo_t* hnom, RF::Histo_t* hsys, unsigned int nmax)
{
  std::vector<int> bins;
  double err = 0;
  float sum = hnom->IntegralAndError(0, hnom->GetNbinsX()+1, err);
  // too large stat unc: no shape
  if(err / sum > 0.05) {
    bins.push_back(1);
    bins.push_back(hnom->GetNbinsX()+1);
    return bins;
  }

  // normal case. Then, beginning with no rebinning
  for(int i = 1 ; i < hnom->GetNbinsX()+2 ; i++) {
    bins.push_back(i);
  }
  
  TH1* ratio = getRatioHist(hnom, hsys, bins);
  std::vector<int> extrema = findExtrema(ratio);

  while(extrema.size() > nmax+2) {
    int pos = findSmallerChi2(hnom, hsys, extrema);
    mergeBins(extrema[pos], extrema[pos+1], bins);
    getRatioHist(hnom, hsys, bins, ratio);
    extrema = findExtrema(ratio);
  }

  // second pass to avoid bins with too large stat uncertainty
  std::vector<int>::iterator fst = bins.end();
  std::vector<int>::iterator lst = bins.end();
  std::vector<int> to_remove;
  --lst;
  --fst;
  while(fst != bins.begin()) {
    if(fst == lst) {
      --fst;
    }
    else {
      float statE = statError(hnom, *fst, *lst);
      if(statE > 0.05) {
        to_remove.push_back(fst - bins.begin());
        --fst;
      }
      else {
        lst=fst;
      }
    }
  }
  for(unsigned int i = 0; i < to_remove.size(); i++) {
    bins.erase(bins.begin()+to_remove[i]);
  }

  delete ratio;

  return bins;
}

TH1* RF::SystematicsSmoother::getRatioHist(TH1* hnom, TH1* hsys, const std::vector<int>& bins) {
  TH1* res = (TH1*) hsys->Clone();
  getRatioHist(hnom, hsys, bins, res);
  return res;
}

void RF::SystematicsSmoother::getRatioHist(TH1* hnom, TH1* hsys, const std::vector<int>& bins, TH1* res) {
  for(unsigned int iRefBin = 0; iRefBin<bins.size()-1; iRefBin++) {
    float nomInt = hnom->Integral(bins.at(iRefBin), bins.at(iRefBin+1) - 1);
    float varInt = hsys->Integral(bins.at(iRefBin), bins.at(iRefBin+1) - 1);
    for(int b=bins.at(iRefBin); b<bins.at(iRefBin+1); b++) {
      if(nomInt != 0) {
        res->SetBinContent( b, varInt / nomInt );
      }
      else {
        res->SetBinContent( b, 1 );
      }
    }
  }
}

std::vector<int> RF::SystematicsSmoother::findExtrema(TH1* h) {
  std::vector<int> res;
  res.push_back(1);
  int status = 0; // 1: potential max, -1: potential min
  for(int i=2; i<h->GetNbinsX()+1; i++) {
    if(status == 1 && h->GetBinContent(i) < h->GetBinContent(i-1) - 1.e-6) {
      res.push_back(i-1);
      status = -1;
    }
    if(status == -1 && h->GetBinContent(i) > h->GetBinContent(i-1) + 1.e-6) {
      res.push_back(i-1);
      status = 1;
    }
    if(status == 0 && h->GetBinContent(i) < h->GetBinContent(i-1) - 1.e-6) {
      status = -1;
    }
    if(status == 0 && h->GetBinContent(i) > h->GetBinContent(i-1) + 1.e-6) {
      status = 1;
    }
  }
  res.push_back(h->GetNbinsX());

  return res;
}

int RF::SystematicsSmoother::findSmallerChi2(TH1* hnom, TH1* hsys, const std::vector<int>& extrema) {
  int pos = 0;
  float minval = 99999;
  for(unsigned int i=0; i<extrema.size()-2; i++) {
    float chi2 = computeChi2(hnom, hsys, extrema[i], extrema[i+1]);
    if(chi2 < minval) {
      pos = i;
    }
  }
  return pos;
}

float RF::SystematicsSmoother::computeChi2(TH1* hnom, TH1* hsys, int beg, int end) {
  float ratio = hsys->Integral(beg, end) / hnom->Integral(beg, end);
  float chi2 = 0;
  for(int i = beg; i < end+1; i++) {
    if(hnom->GetBinContent(i) != 0) {
    float iratio = hsys->GetBinContent(i) / hnom->GetBinContent(i);
    float err = hnom->GetBinError(i) / hnom->GetBinContent(i);
    chi2 += ((iratio-ratio)/err) * ((iratio-ratio)/err);
    }
  }
  return chi2;
}


// inclusive in lo and hi
void RF::SystematicsSmoother::mergeBins(int lo, int hi, std::vector<int>& bins) {
  std::vector<int>::iterator beg = std::upper_bound(bins.begin(), bins.end(), lo);
  // +1 because inclusive merge
  std::vector<int>::iterator last = std::lower_bound(bins.begin(), bins.end(), hi+1);
  bins.erase(beg, last);
}

float RF::SystematicsSmoother::statError(TH1* hnom, int beg, int end) { // end is excluded
  double err = 0;
  float nomInt = hnom->IntegralAndError(beg, end-1, err);
  return fabs(err / nomInt);
}

void RF::SystematicsSmoother::smoothHisto(RF::Histo_t* hnom, RF::Histo_t* hsys, const std::vector<int>& bins, bool smooth)
{
  //Check if we have non-zero contents.  What about negative contents??
  if((hsys->Integral()==0) || (hnom->Integral()==0)) return;

  float norm_init = hsys->Integral();
  TH1* ratio = getRatioHist(hnom, hsys, bins);
  
  if(smooth && ratio->GetNbinsX() > 2) {
    std::vector<float> vals(ratio->GetNbinsX() - 2);
    for(int i=2 ; i<ratio->GetNbinsX(); i++) {
      vals[i-2] = (2. * ratio->GetBinContent(i) + ratio->GetBinContent(i-1) + ratio->GetBinContent(i+1)) / 4.;
      if(isnan(vals[i-2]) || isinf(vals[i-2])){ fprintf(stderr,"RF::SystematicsSmoother::smoothHisto returned inf/nan"); vals[i-2] = 1;}
      if(vals[i-2]==0) vals[i-2]=1e-9;
    }
    ratio->SetBinContent(1, (ratio->GetBinContent(1)+ratio->GetBinContent(2))/2.); // check!!! TN
    for(int i=2 ; i<ratio->GetNbinsX(); i++) {
      ratio->SetBinContent(i, vals[i-2]);
    }
  }
  
  for(int i=1; i<hsys->GetNbinsX()+1; i++) {
    if(hnom->GetBinContent(i) != 0) {
      double inval = ratio->GetBinContent(i);
      if(isnan(inval) || isinf(inval)){ fprintf(stderr,"RF::SystematicsSmoother::smoothHisto ratio returned inf/nan"); inval=1;}
      hsys->SetBinContent(i, (inval+1e-9) * hnom->GetBinContent(i));
    }
  }

  //hopefully we didn't have -1e-9 as the integral!
  hsys->Scale(norm_init / (hsys->Integral()+1e-9));
  delete ratio;

  // set bin errors to 0 for systematics. Easier later when doing chi2 tests
  for(int i=0 ; i<hsys->GetNbinsX()+2; i++) {
    hsys->SetBinError(i, 0);
  }
}


void RF::SystematicsSmoother::manipulate(HistoCollection &hc)
{
  
  if(!m_smoothFlags.size()){
    std::cout << "RF::SystematicsSmoother::manipulate no list of variations to smooth provided! Variations are not smoothed!!" << std::endl;      
    return;
  }

  for (auto flag : m_smoothFlags) {
    if(flag.second == 0) continue;  //these didn't get flagged for smoothing
    std::cout << "Performing systematics smoothing for " << flag.first << std::endl;	  
  }

  for (auto chan : hc.channels()) {
    for (auto sample : hc.samples(chan)) {
      if(sample.Contains(hc.tagData())) continue;
      
      RF::Histo_t *h_nom = hc.getHistogram(chan, sample, hc.tagNominal());
      
      for (auto variation : hc.variations(chan, sample)){
	if(variation.Contains(hc.tagNominal())) continue;
	if(variation.Contains(hc.tagData())) continue;

	bool smoothIt = false;
	bool smoothFlag = 0;
	for (auto flag : m_smoothFlags) {
	  if(flag.second == 0) continue;  //these didn't get flagged for smoothing
	  if(variation.Contains(flag.first)){
	    smoothIt = true;
	    smoothFlag = flag.second;
	  }
	  if(smoothIt) break;
	}
	if(!smoothIt) continue;
	//for now we're ignoreing the smoothing flag, but someday it will have meaning!

	RF::Histo_t *h_sys = hc.getHistogram(chan, sample, variation);
	if(!h_sys){
	  std::cout << "RF::SystematicsSmoother::manipulate did not find hist for "<< variation << " Skip!"<<std::endl;
	  continue;
	}
	
	std::vector<int> bins;
	// allow parabolic shape for mVH -> m_nole = 1
	// only monotonic cases for the other variables -> m_nole = 0
	bins = getLocalExtremaBinning(h_nom, h_sys, m_nole);
	
	if(h_sys && h_nom){
	  //Only smooth for non-negative bin contents
	  //std::cout << variation << " is smoothed" << std::endl;
	  if(h_sys->Integral()!=0 && h_nom->Integral()!=0){
	    smoothHisto(h_nom, h_sys, bins, m_doAvg);
	  }
	}
      }
    }
  }
}

