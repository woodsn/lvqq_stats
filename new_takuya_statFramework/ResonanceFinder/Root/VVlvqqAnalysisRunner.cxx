#include "ResonanceFinder/VVlvqqAnalysisRunner.h"
#include "ResonanceFinder/DownVariationAdder.h"
#include "ResonanceFinder/SystematicsSmoother.h"
#include "ResonanceFinder/HistoLumiRescaler.h"
#include "ResonanceFinder/BackgroundTemplateAdder.h"


RF::VVlvqqAnalysisRunner::VVlvqqAnalysisRunner(TString analysis) : IBinnedTreeAnalysisRunner(analysis)
{
   // note: we create the RF::BackgroundTemplateAdder although it's not really needed
   // destruction is, in any case, not our task by IBinnedAnalysisRunner's task
  m_oldLumi = 1;
  m_newLumi = 1;
  m_useStrings = kTRUE;

}

RF::VVlvqqAnalysisRunner::~VVlvqqAnalysisRunner()
{
}

void RF::VVlvqqAnalysisRunner::doApplySmoothing(bool doSmooth) { m_doSmooth = doSmooth; }

void RF::VVlvqqAnalysisRunner::setSystSmoothFlag(TString syst, int flag){
  m_systSmoothFlags[syst] =  flag;
}

void RF::VVlvqqAnalysisRunner::setLumiRescale(Double_t oldLumi, Double_t newLumi) {
  m_oldLumi = oldLumi;
  m_newLumi = newLumi;
}

void RF::VVlvqqAnalysisRunner::setChannelFitFunction( TString channel, TString name, TString obs){

m_ChFunctionMap[channel]=name;
m_ChObsMap[channel]=obs;
//m_ChObsMap
}

void RF::VVlvqqAnalysisRunner::setChannelFitFunction(TString channel, RooAbsPdf *pdf, RooRealVar *obs)
{
   m_pdf[channel] = pdf;
   m_obs[channel] = obs;
   m_useStrings = kFALSE;
}




void RF::VVlvqqAnalysisRunner::addOneSideVariation(TString variation)
{

   if (variation != "") {
      bool alreadyAdded =  std::find(m_onesidevar.begin(), m_onesidevar.end(), variation) != m_onesidevar.end();
      if (!alreadyAdded) {
         m_onesidevar.push_back(variation);
      }

   }
   return ;
}

void RF::VVlvqqAnalysisRunner::manipulate()
{


   // do nothing (TODO: possibly implement something a la VVJJ?)
   RF::HistoCollection &hc = histoCollection();
   cout <<   "------" << endl;
   //cout <<   hc.getHistogram("SRWW", "background", hc.tagNominal())->Integral() << endl;



   for (auto kv : m_onesidevar) {

      // add down variations
      if (kv != "") {
         RF::DownVariationAdder dva;
         dva.setVariation(kv + "_up", kv + "_down");
         dva.manipulate(hc);
      }

   }

  if (m_oldLumi != m_newLumi) {
    RF::HistoLumiRescaler hlr;
    hlr.setLumi(m_oldLumi, m_newLumi);
    hlr.manipulate(hc);
  }



  if(m_doSmooth){
   RF::SystematicsSmoother SysS;
   SysS.setNumberOfLocalExtrema(4); //1: allows parabolic shape for mVH 
   SysS.doApplyAveraging(true); //false: no averaging of adjacent bins 
   SysS.setSmoothFlags(m_systSmoothFlags); //provide list of smoothing flags
   SysS.manipulate(hc);
  }


   RF::BackgroundTemplateAdder bta;
   if(m_useStrings){
     
     for( auto kv : m_ChFunctionMap ){
       
       bta.setChannel( kv.first  );
       bta.setBkgName( kv.second );
       bta.defineFitFunction(kv.second, m_ChObsMap[kv.first]);      
       bta.manipulate(hc);
     }

   }else{
       
     for( auto kv : m_pdf ){
 
	 bta.setChannel( kv.first  );
	 bta.setBkgName( kv.second->GetName() );
	 bta.defineFitFunction(kv.second, m_obs[kv.first]);
	 bta.manipulate(hc);
     }
   }
}
