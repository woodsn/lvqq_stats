#include <ResonanceFinder/FitTool.h>

#include "TFile.h"
#include "RooWorkspace.h"
#include "RooAbsPdf.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooStats/ModelConfig.h"
#include "RooRandom.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TEnv.h"

#include "RooStats/AsymptoticCalculator.h"
#include "RooStats/HybridCalculator.h"
#include "RooStats/FrequentistCalculator.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/HypoTestPlot.h"

#include "RooStats/NumEventsTestStat.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/SimpleLikelihoodRatioTestStat.h"
#include "RooStats/RatioOfProfiledLikelihoodsTestStat.h"
#include "RooStats/MaxLikelihoodEstimateTestStat.h"
#include "RooStats/NumEventsTestStat.h"

#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/HypoTestInverterPlot.h"


using namespace RooFit;
using namespace RooStats;
using namespace std;

RooStats::FitTool::FitTool(): mMinimizerType(""), mSBFitFileName(""), mBFitFileName("")
{
}



void
RooStats::FitTool::SetParameter(const char * name, bool value)
{
   //
   // set boolean parameters
   //

   std::string s_name(name);

   //   if (s_name.find("PlotHypoTestResult") != std::string::npos) mPlotHypoTestResult = value;

   return;
}



void
RooStats::FitTool::SetParameter(const char * name, int value)
{
   //
   // set integer parameters
   //

   std::string s_name(name);

   //   if (s_name.find("NWorkers") != std::string::npos) mNWorkers = value;

   return;
}



void
RooStats::FitTool::SetParameter(const char * name, double value)
{
   //
   // set double precision parameters
   //

   std::string s_name(name);
   
   //   if (s_name.find("Dummy") != std::string::npos) value;

   return;
}



void
RooStats::FitTool::SetParameter(const char * name, const char * value)
{
   //
   // set string parameters
   //

   std::string s_name(name);

   if (s_name.find("MinimizerType") != std::string::npos) mMinimizerType.assign(value);
   if (s_name.find("SBFitFileName") != std::string::npos) mSBFitFileName.assign(value);
   if (s_name.find("BFitFileName") != std::string::npos) mBFitFileName.assign(value);

   return;
}

// internal routine to run the fits
void 
RooStats::FitTool::generateSBFit(RooWorkspace * w,
			       const char * modelSBName, const char * modelBName,
			       const char * dataName)
				      
{

   std::cout << "Running FitTool on the workspace " << w->GetName() << " pointer " << w << std::endl;

   RooAbsData * data = w->data(dataName);
   if (!data) {
      Error("FitTool", "Not existing data %s", dataName);
      return;
   } else
      std::cout << "Using data set " << dataName << std::endl;

   // get models from WS
   // get the modelConfig out of the file
   ModelConfig* bModel = (ModelConfig*) w->obj(modelBName);
   ModelConfig* sbModel = (ModelConfig*) w->obj(modelSBName);

   if (!sbModel) {
      Error("StandardHypoTestDemo", "Not existing ModelConfig %s", modelSBName);
      return;
   }
   // check the model
   if (!sbModel->GetWS()) {
      Error("StandardHypoTestDemo", "Model %s has no associated workspace ", modelSBName);
      return;
   }
   if (!sbModel->GetPdf()) {
      Error("StandardHypoTestDemo", "Model %s has no pdf ", modelSBName);
      return;
   }
   if (!sbModel->GetParametersOfInterest()) {
      Error("StandardHypoTestDemo", "Model %s has no poi ", modelSBName);
      return;
   }
   if (!sbModel->GetObservables()) {
      Error("StandardFitDemo", "Model %s has no observables ", modelSBName);
      return;
   }
   if (!sbModel->GetSnapshot()) {
      Info("StandardFitDemo", "Model %s has no snapshot  - make one using model poi", modelSBName);
      sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
   }

   if (!bModel || bModel == sbModel) {
      Info("StandardFitDemo", "The background model %s does not exist", modelBName);
      Info("StandardFitDemo", "Copy it from ModelConfig %s and set POI to zero", modelSBName);
      bModel = (ModelConfig*) sbModel->Clone();
      bModel->SetName(TString(modelSBName) + TString("_with_poi_0"));
      RooRealVar * var = dynamic_cast<RooRealVar*>(bModel->GetParametersOfInterest()->first());
      if (!var) return;
      double oldval = var->getVal();
      var->setVal(0);
      bModel->SetSnapshot(RooArgSet(*var));
      var->setVal(oldval);
   } else {
      if (!bModel->GetSnapshot()) {
         Info("StandardFitDemo", "Model %s has no snapshot  - make one using model poi and 0 values ", modelBName);
         RooRealVar * var = dynamic_cast<RooRealVar*>(bModel->GetParametersOfInterest()->first());
         if (var) {
            double oldval = var->getVal();
            var->setVal(0);
            bModel->SetSnapshot(RooArgSet(*var));
            var->setVal(oldval);
         } else {
            Error("StandardFitDemo", "Model %s has no valid poi", modelBName);
            return;
         }
      }
   }

   // check model  has global observables when there are nuisance pdf
   // for the hybrid case the globobs are not needed
   bool hasNuisParam = (sbModel->GetNuisanceParameters() && sbModel->GetNuisanceParameters()->getSize() > 0);
   bool hasGlobalObs = (sbModel->GetGlobalObservables() && sbModel->GetGlobalObservables()->getSize() > 0);
   if (hasNuisParam && !hasGlobalObs) {
     // try to see if model has nuisance parameters first
     RooAbsPdf * constrPdf = RooStats::MakeNuisancePdf(*sbModel, "nuisanceConstraintPdf_sbmodel");
     if (constrPdf) {
       Warning("FitTool", "Model %s has nuisance parameters but no global observables associated", sbModel->GetName());
       Warning("FitTool", "\tThe effect of the nuisance parameters will not be treated correctly ");
     }
   }


   // save all initial parameters of the model including the global observables
   RooArgSet initialParameters;
   RooArgSet * allParams = sbModel->GetPdf()->getParameters(*data);
   allParams->snapshot(initialParameters);
   delete allParams;

   // run first a data fit
   const RooArgSet * poiSet = sbModel->GetParametersOfInterest();
   RooRealVar *poi = (RooRealVar*)poiSet->first();

   std::cout << "StandardFitDemo : POI initial value:   " << poi->GetName() << " = " << poi->getVal()   << std::endl;

   // fit the data first (need to use constraint )
   double poihat = 0;

   if (mMinimizerType.size() == 0) mMinimizerType = ROOT::Math::MinimizerOptions::DefaultMinimizerType();
   else  ROOT::Math::MinimizerOptions::SetDefaultMinimizer(mMinimizerType.c_str());

   Info("FitTool", "Using %s as minimizer for computing the test statistic",
        ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str());

   // do the fit : By doing a fit the POI snapshot (for S+B)  is set to the fit value
   // and the nuisance parameters nominal values will be set to the fit value.
   
   Info("FitTool", " Doing a first fit to the observed data ");
   RooArgSet constrainParams;
   if (sbModel->GetNuisanceParameters()) constrainParams.add(*sbModel->GetNuisanceParameters());
   RooStats::RemoveConstantParameters(&constrainParams);

   RooFitResult * fitres = sbModel->GetPdf()->fitTo(*data, InitialHesse(false), Hesse(false),
						    Minimizer(mMinimizerType.c_str(), "Migrad"), Strategy(0), PrintLevel(0), Constrain(constrainParams), Save(true), Offset(RooStats::IsNLLOffset()));
   if (fitres->status() != 0) {
     Warning("FitTool", "Fit to the model failed - try with strategy 1 and perform first an Hesse computation");
     fitres = sbModel->GetPdf()->fitTo(*data, InitialHesse(true), Hesse(false), Minimizer(mMinimizerType.c_str(), "Migrad"), Strategy(1), PrintLevel(1), Constrain(constrainParams),
				       Save(true), Offset(RooStats::IsNLLOffset()));
      }
   if (fitres->status() != 0)
     Warning("StandardFitDemo", " Fit still failed - continue anyway.....");


   poihat  = poi->getVal();
   std::cout << "StandardFitDemo - Best Fit value : " << poi->GetName() << " = "
	     << poihat << " +/- " << poi->getError() << std::endl;
   std::cout << "Time for fitting : ";
   
   //save best fit value in the poi snapshot
   sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
   std::cout << "StandardFito: snapshot of S+B Model " << sbModel->GetName()
	     << " is set to the best fit value" << std::endl;

   //also save S+B fit to file
   if(mSBFitFileName.size()==0){
     mSBFitFileName = w->GetName()+string("_SB_FitResult.root");
   }
   TFile * sbFitFile = new TFile(mSBFitFileName.c_str(), "RECREATE");
   fitres->Write();
   sbFitFile->Close();
}
