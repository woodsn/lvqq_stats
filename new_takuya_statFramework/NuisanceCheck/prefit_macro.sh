#!/bin/bash
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheckCombination/NuisanceCheck04302017_1708/job_011/results_f000_VBFWWNWA_ws_HPLP_700.root/0/FitCrossChecks.root\""
#signal="\"VBFWWNWA 700GeV\""

#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheckCombination/NuisanceCheck04302017_1708/job_000/results_f000_HVTWW_ws_HPLP_1000.root/0/FitCrossChecks.root\""
#signal="\"HVTWW 700GeV\""

#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck04302017_1708/job_005/results_f000_RSGWW_ws_HPLP_700.root/0/FitCrossChecks.root\""
#signal="\"RSGWW 700GeV\""

#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheckCombination/NuisanceCheck04302017_1708/job_011/results_f000_VBFWWNWA_ws_HPLP_700.root/0/FitCrossChecks.root\""
#signal="\"VBFWWNWA 700GeV\""

#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheckCombination/NuisanceCheck04302017_1708/job_007/results_f000_VBF-HVTWW_ws_HPLP_700.root/0/FitCrossChecks.root\""
#signal="\"VBF-HVTWW 700GeV\""


#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/default0/JEScorr/job_000/results_f000_HVTWW_ws_HPLP_1000.root/0/FitCrossChecks.root\""
#signal="\"HVTWW 1000GeV\""

#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/pulltest/default_strat_2/jes_corr/job_003/results_f000_VBF-HVTWW_ws_HPLP_1000.root/0/FitCrossChecks.root\""
#signal="\"VBF HVTWW 1000GeV\""


#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheckCombination/resultsVBFWW/FitCrossChecks.root\""
#signal="\"VBF HVTWW 1000GeV\""
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_May19/HVTWW/results_f000_HVTWW_ws_HPLP_500.root/0/FitCrossChecks.root\""
#signal="\"HVT WW 500 GeV\""
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_May19/HVTWZ/results_f000_HVTWZ_ws_HPLP_500.root/0/FitCrossChecks.root\""
#signal="\"HVT WZ 500 GeV\""
rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_d05262017_1414/job_004/results_f000_VBF-HVTWW_ws_HPLP_1000.root/0/FitCrossChecks.root\""
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_May19/VBF-HVTWZ/results_f000_VBF-HVTWZ_ws_HPLP_500.root/0/FitCrossChecks.root\""
#signal="\"VBF HVT WZ 500 GeV\""
#rootfile="\"/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_May19/VBF-HVTWW/results_f000_VBF-HVTWW_ws_HPLP_500.root/0/FitCrossChecks.root\""
#signal="\"VBF HVT WW 500 GeV\""


#xsec="0.00192" #HVTWW 500 GeV
#xsec="0.00207" #HVTWZ 500 GeV
#xsec="0.0041372" #VBF HVTWZ 500 GeV
#xsec="0.005033" #VBF HVTWW 500 GeV
root -l -b /export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck_d05262017_1414/job_004/results_f000_VBF-HVTWW_ws_HPLP_1000.root/0/FitCrossChecks.root <<EOF



.q

EOF



#LimitCrossCheck::PlotFitCrossChecks("/afs/cern.ch/work/k/kalliopi/private/lvqq_Resolved_WS/v17_Aug_2016/Combined/combined_t_VBFWWNWA700.root", "results/", "combined", "ModelConfig", "obsData" )
#LimitCrossCheck::PlotFitCrossChecks("../CombinationTool/ws_combined_stats/RSGWW/ws_HPLP_4000.root", "results/", "combined", "ModelConfig", "obsData" )  



#drawPostFit("SRWW_ResolvedVBF_fileThree","/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck04282017_1051/job_000/results_f000_VBFWWNWA_ws_HPLP_1000.root/FitCrossChecks.root", "ggHWWNWA 700 GeV", 1.0)

#drawPostFit("Sideband_ResolvedVBF_fileThree","/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck04282017_1051/job_000/results_f000_VBFWWNWA_ws_HPLP_1000.root/FitCrossChecks.root", "ggHWWNWA 700 GeV", 1.0)

#drawPostFit("Btag_ResolvedVBF_fileThree","/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/NuisanceCheck/NuisanceCheck04282017_1051/job_000/results_f000_VBFWWNWA_ws_HPLP_1000.root/FitCrossChecks.root", "ggHWWNWA 700 GeV", 1.0)
