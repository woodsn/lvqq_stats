#./reco_combinednickSCT81116.sh RDO.09094178._000773.pool.root.1
import condor, time, os, commands, sys
timestr = time.strftime("%m%d%Y_%H%M")
label = sys.argv[1]
regions = "test"
#signals = ['ggHWWNWA', 'VBFWWNWA', 'HVTWW', 'HVTWZ', 'RSGWW', 'VBF-HVTWW', 'VBF-HVTWZ']
#signals = ['HVTWW', 'HVTWZ', 'VBF-HVTWW', 'VBF-HVTWZ']
#signals=['VBF-HVTWW','VBF-HVTWZ','HVTWW','HVTWZ']
#signals=['VBF-HVTWW','VBF-HVTWZ']
signals=['HVTWW','HVTWZ', 'VBF-HVTWW', 'VBF-HVTWZ']
#mass = ['500','1000']
mass = '1000'
files=[]


if regions == "test":
	selection = 'test'
	directory = '/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/NuisanceCheck/ws_june25/'
	for s in signals:
		this_file=directory+s+'/ws_HPLP_'+mass+'.root'
		files.append(this_file)



if regions == "combined":
	selection = 'combined'
#	directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_combined/june14/'

	for s in signals:
		for m in mass:
			this_file=directory+s+'/ws_HPLP_'+m+'.root'
			files.append(this_file)

if regions == "resolved":
	directory = '/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/ws/'
	for s in signals:
		selection = "RESVBF"
		if "VBF" not in s:
			selection = "RES" 
		for m in mass:
			this_file=directory+'VVlvqq_mc15_v2016_'+s+m+'_ws_'+s+'_'+selection+'_'+m+'.root'
			files.append(this_file)




for i, iFile in enumerate(files):
    files[i] = iFile
print files
exe = 'condor_combined_runFitCrossCheck.py'
argtemplate = ' %s'
dirname = 'NuisanceCheck_' + label+timestr

condor.run(exe,argtemplate,files, dirname, 1)







