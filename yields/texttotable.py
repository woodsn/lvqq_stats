import sys
import argparse
from collections import OrderedDict

yld={}
unc={}

region=""

import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument("--WW"  , action='store_true')
parser.add_argument("--WZ"  , action='store_true')
parser.add_argument("--VBFWW"  , action='store_true')
parser.add_argument("--VBFWZ", action='store_true')
parser.add_argument("--debug", action='store_true')
args=parser.parse_args()

if args.WW:
    bosons="WW"
    region="WW"
    regionlabel="$WW$"
    infile="WW.out"
elif args.WZ:
    bosons="WZ"
    region="WZ"
    regionlabel="$WZ$"
    infile="WZ.out"
elif args.VBFWW:
    bosons="WW"
    region="VBFWW"
    regionlabel="VBF $WW$"
    infile="VBFWW.out"
elif args.VBFWZ:
    bosons="WZ"
    region="VBFWZ"
    regionlabel="VBF $WZ$"
    infile="VBFWZ.out"

colnamemap=OrderedDict()
nCRcol=6
nSRcol=8
#colnamemap[]=""
tabledef="| c | c@{\ $\pm$\ }c c@{\ $\pm$\ }c c@{\ $\pm$\ }c | c@{\ $\pm$\ }c  c@{\ $\pm$\ }c c@{\ $\pm$\ }c | c@{\ $\pm$\ }c  c@{\ $\pm$\ }c c@{\ $\pm$\ }c |"

colnamemap["SR"+bosons+"_fileOne"]="SR"
colnamemap["Sideband_fileOne"]="$W$+jets CR"
colnamemap["Btag_fileOne"]="Top CR"
colnamemap["SR"+bosons+"_LP_fileTwo"]="Signal Region"
colnamemap["Sideband_LP_fileTwo"]="$W$+jets CR"
colnamemap["Btag_LP_fileTwo"]="Top CR"
if args.VBFWW or args.VBFWZ:
    colnamemap["SR"+bosons+"_ResolvedVBF_fileThree"]="SR"
    colnamemap["Sideband_ResolvedVBF_fileThree"]="$W$+jets CR"
    colnamemap["Btag_ResolvedVBF_fileThree"]="Top CR"
else:
    colnamemap["SR"+bosons+"_Resolved_fileThree"]="SR"
    colnamemap["Sideband_Resolved_fileThree"]="$W$+jets CR"
    colnamemap["Btag_Resolved_fileThree"]="Top CR"

smpl=""
for line in open("%s" % infile):
    line=line.replace("My_","")
    line=line.replace("jets","")
    line=line.replace("ttbar","Top")
    line=line.replace("BtagBtag","")
    line=line.replace("BtagLP","")
    line=line.replace("BtagResolved","")
    line=line.replace("SidebandSideband","")
    line=line.replace("SidebandResolved","")
    line=line.replace("SidebandLP","")
    line=line.replace("SRWZSRWZ","")
    line=line.replace("SRWWSRWW","")
    line=line.replace("SRWZResolvedSRWZResolved","")
    line=line.replace("SRWWResolvedSRWWResolved","")
    line=line.replace("SRWWResolvedVBFSRWWResolvedVBF","")
    line=line.replace("SRWZResolvedVBFSRWZResolvedVBF","")
    line=line.replace("SRWZLPSRWZLP","")
    line=line.replace("SRWWLPSRWWLP","")
    line=line.replace("VBFVBF","")
    line=line.replace("singletop","SingleTop")
    line=line.replace("_file","_File")
    line=line.replace("fileOne","")
    line=line.replace("fileTwo","")
    line=line.replace("fileThree","")
    line=line.replace("_File","_file")
    line=line.replace("FitError_AfterFit","Total")
    line=line.replace("$\pm$","")
    line=line.replace("\\","")
    line=line.replace("&","")
    line=line.strip()

    spline=line.split()
    
    if args.debug: print line

    if len(spline)==0:
        continue

    if args.debug: print len(spline)
    if len(spline)==1:
        if args.debug: print "colname %s" % spline[0]
        smpl=spline[0]
        yld[smpl]={}
        unc[smpl]={}
    else:
        if spline[0]=="background":
            continue
        else:
            bg=spline[0]
            yld[smpl][bg]=float(spline[1])
            if bg != "Data":
                unc[smpl][bg]=float(spline[2])

rownamemap=OrderedDict()
rownamemap["W"]="$W$+jets"
rownamemap["Top"]="$\\ttbar$"
rownamemap["SingleTop"]="Single-$t$"
rownamemap["Diboson"]="SM Diboson"
rownamemap["Z"]="$Z$+jets"
rownamemap["multijet"]="Multijet"
rownamemap["Total"]="Total Background"
rownamemap["Data"]="Observed"

print "\\begin{table}[tbp]"
print "\\renewcommand{\\arraystretch}{1.25}"
print "\\caption{Expected and observed yields in signal and control regions for the %s signal hypothesis.  Yields and uncertainties are evaluated after a background-only fit to the data in all regions indicated above.  The uncertainty on the total background estimate can be smaller than the quadratic sum of the individual background contributions due to anti-correlations between the estimates of different background sources.}" % regionlabel
print "\\begin{center}"
print "\\resizebox{\\textwidth}{!}{%"
print "\\begin{tabular}{%s}" % tabledef

print "\\hline"
print "\\hline"
print "                     &\multicolumn{6}{c|}{Boosted, High Purity}  &\multicolumn{6}{c|}{Boosted, Low Purity}  &\multicolumn{6}{c|}{Resolved}   \\\\"
print "\cline{1-%d}" % (1+9*2)
ln="%22s" % ""
for colname,colfancy in colnamemap.iteritems():
    rightbar=""
    if "Btag" in colname:
        rightbar="|"
    ln+="&\multicolumn{2}{c%s}{%17s}" % (rightbar,colfancy)
ln+="\\\\"
print ln
print "\\hline"
for rowname,rowfancy in rownamemap.iteritems():
    ln="%22s" % rowfancy
    for colname,colfancy in colnamemap.iteritems():
        rightbar=""
        if "Btag" in colname:
            rightbar="|"
        if rowname=="multijet" and rowname not in yld[colname]:
            ln += "&\multicolumn{2}{c%s}{%5s}%13s" % (rightbar,"--","")
        elif rowname not in yld[colname]:
            ln += "&\multicolumn{2}{c%s}{%5s}%13s" % (rightbar,"","")
        elif rowname=="Data":
            ln += "&\multicolumn{2}{c%s}{%4d}%13s" % (rightbar,yld[colname][rowname],"")
        else:
            y=yld[colname][rowname]
            u=unc[colname][rowname]
            if y<10:
                if (y%0.1)>0.05:
                    y+=0.1
                if (u%0.1)>0.05:
                    u+=0.1
                ln+="&%23.1f &%12.1f" % (y,u)
            else:
                if (y%1)>0.5:
                    y+=1
                if (u%1)>0.5:
                    u+=1
                ln+="&%23d &%12d" % (y,u)
    ln+="\\\\"
    print ln
    if rowname=="multijet" or rowname=="Total" or rowname=="Data":
        print "\hline"

print "\\hline"
print "\\end{tabular}"
print "}"
print "\\label{tab:yields_%s}" % region
print "\end{center}"
print "\\end{table}"
